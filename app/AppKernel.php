<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new IMAG\LdapBundle\IMAGLdapBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new Ob\HighchartsBundle\ObHighchartsBundle(),
            new Ps\PdfBundle\PsPdfBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new Liuggio\ExcelBundle\LiuggioExcelBundle(),

            new Administration\SocieteBundle\AdministrationSocieteBundle(),
            new Administration\HierarchieBundle\HierarchieBundle(),
            new Administration\DirectionBundle\AdministrationDirectionBundle(),
            new ChecklistBundle\ChecklistBundle(),
            new Direction\DirectionBundle\DirectionDirectionBundle(),
            new EFN\DemandeFormationBundle\EFNDemandeFormationBundle(),
            new User\UserBundle\UserUserBundle(),
            new Suivi\EtudesBundle\SuiviEtudesBundle(),
            new Suivi\ContactBundle\SuiviContactBundle(),
            new Indicateurs\DAMBundle\IndicateursDAMBundle(),
            new Suivi\ReportingBundle\SuiviReportingBundle(),
            new Administration\AclBundle\AclBundle(),
            new HotelBookingBundle\HotelBookingBundle(),
            new Formulaire\DDRCBundle\DDRCBundle(),
            new Paie\AdministrationBundle\PaieAdministrationBundle(),
            new Formulaire\dmliBundle\dmliBundle(),
            new Formulaire\AdminBundle\AdminBundle(),
            new Paie\ArretTravailBundle\PaieArretTravailBundle(),
            new DPDCBundle\DPDCBundle(),
            new DRIBundle\DRIBundle(),
            new WhiteOctober\TCPDFBundle\WhiteOctoberTCPDFBundle(),
            new SuiviHierarchieBundle\SuiviHierarchieBundle(),
            new CovoiturageBundle\CovoiturageBundle(),

        );

        if (in_array($this->getEnvironment(), array('dev', 'test', 'recette'))) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            $bundles[] = new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
		
        $loader = new Twig_Loader_Filesystem(array());
        $twig = new Twig_Environment($loader);
		$twig->addExtension(new Twig_Extensions_Extension_Intl());
    }
}
