<?php

namespace Formulaire\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AdminType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('direction', 'choice', array('label'=>'Direction',  'mapped'=>false, 'attr'=>array('class'=>'form-control input-sm')))
            ->add('service','choice', array('label'=>'Service',  'mapped'=>false, 'attr'=>array('class'=>'form-control input-sm') ))
            ->add('pole','choice', array('label'=>'pole',  'mapped'=>false, 'attr'=>array('class'=>'form-control input-sm')))
            ->add('poste','choice', array('label'=>'Poste', 'mapped'=>false, 'attr'=>array('class'=>'form-control input-sm') ))
            ->add('posteHolder','text', array('label'=>'Poste', 'mapped'=>false, 'attr'=>array('class'=>'form-control input-sm') ))
            ->add('mailHolder','text', array('label'=>'Groupe Outlook', 'mapped'=>false, 'attr'=>array('class'=>'form-control input-sm') ))
            ->add('crmHolder','text', array('label'=>'Nom de l\'equipe', 'mapped'=>false, 'attr'=>array('class'=>'form-control input-sm') ))
            ->add('crm','choice', array('label'=>'Nom de l\'equipe', 'mapped'=>false, 'attr'=>array('class'=>'form-control input-sm') ))

            ->add('selectListMail','choice', array('label'=>'','multiple'=>true, 'mapped'=>false, 'attr'=>array('class'=>'form-control input-sm') ))
            #validation de formulaire
            ->add('Valider', 'submit', array('attr'=>array('class'=>'btn btn-info pull-right')))
            ->add('Annuler', 'button', array('attr'=>array('class'=>'btn btn-default')))

            #liste logiciel
        ;
    }
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'allow_extra_fields' => true
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gestion_adminbundle_profil';
    }
}
