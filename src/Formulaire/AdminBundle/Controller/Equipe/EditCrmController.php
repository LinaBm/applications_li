<?php

namespace Formulaire\AdminBundle\Controller\Equipe;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Formulaire\AdminBundle\Form\AdminType;
use Symfony\Component\HttpFoundation\Response;

class EditCrmController extends Controller
{
    /**
     * @Route("/crm/edit/{id}", name="edit_crm")
     * @Template()
     */
    public function editAction($id)
    {$em = $this->getDoctrine()->getManager();
        $services = $em->getRepository('dmliBundle:DmliCrmequipeCrmEqu')->findBy(
            array('crmEquId'=>$id));
//var_dump($services);die;

        $result['id'] = $services[0]->getCrmEquId();
        $result['libelle'] = $services[0]->getCrmEquLibelle();
        if($services[0]->getSrvId() != ''){
            $direction=$em->getRepository('AdministrationDirectionBundle:Service')->findBy(
                array('id'=> $services[0]->getSrvId()));
            //var_dump($direction[0]->getLibelle());die;
            $result['service']= $direction[0]->getLibelle();
        }else{
            $result['service']= 'Autres';
        }

        $form = $this->get('form.factory')->create(new AdminType());

        return $this->render('AdminBundle:Equipe:ModifierCrm.html.twig', array('result'=>$result,
            'form' => $form->createView()
        ));

    }

}