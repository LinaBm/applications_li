<?php

namespace Formulaire\AdminBundle\Controller\Equipe;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Formulaire\AdminBundle\Form\AdminType;
use Symfony\Component\HttpFoundation\Response;

class CrmController extends Controller
{
    /**
     * @Route("/crm", name="index_crm")
     * @Template()
     */
    public function indexAction()
    {

        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository('dmliBundle:DmliCrmequipeCrmEqu')->findAll();
//var_dump($services);die;
        $i = 0;
        foreach ($services as $service) {
            $result[$i]['id']=$service->getCrmEquId();
            $result[$i]['libelle'] = $service->getCrmEquLibelle();
            if($service->getSrvId() != ''){
                $direction=$em->getRepository('AdministrationDirectionBundle:Service')->findBy(
                    array('id'=> $service->getSrvId()));
                //var_dump($direction[0]->getLibelle());die;
                $result[$i]['service']= $direction[0]->getLibelle();
            }else{
                $result[$i]['service']= 'Autres';
            }

            $i++;
        }
        return $this->render('AdminBundle:Equipe:ListeCrm.html.twig', array('result'=> $result));

    }
    /**
     * @Route("/crm/remove/{id}", name="remove_crm")
     * @Template()
     */
    public function removeAction($id)
    {  $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('dmliBundle:DmliCrmequipeCrmEqu')->findBy(array('crmEquId'=>$id));

        $em->remove($product[0]);
        $em->flush();
        $request->getSession()->getFlashBag()->add(
            'success',
            'Equipe CRM supprimée!'
        );
        if($this->get('security.context')->getToken()->getUser()->isGranted('ROLE_DMLI_DAM')){
        $this->sendMail('vient de supprimer une equipe CRM'.$product[0]->getCrmEquLibelle());}
        return $this->redirect($this->generateUrl('index_crm'));
    }


    public function sendMail($message){

        $this->data['senderName'] = strtoupper($this->get('security.context')->getToken()->getUser()->getSurname());
        $this->data['senderFirstname'] = strtolower($this->get('security.context')->getToken()->getUser()->getGivenname());
        $this->data['senderFirstname'] = ucfirst($this->get('security.context')->getToken()->getUser()->getGivenname());
        $this->data['senderMail'] = ucfirst($this->get('security.context')->getToken()->getUser()->getEmail());

        $message = \Swift_Message::newInstance()
            ->setSubject("[ADMINISTRAION - DMLI]: Remove Equipe CRM")
            ->setFrom($this->data['senderMail'])
            ->setTo('materiel@ra-expansion.fr') // hotel and Valerie
            ->setCC($this->data['senderMail'])
            ->setBCC('poleweb@noz.fr')
            ->setBody(
                $this->renderView(
                // app/Resources/views/Emails/registration.html.twig
                    'AdminBundle:email:MailAdd.html.twig', array('nameuser' => $this->data['senderName'], 'firstnameuser' => $this->data['senderFirstname'], 'message'=>$message, 'add'=>"")
                ),
                'text/html'
            );

        $this->get('mailer')->send($message);
    }
}
