<?php

namespace Formulaire\AdminBundle\Controller\Mail;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Formulaire\AdminBundle\Form\AdminType;
use Symfony\Component\HttpFoundation\Response;

class EditMailController extends Controller
{
    /**
     * @Route("/mail/edit/{id}", name="edit_mail")
     * @Template()
     */
    public function editAction($id)
    {$em = $this->getDoctrine()->getManager();
        $services = $em->getRepository('dmliBundle:DmliMailMel')->findBy(
            array('melId'=>$id));
//var_dump($services);die;

            $result['id']=$services[0]->getMelId();
            $result['libelle'] = $services[0]->getMelLibelle();
            if($services[0]->getDirId() != '0'){
                $direction=$em->getRepository('AdministrationDirectionBundle:Direction')->findBy(
                    array('id'=> $services[0]->getDirId()));
                //var_dump($direction[0]->getLibelle());die;
                $result['direction']= $direction[0]->getLibelle();
            }else{
                $result['direction']= 'Autres';

        }

        $form = $this->get('form.factory')->create(new AdminType());

        return $this->render('AdminBundle:Mail:ModifierMail.html.twig', array('result'=>$result,
            'form' => $form->createView()
        ));

    }
}