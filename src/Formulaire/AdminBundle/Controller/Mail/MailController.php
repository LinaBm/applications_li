<?php

namespace Formulaire\AdminBundle\Controller\Mail;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Formulaire\AdminBundle\Form\AdminType;
use Symfony\Component\HttpFoundation\Response;

class MailController extends Controller
{
    /**
 * @Route("/mail", name="index_mail")
 * @Template()
 */
    public function indexAction()
    {

        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository('dmliBundle:DmliMailMel')->findAll();
//var_dump($services);die;
        $i = 0;
        foreach ($services as $service) {
            $result[$i]['id']=$service->getMelId();
            $result[$i]['libelle'] = $service->getMelLibelle();
            if($service->getDirId() != '0'){
                $direction=$em->getRepository('AdministrationDirectionBundle:Direction')->findBy(
                    array('id'=> $service->getDirId()));
               //var_dump($direction[0]->getLibelle());die;
                $result[$i]['direction']= $direction[0]->getLibelle();
            }else{
                $result[$i]['direction']= 'Autres';
            }

            $i++;
        }
        return $this->render('AdminBundle:Mail:ListeMail.html.twig', array('result'=> $result));

    }
    /**
     * @Route("/mail/remove/{id}", name="remove_mail")
     * @Template()
     */
    public function removeAction($id)
    {  $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('dmliBundle:DmliMailMel')->findBy(array('melId'=>$id));

        $em->remove($product[0]);
        $em->flush();
        $request->getSession()->getFlashBag()->add(
            'success',
            'Groupe Outlook supprimé!'
        );
        return $this->redirect($this->generateUrl('index_mail'));
    }
}
