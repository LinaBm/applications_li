<?php

namespace Formulaire\AdminBundle\Controller;

use Formulaire\dmliBundle\Entity\DmliCrmequipeCrmEqu;
use Formulaire\dmliBundle\Entity\DmliMailMel;
use Formulaire\dmliBundle\Entity\DmliPackhardwarePkh;
use Formulaire\dmliBundle\Entity\DmliSoftwareSft;
use Formulaire\dmliBundle\Entity\PosteBridge;
use Formulaire\dmliBundle\Entity\PostePst;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Formulaire\AdminBundle\Form\AdminType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class GestionController extends Controller
{
    /**
     * @Route("/addprofil", name="add_profil_valid")
     * @Template()
     */
    public function addProfilAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        if ($request->isMethod('POST')) {


            $poste = $this->add_traitement($_POST);
            $newPoste = new PosteBridge();

            $newPoste->setDir(
                $this->getDoctrine()
                    ->getRepository('AdministrationDirectionBundle:Direction')
                    ->find($_POST['gestion_adminbundle_profil']['direction'])
            );
            if (isset($_POST['gestion_adminbundle_profil']['pole'])) {
                $newPoste->setPol($this->getDoctrine()
                    ->getRepository('dmliBundle:PolePol')
                    ->find($_POST['gestion_adminbundle_profil']['pole']));
            }
            $newPoste->setSrv($this->getDoctrine()
                ->getRepository('AdministrationDirectionBundle:Service')
                ->find($_POST['gestion_adminbundle_profil']['service']));
            $newPoste->setPst($this->getDoctrine()
                ->getRepository('dmliBundle:PostePst')
                ->find($poste));

            $ex=$em->getRepository('dmliBundle:PosteBridge')->findBy(array('dir' => $newPoste->getDir(),'srv'=>$newPoste->getSrv(),
                'pol'=> $newPoste->getPol(), 'pst'=> $newPoste->getPst()
                ));

            if(count($ex) == 0){
                if($this->get('security.context')->getToken()->getUser()->isGranted('ROLE_DMLI_DAM')){
                    $this->sendMailAdd('vient d\'ajouter un nouveau Profil ','','Ajouter un Profil');
                }

            $em->persist($newPoste);
            $em->flush();
                $request->getSession()->getFlashBag()->add(
                    'success',
                    'Profil crée !'
                );
                return $this->redirect($this->generateUrl('index_profil'));
            // return $this->render('AdminBundle:Profil:ListeProfil.html.twig', array('result'=> $result));
        } else {
                $request->getSession()->getFlashBag()->add(
                    'myError',
                    'Profil Existant!'
                );
                return $this->redirect($this->generateUrl('add_profil'));
        }}

    }
    public function add_traitement($data)
    {
        $em = $this->getDoctrine()->getManager();
        if (isset($data['gestion_adminbundle_profil']['poste'])) {

            if ($data['gestion_adminbundle_profil']['poste'] > -1) {
                $this->poste = $data['gestion_adminbundle_profil']['poste'];


            } else {
                // var_dump($data['userNewPost']);

                $newPoste = new PostePst();

                $newPoste->setPstLibelle($data['userNewPost']);
                $em->persist($newPoste);
                $em->flush();

                $product = $this->getDoctrine()
                    ->getRepository('dmliBundle:PostePst')
                    ->findBy(array('pstLibelle' => $data['userNewPost']));
                $this->poste = $product[0]->getPstId();
            }
        }
        return $this->poste;
    }

    /**
     * @Route("/affectprofil", name="affect_profil_valid")
     * @Template()
     */
    public function affectProfilAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        if ($request->isMethod('POST')) {
            // var_dump($_POST);die;
            $this->data = $_POST;
            //remove all
            if (isset($this->data['software'])) {
                $y = 0;

                while (isset($this->data['software'][$y])) {
                    $sql = "SELECT t.sft_id, t.bdg_id FROM dmli_bdg_has_sft t WHERE t.bdg_id = " . $this->data['BdgId'] . ";";
                    $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
                    $query->execute();
                    $result = $query->fetchAll();
                    //var_dump($result);die;
                    if ($result > 0) {
                        foreach ($result as $row) {

                            $sql1 = "DELETE FROM dmli_bdg_has_sft WHERE sft_id = " . $row['sft_id'] . " AND bdg_id = " . $this->data['BdgId'] . ";";
                            $query1 = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql1);
                            $query1->execute();
                        }
                    }
                    /*$sql = "INSERT INTO dmli_bdg_has_sft (sft_id, bdg_id)
                    VALUES (".$this->data['software'][$y].",".$this->data['BdgId'].");";

                    $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
                    $query->execute();*/
                    $y++;
                }
            }

            if (isset($this->data['mailing'])) {
                $y = 0;
                while (isset($this->data['mailing'][$y])) {
                    $sql = "SELECT t.mel_id, t.bdg_id FROM dmli_bdg_has_mel t WHERE t.bdg_id = " . $this->data['BdgId'] . ";";
                    $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
                    $query->execute();
                    $result = $query->fetchAll();
                    //var_dump($result);die;
                    if ($result > 0) {
                        foreach ($result as $row) {

                            $sql1 = "DELETE FROM dmli_bdg_has_mel WHERE mel_id = " . $row['mel_id'] . " AND bdg_id = " . $this->data['BdgId'] . ";";
                            $query1 = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql1);
                            $query1->execute();
                        }
                    }
                    $y++;
                }
            }

            if (isset($this->data['hardware'])) {
                $y = 0;
                while (isset($this->data['hardware'][$y])) {
                    $sql = "SELECT t.har_id, t.bdg_id FROM dmli_bdg_has_har t WHERE t.bdg_id = " . $this->data['BdgId'] . ";";
                    $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
                    $query->execute();
                    $result = $query->fetchAll();
                    //var_dump($result);die;
                    if ($result > 0) {
                        foreach ($result as $row) {

                            $sql1 = "DELETE FROM dmli_bdg_has_har WHERE har_id = " . $row['har_id'] . " AND bdg_id = " . $this->data['BdgId'] . ";";
                            $query1 = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql1);
                            $query1->execute();
                        }
                    }
                    $y++;
                }
            }

            //persist new


            if (isset($this->data['software'])) {
                $y = 0;

                while (isset($this->data['software'][$y])) {
                    $sql = "INSERT INTO dmli_bdg_has_sft (sft_id, bdg_id)
        VALUES (" . $this->data['software'][$y] . "," . $this->data['BdgId'] . ");";
                    $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
                    $query->execute();
                    $y++;
                }
            }
            if (isset($this->data['hardware'])) {
                $y = 0;

                while (isset($this->data['hardware'][$y])) {
                    $sql = "INSERT INTO dmli_bdg_has_har (har_id, bdg_id)
        VALUES (" . $this->data['hardware'][$y] . "," . $this->data['BdgId'] . ");";
                    $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
                    $query->execute();
                    $y++;
                }
            }
            if (isset($this->data['mailing'])) {
                $y = 0;

                while (isset($this->data['mailing'][$y])) {
                    $sql = "INSERT INTO dmli_bdg_has_mel (mel_id, bdg_id)
        VALUES (" . $this->data['mailing'][$y] . "," . $this->data['BdgId'] . ");";
                    $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
                    $query->execute();
                    $y++;
                }
            }
            if($this->get('security.context')->getToken()->getUser()->isGranted('ROLE_DMLI_DAM')){
                $this->sendMailAdd('vient d\'affecter un logiciel/matériel/groupe outlook à un profil.' ,'','Affecter d\'un logiciel/materiel/groupe Outlook');
            }
            $request->getSession()->getFlashBag()->add(
                'success',
                'Profil crée !'
            );
            return $this->redirect($this->generateUrl('index_profil'));
            // return $this->render('AdminBundle:Profil:ListeProfil.html.twig', array('result'=> $result));
        } else {
            $request->getSession()->getFlashBag()->add(
                'myError',
                'Opération echouée !'
            );
              return $this->redirect($this->generateUrl('affect_profil'));
        }

    }

    /**
     * @Route("/editprofil", name="edit_profil_valid")
     * @Template()
     */
    public function editProfilAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        if ($request->isMethod('POST')) {
            $newPoste = $em->getRepository('dmliBundle:PosteBridge')
                ->findBy(array('bdgId'=>$_POST['BdgId']));
            $avant= $newPoste[0]->getPst()->getPstLibelle();
            $newPoste[0]->getPst()->setPstLibelle($_POST['gestion_adminbundle_profil']['posteHolder']);
            $em->persist($newPoste[0]);
            $em->flush();

            if($this->get('security.context')->getToken()->getUser()->isGranted('ROLE_DMLI_DAM')){
                $this->sendMailEdit('vient de modifier un poste ',$avant,$_POST['gestion_adminbundle_profil']['posteHolder'],'Modifier un poste');
            }
            $request->getSession()->getFlashBag()->add(
                'success',
                'Profil modifié !'
            );
            return $this->redirect($this->generateUrl('index_profil'));
            // return $this->render('AdminBundle:Profil:ListeProfil.html.twig', array('result'=> $result));
        } else {
            $request->getSession()->getFlashBag()->add(
                'myError',
                'Profil existant !'
            );
              return $this->redirect($this->generateUrl('edit_profil'));
        }

    }

    /**
     * @Route("/addkit", name="add_kit_valid")
     * @Template()
     */
    public function addKitAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        if ($request->isMethod('POST')) {
           $ex=$em->getRepository('dmliBundle:DmliPackhardwarePkh')->findBy(array('pkhLibelle' => $_POST['libelleKit']));


//var_dump(count($ex));die;
if(count($ex) == 0){
            $newKit = new DmliPackhardwarePkh();
            $newKit->setPkhLibelle($_POST['libelleKit']);
            $em->persist($newKit);
            $em->flush();

            $pkh=$em->getRepository('dmliBundle:DmliPackhardwarePkh')->findBy(array('pkhLibelle' => $_POST['libelleKit']));

            if (isset($_POST['hardware'])) {
                $y = 0;

                while (isset($_POST['hardware'][$y])) {
                    $sql = "INSERT INTO dmli_pkh_has_har (har_id, pkh_id)
        VALUES (" . $_POST['hardware'][$y] . "," . $pkh[0]->getPkhId() . ");";
                    $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
                    $query->execute();
                    $y++;
                }
            }}
            $request->getSession()->getFlashBag()->add(
                'success',
                'Kit crée !'
            );
            return $this->redirect($this->generateUrl('index_kit'));
            // return $this->render('AdminBundle:Profil:ListeProfil.html.twig', array('result'=> $result));
        } else {
            $request->getSession()->getFlashBag()->add(
                'myError',
                'Operation echouée !'
            );
            return $this->redirect($this->generateUrl('add_kit'));
        }

    }

    /**
     * @Route("/editkit", name="edit_kit_valid")
     * @Template()
     */
    public function editKitAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        if ($request->isMethod('POST')) {
//var_dump($_POST);die;
            $this->data = $_POST;
            //remove all
            if (isset($this->data['hardware'])) {
                $y = 0;
                while (isset($this->data['hardware'][$y])) {
                    $sql = "SELECT t.har_id, t.pkh_id FROM dmli_pkh_has_har t WHERE t.pkh_id = " . $this->data['pkh'] . ";";
                    $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
                    $query->execute();
                    $result = $query->fetchAll();
                    //var_dump($result);die;
                    if ($result > 0) {
                        foreach ($result as $row) {

                            $sql1 = "DELETE FROM dmli_pkh_has_har WHERE pkh_id = " . $this->data['pkh'] . ";";
                            $query1 = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql1);
                            $query1->execute();
                        }
                    }
                    $y++;
                }
            }

            if (isset($this->data['hardware'])) {
                $y = 0;

                while (isset($this->data['hardware'][$y])) {
                    $sql = "INSERT INTO dmli_pkh_has_har (har_id, pkh_id)
        VALUES (" . $this->data['hardware'][$y] . "," . $this->data['pkh'] . ");";
                    $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
                    $query->execute();
                    $y++;
                }
            }
            $request->getSession()->getFlashBag()->add(
                'success',
                'Kit modifié !'
            );
            return $this->redirect($this->generateUrl('index_kit'));
            // return $this->render('AdminBundle:Profil:ListeProfil.html.twig', array('result'=> $result));
        } else {
            $request->getSession()->getFlashBag()->add(
                'myError',
                'opération echouée !'
            );
            return $this->redirect($this->generateUrl('edit_kit'));
        }

    }

    /**
     * @Route("/addmail", name="add_mail_valid")
     * @Template()
     */
    public function addMailAction(){
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod('POST')) {

            $ex=$em->getRepository('dmliBundle:DmliMailMel')->findBy(array('melLibelle' => $_POST['gestion_adminbundle_profil']['mailHolder']));
            if(count($ex) == 0){
            $newMail = new DmliMailMel();
//$newMail->setMelId('172');
                //$sql="INSERT INTO `dmli_mail_mel`(`mel_libelle`, `dir_id`) VALUES ('".$_POST['gestion_adminbundle_profil']['mailHolder']."', ".$_POST['gestion_adminbundle_profil']['direction'].");";
           $newMail->setMelLibelle($_POST['gestion_adminbundle_profil']['mailHolder']);
            $newMail->setDirId($_POST['gestion_adminbundle_profil']['direction']);

            $em->persist($newMail);
            $em->flush();
               /* $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
                $query->execute();*/
                if($this->get('security.context')->getToken()->getUser()->isGranted('ROLE_DMLI_DAM') ){
                    $this->sendMailAdd('vient d\'ajouter un groupe Outlook ',$_POST['gestion_adminbundle_profil']['mailHolder'],'Ajouter un groupe Outlook');
                }
                $request->getSession()->getFlashBag()->add(
                    'success',
                    'Groupe Outlook crée !'
                );
            return $this->redirect($this->generateUrl('index_mail'));
        } else {
                $request->getSession()->getFlashBag()->add(
                    'myError',
                    'Groupe Outlook Existant!'
                );
            return $this->redirect($this->generateUrl('add_mail'));
        }}

    }

    /**
     * @Route("/editMail", name="edit_mail_valid")
     * @Template()
     */
    public function editMailAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        if ($request->isMethod('POST')) {
            $newMail = $em->getRepository('dmliBundle:DmliMailMel')
                ->findBy(array('melId'=>$_POST['mal']));
            $newMail[0]->setMelLibelle($_POST['gestion_adminbundle_profil']['posteHolder']);
            $em->persist($newMail[0]);
            $em->flush();
            $request->getSession()->getFlashBag()->add(
                'success',
                'Groupe Outlook Modifiée!'
            );
            return $this->redirect($this->generateUrl('index_mail'));
        } else {
            $request->getSession()->getFlashBag()->add(
                'myError',
                'Groupe Outlook Existant!'
            );
              return $this->redirect($this->generateUrl('edit_mail'));
        }

    }

    /**
     * @Route("/addposte", name="add_poste_valid")
     * @Template()
     */
    public function addPosteAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod('POST')) {
            $ex=$em->getRepository('dmliBundle:PostePst')->findBy(array('pstLibelle' => $_POST['gestion_adminbundle_profil']['posteHolder']));
//var_dump(count($ex));die;
            if(count($ex) == 0){
                $newMail = new PostePst();
                $newMail->setPstLibelle($_POST['gestion_adminbundle_profil']['posteHolder']);
                $em->persist($newMail);
                $em->flush();
                $request->getSession()->getFlashBag()->add(
                    'success',
                    'Poste crée !'
                );
                if($this->get('security.context')->getToken()->getUser()->isGranted('ROLE_DMLI_DAM') ){
                    $this->sendMailAdd('vient d\'ajouter un poste ',$_POST['gestion_adminbundle_profil']['posteHolder'],'Ajouter un poste');
                }
                return $this->redirect($this->generateUrl('index_poste'));
            } else {
                $request->getSession()->getFlashBag()->add(
                    'myError',
                    'Poste Existant!'
                );
                return $this->redirect($this->generateUrl('add_poste'));
            }}

    }

    /**
     * @Route("/editPoste", name="edit_poste_valid")
     * @Template()
     */
    public function editPosteAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        if ($request->isMethod('POST')) {
            $newMail = $em->getRepository('dmliBundle:PostePst')
                ->findBy(array('pstId'=>$_POST['mal']));
            $avant= $newMail[0]->getPstLibelle();
            $newMail[0]->setPstLibelle($_POST['gestion_adminbundle_profil']['posteHolder']);
            $em->persist($newMail[0]);
            $em->flush();
            $request->getSession()->getFlashBag()->add(
                'success',
                'Poste modifié !'
            );
            if($this->get('security.context')->getToken()->getUser()->isGranted('ROLE_DMLI_DAM')){
                $this->sendMailEdit('vient de modifier un poste ',$avant,$_POST['gestion_adminbundle_profil']['posteHolder'],'Modifier un Poste');
            }
            return $this->redirect($this->generateUrl('index_poste'));
        } else {
            $request->getSession()->getFlashBag()->add(
                'success',
                'Poste Existant!'
            );
            return $this->redirect($this->generateUrl('edit_poste'));
        }

    }

    /**
     * @Route("/addcrm", name="add_crm_valid")
     * @Template()
     */
    public function addEquipeAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod('POST')) {

            if($_POST['gestion_adminbundle_profil']['crm'] > -1) {
                $newC = $em->getRepository('dmliBundle:DmliCrmequipeCrmEqu')
                    ->findBy(array('crmEquId' => $_POST['gestion_adminbundle_profil']['crm']));
                if ($_POST['gestion_adminbundle_profil']['service'] != '') {
                    $ex = $em->getRepository('dmliBundle:DmliCrmequipeCrmEqu')->findBy(array('crmEquLibelle' => $newC[0]->getCrmEquLibelle(), 'srvId' => $_POST['gestion_adminbundle_profil']['service']));
                } else {
                    $ex = $em->getRepository('dmliBundle:DmliCrmequipeCrmEqu')->findBy(array('crmEquLibelle' => $newC[0]->getCrmEquLibelle(), 'srvId' => NULL));
                }
            }else{ $ex = array();}
        //    var_dump(count($ex));die;
            if (count($ex) == 0) {
                if ($_POST['gestion_adminbundle_profil']['crm'] > -1) {

                    $newCrm = new DmliCrmequipeCrmEqu();
                    $newC = $em->getRepository('dmliBundle:DmliCrmequipeCrmEqu')
                        ->findBy(array('crmEquId' => $_POST['gestion_adminbundle_profil']['crm']));
                    $newCrm->setCrmEquLibelle($newC[0]->getCrmEquLibelle());
                    if ($_POST['gestion_adminbundle_profil']['service'] != '') {
                        $newCrm->setSrvId($_POST['gestion_adminbundle_profil']['service']);
                    } else {
                        $newCrm->setSrvId('');
                    }
                } else {
                    $newCrm = new DmliCrmequipeCrmEqu();
                    $newCrm->setCrmEquLibelle($_POST['gestion_adminbundle_profil']['crmHolder']);
                    if ($_POST['gestion_adminbundle_profil']['service'] != '') {
                        $newCrm->setSrvId($_POST['gestion_adminbundle_profil']['service']);
                    } else {
                        $newCrm->getSrvId('');
                    }
                }


                if ($this->get('security.context')->getToken()->getUser()->isGranted('ROLE_DMLI_DAM')) {
                    $this->sendMailAdd('vient d\'ajouter une equipe crm ', $_POST['gestion_adminbundle_profil']['crmHolder'], 'Ajouter une Equipe CRM');
                }
                $em->persist($newCrm);
                $em->flush();
                $request->getSession()->getFlashBag()->add(
                    'success',
                    'Equipe CRM crée !'
                );
                return $this->redirect($this->generateUrl('index_crm'));
            }

        }
        $request->getSession()->getFlashBag()->add(
            'myError',
            'Equipe CRM existante!'
        );
        return $this->redirect($this->generateUrl('add_crm'));}


    /**
     * @Route("/editcrm", name="edit_crm_valid")
     * @Template()
     */
    public function editEquipeAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod('POST')) {
           // var_dump($_POST);die;

            $newCrm = $em->getRepository('dmliBundle:DmliCrmequipeCrmEqu')
                ->findBy(array('crmEquId'=>$_POST['srvId']));
            $avant=$newCrm[0]->getCrmEquLibelle();
            $newCrm[0]->setCrmEquLibelle($_POST['gestion_adminbundle_profil']['crmHolder']);
            $em->persist($newCrm[0]);
            $em->flush();
            if($this->get('security.context')->getToken()->getUser()->isGranted('ROLE_DMLI_DAM')){
                $this->sendMailEdit('vient de modifier une equipe crm ',$avant,$_POST['gestion_adminbundle_profil']['crmHolder'],'Modifier une Equipe CRM');
            }
            $request->getSession()->getFlashBag()->add(
                'success',
                'Equipe CRM modifiée!'
            );
            return $this->redirect($this->generateUrl('index_crm'));
        } else {
            $request->getSession()->getFlashBag()->add(
                'myError',
                'Opération echouée!'
            );
            return $this->redirect($this->generateUrl('add_crm'));
        }

    }


    function sendMailEdit($messages,$avant,$apres,$sub)
    {
        $this->data['senderName'] = strtoupper($this->get('security.context')->getToken()->getUser()->getSurname());
        $this->data['senderFirstname'] = strtolower($this->get('security.context')->getToken()->getUser()->getGivenname());
        $this->data['senderFirstname'] = ucfirst($this->get('security.context')->getToken()->getUser()->getGivenname());
        $this->data['senderMail'] = ucfirst($this->get('security.context')->getToken()->getUser()->getEmail());

        $message = \Swift_Message::newInstance()
            ->setSubject("[ADMINISTRAION - DMLI]".$sub)
            ->setFrom($this->data['senderMail'])
            ->setTo('materiel@ra-expansion.fr') // hotel and Valerie
            ->setCC($this->data['senderMail'])
            ->setBCC('poleweb@noz.fr')
            ->setBody(
                $this->renderView(
                // app/Resources/views/Emails/registration.html.twig
                    'AdminBundle:email:Mail.html.twig', array('nameuser' => $this->data['senderName'], 'firstnameuser' => $this->data['senderFirstname'], 'message' => $messages,
                    'avant'=>$avant,'apres'=>$apres)
                ),
                'text/html'
            );

        $this->get('mailer')->send($message);
    }

    function sendMailAdd($messages,$avant,$sub)
    {
        $this->data['senderName'] = strtoupper($this->get('security.context')->getToken()->getUser()->getSurname());
        $this->data['senderFirstname'] = strtolower($this->get('security.context')->getToken()->getUser()->getGivenname());
        $this->data['senderFirstname'] = ucfirst($this->get('security.context')->getToken()->getUser()->getGivenname());
        $this->data['senderMail'] = ucfirst($this->get('security.context')->getToken()->getUser()->getEmail());

        $message = \Swift_Message::newInstance()
            ->setSubject("[ADMINISTRAION - DMLI]".$sub)
            ->setFrom($this->data['senderMail'])
            ->setTo('materiel@ra-expansion.fr') // hotel and Valerie
            ->setCC($this->data['senderMail'])
            ->setBCC('poleweb@noz.fr')
            ->setBody(
                $this->renderView(
                // app/Resources/views/Emails/registration.html.twig
                    'AdminBundle:email:MailAdd.html.twig', array('nameuser' => $this->data['senderName'], 'firstnameuser' => $this->data['senderFirstname'], 'message' => $messages,
                        'add'=>$avant)
                ),
                'text/html'
            );

        $this->get('mailer')->send($message);
    }

}
