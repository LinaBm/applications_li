<?php

namespace Formulaire\AdminBundle\Controller\Poste;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Formulaire\AdminBundle\Form\AdminType;
use Symfony\Component\HttpFoundation\Response;

class PosteController extends Controller
{
    /**
     * @Route("/poste", name="index_poste")
     * @Template()
     */
    public function indexAction()
    {

        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository('dmliBundle:PostePst')->findAll();
//var_dump($services);die;
        $i = 0;
        foreach ($services as $service) {
            $result[$i]['id']=$service->getPstId();
            $result[$i]['libelle'] = $service->getPstLibelle();
            $i++;
        }
        return $this->render('AdminBundle:Poste:ListePoste.html.twig', array('result'=> $result));

    }
    /**
     * @Route("/poste/remove/{id}", name="remove_poste")
     * @Template()
     */
    public function removeAction($id)
    {  $request = $this->getRequest();

        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('dmliBundle:PostePst')->findBy(array('pstId'=>$id));

        $product1 = $em->getRepository('dmliBundle:PosteBridge')->findBy(array('pst'=>$product[0]));

        if(count($product1) == 0 ){
            $em->remove($product[0]);
            $em->flush();
            $request->getSession()->getFlashBag()->add(
                'success',
                'poste supprimé!'
            );
        }else{
            $request->getSession()->getFlashBag()->add(
                'myError',
                'Vous ne pouvez pas supprimer ce poste!'
            );
        }
        if($this->get('security.context')->getToken()->getUser()->isGranted('ROLE_DMLI_DAM')) {
            $this->sendMail('vient de supprimer le poste: ' . $product[0]->getPstLibelle());
        }
        return $this->redirect($this->generateUrl('index_poste'));
    }


    public function sendMail($message){

        $this->data['senderName'] = strtoupper($this->get('security.context')->getToken()->getUser()->getSurname());
        $this->data['senderFirstname'] = strtolower($this->get('security.context')->getToken()->getUser()->getGivenname());
        $this->data['senderFirstname'] = ucfirst($this->get('security.context')->getToken()->getUser()->getGivenname());
        $this->data['senderMail'] = ucfirst($this->get('security.context')->getToken()->getUser()->getEmail());

        $message = \Swift_Message::newInstance()
            ->setSubject("[ADMINISTRAION - DMLI]: Remove Poste")
            ->setFrom($this->data['senderMail'])
            ->setTo('materiel@ra-expansion.fr') // hotel and Valerie
            ->setCC($this->data['senderMail'])
            ->setBCC('poleweb@noz.fr')
            ->setBody(
                $this->renderView(
                // app/Resources/views/Emails/registration.html.twig
                    'AdminBundle:email:MailAdd.html.twig', array('nameuser' => $this->data['senderName'], 'firstnameuser' => $this->data['senderFirstname'], 'message'=>$message, 'add'=>"")
                ),
                'text/html'
            );

        $this->get('mailer')->send($message);
    }
}
