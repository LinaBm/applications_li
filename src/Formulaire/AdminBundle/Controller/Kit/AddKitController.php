<?php

namespace Formulaire\AdminBundle\Controller\Kit;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Formulaire\AdminBundle\Form\AdminType;
use Symfony\Component\HttpFoundation\Response;

class AddKitController extends Controller
{
    /**
     * @Route("/kit/add", name="add_kit")
     * @Template()
     */
    public function indexAction()
    {

        $form = $this->get('form.factory')->create(new AdminType());

        return $this->render('AdminBundle:Kit:AjouterKit.html.twig', array(
            'form' => $form->createView()
        ));

    }



}
