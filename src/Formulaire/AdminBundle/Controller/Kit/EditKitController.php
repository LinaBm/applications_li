<?php

namespace Formulaire\AdminBundle\Controller\Kit;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Formulaire\AdminBundle\Form\AdminType;
use Symfony\Component\HttpFoundation\Response;

class EditKitController extends Controller
{
    /**
     * @Route("/kit/edit/{id}", name="edit_kit")
     * @Template()
     */
    public function indexAction($id)
    {

        $em = $this->getDoctrine()->getManager();
        $services = $em->getRepository('dmliBundle:DmliPackhardwarePkh')->findBy(array('pkhId'=>$id));
//var_dump($services);die;

            $result['id']=$services[0]->getPkhId();
            $result['libelle'] = $services[0]->getPkhLibelle();

        $form = $this->get('form.factory')->create(new AdminType());
        return $this->render('AdminBundle:Kit:ModifierKit.html.twig', array(
            'form' => $form->createView(), 'result'=>$result
        ));
    }



}
