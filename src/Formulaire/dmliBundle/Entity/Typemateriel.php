<?php

namespace Formulaire\dmliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Typemateriel
 *
 * @ORM\Table(name="typemateriel", indexes={@ORM\Index(name="fk_typ_eta_id", columns={"eta_id"})})
 * @ORM\Entity
 */
class Typemateriel
{
    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=70, nullable=false)
     */
    private $libelle;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Formulaire\dmliBundle\Entity\Etat
     *
     * @ORM\ManyToOne(targetEntity="Formulaire\dmliBundle\Entity\Etat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="eta_id", referencedColumnName="id")
     * })
     */
    private $eta;


    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return Typemateriel
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    
        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set eta
     *
     * @param \Formulaire\dmliBundle\Entity\Etat $eta
     *
     * @return Typemateriel
     */
    public function setEta(\Formulaire\dmliBundle\Entity\Etat $eta = null)
    {
        $this->eta = $eta;
    
        return $this;
    }

    /**
     * Get eta
     *
     * @return \Formulaire\dmliBundle\Entity\Etat
     */
    public function getEta()
    {
        return $this->eta;
    }
}

