<?php

namespace Formulaire\dmliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DmliPackhardwarePkh
 *
 * @ORM\Table(name="dmli_packhardware_pkh")
 * @ORM\Entity
 */
class DmliPackhardwarePkh
{
    /**
     * @var string
     *
     * @ORM\Column(name="pkh_libelle", type="string", length=70, nullable=false)
     */
    private $pkhLibelle;

    /**
     * @var integer
     *
     * @ORM\Column(name="pkh_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $pkhId;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Formulaire\dmliBundle\Entity\DmliHardwareHar", inversedBy="pkh")
     * @ORM\JoinTable(name="dmli_pkh_has_har",
     *   joinColumns={
     *     @ORM\JoinColumn(name="pkh_id", referencedColumnName="pkh_id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="har_id", referencedColumnName="har_id")
     *   }
     * )
     */
    private $har;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->har = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set pkhLibelle
     *
     * @param string $pkhLibelle
     *
     * @return DmliPackhardwarePkh
     */
    public function setPkhLibelle($pkhLibelle)
    {
        $this->pkhLibelle = $pkhLibelle;
    
        return $this;
    }

    /**
     * Get pkhLibelle
     *
     * @return string
     */
    public function getPkhLibelle()
    {
        return $this->pkhLibelle;
    }

    /**
     * Get pkhId
     *
     * @return integer
     */
    public function getPkhId()
    {
        return $this->pkhId;
    }

    /**
     * Add har
     *
     * @param \Formulaire\dmliBundle\Entity\DmliHardwareHar $har
     *
     * @return DmliPackhardwarePkh
     */
    public function addHar(\Formulaire\dmliBundle\Entity\DmliHardwareHar $har)
    {
        $this->har[] = $har;
    
        return $this;
    }

    /**
     * Remove har
     *
     * @param \Formulaire\dmliBundle\Entity\DmliHardwareHar $har
     */
    public function removeHar(\Formulaire\dmliBundle\Entity\DmliHardwareHar $har)
    {
        $this->har->removeElement($har);
    }

    /**
     * Get har
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHar()
    {
        return $this->har;
    }
}

