<?php

namespace Formulaire\dmliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PosteBridge
 *
 * @ORM\Table(name="poste_bridge", indexes={@ORM\Index(name="fk_poste_bridge_direction_dir1_idx", columns={"dir_id"}), @ORM\Index(name="fk_poste_bridge_service_srv1_idx", columns={"srv_id"}), @ORM\Index(name="fk_poste_bridge_pole_pol1_idx", columns={"pol_id"}), @ORM\Index(name="fk_poste_bridge_poste_pst1_idx", columns={"pst_id"})})
 * @ORM\Entity
 */
class PosteBridge
{
    /**
     * @var integer
     *
     * @ORM\Column(name="bdg_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $bdgId;

    /**
     * @var \Administration\DirectionBundle\Entity\Direction
     *
     * @ORM\ManyToOne(targetEntity="Administration\DirectionBundle\Entity\Direction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dir_id", referencedColumnName="id")
     * })
     */
    private $dir;

    /**
     * @var \Formulaire\dmliBundle\Entity\PostePst
     *
     * @ORM\ManyToOne(targetEntity="Formulaire\dmliBundle\Entity\PostePst")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pst_id", referencedColumnName="pst_id")
     * })
     */
    private $pst;

    /**
     * @var \Administration\DirectionBundle\Entity\Service
     *
     * @ORM\ManyToOne(targetEntity="Administration\DirectionBundle\Entity\Service")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="srv_id", referencedColumnName="id")
     * })
     */
    private $srv;

    /**
     * @var \Formulaire\dmliBundle\Entity\PolePol
     *
     * @ORM\ManyToOne(targetEntity="Formulaire\dmliBundle\Entity\PolePol")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pol_id", referencedColumnName="pol_id")
     * })
     */
    private $pol;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Formulaire\dmliBundle\Entity\DmliSoftwareSft", mappedBy="bdg")
     */
    private $sft;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Formulaire\dmliBundle\Entity\DmliHardwareHar", mappedBy="bdg")
     */
    private $har;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Formulaire\dmliBundle\Entity\DmliMailMel", mappedBy="bdg")
     */
    private $mel;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sft = new \Doctrine\Common\Collections\ArrayCollection();
        $this->har = new \Doctrine\Common\Collections\ArrayCollection();
        $this->mel = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get bdgId
     *
     * @return integer
     */
    public function getBdgId()
    {
        return $this->bdgId;
    }

    /**
     * Set dir
     *
     * @param \Administration\DirectionBundle\Entity\Direction $dir
     *
     * @return PosteBridge
     */
    public function setDir(\Administration\DirectionBundle\Entity\Direction $dir = null)
    {
        $this->dir = $dir;
    
        return $this;
    }

    /**
     * Get dir
     *
     * @return \Administration\DirectionBundle\Entity\Direction
     */
    public function getDir()
    {
        return $this->dir;
    }

    /**
     * Set pst
     *
     * @param \Formulaire\dmliBundle\Entity\PostePst $pst
     *
     * @return PosteBridge
     */
    public function setPst(\Formulaire\dmliBundle\Entity\PostePst $pst = null)
    {
        $this->pst = $pst;
    
        return $this;
    }

    /**
     * Get pst
     *
     * @return \Formulaire\dmliBundle\Entity\PostePst
     */
    public function getPst()
    {
        return $this->pst;
    }

    /**
     * Set srv
     *
     * @param \Administration\DirectionBundle\Entity\Service $srv
     *
     * @return PosteBridge
     */
    public function setSrv(\Administration\DirectionBundle\Entity\Service $srv = null)
    {
        $this->srv = $srv;
    
        return $this;
    }

    /**
     * Get srv
     *
     * @return \Administration\DirectionBundle\Entity\Service
     */
    public function getSrv()
    {
        return $this->srv;
    }

    /**
     * Set pol
     *
     * @param \Formulaire\dmliBundle\Entity\PolePol $pol
     *
     * @return PosteBridge
     */
    public function setPol(\Formulaire\dmliBundle\Entity\PolePol $pol = null)
    {
        $this->pol = $pol;
    
        return $this;
    }

    /**
     * Get pol
     *
     * @return \Formulaire\dmliBundle\Entity\PolePol
     */
    public function getPol()
    {
        return $this->pol;
    }

    /**
     * Add sft
     *
     * @param \Formulaire\dmliBundle\Entity\DmliSoftwareSft $sft
     *
     * @return PosteBridge
     */
    public function addSft(\Formulaire\dmliBundle\Entity\DmliSoftwareSft $sft)
    {
        $this->sft[] = $sft;
    
        return $this;
    }

    /**
     * Remove sft
     *
     * @param \Formulaire\dmliBundle\Entity\DmliSoftwareSft $sft
     */
    public function removeSft(\Formulaire\dmliBundle\Entity\DmliSoftwareSft $sft)
    {
        $this->sft->removeElement($sft);
    }

    /**
     * Get sft
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSft()
    {
        return $this->sft;
    }

    /**
     * Add har
     *
     * @param \Formulaire\dmliBundle\Entity\DmliHardwareHar $har
     *
     * @return PosteBridge
     */
    public function addHar(\Formulaire\dmliBundle\Entity\DmliHardwareHar $har)
    {
        $this->har[] = $har;
    
        return $this;
    }

    /**
     * Remove har
     *
     * @param \Formulaire\dmliBundle\Entity\DmliHardwareHar $har
     */
    public function removeHar(\Formulaire\dmliBundle\Entity\DmliHardwareHar $har)
    {
        $this->har->removeElement($har);
    }

    /**
     * Get har
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHar()
    {
        return $this->har;
    }

    /**
     * Add mel
     *
     * @param \Formulaire\dmliBundle\Entity\DmliMailMel $mel
     *
     * @return PosteBridge
     */
    public function addMel(\Formulaire\dmliBundle\Entity\DmliMailMel $mel)
    {
        $this->mel[] = $mel;
    
        return $this;
    }

    /**
     * Remove mel
     *
     * @param \Formulaire\dmliBundle\Entity\DmliMailMel $mel
     */
    public function removeMel(\Formulaire\dmliBundle\Entity\DmliMailMel $mel)
    {
        $this->mel->removeElement($mel);
    }

    /**
     * Get mel
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMel()
    {
        return $this->mel;
    }
}

