<?php

namespace Formulaire\dmliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ChkChecklists
 *
 * @ORM\Table(name="chk_checklists")
 * @ORM\Entity
 */
class ChkChecklists
{
    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;

    /**
     * @var boolean
     *
     * @ORM\Column(name="desactive", type="boolean", nullable=false)
     */
    private $desactive = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Formulaire\dmliBundle\Entity\ChkPointsDeControle", inversedBy="checklist")
     * @ORM\JoinTable(name="checklist_points_de_controle",
     *   joinColumns={
     *     @ORM\JoinColumn(name="checklist_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="pointdecontrole_id", referencedColumnName="id")
     *   }
     * )
     */
    private $pointdecontrole;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pointdecontrole = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return ChkChecklists
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set desactive
     *
     * @param boolean $desactive
     *
     * @return ChkChecklists
     */
    public function setDesactive($desactive)
    {
        $this->desactive = $desactive;
    
        return $this;
    }

    /**
     * Get desactive
     *
     * @return boolean
     */
    public function getDesactive()
    {
        return $this->desactive;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add pointdecontrole
     *
     * @param \Formulaire\dmliBundle\Entity\ChkPointsDeControle $pointdecontrole
     *
     * @return ChkChecklists
     */
    public function addPointdecontrole(\Formulaire\dmliBundle\Entity\ChkPointsDeControle $pointdecontrole)
    {
        $this->pointdecontrole[] = $pointdecontrole;
    
        return $this;
    }

    /**
     * Remove pointdecontrole
     *
     * @param \Formulaire\dmliBundle\Entity\ChkPointsDeControle $pointdecontrole
     */
    public function removePointdecontrole(\Formulaire\dmliBundle\Entity\ChkPointsDeControle $pointdecontrole)
    {
        $this->pointdecontrole->removeElement($pointdecontrole);
    }

    /**
     * Get pointdecontrole
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPointdecontrole()
    {
        return $this->pointdecontrole;
    }
}

