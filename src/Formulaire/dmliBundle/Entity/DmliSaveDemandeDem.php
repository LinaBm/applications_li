<?php

namespace Formulaire\dmliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DmliSaveDemandeDem
 *
 * @ORM\Table(name="dmli_save_demande_dem", indexes={@ORM\Index(name="fk_dmli_save_demande_dem_societes_ste1_idx", columns={"ste_id"}), @ORM\Index(name="fk_dmli_save_demande_dem_dmli_crmequipe_crm_equ1_idx", columns={"crm_equ_id"}), @ORM\Index(name="fk_dmli_save_demande_dem_dmli_crmprofil_crm_pro1_idx", columns={"crm_pro_id"})})
 * @ORM\Entity
 */
class DmliSaveDemandeDem
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dem_date_demande", type="datetime", nullable=false)
     */
    private $demDateDemande;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dem_date_livraison", type="date", nullable=false)
     */
    private $demDateLivraison;

    /**
     * @var string
     *
     * @ORM\Column(name="dem_demandeur_nom", type="string", length=70, nullable=false)
     */
    private $demDemandeurNom;

    /**
     * @var string
     *
     * @ORM\Column(name="dem_demandeur_prenom", type="string", length=70, nullable=false)
     */
    private $demDemandeurPrenom;

    /**
     * @var string
     *
     * @ORM\Column(name="dem_demandeur_mail", type="string", length=70, nullable=false)
     */
    private $demDemandeurMail;

    /**
     * @var string
     *
     * @ORM\Column(name="dem_utilisateur_nom", type="string", length=70, nullable=false)
     */
    private $demUtilisateurNom;

    /**
     * @var string
     *
     * @ORM\Column(name="dem_utilisateur_prenom", type="string", length=70, nullable=false)
     */
    private $demUtilisateurPrenom;

    /**
     * @var integer
     *
     * @ORM\Column(name="dir_id", type="integer", nullable=false)
     */
    private $dirId;

    /**
     * @var integer
     *
     * @ORM\Column(name="srv_id", type="integer", nullable=false)
     */
    private $srvId;

    /**
     * @var integer
     *
     * @ORM\Column(name="pol_id", type="integer", nullable=true)
     */
    private $polId;

    /**
     * @var integer
     *
     * @ORM\Column(name="pst_id", type="integer", nullable=true)
     */
    private $pstId;

    /**
     * @var string
     *
     * @ORM\Column(name="dem_poste_libelle", type="string", length=70, nullable=true)
     */
    private $demPosteLibelle;

    /**
     * @var integer
     *
     * @ORM\Column(name="ste_id", type="integer", nullable=true)
     */
    private $steId;

    /**
     * @var string
     *
     * @ORM\Column(name="dem_societe_libelle", type="string", length=70, nullable=true)
     */
    private $demSocieteLibelle;

    /**
     * @var string
     *
     * @ORM\Column(name="dem_sap_profil", type="string", length=70, nullable=true)
     */
    private $demSapProfil;

    /**
     * @var string
     *
     * @ORM\Column(name="dem_cognos_profil", type="string", length=70, nullable=true)
     */
    private $demCognosProfil;

    /**
     * @var string
     *
     * @ORM\Column(name="dem_sap_remarque", type="string", length=500, nullable=true)
     */
    private $demSapRemarque;

    /**
     * @var string
     *
     * @ORM\Column(name="dem_cognos_remarque", type="string", length=500, nullable=true)
     */
    private $demCognosRemarque;

    /**
     * @var integer
     *
     * @ORM\Column(name="crm_equ_id", type="integer", nullable=true)
     */
    private $crmEquId;

    /**
     * @var integer
     *
     * @ORM\Column(name="crm_pro_id", type="integer", nullable=true)
     */
    private $crmProId;

    /**
     * @var string
     *
     * @ORM\Column(name="dem_remarque", type="string", length=500, nullable=true)
     */
    private $demRemarque;

    /**
     * @var integer
     *
     * @ORM\Column(name="dem_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $demId;


    /**
     * Set demDateDemande
     *
     * @param \DateTime $demDateDemande
     *
     * @return DmliSaveDemandeDem
     */
    public function setDemDateDemande($demDateDemande)
    {
        $this->demDateDemande = $demDateDemande;
    
        return $this;
    }

    /**
     * Get demDateDemande
     *
     * @return \DateTime
     */
    public function getDemDateDemande()
    {
        return $this->demDateDemande;
    }

    /**
     * Set demDateLivraison
     *
     * @param \DateTime $demDateLivraison
     *
     * @return DmliSaveDemandeDem
     */
    public function setDemDateLivraison($demDateLivraison)
    {
        $this->demDateLivraison = $demDateLivraison;
    
        return $this;
    }

    /**
     * Get demDateLivraison
     *
     * @return \DateTime
     */
    public function getDemDateLivraison()
    {
        return $this->demDateLivraison;
    }

    /**
     * Set demDemandeurNom
     *
     * @param string $demDemandeurNom
     *
     * @return DmliSaveDemandeDem
     */
    public function setDemDemandeurNom($demDemandeurNom)
    {
        $this->demDemandeurNom = $demDemandeurNom;
    
        return $this;
    }

    /**
     * Get demDemandeurNom
     *
     * @return string
     */
    public function getDemDemandeurNom()
    {
        return $this->demDemandeurNom;
    }

    /**
     * Set demDemandeurPrenom
     *
     * @param string $demDemandeurPrenom
     *
     * @return DmliSaveDemandeDem
     */
    public function setDemDemandeurPrenom($demDemandeurPrenom)
    {
        $this->demDemandeurPrenom = $demDemandeurPrenom;
    
        return $this;
    }

    /**
     * Get demDemandeurPrenom
     *
     * @return string
     */
    public function getDemDemandeurPrenom()
    {
        return $this->demDemandeurPrenom;
    }

    /**
     * Set demDemandeurMail
     *
     * @param string $demDemandeurMail
     *
     * @return DmliSaveDemandeDem
     */
    public function setDemDemandeurMail($demDemandeurMail)
    {
        $this->demDemandeurMail = $demDemandeurMail;
    
        return $this;
    }

    /**
     * Get demDemandeurMail
     *
     * @return string
     */
    public function getDemDemandeurMail()
    {
        return $this->demDemandeurMail;
    }

    /**
     * Set demUtilisateurNom
     *
     * @param string $demUtilisateurNom
     *
     * @return DmliSaveDemandeDem
     */
    public function setDemUtilisateurNom($demUtilisateurNom)
    {
        $this->demUtilisateurNom = $demUtilisateurNom;
    
        return $this;
    }

    /**
     * Get demUtilisateurNom
     *
     * @return string
     */
    public function getDemUtilisateurNom()
    {
        return $this->demUtilisateurNom;
    }

    /**
     * Set demUtilisateurPrenom
     *
     * @param string $demUtilisateurPrenom
     *
     * @return DmliSaveDemandeDem
     */
    public function setDemUtilisateurPrenom($demUtilisateurPrenom)
    {
        $this->demUtilisateurPrenom = $demUtilisateurPrenom;
    
        return $this;
    }

    /**
     * Get demUtilisateurPrenom
     *
     * @return string
     */
    public function getDemUtilisateurPrenom()
    {
        return $this->demUtilisateurPrenom;
    }

    /**
     * Set dirId
     *
     * @param integer $dirId
     *
     * @return DmliSaveDemandeDem
     */
    public function setDirId($dirId)
    {
        $this->dirId = $dirId;
    
        return $this;
    }

    /**
     * Get dirId
     *
     * @return integer
     */
    public function getDirId()
    {
        return $this->dirId;
    }

    /**
     * Set srvId
     *
     * @param integer $srvId
     *
     * @return DmliSaveDemandeDem
     */
    public function setSrvId($srvId)
    {
        $this->srvId = $srvId;
    
        return $this;
    }

    /**
     * Get srvId
     *
     * @return integer
     */
    public function getSrvId()
    {
        return $this->srvId;
    }

    /**
     * Set polId
     *
     * @param integer $polId
     *
     * @return DmliSaveDemandeDem
     */
    public function setPolId($polId)
    {
        $this->polId = $polId;
    
        return $this;
    }

    /**
     * Get polId
     *
     * @return integer
     */
    public function getPolId()
    {
        return $this->polId;
    }

    /**
     * Set pstId
     *
     * @param integer $pstId
     *
     * @return DmliSaveDemandeDem
     */
    public function setPstId($pstId)
    {
        $this->pstId = $pstId;
    
        return $this;
    }

    /**
     * Get pstId
     *
     * @return integer
     */
    public function getPstId()
    {
        return $this->pstId;
    }

    /**
     * Set demPosteLibelle
     *
     * @param string $demPosteLibelle
     *
     * @return DmliSaveDemandeDem
     */
    public function setDemPosteLibelle($demPosteLibelle)
    {
        $this->demPosteLibelle = $demPosteLibelle;
    
        return $this;
    }

    /**
     * Get demPosteLibelle
     *
     * @return string
     */
    public function getDemPosteLibelle()
    {
        return $this->demPosteLibelle;
    }

    /**
     * Set steId
     *
     * @param integer $steId
     *
     * @return DmliSaveDemandeDem
     */
    public function setSteId($steId)
    {
        $this->steId = $steId;
    
        return $this;
    }

    /**
     * Get steId
     *
     * @return integer
     */
    public function getSteId()
    {
        return $this->steId;
    }

    /**
     * Set demSocieteLibelle
     *
     * @param string $demSocieteLibelle
     *
     * @return DmliSaveDemandeDem
     */
    public function setDemSocieteLibelle($demSocieteLibelle)
    {
        $this->demSocieteLibelle = $demSocieteLibelle;
    
        return $this;
    }

    /**
     * Get demSocieteLibelle
     *
     * @return string
     */
    public function getDemSocieteLibelle()
    {
        return $this->demSocieteLibelle;
    }

    /**
     * Set demSapProfil
     *
     * @param string $demSapProfil
     *
     * @return DmliSaveDemandeDem
     */
    public function setDemSapProfil($demSapProfil)
    {
        $this->demSapProfil = $demSapProfil;
    
        return $this;
    }

    /**
     * Get demSapProfil
     *
     * @return string
     */
    public function getDemSapProfil()
    {
        return $this->demSapProfil;
    }

    /**
     * Set demCognosProfil
     *
     * @param string $demCognosProfil
     *
     * @return DmliSaveDemandeDem
     */
    public function setDemCognosProfil($demCognosProfil)
    {
        $this->demCognosProfil = $demCognosProfil;
    
        return $this;
    }

    /**
     * Get demCognosProfil
     *
     * @return string
     */
    public function getDemCognosProfil()
    {
        return $this->demCognosProfil;
    }

    /**
     * Set demSapRemarque
     *
     * @param string $demSapRemarque
     *
     * @return DmliSaveDemandeDem
     */
    public function setDemSapRemarque($demSapRemarque)
    {
        $this->demSapRemarque = $demSapRemarque;
    
        return $this;
    }

    /**
     * Get demSapRemarque
     *
     * @return string
     */
    public function getDemSapRemarque()
    {
        return $this->demSapRemarque;
    }

    /**
     * Set demCognosRemarque
     *
     * @param string $demCognosRemarque
     *
     * @return DmliSaveDemandeDem
     */
    public function setDemCognosRemarque($demCognosRemarque)
    {
        $this->demCognosRemarque = $demCognosRemarque;
    
        return $this;
    }

    /**
     * Get demCognosRemarque
     *
     * @return string
     */
    public function getDemCognosRemarque()
    {
        return $this->demCognosRemarque;
    }

    /**
     * Set crmEquId
     *
     * @param integer $crmEquId
     *
     * @return DmliSaveDemandeDem
     */
    public function setCrmEquId($crmEquId)
    {
        $this->crmEquId = $crmEquId;
    
        return $this;
    }

    /**
     * Get crmEquId
     *
     * @return integer
     */
    public function getCrmEquId()
    {
        return $this->crmEquId;
    }

    /**
     * Set crmProId
     *
     * @param integer $crmProId
     *
     * @return DmliSaveDemandeDem
     */
    public function setCrmProId($crmProId)
    {
        $this->crmProId = $crmProId;
    
        return $this;
    }

    /**
     * Get crmProId
     *
     * @return integer
     */
    public function getCrmProId()
    {
        return $this->crmProId;
    }

    /**
     * Set demRemarque
     *
     * @param string $demRemarque
     *
     * @return DmliSaveDemandeDem
     */
    public function setDemRemarque($demRemarque)
    {
        $this->demRemarque = $demRemarque;
    
        return $this;
    }

    /**
     * Get demRemarque
     *
     * @return string
     */
    public function getDemRemarque()
    {
        return $this->demRemarque;
    }

    /**
     * Get demId
     *
     * @return integer
     */
    public function getDemId()
    {
        return $this->demId;
    }
}

