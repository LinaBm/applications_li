<?php

namespace Formulaire\dmliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DmliDampersonnelDamPrs
 *
 * @ORM\Table(name="dmli_dampersonnel_dam_prs")
 * @ORM\Entity
 */
class DmliDampersonnelDamPrs
{
    /**
     * @var string
     *
     * @ORM\Column(name="dam_prs_libelle", type="string", length=10, nullable=false)
     */
    private $damPrsLibelle;

    /**
     * @var integer
     *
     * @ORM\Column(name="dam_prs_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $damPrsId;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Formulaire\dmliBundle\Entity\PostePst", inversedBy="damPrs")
     * @ORM\JoinTable(name="dmli_dam_prs_has_pst",
     *   joinColumns={
     *     @ORM\JoinColumn(name="dam_prs_id", referencedColumnName="dam_prs_id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="pst_id", referencedColumnName="pst_id")
     *   }
     * )
     */
    private $pst;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pst = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set damPrsLibelle
     *
     * @param string $damPrsLibelle
     *
     * @return DmliDampersonnelDamPrs
     */
    public function setDamPrsLibelle($damPrsLibelle)
    {
        $this->damPrsLibelle = $damPrsLibelle;
    
        return $this;
    }

    /**
     * Get damPrsLibelle
     *
     * @return string
     */
    public function getDamPrsLibelle()
    {
        return $this->damPrsLibelle;
    }

    /**
     * Get damPrsId
     *
     * @return integer
     */
    public function getDamPrsId()
    {
        return $this->damPrsId;
    }

    /**
     * Add pst
     *
     * @param \Formulaire\dmliBundle\Entity\PostePst $pst
     *
     * @return DmliDampersonnelDamPrs
     */
    public function addPst(\Formulaire\dmliBundle\Entity\PostePst $pst)
    {
        $this->pst[] = $pst;
    
        return $this;
    }

    /**
     * Remove pst
     *
     * @param \Formulaire\dmliBundle\Entity\PostePst $pst
     */
    public function removePst(\Formulaire\dmliBundle\Entity\PostePst $pst)
    {
        $this->pst->removeElement($pst);
    }

    /**
     * Get pst
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPst()
    {
        return $this->pst;
    }
}

