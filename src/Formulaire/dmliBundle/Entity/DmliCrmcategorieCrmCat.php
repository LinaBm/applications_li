<?php

namespace Formulaire\dmliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DmliCrmcategorieCrmCat
 *
 * @ORM\Table(name="dmli_crmcategorie_crm_cat")
 * @ORM\Entity
 */
class DmliCrmcategorieCrmCat
{
    /**
     * @var string
     *
     * @ORM\Column(name="crm_cat_libelle", type="string", length=70, nullable=false)
     */
    private $crmCatLibelle;

    /**
     * @var integer
     *
     * @ORM\Column(name="crm_cat_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $crmCatId;


    /**
     * Set crmCatLibelle
     *
     * @param string $crmCatLibelle
     *
     * @return DmliCrmcategorieCrmCat
     */
    public function setCrmCatLibelle($crmCatLibelle)
    {
        $this->crmCatLibelle = $crmCatLibelle;
    
        return $this;
    }

    /**
     * Get crmCatLibelle
     *
     * @return string
     */
    public function getCrmCatLibelle()
    {
        return $this->crmCatLibelle;
    }

    /**
     * Get crmCatId
     *
     * @return integer
     */
    public function getCrmCatId()
    {
        return $this->crmCatId;
    }
}

