<?php

namespace Formulaire\dmliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DmliCrmequipeCrmEqu
 *
 * @ORM\Table(name="dmli_crmequipe_crm_equ")
 * @ORM\Entity
 */
class DmliCrmequipeCrmEqu
{
    /**
     * @var string
     *
     * @ORM\Column(name="crm_equ_libelle", type="string", length=70, nullable=false)
     */
    private $crmEquLibelle;

    /**
     * @var integer
     *
     * @ORM\Column(name="srv_id", type="integer", nullable=true)
     */
    private $srvId;

    /**
     * @var integer
     *
     * @ORM\Column(name="crm_equ_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $crmEquId;


    /**
     * Set crmEquLibelle
     *
     * @param string $crmEquLibelle
     *
     * @return DmliCrmequipeCrmEqu
     */
    public function setCrmEquLibelle($crmEquLibelle)
    {
        $this->crmEquLibelle = $crmEquLibelle;
    
        return $this;
    }

    /**
     * Get crmEquLibelle
     *
     * @return string
     */
    public function getCrmEquLibelle()
    {
        return $this->crmEquLibelle;
    }

    /**
     * Set srvId
     *
     * @param integer $srvId
     *
     * @return DmliCrmequipeCrmEqu
     */
    public function setSrvId($srvId)
    {
        $this->srvId = $srvId;
    
        return $this;
    }

    /**
     * Get srvId
     *
     * @return integer
     */
    public function getSrvId()
    {
        return $this->srvId;
    }

    /**
     * Get crmEquId
     *
     * @return integer
     */
    public function getCrmEquId()
    {
        return $this->crmEquId;
    }
}

