<?php

namespace Formulaire\dmliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DmliSoftwareSft
 *
 * @ORM\Table(name="dmli_software_sft")
 * @ORM\Entity
 */
class DmliSoftwareSft
{
    /**
     * @var string
     *
     * @ORM\Column(name="sft_libelle", type="string", length=70, nullable=false)
     */
    private $sftLibelle;

    /**
     * @var string
     *
     * @ORM\Column(name="sft_prix", type="decimal", precision=7, scale=0, nullable=true)
     */
    private $sftPrix;

    /**
     * @var string
     *
     * @ORM\Column(name="sft_photo", type="string", length=70, nullable=true)
     */
    private $sftPhoto;

    /**
     * @var string
     *
     * @ORM\Column(name="sft_description", type="string", length=500, nullable=true)
     */
    private $sftDescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="sft_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $sftId;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Formulaire\dmliBundle\Entity\PosteBridge", inversedBy="sft")
     * @ORM\JoinTable(name="dmli_bdg_has_sft",
     *   joinColumns={
     *     @ORM\JoinColumn(name="sft_id", referencedColumnName="sft_id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="bdg_id", referencedColumnName="bdg_id")
     *   }
     * )
     */
    private $bdg;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->bdg = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set sftLibelle
     *
     * @param string $sftLibelle
     *
     * @return DmliSoftwareSft
     */
    public function setSftLibelle($sftLibelle)
    {
        $this->sftLibelle = $sftLibelle;
    
        return $this;
    }

    /**
     * Get sftLibelle
     *
     * @return string
     */
    public function getSftLibelle()
    {
        return $this->sftLibelle;
    }

    /**
     * Set sftPrix
     *
     * @param string $sftPrix
     *
     * @return DmliSoftwareSft
     */
    public function setSftPrix($sftPrix)
    {
        $this->sftPrix = $sftPrix;
    
        return $this;
    }

    /**
     * Get sftPrix
     *
     * @return string
     */
    public function getSftPrix()
    {
        return $this->sftPrix;
    }

    /**
     * Set sftPhoto
     *
     * @param string $sftPhoto
     *
     * @return DmliSoftwareSft
     */
    public function setSftPhoto($sftPhoto)
    {
        $this->sftPhoto = $sftPhoto;
    
        return $this;
    }

    /**
     * Get sftPhoto
     *
     * @return string
     */
    public function getSftPhoto()
    {
        return $this->sftPhoto;
    }

    /**
     * Set sftDescription
     *
     * @param string $sftDescription
     *
     * @return DmliSoftwareSft
     */
    public function setSftDescription($sftDescription)
    {
        $this->sftDescription = $sftDescription;
    
        return $this;
    }

    /**
     * Get sftDescription
     *
     * @return string
     */
    public function getSftDescription()
    {
        return $this->sftDescription;
    }

    /**
     * Get sftId
     *
     * @return integer
     */
    public function getSftId()
    {
        return $this->sftId;
    }

    /**
     * Add bdg
     *
     * @param \Formulaire\dmliBundle\Entity\PosteBridge $bdg
     *
     * @return DmliSoftwareSft
     */
    public function addBdg(\Formulaire\dmliBundle\Entity\PosteBridge $bdg)
    {
        $this->bdg[] = $bdg;
    
        return $this;
    }

    /**
     * Remove bdg
     *
     * @param \Formulaire\dmliBundle\Entity\PosteBridge $bdg
     */
    public function removeBdg(\Formulaire\dmliBundle\Entity\PosteBridge $bdg)
    {
        $this->bdg->removeElement($bdg);
    }

    /**
     * Get bdg
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBdg()
    {
        return $this->bdg;
    }
}

