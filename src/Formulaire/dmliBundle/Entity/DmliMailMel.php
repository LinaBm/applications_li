<?php

namespace Formulaire\dmliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DmliMailMel
 *
 * @ORM\Table(name="dmli_mail_mel", indexes={@ORM\Index(name="fk_dmli_mail_mel_direction_dir1_idx", columns={"dir_id"})})
 * @ORM\Entity
 */
class DmliMailMel
{
    /**
     * @var string
     *
     * @ORM\Column(name="mel_libelle", type="string", length=70, nullable=false)
     */
    private $melLibelle;

    /**
     * @var integer
     *
     * @ORM\Column(name="dir_id", type="integer", nullable=false)
     */
    private $dirId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="mel_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $melId;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Formulaire\dmliBundle\Entity\PosteBridge", inversedBy="mel")
     * @ORM\JoinTable(name="dmli_bdg_has_mel",
     *   joinColumns={
     *     @ORM\JoinColumn(name="mel_id", referencedColumnName="mel_id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="bdg_id", referencedColumnName="bdg_id")
     *   }
     * )
     */
    private $bdg;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->bdg = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set melLibelle
     *
     * @param string $melLibelle
     *
     * @return DmliMailMel
     */
    public function setMelLibelle($melLibelle)
    {
        $this->melLibelle = $melLibelle;
    
        return $this;
    }

    /**
     * Get melLibelle
     *
     * @return string
     */
    public function getMelLibelle()
    {
        return $this->melLibelle;
    }

    /**
     * Set dirId
     *
     * @param integer $dirId
     *
     * @return DmliMailMel
     */
    public function setDirId($dirId)
    {
        $this->dirId = $dirId;
    
        return $this;
    }

    /**
     * Get dirId
     *
     * @return integer
     */
    public function getDirId()
    {
        return $this->dirId;
    }

    /**
     * Get melId
     *
     * @return integer
     */
    public function getMelId()
    {
        return $this->melId;
    }

    /**
     * Add bdg
     *
     * @param \Formulaire\dmliBundle\Entity\PosteBridge $bdg
     *
     * @return DmliMailMel
     */
    public function addBdg(\Formulaire\dmliBundle\Entity\PosteBridge $bdg)
    {
        $this->bdg[] = $bdg;
    
        return $this;
    }

    /**
     * Remove bdg
     *
     * @param \Formulaire\dmliBundle\Entity\PosteBridge $bdg
     */
    public function removeBdg(\Formulaire\dmliBundle\Entity\PosteBridge $bdg)
    {
        $this->bdg->removeElement($bdg);
    }

    /**
     * Get bdg
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBdg()
    {
        return $this->bdg;
    }
}

