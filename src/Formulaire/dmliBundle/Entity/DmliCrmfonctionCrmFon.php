<?php

namespace Formulaire\dmliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DmliCrmfonctionCrmFon
 *
 * @ORM\Table(name="dmli_crmfonction_crm_fon")
 * @ORM\Entity
 */
class DmliCrmfonctionCrmFon
{
    /**
     * @var string
     *
     * @ORM\Column(name="crm_fon_libelle", type="string", length=70, nullable=false)
     */
    private $crmFonLibelle;

    /**
     * @var integer
     *
     * @ORM\Column(name="crm_fon_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $crmFonId;


    /**
     * Set crmFonLibelle
     *
     * @param string $crmFonLibelle
     *
     * @return DmliCrmfonctionCrmFon
     */
    public function setCrmFonLibelle($crmFonLibelle)
    {
        $this->crmFonLibelle = $crmFonLibelle;
    
        return $this;
    }

    /**
     * Get crmFonLibelle
     *
     * @return string
     */
    public function getCrmFonLibelle()
    {
        return $this->crmFonLibelle;
    }

    /**
     * Get crmFonId
     *
     * @return integer
     */
    public function getCrmFonId()
    {
        return $this->crmFonId;
    }
}

