<?php

namespace Formulaire\dmliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DmliDamacheteurDamAch
 *
 * @ORM\Table(name="dmli_damacheteur_dam_ach")
 * @ORM\Entity
 */
class DmliDamacheteurDamAch
{
    /**
     * @var string
     *
     * @ORM\Column(name="dam_ach_libelle", type="string", length=45, nullable=false)
     */
    private $damAchLibelle;

    /**
     * @var integer
     *
     * @ORM\Column(name="dam_ach_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $damAchId;


    /**
     * Set damAchLibelle
     *
     * @param string $damAchLibelle
     *
     * @return DmliDamacheteurDamAch
     */
    public function setDamAchLibelle($damAchLibelle)
    {
        $this->damAchLibelle = $damAchLibelle;
    
        return $this;
    }

    /**
     * Get damAchLibelle
     *
     * @return string
     */
    public function getDamAchLibelle()
    {
        return $this->damAchLibelle;
    }

    /**
     * Get damAchId
     *
     * @return integer
     */
    public function getDamAchId()
    {
        return $this->damAchId;
    }
}

