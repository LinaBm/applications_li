<?php

namespace Formulaire\dmliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SeDemandeUserColumns
 *
 * @ORM\Table(name="se_demande_user_columns", uniqueConstraints={@ORM\UniqueConstraint(name="user_id_UNIQUE", columns={"user_id"})})
 * @ORM\Entity
 */
class SeDemandeUserColumns
{
    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="title", type="integer", nullable=true)
     */
    private $title;

    /**
     * @var integer
     *
     * @ORM\Column(name="date", type="integer", nullable=true)
     */
    private $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="urgence", type="integer", nullable=true)
     */
    private $urgence;

    /**
     * @var integer
     *
     * @ORM\Column(name="application", type="integer", nullable=true)
     */
    private $application;

    /**
     * @var integer
     *
     * @ORM\Column(name="applicant", type="integer", nullable=true)
     */
    private $applicant;

    /**
     * @var integer
     *
     * @ORM\Column(name="departament", type="integer", nullable=true)
     */
    private $departament;

    /**
     * @var integer
     *
     * @ORM\Column(name="service", type="integer", nullable=true)
     */
    private $service;

    /**
     * @var integer
     *
     * @ORM\Column(name="observer", type="integer", nullable=true)
     */
    private $observer;

    /**
     * @var integer
     *
     * @ORM\Column(name="technician", type="integer", nullable=true)
     */
    private $technician;

    /**
     * @var integer
     *
     * @ORM\Column(name="project_manager", type="integer", nullable=true)
     */
    private $projectManager;

    /**
     * @var integer
     *
     * @ORM\Column(name="test_date", type="integer", nullable=true)
     */
    private $testDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="prod_date", type="integer", nullable=true)
     */
    private $prodDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="project_manager_hours", type="integer", nullable=true)
     */
    private $projectManagerHours;

    /**
     * @var integer
     *
     * @ORM\Column(name="technician_hours", type="integer", nullable=true)
     */
    private $technicianHours;

    /**
     * @var integer
     *
     * @ORM\Column(name="frequency", type="integer", nullable=true)
     */
    private $frequency;

    /**
     * @var integer
     *
     * @ORM\Column(name="origin", type="integer", nullable=true)
     */
    private $origin;

    /**
     * @var integer
     *
     * @ORM\Column(name="population", type="integer", nullable=true)
     */
    private $population;

    /**
     * @var integer
     *
     * @ORM\Column(name="comment_estimation", type="integer", nullable=true)
     */
    private $commentEstimation;

    /**
     * @var integer
     *
     * @ORM\Column(name="comment_test", type="integer", nullable=true)
     */
    private $commentTest;

    /**
     * @var integer
     *
     * @ORM\Column(name="comment_stand_by", type="integer", nullable=true)
     */
    private $commentStandBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return SeDemandeUserColumns
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    
        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set title
     *
     * @param integer $title
     *
     * @return SeDemandeUserColumns
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return integer
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set date
     *
     * @param integer $date
     *
     * @return SeDemandeUserColumns
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return integer
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return SeDemandeUserColumns
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return SeDemandeUserColumns
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set urgence
     *
     * @param integer $urgence
     *
     * @return SeDemandeUserColumns
     */
    public function setUrgence($urgence)
    {
        $this->urgence = $urgence;
    
        return $this;
    }

    /**
     * Get urgence
     *
     * @return integer
     */
    public function getUrgence()
    {
        return $this->urgence;
    }

    /**
     * Set application
     *
     * @param integer $application
     *
     * @return SeDemandeUserColumns
     */
    public function setApplication($application)
    {
        $this->application = $application;
    
        return $this;
    }

    /**
     * Get application
     *
     * @return integer
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set applicant
     *
     * @param integer $applicant
     *
     * @return SeDemandeUserColumns
     */
    public function setApplicant($applicant)
    {
        $this->applicant = $applicant;
    
        return $this;
    }

    /**
     * Get applicant
     *
     * @return integer
     */
    public function getApplicant()
    {
        return $this->applicant;
    }

    /**
     * Set departament
     *
     * @param integer $departament
     *
     * @return SeDemandeUserColumns
     */
    public function setDepartament($departament)
    {
        $this->departament = $departament;
    
        return $this;
    }

    /**
     * Get departament
     *
     * @return integer
     */
    public function getDepartament()
    {
        return $this->departament;
    }

    /**
     * Set service
     *
     * @param integer $service
     *
     * @return SeDemandeUserColumns
     */
    public function setService($service)
    {
        $this->service = $service;
    
        return $this;
    }

    /**
     * Get service
     *
     * @return integer
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set observer
     *
     * @param integer $observer
     *
     * @return SeDemandeUserColumns
     */
    public function setObserver($observer)
    {
        $this->observer = $observer;
    
        return $this;
    }

    /**
     * Get observer
     *
     * @return integer
     */
    public function getObserver()
    {
        return $this->observer;
    }

    /**
     * Set technician
     *
     * @param integer $technician
     *
     * @return SeDemandeUserColumns
     */
    public function setTechnician($technician)
    {
        $this->technician = $technician;
    
        return $this;
    }

    /**
     * Get technician
     *
     * @return integer
     */
    public function getTechnician()
    {
        return $this->technician;
    }

    /**
     * Set projectManager
     *
     * @param integer $projectManager
     *
     * @return SeDemandeUserColumns
     */
    public function setProjectManager($projectManager)
    {
        $this->projectManager = $projectManager;
    
        return $this;
    }

    /**
     * Get projectManager
     *
     * @return integer
     */
    public function getProjectManager()
    {
        return $this->projectManager;
    }

    /**
     * Set testDate
     *
     * @param integer $testDate
     *
     * @return SeDemandeUserColumns
     */
    public function setTestDate($testDate)
    {
        $this->testDate = $testDate;
    
        return $this;
    }

    /**
     * Get testDate
     *
     * @return integer
     */
    public function getTestDate()
    {
        return $this->testDate;
    }

    /**
     * Set prodDate
     *
     * @param integer $prodDate
     *
     * @return SeDemandeUserColumns
     */
    public function setProdDate($prodDate)
    {
        $this->prodDate = $prodDate;
    
        return $this;
    }

    /**
     * Get prodDate
     *
     * @return integer
     */
    public function getProdDate()
    {
        return $this->prodDate;
    }

    /**
     * Set projectManagerHours
     *
     * @param integer $projectManagerHours
     *
     * @return SeDemandeUserColumns
     */
    public function setProjectManagerHours($projectManagerHours)
    {
        $this->projectManagerHours = $projectManagerHours;
    
        return $this;
    }

    /**
     * Get projectManagerHours
     *
     * @return integer
     */
    public function getProjectManagerHours()
    {
        return $this->projectManagerHours;
    }

    /**
     * Set technicianHours
     *
     * @param integer $technicianHours
     *
     * @return SeDemandeUserColumns
     */
    public function setTechnicianHours($technicianHours)
    {
        $this->technicianHours = $technicianHours;
    
        return $this;
    }

    /**
     * Get technicianHours
     *
     * @return integer
     */
    public function getTechnicianHours()
    {
        return $this->technicianHours;
    }

    /**
     * Set frequency
     *
     * @param integer $frequency
     *
     * @return SeDemandeUserColumns
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;
    
        return $this;
    }

    /**
     * Get frequency
     *
     * @return integer
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * Set origin
     *
     * @param integer $origin
     *
     * @return SeDemandeUserColumns
     */
    public function setOrigin($origin)
    {
        $this->origin = $origin;
    
        return $this;
    }

    /**
     * Get origin
     *
     * @return integer
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * Set population
     *
     * @param integer $population
     *
     * @return SeDemandeUserColumns
     */
    public function setPopulation($population)
    {
        $this->population = $population;
    
        return $this;
    }

    /**
     * Get population
     *
     * @return integer
     */
    public function getPopulation()
    {
        return $this->population;
    }

    /**
     * Set commentEstimation
     *
     * @param integer $commentEstimation
     *
     * @return SeDemandeUserColumns
     */
    public function setCommentEstimation($commentEstimation)
    {
        $this->commentEstimation = $commentEstimation;
    
        return $this;
    }

    /**
     * Get commentEstimation
     *
     * @return integer
     */
    public function getCommentEstimation()
    {
        return $this->commentEstimation;
    }

    /**
     * Set commentTest
     *
     * @param integer $commentTest
     *
     * @return SeDemandeUserColumns
     */
    public function setCommentTest($commentTest)
    {
        $this->commentTest = $commentTest;
    
        return $this;
    }

    /**
     * Get commentTest
     *
     * @return integer
     */
    public function getCommentTest()
    {
        return $this->commentTest;
    }

    /**
     * Set commentStandBy
     *
     * @param integer $commentStandBy
     *
     * @return SeDemandeUserColumns
     */
    public function setCommentStandBy($commentStandBy)
    {
        $this->commentStandBy = $commentStandBy;
    
        return $this;
    }

    /**
     * Get commentStandBy
     *
     * @return integer
     */
    public function getCommentStandBy()
    {
        return $this->commentStandBy;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}

