<?php

namespace Formulaire\dmliBundle\Utils;
use Formulaire\dmliBundle\Form\FormulaireType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class BD extends Controller
{
    function insertDemande($aDemande, $em)
    {
        $demDateDemande = $aDemande[0];
        $demDateLivraison = $aDemande[1];

        $demDemandeurNom = addslashes($aDemande[2]);
        $demDemandeurPrenom= addslashes($aDemande[3]);
        $demDemandeurMail = addslashes($aDemande[4]);

        $demUtilisateurNom = addslashes($aDemande[5]);
        $demUtilisateurPrenom = addslashes($aDemande[6]);

        $demDirectionId= $aDemande[7];
        $demServiceId = $aDemande[8];
        $demPoleId = $aDemande[9];

        $demPosteId = $aDemande[10];
        $demPosteLibelle = addslashes($aDemande[11]);

        $demSocieteId = addslashes($aDemande[12]);
        $demSocieteLibelle = addslashes($aDemande[13]);

        $demSapProfil = addslashes($aDemande[14]);
        $demSapRemarque = addslashes($aDemande[15]);

        $demCrmEquId = $aDemande[16];
        $demCrmProId = $aDemande[17];
        $demRemarque = addslashes($aDemande[18]);

        $demCognosProfil = addslashes($aDemande[19]);
        $demCognosRemarque = addslashes($aDemande[20]);

        $sql = "	INSERT INTO dmli_save_demande_dem  (

			   				`dem_date_demande`,
						    `dem_date_livraison`,
						    `dem_demandeur_nom`,
						    `dem_demandeur_prenom`,
						    `dem_demandeur_mail`,
						    `dem_utilisateur_nom`,
						    `dem_utilisateur_prenom`,
						    `dir_id`,
						    `srv_id`,
						    `pol_id`,
						    `pst_id`,
						    `dem_poste_libelle`,
						    `ste_id`,
						    `dem_societe_libelle`,
						    `dem_sap_profil`,
						    `dem_sap_remarque`,
						    `crm_equ_id`,
						    `crm_pro_id`,
						    `dem_remarque`,
    						`dem_cognos_profil`,
    						`dem_cognos_remarque`
					)
					VALUES (
    						'".utf8_encode($demDateDemande)."',
							'".utf8_encode($demDateLivraison)."',
							'".utf8_encode($demDemandeurNom)."',
							'".utf8_encode($demDemandeurPrenom)."',
							'".utf8_encode($demDemandeurMail)."',
							'".utf8_encode($demUtilisateurNom)."',
							'".utf8_encode($demUtilisateurPrenom)."',
							'".utf8_encode($demDirectionId)."',
							'".utf8_encode($demServiceId)."',
							'".utf8_encode($demPoleId)."',
							'".utf8_encode($demPosteId)."',
							'".utf8_encode($demPosteLibelle)."',
							'".utf8_encode($demSocieteId)."',
							'".utf8_encode($demSocieteLibelle)."',
							'".utf8_encode($demSapProfil)."',
							'".utf8_encode($demSapRemarque)."',
							'".utf8_encode($demCrmEquId)."',
							'".utf8_encode($demCrmProId)."',
							'".utf8_encode($demRemarque)."',
							'".utf8_encode($demCognosProfil)."',
							'".utf8_encode($demCognosRemarque)."'
					);";
        $query = $em->prepare($sql);
        $query->execute();
        return true;

    }



    function getSociete($id, $em)
    {
        $result = $em->getRepository('AdministrationSocieteBundle:Societe')->findById($id);
         return ($result[0]);
     }


     function getDirection($id, $em)
     {
         $result = $em->getRepository('AdministrationDirectionBundle:Direction')->findById($id);

         return ($result[0]->getLibelle());
     }

     function getDepartment($id,$em)
     {
         $result = $em->getRepository('AdministrationDirectionBundle:Service')->findById($id);

         return ($result[0]->getLibelle());
     }

     function getDivision($id,$em)
     {
         if($id != ''){
             $result = $em->getRepository('dmliBundle:PolePol')->findBy(array('polId' => $id));
         }

         return ($result[0]->getPolLibelle());
     }

     function getPost($id,$em)
     {
         $result = $em->getRepository('dmliBundle:PostePst')->findBy(array('pstId'=>$id));

         return ($result[0]->getPstLibelle());
     }


     function getHardware($id,$em)
     {
         $sql = 	"	SELECT dmli_hardware_har.har_libelle, dmli_hardware_har.har_prix
                     FROM dmli_hardware_har
                     WHERE dmli_hardware_har.har_id = '".$id."' ;
                 ";
         // execute request


         $query = $em->prepare($sql);
         $query->execute();
         $result = $query->fetchAll();
         return ($result[0]);
     }

     function getSoftware($id,$em)
     {
         $sql = 	"	SELECT dmli_software_sft.sft_libelle, dmli_software_sft.sft_prix
                     FROM dmli_software_sft
                     WHERE dmli_software_sft.sft_id = '".$id."' ;
                 ";
         // execute request
         $query = $em->prepare($sql);
         $query->execute();
         $result = $query->fetchAll();

         return ($result[0]);
     }


     function getCrmProfil($id, $em)
     {

         $sql = 	"	SELECT dmli_crmprofil_crm_pro.crm_pro_libelle
                     FROM dmli_crmprofil_crm_pro
                     WHERE dmli_crmprofil_crm_pro.crm_pro_id = ".$id.";
                 ";
         // execute request
         $query = $em->prepare($sql);
         $query->execute();
         $result = $query->fetchAll();

         return ($result[0]['crm_pro_libelle']);
     }

     function getCrmFunction($id, $em){
         $sql = 	"	SELECT dmli_crmfonction_crm_fon.crm_fon_libelle
                     FROM dmli_crmfonction_crm_fon
                     WHERE dmli_crmfonction_crm_fon.crm_fon_id = (
                         SELECT dmli_crmprofil_crm_pro.crm_fon_id
                         FROM dmli_crmprofil_crm_pro
                         WHERE dmli_crmprofil_crm_pro.crm_pro_id = ".$id.
             "	);";
         // execute request
         $query = $em->prepare($sql);
         $query->execute();
         $result = $query->fetchAll();


         return ($result[0]['crm_fon_libelle']);
     }

     function getCrmCategorie($id,$em){
         $sql = 	"	SELECT dmli_crmcategorie_crm_cat.crm_cat_libelle
                     FROM dmli_crmcategorie_crm_cat
                     WHERE dmli_crmcategorie_crm_cat.crm_cat_id = (
                         SELECT dmli_crmprofil_crm_pro.crm_cat_id
                         FROM dmli_crmprofil_crm_pro
                         WHERE dmli_crmprofil_crm_pro.crm_pro_id = ".$id.
             "	);";
         // execute request
         $query = $em->prepare($sql);
         $query->execute();
         $result = $query->fetchAll();

         return ($result[0]['crm_cat_libelle']);
     }

     function getCrmTeam($id,$em)
     {
         $sql = 	"	SELECT dmli_crmequipe_crm_equ.crm_equ_libelle
                     FROM dmli_crmequipe_crm_equ
                     WHERE dmli_crmequipe_crm_equ.crm_equ_id = ".$id.";
                 ";
         // execute request
         $query = $em->prepare($sql);
         $query->execute();
         $result = $query->fetchAll();

         return ($result[0]['crm_equ_libelle']);
     }

    function getLastId($primaryKey, $table,$em){

        $sql = "SELECT max(".$primaryKey.") FROM ".$table.";";

        $query = $em->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();

        return ($result[0]["max(dem_id)"]);
    }

    function insertDemandeMail($melLibelle,$melId, $demId, $em)
    {
        $sql = "	INSERT INTO `dmli_save_mail` ( `sav_mel_id`, `dem_id`, `mel_libelle`, `mel_id` )
					VALUES 										(	NULL, '".$demId."',	'".$melLibelle."','".$melId."' );";
        $query = $em->prepare($sql);
        $query->execute();
    }

    function insertDemandeSoftware($sftId, $demId, $em)
    {
        $sql = "	INSERT INTO `dmli_save_sft_has_dem`  (`dem_id`, `sft_id`)
					VALUES 	(	'".$demId."', '".$sftId."' );";
        $query = $em->prepare($sql);
        $query->execute();
    }

    function insertDemandeHardware($harId, $demId, $em)
    {
        $sql = "	INSERT INTO `dmli_save_dem_has_har`  (`dem_id`, `har_id`)
					VALUES 	(	'".$demId."', '".$harId."' );";
        $query = $em->prepare($sql);
        $query->execute();
    }
     function getDamTypeAcheteur($id,$em){
         $sql = 	"	SELECT dam_ach_libelle
                     FROM dmli_damacheteur_dam_ach
                     WHERE dam_ach_id = ".$id.";
                 ";

         // execute request
         $query = $em->prepare($sql);
         $query->execute();
         $result = $query->fetchAll();

         return ($result[0]['dam_ach_libelle']);
     }

     function getDamComment($id,$em){
         $sql = 	"	SELECT dam_cmt_libelle
                     FROM dmli_damcommentaire_dam_cmt
                     WHERE dam_cmt_id = ".$id.";
                 ";

         $query = $em->prepare($sql);
         $query->execute();
         $result = $query->fetchAll();

         return ($result[0]['dam_cmt_libelle']);
     }

     function getDamClePays($id,$em){
         $sql = 	"	SELECT dam_pay_cle
                     FROM dmli_dampays_dam_pay
                     WHERE dam_pay_id = ".$id.";
                 ";
         // execute request
         $query = $em->prepare($sql);
         $query->execute();
         $result = $query->fetchAll();

         return ($result[0]['dam_pay_cle']);
     }
     /**
      * recupere le libelle du sous pole Market
      */
    function getDamSSPole($id,$em){
        $sql = 	"	SELECT dam_ssp_libelle
    				FROM dmli_damsspol_dam_ssp
    				WHERE dam_ssp_id = ".$id.";
    			";
        // execute request
        $query = $em->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();

        return ($result[0]['dam_ssp_libelle']);
    }

    function getMail($id,$em){
        $sql = 	"	SELECT mel_libelle
    				FROM dmli_mail_mel
    				WHERE mel_id = ".$id.";
    			";
        // execute request
        $query = $em->prepare($sql);
        $query->execute();
        $result = $query->fetchAll();

        return ($result[0]['mel_libelle']);
    }
}