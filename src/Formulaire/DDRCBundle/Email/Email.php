<?php

namespace Formulaire\DDRCBundle\Email;

/**
 * Description of Email
 *
 * @author Pana Cristian
 */
class Email {
    
    private $mailer;
    private $templating;
    private $uploadDir;
    private $mail_to;
    private $mail_from;
    private $mail_cc;
    private $mail_bcc;
    
    public function __construct($mailer, $templating, $uploadDir, $mail_to, $mail_from, $mail_cc, $mail_bcc)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->uploadDir = $uploadDir;
        $this->mail_to = $mail_to;
        $this->mail_from = $mail_from;
        $this->mail_cc = $mail_cc;
        $this->mail_bcc = $mail_bcc;
        
        \Swift_Preferences::getInstance()->setCacheType('null');
        
    }
    
    public function send($entity, $files, $pathWordFile)
    {
       
        $subject='Flux Facture Objet :' . $entity->getIdMagasin()->getMagId() ." ".  $entity->getIdMagasin()->getMagLibelle() ." ". $entity->getId()  .' '. $entity->getLastName() ." ". $entity->getDate()->format('dmo');
        $message = \Swift_Message::newInstance()
        ->setSubject($subject)
        ->setFrom($this->mail_from,'Remboursement Client')
        ->setTo($this->mail_to)
        ->setCc($this->mail_cc)
        ->setBcc($this->mail_bcc)
        ->setBody(
            $this->templating->render(
                'DDRCBundle:Email:form.html.twig',
                array('entity' => $entity, 'files'=> $files)
            )
        )
        ->attach(\Swift_Attachment::fromPath($this->uploadDir .'/'.$entity->getAttachement1() ))
        ->attach(\Swift_Attachment::fromPath($this->uploadDir .'/'.$entity->getAttachement2() ))
        ->attach(\Swift_Attachment::fromPath($this->uploadDir .'/'.$entity->getAttachement3() ))
        ->attach(\Swift_Attachment::fromPath($pathWordFile))
        ;
        
        if ((property_exists($entity, 'attachement4')) && (!is_null($entity->getAttachement3()))){
            $message->attach(\Swift_Attachment::fromPath($this->uploadDir .'/'.$entity->getAttachement4() ));
        }
        
        $this->mailer->send($message);

    }
    
    public function send3($entity, $files, $pathWordFile)
    {
        $subject='Flux Facture Objet :' .  $entity->getIdMagasin()->getMagId() ." ".  $entity->getIdMagasin()->getMagLibelle() ." ". $entity->getId()  .' '. $entity->getLastName() ." ". $entity->getDate()->format('dmo');
        $message = \Swift_Message::newInstance()
        ->setSubject($subject)
        ->setFrom($this->mail_from,'Remboursement Client')
        ->setTo($this->mail_to)
        ->setCc($this->mail_cc)
        ->setBcc($this->mail_bcc)
        ->setBody(
            $this->templating->render(
                'DDRCBundle:Email:form3.html.twig',
                array('entity' => $entity, 'files'=> $files)
            )
        )
        ->attach(\Swift_Attachment::fromPath($this->uploadDir .'/'.$entity->getAttachement2() ))
        ->attach(\Swift_Attachment::fromPath($pathWordFile))
        ;
        
        if(!is_null($entity->getAttachement1())){
            $message->attach(\Swift_Attachment::fromPath($this->uploadDir .'/'.$entity->getAttachement1() ));
        }
        
        if(!is_null($entity->getAttachement3())){
            $message->attach(\Swift_Attachment::fromPath($this->uploadDir .'/'.$entity->getAttachement3() ));
        }
        
        if(!is_null($entity->getAttachement4())){
            $message->attach(\Swift_Attachment::fromPath($this->uploadDir .'/'.$entity->getAttachement4() ));
        }
        $this->mailer->send($message);
    }
    
   
}
