<?php

namespace Formulaire\DDRCBundle\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

//check that at least one file was attached
class AtLeastOneValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        $counter=0;
        if (property_exists($value, 'attachement1')){
            if(!is_null($value->getAttachement1())){
                $counter++;
            }
        }
        
        if (property_exists($value, 'attachement3')){
            if(!is_null($value->getAttachement3())){
                $counter++;
            }
        }

        if ($counter < 1) {  //check if at least one file was attached
            $this->context->addViolation("S'il vous plaît joindre facture de réparation et/ou du devis payant!");
        }
    }
}
