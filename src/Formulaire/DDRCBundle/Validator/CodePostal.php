<?php
// src/AppBundle/Validator/Constraints/ContainsAlphanumeric.php
namespace Formulaire\DDRCBundle\Validator;

use Symfony\Component\Validator\Constraint;

/** @Annotation */
class CodePostal extends Constraint
{
	public $message = 'Le code Postal n\'est pas valide.';
	
	public function validatedBy()
	{
		return get_class($this).'Validator';
	}
}