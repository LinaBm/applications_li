<?php

namespace Formulaire\DDRCBundle\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;


class FilesTotalSizeValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        $size_att1=0;
        $size_att2=0;
        $size_att3=0;
        $size_att4=0;
        
        if (property_exists($value, 'attachement1')){
            if(!is_null($value->getAttachement1())){
                $size_att1= $value->getAttachement1()->getClientSize();
            }
        }
        
        if (property_exists($value, 'attachement2')){
            if(!is_null($value->getAttachement2())){
                $size_att2= $value->getAttachement2()->getClientSize();
            }
        }
        
        if (property_exists($value, 'attachement3')){
            if(!is_null($value->getAttachement3())){
                $size_att3= $value->getAttachement3()->getClientSize();
            }
        }
        
        if (property_exists($value, 'attachement4')){
            if(!is_null($value->getAttachement4())){
                $size_att4= $value->getAttachement4()->getClientSize();
            }
        }
        
        $totalSize=0;
        $totalSize=(int)$size_att1 +(int)$size_att2 +(int)$size_att3 +(int)$size_att4;

        if ($totalSize > 4194304) {  //check if total size of files is bigger than 3MB - 3145728
            $this->context->addViolation('Taille maximale pour les pièces jointes est 4 mégaoctets(Mo)!');
        }
    }
}
