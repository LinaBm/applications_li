<?php

namespace Formulaire\DDRCBundle\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

//check that at least one file was attached
class CodePostalValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
    	if (!preg_match('#^[0-9]{5}$#', $value, $matches)) {
    		// If you're using the new 2.5 validation API (you probably are!)
    		$this->context->addViolation("Le code Postal n\'est pas valide.");
        }
    }
}
