<?php

namespace Formulaire\DDRCBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class ReparationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('civilite','choice',array('label' => false, 'choices'   => array('Monsieur' => 'Monsieur', 'Madame' => 'Madame'), 'required' => true, 'attr'=> array('placeholder'=> 'Civilite')))
            ->add('firstName','text',array('label' => false, 'required' => true, 'attr'=> array('placeholder'=> 'Prénom')))
            ->add('lastName','text',array('label' => false, 'required' => true, 'attr'=> array('placeholder'=> 'Nom')))
            ->add('address','text',array('label' => false, 'required' => true, 'attr'=> array('placeholder'=> 'Adresse')))
            ->add('zipcode','text',array('label' => false, 'required' => true, 'attr'=> array('placeholder'=> 'Code Postal')))
            ->add('city','text',array('label' => false, 'required' => true, 'attr'=> array('placeholder'=> 'Ville')))
            ->add('date','date',array('widget' => 'single_text','format' => 'dd-MM-yyyy', 'label' => false, 'required' => true, 'data' => new \DateTime(), 'attr'=> array('placeholder'=> 'Date')))
            ->add('paybackAmount', 'money', array( 'currency'=>'EUR', 'label' => false, 'required' => false, 'attr' => array('style' => ' z-index: 1', 'placeholder'=> 'Montant du remboursement')))
            ->add('idMagasin','entity', array(
                'empty_value' => 'Sélectionnez un magasin',
                'label' => false,
                'required' => false,
                'class'=>'Administration\SocieteBundle\Entity\AdmMagasin', 
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.magLibelle', 'ASC');
                 },
                'property'=>'magLibelle'
            ))
            ->add('attachement1','file', array('label' => 'Facture de reparation', 'required' => false))
            ->add('attachement2','file', array('label' => 'Ticket achat', 'required' => false))
            ->add('attachement3','file', array('label' => 'Devis payant', 'required' => false))
            ->add('attachement4','file', array('label' => 'La  fiche de remboursement ', 'required' => true))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Formulaire\DDRCBundle\Entity\Reparation',
			'attr' => ['id' => 'ddrcbundle_reparation_new']
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ddrcbundle_reparation';
    }
}
