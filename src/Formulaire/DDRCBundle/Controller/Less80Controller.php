<?php

namespace Formulaire\DDRCBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Formulaire\DDRCBundle\Entity\Less80;
use Formulaire\DDRCBundle\Form\Less80Type;
use Formulaire\DDRCBundle\Email\Email;
use Symfony\Component\HttpFoundation\Response;
use Formulaire\DDRCBundle\Word\WordGenerator;

/**
 * Less80 controller.
 *
 * @Route("/less80")
 */
class Less80Controller extends Controller
{

    /**
     * Creates a new Less80 entity.
     *
     * @Route("/", name="less80_create")
     * @Method("POST")
     * @Template("DDRCBundle:Less80:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Less80();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            
            //Retrieve the service (object) that handles moving files to uploadDir and returns the file name to be saved in Database
            $attachement_service = $this->get('attachment_handling_service');           
            
            $fileName = $attachement_service->addAttachments($entity->getAttachement1(),$entity, 'Ticket achat');
            $entity->setAttachement1($fileName);
            
            $fileName = $attachement_service->addAttachments($entity->getAttachement2(),$entity, 'Ticket retour');
            $entity->setAttachement2($fileName);
            
            $fileName = $attachement_service->addAttachments($entity->getAttachement3(),$entity, 'Devis');
            $entity->setAttachement3($fileName);
            
            $fileName = $attachement_service->addAttachments($entity->getAttachement4(),$entity, 'La  fiche de remboursement ');
            $entity->setAttachement4($fileName);

            // ... persist the $entity variable or any other work
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
  
            //------- Generate word --------------
            $societe= $em->getRepository('Administration\SocieteBundle\Entity\PandoreSociete')->find($entity->getIdMagasin()->getSociete());
            $wordGenerator = $this->get('word_gen');
            $pathWordFile = $wordGenerator->generate($entity, $societe);
           
            //------- Send email------------------
            $files=array('le ticket achat','le ticket retour', 'le devis', 'la  fiche de remboursement ');
            $Email = $this->get('email_service');
            $Email->send($entity, $files, $pathWordFile);
            
            return $this->render('DDRCBundle:Default:success.html.twig');
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Less80 entity.
     *
     * @param Less80 $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Less80 $entity)
    {
        $form = $this->createForm(new Less80Type(), $entity, array(
            'action' => $this->generateUrl('less80_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Envoyer la demande'));

        return $form;
    }

    /**
     * Displays a form to create a new Less80 entity.
     *
     * @Route("/new", name="less80_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Less80();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

}
