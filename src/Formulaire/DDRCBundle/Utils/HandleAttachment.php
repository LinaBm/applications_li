<?php


namespace Formulaire\DDRCBundle\Utils;

use Formulaire\DDRCBundle\Utils\NameGenerator;

/**
 * Description of HandleAttachment
 *
 * @author pana_cr
 */
class HandleAttachment {
    
    private $uploadDir;
    private $counter;
    
    public function __construct($uploadDir)
    {
        $this->uploadDir = $uploadDir;
        $this->counter =0;
    }
    /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
    public function addAttachments(\Symfony\Component\HttpFoundation\File\UploadedFile $file, $entity, $file_type)
    {
            // Generate a unique name for the file before saving it
            $extension=$file->guessExtension();
            $fileName = NameGenerator::createNameForUploadedFile($entity, $file_type, $extension);
            // Move the file to the directory where uploads are stored
            $file->move($this->uploadDir, $fileName);

            // Update the 'Attachement' property to store the  file name
            // instead of its contents
            
            return $fileName;
    }
}
