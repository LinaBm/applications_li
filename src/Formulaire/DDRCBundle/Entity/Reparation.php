<?php

namespace Formulaire\DDRCBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Formulaire\DDRCBundle\Validator\FilesTotalSize;
use Formulaire\DDRCBundle\Validator\AtLeastOne;

/**
 * @ORM\Entity
 * @FilesTotalSize
 * @AtLeastOne
 * Reparation
 *
 * This class extends DdrcRefunds and represents a Payback with value over 80 euros
 */
class Reparation extends DdrcRefunds
{
   /**
     * @var float
     *
     * @ORM\Column(name="payback_amount", type="float", precision=10, scale=0, nullable=false)
     * @Assert\NotNull(message="Vous devez spécifier le montant du remboursement" )
     */
    protected $paybackAmount;
    
    /**
     * @var string
     *
     * @ORM\Column(name="attachement_c_1", type="text", length=65535, nullable=false)
     * @Assert\File(
     *     maxSize = "3024k",
     *     mimeTypes = {"application/pdf", "application/x-pdf", "image/jpeg", "image/pjpeg", "image/png", "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document" },
     *     mimeTypesMessage = "Fichier invalide, veuillez recommencer."
     * )
     */
    protected $attachement1;

    /**
     * @var string
     *
     * @ORM\Column(name="attachement_c_2", type="text", length=65535, nullable=true)
     * @Assert\NotNull(message="S'il vous plaît sélectionner un fichier!" )
     * @Assert\File(
     *     maxSize = "3024k",
     *     mimeTypes = {"application/pdf", "application/x-pdf", "image/jpeg", "image/pjpeg", "image/png", "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document" },
     *     mimeTypesMessage = "Fichier invalide, veuillez recommencer."
     * )
     */
    protected $attachement2;
    
    /**
     * @var string
     *
     * @ORM\Column(name="attachement_c_3", type="text", length=65535, nullable=true)
     * @Assert\File(
     *     maxSize = "3000k",
     *     mimeTypes = {"application/pdf", "application/x-pdf", "image/jpeg", "image/pjpeg", "image/png", "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document" },
     *     mimeTypesMessage = "Fichier invalide, veuillez recommencer."
     * )
     */
    protected $attachement3;
    
    /**
     * @var string
     *
     * @ORM\Column(name="attachement_c_4", type="text", length=65535, nullable=true)
     * @Assert\File(
     *     maxSize = "3000k",
     *     mimeTypes = {"application/pdf", "application/x-pdf", "image/jpeg", "image/pjpeg", "image/png", "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document" },
     *     mimeTypesMessage = "Fichier invalide, veuillez recommencer."
     * )
     */
    protected $attachement4;

    /**
     * Set paybackAmount
     *
     * @param float $paybackAmount
     *
     * @return DdrcRefunds
     */
    public function setPaybackAmount($paybackAmount)
    {
        $this->paybackAmount = $paybackAmount;

        return $this;
    }

    /**
     * Get paybackAmount
     *
     * @return float
     */
    public function getPaybackAmount()
    {
        return $this->paybackAmount;
    }
    
    /**
     * Set attachement1
     *
     * @param string $attachement1
     *
     * @return DdrcRefunds
     */
    public function setAttachement1($attachement1)
    {
        $this->attachement1 = $attachement1;

        return $this;
    }

    /**
     * Get attachement1
     *
     * @return string
     */
    public function getAttachement1()
    {
        return $this->attachement1;
    }

    /**
     * Set attachement2
     *
     * @param string $attachement2
     *
     * @return DdrcRefunds
     */
    public function setAttachement2($attachement2)
    {
        $this->attachement2 = $attachement2;

        return $this;
    }

    /**
     * Get attachement2
     *
     * @return string
     */
    public function getAttachement2()
    {
        return $this->attachement2;
    }
    
    /**
     * Set attachement3
     *
     * @param string $attachement3
     *
     * @return DdrcRefunds
     */
    public function setAttachement3($attachement3)
    {
        $this->attachement3 = $attachement3;

        return $this;
    }

    /**
     * Get attachement4
     *
     * @return string
     */
    public function getAttachement4()
    {
        return $this->attachement4;
    }
    
    /**
     * Set attachement4
     *
     * @param string $attachement4
     *
     * @return DdrcRefunds
     */
    public function setAttachement4($attachement4)
    {
        $this->attachement4 = $attachement4;

        return $this;
    }

    /**
     * Get attachement3
     *
     * @return string
     */
    public function getAttachement3()
    {
        return $this->attachement3;
    }
  
}

