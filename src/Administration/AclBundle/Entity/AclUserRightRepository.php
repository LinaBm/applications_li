<?php

namespace Administration\AclBundle\Entity;

use Doctrine\ORM\EntityRepository;

class AclUserRightRepository extends EntityRepository {

    public function findResourceByUserId($userId) {
        $user = $this->createQueryBuilder('aur')
                ->select('ar.route as route', 'aur.isAllowed')
                ->leftJoin('AclBundle:AclResource', 'ar', 'WITH', 'ar.id = aur.resourceId')
                ->where('aur.userId = :userId')
                ->setParameter('userId', $userId)
                ->getQuery()
                ->getOneOrNullResult();
        return $user;
    }

    public function findResourcesByRouteName($routeName) {
        $user = $this->createQueryBuilder('aur')
                ->select('ar.route as route', 'aur.isAllowed')
                ->leftJoin('AclBundle:AclResource', 'ar', 'WITH', 'ar.id = aur.resourceId')
                ->where('aur.userId = :userId')
                ->setParameter('userId', $userId)
                ->getQuery()
                ->getOneOrNullResult();
        return $user;
    }

}
