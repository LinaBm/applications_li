<?php

namespace Administration\AclBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Tools\Pagination\Paginator;

class UserController extends Controller {

    /**
     * @Route("/utilisateur/visualiser/{page}/{status}", name="acl_utilisateur_view", 
     * defaults={"page"=1, "status"="actif"},requirements={"page": "\d+", "status": "\w+"})
     * @Method({"GET"})
     */
    public function viewAction($page, $status) {
        $request = $this->container->get('request');
        $em = $this->getDoctrine()->getManager();
        $filters = ($status == "actif") ? 'u.enabled=1' : ($status == "desactive" ? 'u.enabled=0' : '1=1');
        $onPage = 10;
        #sorting params
        $sort = null !== $request->query->get('sort') ? $request->query->get('sort') : 'u.surname';
        $direction = null !== $request->query->get('direction') && ($request->query->get('direction') == "asc" || $request->query->get('direction') == "desc") ? $request->query->get('direction') : 'ASC';
        $data = $em->createQueryBuilder('query')
                        ->select('u')
                        ->from('UserUserBundle:User', 'u')
                        ->where($filters)
                        ->orderBy($sort, $direction)
                        ->getQuery()->getResult();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $data, $request->query->getInt('page', $page)/* page number */
                , $onPage/* limit per page */
        );
        return $this->render('AclBundle:admin:user\visualiser.html.twig', array(
                    'pagination' => $pagination,
                    'page' => $page,
                    'status' => $status,
                    'onPage' => $onPage,
        ));
    }

    /**
     * @Route("/utilisateur/droits/{userId}", name="acl_utilisateur_rights", defaults={"userId"=0})
     * @Method({"GET"})
     */
    public function rightsAction($userId) {
        if (empty($userId)) {
            return $this->redirect($this->generateUrl('acl_utilisateur_view'));
        }
        $em = $this->getDoctrine()->getManager('acl');
        $user = $this->getDoctrine()->getManager()->getRepository('UserUserBundle:User')->find($userId);
        $data = $em->createQueryBuilder('query')
                        ->select('urg.id', 'urg.userId userId', 'r.name as resourceName', 
                                'grp.name as grpName', 'grp.isActive activGroup', 'urg.isAllowed', 'r1.name as appName', 
                                'r.id as resourceId', 'r.type resourceType','r.isActive activResource')
                        ->from('AclBundle:AclUserRight', 'urg')
                        ->leftJoin('AclBundle:AclResource', 'r', 'WITH', 'r.id = urg.resourceId')
                        ->leftJoin('AclBundle:AclResource', 'r1', 'WITH', 'r1.id = r.parentId')
                        ->leftJoin('AclBundle:AclGroup', 'grp', 'WITH', 'grp.id = urg.groupId')
                        ->leftJoin('AclBundle:AclUserGroup', 'usrGrp', 'WITH', 'usrGrp.groupId = urg.groupId')
                        ->where('urg.userId = :userId OR urg.userId IS NULL')
                        ->setParameter('userId', $userId)
                        ->groupBy('urg.id')
                        ->getQuery()->getResult();
        $sort = array();
        if (!empty($data)) {
            foreach ($data as $k => $v) {
                if (!empty($v['grpUserId']) && $v['grpUserId'] != $userId) {
                    unset($data[$k]);
                }
            }
            foreach ($data as $k => $v) {
                $sort['resourceType'][$k] = $v['resourceType'];
                $sort['userId'][$k] = $v['userId'];
                $sort['grpName'][$k] = $v['grpName'];
                $sort['appName'][$k] = $v['appName'];
            }
            array_multisort($sort['appName'], SORT_ASC, $sort['userId'], SORT_DESC, $sort['grpName'], SORT_ASC, $sort['resourceType'], SORT_ASC, $data);
        }

        return $this->render('AclBundle:admin:user\droits.html.twig', array(
                    'data' => $data,
                    'user' => $user,
                    'userId' => $userId,
        ));
    }

    /**
     * @Route("/utilisateur/desactiver/{userId}", name="acl_utilisateur_deactivate", defaults={"userId"=0})
     * @Method({"POST"})
     */
    public function deactivateAction($userId) {
        $request = $this->container->get('request');
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserUserBundle:User')->find($userId);
        if (!empty($user)) {
            try {
                $user->setEnabled(0);
                $em->persist($user);
                $em->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
                $this->get('session')->getFlashBag()->add('error', 'Il y avait une erreur.');
            }
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Il y avait une erreur.');
        }
        return new Response('Terminer supprimer.');
    }

    /**
     * @Route("/utilisateur/activer/{userId}", name="acl_utilisateur_activate", defaults={"userId"=0})
     * @Method({"POST"})
     */
    public function activateAction($userId) {
        $request = $this->container->get('request');
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserUserBundle:User')->find($userId);
        if (!empty($user)) {
            try {
                $user->setEnabled(1);
                $em->persist($user);
                $em->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
                $this->get('session')->getFlashBag()->add('error', 'Il y avait une erreur.');
            }
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Il y avait une erreur.');
        }
        return new Response('Terminer supprimer.');
    }

    /**
     * @Route("/utilisateur/droit/ajouter/{userId}", name="acl_utilisateur_add_rights", defaults={"userId"=0})
     * @Method({"GET", "POST"})
     */
    public function addAction($userId) {
        $form = $this->createForm(new \Administration\AclBundle\Form\AclUserRightForm(), array('userId' => $userId));
        $request = $this->container->get('request');
        if ($request->isMethod('POST')) {
            $form->bind($request);
            if ($form->isValid()) {
                try {
                    $em = $this->getDoctrine()->getManager("acl");
                    $resourceSet = $em->getRepository('AclBundle:AclUserRight')->findBy(array(
                        'groupId' => empty($request->request->get('userRightForm')['groupId']) ? null : $request->request->get('userRightForm')['groupId'],
                        'userId' => empty($request->request->get('userRightForm')['userId']) ? null : $request->request->get('userRightForm')['userId'],
                        'resourceId' => $request->request->get('userRightForm')['resourceId'],
                    ));
                    if (!empty($resourceSet)) {
                        $this->get('session')->getFlashBag()->add('error', 'Cette ressource est déjà définie pour cet groupe.');
                        return $this->redirect($this->generateUrl('acl_utilisateur_add_rights'));
                    }
                    $resourceObj = new \Administration\AclBundle\Entity\AclUserRight();
                    $userId = empty($request->request->get('userRightForm')['userId']) ? null : $request->request->get('userRightForm')['userId'];
                    $resourceObj->setUserId($userId);
                    $resourceObj->setGroupId(empty($request->request->get('userRightForm')['groupId']) ? null : $request->request->get('userRightForm')['groupId']);
                    $resourceObj->setResourceId($request->request->get('userRightForm')['resourceId']);
                    $resourceObj->setIsAllowed($request->request->get('userRightForm')['isAllowed']);
                    $resourceObj->setCreatedAt(new \DateTime("now"));
                    $resourceObj->setCreatedBy($this->getUser()->getId());
                    $em->persist($resourceObj);

                    $userInGroup = $em->getRepository('AclBundle:AclUserGroup')->findBy(array(
                        'groupId' => $request->request->get('userRightForm')['groupId'],
                        'userId' => $request->request->get('userRightForm')['userId']
                    ));
                    // if user not in group
                    if (!empty($request->request->get('userRightForm')['userId']) && empty($userInGroup) && !empty($request->request->get('userRightForm')['groupId'])) {
                        $grpObj = new \Administration\AclBundle\Entity\AclUserGroup();
                        $grpObj->setUserId($request->request->get('userRightForm')['userId']);
                        $grpObj->setGroupId($request->request->get('userRightForm')['groupId']);
                        $grpObj->setCreatedAt(new \DateTime("now"));
                        $grpObj->setCreatedBy($this->getUser()->getId());
                        $em->persist($grpObj);
                    }
                    $em->flush();
                } catch (\Doctrine\DBAL\DBALException $e) {
                    $this->get('session')->getFlashBag()->add('error', 'Erreur sur insert.Vérifiez cet utilisateur droits.');
                    return $this->redirect($this->generateUrl('acl_utilisateur_add_rights'));
                }
                $this->get('session')->getFlashBag()->add('success', 'La ressource ' . $request->request->get('resourceForm')['resourceName'] . ' a été créée avec succès.');
                return $this->redirect($this->generateUrl('acl_utilisateur_add_rights'));
            }
        }
        return $this->render('AclBundle:admin:user\ajouter.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/utilisateur/desactiver/resource/{id}", name="acl_utilisateur_deactivate_resource", defaults={"id"=0})
     * @Method({"POST"})
     */
    public function deactivateResourceAction($id) {
        $request = $this->container->get('request');
        $em = $this->getDoctrine()->getManager("acl");
        $userRight = $em->getRepository('AclBundle:AclUserRight')->find($id);
        if (!empty($userRight)) {
            try {
                $userRight->setIsAllowed(0);
                $em->persist($userRight);
                $em->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
                $this->get('session')->getFlashBag()->add('error', 'Il y avait une erreur.');
            }
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Il y avait une erreur.');
        }
        return new Response('Terminer desactiver resource.');
    }

    /**
     * @Route("/utilisateur/activer/resource/{id}", name="acl_utilisateur_activate_resource", defaults={"id"=0})
     * @Method({"POST"})
     */
    public function activateResourceAction($id) {
        $request = $this->container->get('request');
        $em = $this->getDoctrine()->getManager("acl");
        $userRight = $em->getRepository('AclBundle:AclUserRight')->find($id);
        if (!empty($userRight)) {
            try {
                $userRight->setIsAllowed(1);
                $em->persist($userRight);
                $em->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
                $this->get('session')->getFlashBag()->add('error', 'Il y avait une erreur.');
            }
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Il y avait une erreur.');
        }
        return new Response('Terminer activer resource.');
    }

}
