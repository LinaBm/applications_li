<?php

namespace Administration\AclBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Tools\Pagination\Paginator;

class DefaultController extends Controller {

    /**
     * @Route("/", name="acl_dashboard")
     * @Method({"GET"})
     */
    public function dashboardAction() { // de sters
        return $this->render('AclBundle:admin:default\dashboard.html.twig', array(
        ));
    }

    /**
     * @Route("/home", name="acl_index")
     * @Method({"GET"})
     */
    public function indexAction() { // de sters
        $allRoutes = $this->get('acl_service')->getBundleRoutes();
        return $this->render('AclBundle:admin:resource\index.html.twig', array(
                    'appRoutes' => $allRoutes,
        ));
    }

    /**
     * @Route("/documentation", name="acl_documentation")
     * @Method({"GET"})
     */
    public function documentationAction() { // de sters
        $allRoutes = $this->get('acl_service')->getBundleRoutes();
        return $this->render('AclBundle:admin:default\documentation.html.twig', array(
                    'appRoutes' => $allRoutes,
        ));
    }

}
