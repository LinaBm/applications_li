<?php

namespace Administration\AclBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Tools\Pagination\Paginator;

class ResourceController extends Controller {

    /**
     * @Route("/ressource/ajouter", name="acl_resource_add")
     * @Method({"GET", "POST"})
     */
    public function resourceAddAction() {
        $bundles = $this->get('acl_service')->getAllRoutes();
        $form = $this->createForm(new \Administration\AclBundle\Form\AclResourceForm());
        $request = $this->container->get('request');
        if ($request->isMethod('POST')) {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager("acl");
                $name = isset($request->request->get('resourceForm')['name']) ? ucfirst($request->request->get('resourceForm')['name']) : null;
                $type = isset($request->request->get('resourceForm')['type']) ? $request->request->get('resourceForm')['type'] : null;
                $route = isset($request->request->get('resourceForm')['route']) ? $request->request->get('resourceForm')['route'] : null;
                $routeName = isset($request->request->get('resourceForm')['routeName']) ? $request->request->get('resourceForm')['routeName'] : null;
                $parentId = isset($request->request->get('resourceForm')['parentId']) && !empty($request->request->get('resourceForm')['parentId']) ? $request->request->get('resourceForm')['parentId'] : null;

                $resourceObj = new \Administration\AclBundle\Entity\AclResource();
                $resourceObj->setName($name);
                $resourceObj->setType($type);
                $resourceObj->setRoute($route);
                $resourceObj->setRouteName($routeName);
                $resourceObj->setParentId($parentId);
                $resourceObj->setCreatedAt(new \DateTime("now"));
                $resourceObj->setCreatedBy($this->getUser()->getId());
                try {
                    $em->persist($resourceObj);
                    $em->flush();
                } catch (\Doctrine\DBAL\DBALException $e) {
                    $this->get('session')->getFlashBag()->add('error', $e->getPrevious()->getMessage());
                    return $this->redirect($this->generateUrl('acl_resource_add'));
                }
                $this->get('session')->getFlashBag()->add('success', 'La ressource ' . $request->request->get('resourceForm')['resourceName'] . ' a été créée avec succès.');
                return $this->redirect($this->generateUrl('acl_resource_add'));
            } else {
                $validator = new \Administration\AclBundle\Validators\Messages();
                $formErrors = $validator->getErrorMessages($form);
                foreach ($formErrors as $name => $errors) {
                    foreach ($errors as $error) {
                        $this->get('session')->getFlashBag()->add('error', $name . ": " . $error);
                    }
                }
                return $this->redirect($this->generateUrl('acl_resource_add'));
            }
        }
        return $this->render('AclBundle:admin:resource\ajouter.html.twig', array(
                    'form' => $form->createView(),
                    'bundles' => $bundles
        ));
    }

    /**
     * @Route("/ressource/paquet", name="acl_resource_all_bundles")
     * @Method({"GET"})
     */
    public function allBundles() {
        $bundles = $this->get('acl_service')->getSetBundles();
        return $this->render('AclBundle:admin:resource\charge_paquet.html.twig', array(
                    'bundles' => $bundles,
        ));
    }

    /**
     * @Route("/ressource/ressource-nom", name="acl_resource_nom")
     * @Method({"GET"})
     */
    public function nom() {
        $routes = $this->get('acl_service')->getRoutesWithDB();
        $em = $this->getDoctrine()->getManager('acl');
        $bundles = $em->getRepository('AclBundle:AclResource')->findBy(array('type' => 1));
        return $this->render('AclBundle:admin:resource\charge_ressourceNom.html.twig', array(
                    'bundles' => $bundles,
                    'routes' => $routes,
        ));
    }

    /**
     * @Route("/ressource/ressource-trajet", name="acl_resource_trajet")
     * @Method({"GET"})
     */
    public function trajet() {
        $em = $this->getDoctrine()->getManager('acl');
        $bundles = $em->getRepository('AclBundle:AclResource')->findBy(array('type' => 1));
        return $this->render('AclBundle:admin:resource\charge_ressourceTrajet.html.twig', array(
                    'bundles' => $bundles,
        ));
    }

    /**
     * @Route("/ressource/modifier/{resourceId}", name="acl_resource_modify", defaults={"resourceId"=0})
     * @Method({"GET", "POST"})
     */
    public function resourceModifyAction($resourceId) {
        $request = $this->container->get('request');
        $em = $this->getDoctrine()->getManager("acl");
        $resource = $em->getRepository('AclBundle:AclResource')->find($resourceId);
        $form = $this->createForm(new \Administration\AclBundle\Form\ AclResourceForm(), $resource);
        $bundles = $em->getRepository('AclBundle:AclResource')->findBy(array('type' => 1));
        if (null === $resource || $resource->getIsActive() != 1) {
            $this->get('session')->getFlashBag()->add('error', 'D\'abord activer ' . ' ressource ' . $resource->getName());
            return $this->redirect($this->generateUrl('acl_resource_view', array('status' => 'tous')));
        }
        if ($request->isMethod('POST')) {
            $form->bind($request);
            if ($form->isValid()) {
                if (!empty($resource)) {
                    try {
                        $name = isset($request->request->get('resourceForm')['name']) ? $request->request->get('resourceForm')['name'] : null;
                        $type = isset($request->request->get('resourceForm')['type']) ? $request->request->get('resourceForm')['type'] : null;
                        $route = isset($request->request->get('resourceForm')['route']) ? $request->request->get('resourceForm')['route'] : null;
                        $resource->setName($name);
                        $resource->setType($type);
                        $resource->setRoute($route);
                        $resource->setUpdatedAt(new \DateTime("now"));
                        $resource->setlastUpdatedBy($this->getUser()->getId());
                        $em->flush();
                    } catch (\Doctrine\DBAL\DBALException $e) {
                        $this->get('session')->getFlashBag()->add('error', $e->getPrevious()->getMessage());
                    }
                    $this->get('session')->getFlashBag()->add('success', 'La ressource ' . $request->request->get('resourceForm')['name'] . ' a été modifier avec succès.');
                } else {
                    $this->get('session')->getFlashBag()->add('error', 'Erreur sur modifier.');
                }
                return $this->redirect($this->generateUrl('acl_resource_modify', array('resourceId' => $resourceId)));
            } else {
                $validator = new \Administration\AclBundle\Validators\Messages();
                $formErrors = $validator->getErrorMessages($form);
                foreach ($formErrors as $name => $errors) {
                    foreach ($errors as $error) {
                        $this->get('session')->getFlashBag()->add('error', $name . ": " . $error);
                    }
                }
                return $this->redirect($this->generateUrl('acl_resource_modify', array('resourceId' => $resourceId)));
            }
        }
        return $this->render('AclBundle:admin:resource\modifier.html.twig', array(
                    'form' => $form->createView(),
                    'bundles' => $bundles,
                    'resource' => $resource
        ));
    }

    /**
     * @Route("/ressource/desactiver/{resourceId}", name="acl_resource_deactivate", defaults={"resourceId"=0})
     * @Method({"POST"})
     */
    public function resourceDeactivateAction($resourceId) {
        $request = $this->container->get('request');
        $em = $this->getDoctrine()->getManager("acl");
        $resource = $em->getRepository('AclBundle:AclResource')->find($resourceId);
        if (!empty($resource)) {
            $resourceName = $resource->getName();
            try {
                $resource->setUpdatedAt(new \DateTime("now"));
                $resource->setlastUpdatedBy($this->getUser()->getId());
                $resource->setDeletedBy($this->getUser()->getId());
                $resource->setIsActive(0);
                $em->persist($resource);
                $em->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
                $this->get('session')->getFlashBag()->add('error', 'Il y avait une erreur.');
            }
            $this->get('session')->getFlashBag()->add('success', 'La ressource ' . $resourceName . ' a été désactivé avec succès.');
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Il y avait une erreur.');
        }
        return new Response('Terminer désactiver.');
    }

    /**
     * @Route("/ressource/activer/{resourceId}", name="acl_resource_activate", defaults={"resourceId"=0})
     * @Method({"POST"})
     */
    public function resourceActivateAction($resourceId) {
        $request = $this->container->get('request');
        $em = $this->getDoctrine()->getManager("acl");
        $resource = $em->getRepository('AclBundle:AclResource')->find($resourceId);
        if (!empty($resource)) {
            $resourceName = $resource->getName();
            try {
                $resource->setUpdatedAt(new \DateTime("now"));
                $resource->setDeletedAt(null);
                $resource->setlastUpdatedBy($this->getUser()->getId());
                $resource->setDeletedBy(null);
                $resource->setIsActive(1);
                $em->persist($resource);
                $em->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
                $this->get('session')->getFlashBag()->add('error', 'Il y avait une erreur.');
            }
            $this->get('session')->getFlashBag()->add('success', 'La ressource ' . $resourceName . ' a été suprimer avec succès.');
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Il y avait une erreur.');
        }
        return new Response('Activer supprimer.');
    }

    /**
     * @Route("/ressource/visualiser/{page}/{status}", name="acl_resource_view", 
     * defaults={"page"=1, "status"="actif"},requirements={"page": "\d+", "status": "\w+"})
     * @Method({"GET"})
     */
    public function resourceViewAction($page, $status) {
        $request = $this->container->get('request');
        $em = $this->getDoctrine()->getManager('acl');
        $filters = ($status == "actif") ? 'r.isActive=1' : ($status == "desactive" ? 'r.isActive=0' : '1=1');
        $onPage = 5;
        #sorting params
        $sort = null !== $request->query->get('sort') && ($request->query->get('sort') == "resourceName" || $request->query->get('sort') == "resourceType" ) ? $request->query->get('sort') : 'r.name';
        $direction = null !== $request->query->get('direction') && ($request->query->get('direction') == "asc" || $request->query->get('direction') == "desc") ? $request->query->get('direction') : 'ASC';
        $data = $em->createQueryBuilder('query')
                        ->select('r.id', 'r.name as resourceName','r.type', 'r.type as resourceType', 'r.route', 'r.isActive', 'r.deletedBy')
                        ->from('AclBundle:AclResource', 'r')
                        ->where($filters)
                        ->orderBy($sort, $direction)
                        ->getQuery()->getResult();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $data, $request->query->getInt('page', $page)/* page number */
                , $onPage/* limit per page */
        );
        return $this->render('AclBundle:admin:resource\visualiser.html.twig', array(
                    'pagination' => $pagination,
                    'page' => $page,
                    'status' => $status,
                    'onPage' => $onPage,
        ));
    }

    /**
     * @Route("/ressource/suprimer/{resourceId}", name="acl_resource_delete", defaults={"resourceId"=0})
     * @Method({"POST"})
     */
    public function resourceDeleteAction($resourceId) {
        $request = $this->container->get('request');
        $em = $this->getDoctrine()->getManager("acl");
        $resource = $em->getRepository('AclBundle:AclResource')->find($resourceId);
//        $rights = $em->getRepository('AclBundle:AclUserRight')->findBy(array('resourceId' => $resourceId));
        $rights = $em->createQuery('DELETE AclBundle:AclUserRight ur WHERE ur.resourceId= :resourceId')->setParameter('resourceId',$resourceId);
        if (!empty($resource)) {
            $resourceName = $resource->getName();
            try {
                $em->remove($resource);
                $rights->execute(); 
                $em->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
                $this->get('session')->getFlashBag()->add('error', 'Il y avait une erreur.');
            }
            $this->get('session')->getFlashBag()->add('success', 'La ressource ' . $resourceName . ' a été suprimer avec succès.');
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Ressource ne peut pas être vide.');
        }
        return new Response('Terminer supprimer.');
    }

}
