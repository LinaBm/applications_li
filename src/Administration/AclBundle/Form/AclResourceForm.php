<?php

namespace Administration\AclBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\Constraints as Assert;

class AclResourceForm extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('type', 'choice', array(
                    'label' => 'Type de ressource: ',
                    'choices' => array(
                        '0' => 'Choisissez une ressource',
                        '1' => 'Application',
                        '2' => 'Nom de la route',
                        '3' => 'Trajet  de la route',
                    ),
                    'constraints' => array(
                        new Assert\NotBlank(),
                        new Assert\Range(
                                array(
                            'min' => 1,
                            'max' => 3,
                            'minMessage' => 'Minimum est {{ limit }}',
                            'maxMessage' => 'Maximum est {{ limit }}',
                                )
                        )
                    ),)
                )
                ->add('name', 'text', array('label' => 'Nom de ressource: ',
                    'constraints' => array(
                        new Assert\NotBlank(),
                        new Assert\NotEqualTo(array('value' => 'Choisissez une paquet'))
                    ),
                ))
                ->add('routeName', 'text', array(
                    'label' => 'Nom de la route: ',
                    'required' => true,
                    'constraints' => new Assert\NotBlank(array('groups' => array('routeNameGroup'))),
                ))
                ->add('route', 'text', array(
                    'label' => 'Trajet de la route: ',
                    'required' => true,
                    'constraints' => new Assert\NotBlank(array('groups' => array('routePathGroup'))),
                ))
                ->add('parentId', 'integer', array(
                    'label' => 'Pere: ',
                    'required' => false
                ))
                ->add('submit', 'submit')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Administration\AclBundle\Entity\AclResource',
            'csrf_protection' => false,
            'attr' => ['id' => 'resourceForm'],
            'validation_groups' => function(FormInterface $form) {
        $data = $form->getData();
        if ($data->getType() == 2) {
            return array('Default','routeNameGroup');
        }else if ($data->getType() == 3) {
            return array('Default','routePathGroup');
        } else {
            return array('Default');
        }
    }
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'resourceForm';
    }

}
