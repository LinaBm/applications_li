<?php

namespace Administration\AclBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class AclUserRightForm extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    protected $userId;

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $this->userId = $options['data']['userId'];

        $builder
                ->add('userId', 'entity', array(
                    'label' => 'Nom d\'utilisateur: ',
                    'class' => 'UserUserBundle:User',
                    'query_builder' => function(EntityRepository $er) {
                        if (!empty($this->userId)) {
                            return $er->createQueryBuilder('table')
                                    ->where('table.id = :id')
                                    ->setParameter('id', $this->userId)
                                    ->orderBy('table.surname', 'ASC');
                        } else {
                            return $er->createQueryBuilder('table')
                                    ->orderBy('table.surname', 'ASC');
                        }
                    },
                    'empty_value' => empty($this->userId) ? 'Tous' : false,
                    'required' => false
                ))
                ->add('groupId', 'entity', array(
                    'label' => 'Type d\'utilisateur: ',
                    'class' => 'AclBundle:AclGroup',
                    'property' => 'name',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('table')
                                ->orderBy('table.name', 'ASC');
                    },
                    'required' => false,
                    'empty_value' => 'Tous'
                        )
                )
                ->add('resourceId', 'entity', array(
                    'label' => 'Ressource nom: ',
                    'class' => 'AclBundle:AclResource',
                    'required' => true,
                    'property' => 'name',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('table')
                                ->orderBy('table.name', 'ASC');
                    },
                        )
                )
                ->add('isAllowed', 'choice', array(
                    'label' => 'On laisse: ',
                    'choices' => array(
                        '1' => 'Oui',
                        '0' => 'Non',
                    ))
                )
                ->add('submit', 'submit', array(
                    'attr' => array('class' => 'save')
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => null,
//            'data_class' => 'Administration\AclBundle\Entity\AclUserRight'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'userRightForm';
    }

}
