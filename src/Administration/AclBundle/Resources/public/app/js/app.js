/*GLOBAL*/
//$currentUrl = window.location.href;
//$(document).ajaxSend(function (event, jqxhr, settings) {
//    settings.url = $currentUrl + settings.url;
//});

$(function () {
    $("[rel=tooltip]").tooltip({placement: 'right'});

    /*RESOURCES*/
    // required route only if type != application
    $('#resourceForm_type').on('change', function () {
        $selected = $(this).val();
        if ($selected > 1) {
            $('#resourceForm_route').removeAttr('disabled');
            $('#resourceForm_routeName').removeAttr('disabled');
        } else {
            $('#resourceForm_route').prop('disabled', 'disabled');
            $('#resourceForm_routeName').prop('disabled', 'disabled');
            $('#resourceForm_route').val('');
        }
    });
    // check required fields values for older browser that doesn't support required attr
    $('#resourceForm').submit(function (event) {
        if ($.trim($('#resourceForm_name').val()) == '') {
            alert('Nom de ressource il est nécessaire.');
            event.preventDefault();
        } else if ($('#resourceForm_type').val() > 1 && $.trim($('#resourceForm_route').val() == '')) {
            alert('La route est nécessaire.');
            event.preventDefault();
        }
    });
    $('#filterSelect').on('change', function () {
        $('#loader').show();
        $selected = $(this).val();
        $page = $(this).attr('data-page');
        $url = $(this).attr('data-url');
        window.location.href = $url + '/' + $page + '/' + $selected;
    });
    $('.xhr-delete-resource').on('click', function () {
        var confirm = window.confirm("Êtes-vous sûr voulez-vous supprimer cette ressource?");
        if (confirm == true) {
            $('#loader').show();
            $.post($(this).attr('data-href'), function (result) {
                window.location.reload();
            });
        }
    });
    $('.xhr-deactivate-resource').on('click', function () {
        var confirm = window.confirm("Êtes-vous sûr voulez-vous désactiver cette ressource?");
        if (confirm == true) {
            $('#loader').show();
            $.post($(this).attr('data-href'), function (result) {
                window.location.reload();
            });
        }
    });
    /*END RESOURCES*/

    $('.xhr-delete-role').on('click', function () {
        var confirm = window.confirm("Êtes-vous sûr voulez-vous supprimer cette role?");
        if (confirm == true) {
            $('#loader').show();
            $.post($(this).attr('data-href'), function (result) {
                window.location.reload();
            });
        }
    })

    $('.xhr-deactivate-user').on('click', function () {
        var confirm = window.confirm("Êtes-vous sûr voulez-vous désactiver cet utilisateur?");
        if (confirm == true) {
            $('#loader').show();
            $.post($(this).attr('data-href'), function (result) {
                window.location.reload();
            });
        }
    });

    $('.xhr-activate-user').on('click', function () {
        var confirm = window.confirm("Êtes-vous sûr voulez-vous activer cet utilisateur?");
        if (confirm == true) {
            $('#loader').show();
            $.post($(this).attr('data-href'), function (result) {
                window.location.reload();
            });
        }
    });
});