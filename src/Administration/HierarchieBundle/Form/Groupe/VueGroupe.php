<?php

namespace Administration\HierarchieBundle\Form\Groupe;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class VueGroupe extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('grpLibelle', 'entity', array(
                    'class' => 'HierarchieBundle:Groupe',
                    'property' => 'grpLibelle',
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('table')
                                ->where('table.grpId <> 100')
                                ->orderBy('table.grpLibelle', 'ASC');
                    },
                    'empty_value' => 'Choisir un groupe',
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Administration\HierarchieBundle\Entity\Groupe',
        ));
    }

    public function getName() {
        return '';
    }

}
