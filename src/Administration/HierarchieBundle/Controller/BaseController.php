<?php

namespace Administration\HierarchieBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BaseController extends Controller {
    /*
     * Creates a tree based on an given array of elements
     */

    protected function buildTree(array $elements, $parentId) {
        $branch = array();
        foreach ($elements as $key => $element) {
            if ($element['parent'] == $parentId) {
                // create elements for children hierarcy
                $children = $this->buildTree($elements, $element['child']);
                if ($children) {
                    $element['children'] = $children;
                }

                // adds users to branch 
                if (!empty($element['userName'])) {
                    $isGone = trim($element['gone']);
                    $gone = (!empty($isGone) && strtotime($element['gone']) < strtotime(date('Ymd'))) ? false : true;
                    $branch[$element['groupName']]['users'][$element['userName']]['gone'] = $gone;
                    $branch[$element['groupName']]['users'][$element['userName']]['userType'] = $element['userType'];
                } else {
                    $branch[$element['groupName']] = null;
                }
                // adds children to branch 
                if (!empty($element['children'])) {
                    $branch[$element['groupName']]['childs'] = $children;
                }
                unset($elements[$key]);
            }
        }
        if (!empty($branch)) {
            return $branch;
        }
    }

    /*
     * Creates a html list (ul) based on the tree list from the "buildTree" function
     */

    protected function listTree($var) {
        $out = '<li class="noListType">';
        foreach ($var as $k => $v) {
            if ($k != 'users' && $k != 'childs') {
                $out .= '<ul class="noListType"><li class="noListType">' . $k . '</li></ul>';
            }
            if (is_array($v)) {
                $out .= '<ul class="noListType">' . $this->listTree($v) . '</ul>';
            }
        }
        return $out . '</li>';
    }

    protected function listTreeUL($var) {
        $out = '<ul>';
        foreach ($var as $k => $v) {
            if ($k != 'users' && $k != 'childs' && !empty($k)) {
                if (is_array($v) && array_key_exists('userType', $v)) {
                    if ($v['userType'] == 'MANAGER') {
                        $redText = ($v['gone'] == false) ? ' class="redText"' : '';
                        $out .= ' <li><a><b' . $redText . '>' . $k . ' (' . $v['userType'] . ')</b></a></li>';
                    }
                    if ($v['userType'] == 'COLLABORATEUR') {
                        $out .= '<li class="user"><a><i>' . $k . '(' . $v['userType'] . ')</i></a></li>';
                    }
                } else {
                    if ($k != 'userType' && $k != 'gone') {
                        $out .= '<li class="dropDown"><a class="redBackground">'
                                . '<span class="glyphicon glyphicon-plus"></span>'
                                . '<span class="glyphicon glyphicon-minus" style="display:none"></span>'
                                . $k .
                                '</a></li>';
                    }
                }
            }
            if (is_array($v)) {
                $out .= '<li>' . $this->listTreeUL($v) . '</li>';
            }
        }
        if ($out !== '<ul>') {
            return $out . '</ul>';
        }
    }

    protected function createExcelList(array $elements, $name) {
        $branch = array();
        foreach ($elements as $key => $element) {
            if ($element['parent'] == $name) {
                $children = $this->createExcelList($elements, $element['child']);
                if ($children) {
                    $element['children'] = $children;
                }

                if (!empty($element['children'])) {
                    $branch[$element['child']]['childsGrp'] = $children;
                }
                $branch[$element['child']][$element['usrMatricule']] = $element['rolId'];
            }
        }
        if (!empty($branch)) {
            return $branch;
        }
    }

    protected function exportGroupsAndUsersExcel($excelList) {
        $list = array();
        foreach ($excelList as $groupName => $dimension) {
            if (is_array($dimension)) {
                foreach ($dimension as $key => $value) {
                    if ($key != 'childsGrp') {
                        $list[$groupName . '$id' . $key] = $value;
                    } else {
                        $childs = $this->exportGroupsAndUsersExcel($value);
                        foreach ($childs as $key1 => $value1) {
                            $list[$groupName . '$id' . $key . '$id' . $key1] = $value1;
                        }
                    }
                }
            }
        }
        return $list;
    }

    protected function exportGroupsExcel($excelList) {
        $list = array();
        $childs = array();
        foreach ($excelList as $groupName => $dimension) {
            if (is_array($dimension)) {
                foreach ($dimension as $key => $value) {
                    if ($key != 'childsGrp') {
                        $list[$groupName] = $groupName;
                    } else {
                        $childs = $this->exportGroupsExcel($value);
                        foreach ($childs as $key1 => $value1) {
                            $list[$groupName . '$id' . $key . '$id' . $key1] = $key1;
                        }
                    }
                }
            }
        }
        return $list;
    }

    protected function indentedTree($dataArray, $parentId) {
        $out = array();
        $newParent = null;
        foreach ($dataArray as $k => $row) {
            $i = 1;
            if ($row['gpfIdpere'] == $parentId) {
                $out[$row['gpfIdfils']] = $row['grpLibelle'];
                $newParent = $row['gpfIdfils'];
                unset($dataArray[$k]);
                $child = $this->indentedTree($dataArray, $newParent);
                if (!empty($child)) {
                    foreach ($child as $newk => $v) {
                        $out[$newk] = str_repeat('-- ', $i) . $v;
                    }
                }
                $i++;
            }
        }

        if (!empty($out)) {
            return $out;
        }
    }

    public function childArray($dataArray, $selected) {
        $childsArray = array();
        $newSelected = null;
        foreach ($dataArray as $row) {
            if ($row['gpfIdpere'] == $selected) {
                $childsArray[$row['gpfIdfils']] = $row['gpfIdfils'];
                $newSelected = $row['gpfIdfils'];
            }
            if (!empty($newSelected)) {
                $child = $this->childArray($dataArray, $newSelected);
                foreach ($child as $k => $v) {
                    $childsArray[$k] = $v;
                }
            }
        }
        return $childsArray;
    }

    public function getParentId($dataArray, $id) {
        foreach ($dataArray as $row) {
            if ($row['gpfIdfils'] == $id) {
                return $row['gpfIdpere'];
            }
        }
    }

    // delete from groupes_pere_fils_gpf all the children of an group
    public function deleteGpfgChildGroups($parent, $hieId) {
        $em = $this->getDoctrine()->getManager("hierarchie_bundle");
        // for child groups
        $gpfgChild = $em->getRepository('HierarchieBundle:GroupePereFils')->findBy(array(
            'gpfIdpere' => $parent,
            'hieId' => $hieId
        ));

        // delete from groupes_pere_fils_gpf
        $em->createQueryBuilder('query')
                ->delete('HierarchieBundle:GroupePereFils', 'urg')
                ->where('urg.hieId = ?1')
                ->andWhere('urg.gpfIdpere = ?2')
                ->setParameter(1, $hieId)
                ->setParameter(2, $parent)
                ->getQuery()->execute();
        // delete from usr_role_groupe
        $em->createQueryBuilder('query')
                ->delete('HierarchieBundle:UsrRoleGroupe', 'urg')
                ->where('urg.hieId = ?1')
                ->andWhere('urg.grpId = ?2')
                ->setParameter(1, $hieId)
                ->setParameter(2, $parent)
                ->getQuery()->execute();

        if (!empty($gpfgChild)) {
            foreach ($gpfgChild as $row) {
                if (!empty($row)) {
                    $gpfgChild = $this->deleteGpfgChildGroups($row->getGpfIdfils(), $hieId);
                }
            }
        }
        return true;
    }

}
