<?php

namespace Administration\HierarchieBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class GroupeController extends BaseController {

    /**
     * @Route("/groupe/ajouter/", name="groupe-ajouter")
     * @Method({"GET", "POST"})
     */
    public function addAction(Request $request) {
        $groupsEntity = new \Administration\HierarchieBundle\Entity\Groupe();
        $form = $this->createForm(new \Administration\HierarchieBundle\Form\Groupe\AjouterGroupe(), $groupsEntity);
        if ($request->isMethod('POST')) {
            $form->bind($request);
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager("hierarchie_bundle");
                $name = $em->getRepository('HierarchieBundle:Groupe')->findOneBy(array('grpLibelle' => trim($request->request->get('grpLibelle'))));
                if (empty($name)) {
                    $this->get('session')->getFlashBag()->add('success', 'La group a bien été ajoutée.');
                    $em = $this->getDoctrine()->getManager("hierarchie_bundle");
                    $em->persist($groupsEntity);
                    $em->flush();
                    return $this->redirect($this->generateUrl('groupe-gerer', array('groupe' => $groupsEntity->getGrpId())));
                } else {
                    $this->get('session')->getFlashBag()->add('myError', 'Ce groupe existe déjà.');
                    return $this->redirect($this->generateUrl('groupe-ajouter'));
                }
            } else {
                $validator = new \Administration\HierarchieBundle\Validators\Messages();
                $formErrors = $validator->getErrorMessages($form);
                $this->get('session')->getFlashBag()->add('error', $formErrors);
                return $this->redirect($this->generateUrl('groupe-ajouter'));
            }
        }
        return $this->render('@hierarchie/groupe/ajouter.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/groupe/modifier-groupe-nom/{id}", name="groupe-modifier",
     * defaults={"id"=null},requirements={"id": "\d+"}))
     * @Method({"GET"})
     */
    public function viewAllGroupsAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager("hierarchie_bundle");
        $allGroups = $em->getRepository('HierarchieBundle:Groupe')->findBy(array(), array('grpLibelle' => 'ASC'));
        if ($request->isXmlHttpRequest() && $id) {// IF REQUEST IS AJAX 
            $grpSurHie = null;
            $groups = $em->getRepository('HierarchieBundle:GroupePereFils')->getGroupToModify($id);
            $group = $em->getRepository('HierarchieBundle:Groupe')->find($id);
            return $this->render('@hierarchie/groupe/modifier_groupe_nom.html.twig', array(
                        'groups' => $groups,
                        'group' => $group,
                        'groupId' => $id,
            ));
        } else {
            return $this->render('@hierarchie/groupe/tous_les_groupes.html.twig', array(
                        'groups' => $allGroups,
            ));
        }
    }

    /**
     * @Route("/groupe/groupe-sur-hieId", name="groupe-sur-hieId")
     * @Method({"POST"})
     */
    public function getGroupeNameToModify(Request $request) {
        $em = $this->getDoctrine()->getManager("hierarchie_bundle");
        $grpId = $request->get('grpId');
        $hieId = $request->get('hieId');
        $grpSurHie = $em->getRepository('HierarchieBundle:GroupeSurHierarchie')->findOneBy(array('grpId' => $grpId, 'hieId' => $hieId));
        if ($grpSurHie) {
            return new Response($grpSurHie->getGrpLibelle());
        }
        return new Response('');
    }

    /**
     * @Route("/groupe/modifier-nom", name="groupe-modifier-nom")
     * @Method({"POST"})
     */
    public function modifierNomAction(Request $request) {
        $em = $this->getDoctrine()->getManager("hierarchie_bundle");
        $grpId = $request->get('grpId');
        $grpName = $request->get('grpName');
        $group = $em->getRepository('HierarchieBundle:Groupe')->find($grpId);
        $grpIn = $em->getRepository('HierarchieBundle:Groupe')->findOneBy(array('grpLibelle' => trim($grpName))); // group name exists
        if ($grpIn) {
            $response = array(
                'message' => 'Ce groupe existe déjà.',
                'type' => 'danger'
            );
        }
        if ($group && $grpName && null == $grpIn) {
            $oldGrpName = $group->getGrpLibelle();
            $group->setGrpLibelle($grpName);
            $em->persist($group);
            try {
                $em->flush();
                $response = array(
                    'message' => 'Le nom du groupe"' . $oldGrpName . '" a été modifié par "' . $grpName . '".',
                    'type' => 'success'
                );
            } catch (\Doctrine\DBAL\DBALException $e) {
                $response = array(
                    'message' => 'Il y avait une erreur.',
                    'type' => 'danger'
                );
            }
        } else {
            $this->get('session')->getFlashBag()->add('myError', $response);
        }
        return new Response(json_encode($response));
    }

    /**
     * @Route("/groupe/gerer/", name="groupe-gerer")
     * @Method({"GET"})
     */
    public function manageAction() {
        $request = $this->getRequest();
        $groupsEntity = new \Administration\HierarchieBundle\Entity\Groupe();
        $form = $this->createForm(new \Administration\HierarchieBundle\Form\Groupe\VueGroupe(), $groupsEntity);
        return $this->render('@hierarchie/groupe/modifier.html.twig', array(
                    'form' => $form->createView(),
                    'selectedGrp' => $request->query->get('groupe'),
        ));
    }

    /**
     * @Route("/groupe/ajouter-groupe")
     * @Method({"POST"})
     */
    public function addGroupAction() {
        $em = $this->getDoctrine()->getManager("hierarchie_bundle");
        $request = $this->getRequest();
        if ($request->request->get('action') == 'modifyLink') {
            $data = $em->getRepository('HierarchieBundle:Hierarchie')->getHieForGroupModifyGroup($request->request->get('grpId'));
            if (empty($data)) {
                return new Response('<div class="well"><p>Cette groupe ne est pas contenue dans aucune hiérarchie.</p></div>');
            }
            return $this->render('@hierarchie/groupe/modifier_groupe.html.twig', array(
                        'list' => $data
            ));
        } else if ($request->request->get('action') == 'deleteLink') {
            $data = $em->getRepository('HierarchieBundle:Hierarchie')->getHieForGroupModifyGroup($request->request->get('grpId'));
            if (empty($data)) {
                return new Response('<div class="well"><p>Cette groupe ne est pas contenue dans aucune hiérarchie.</p></div>');
            }
            return $this->render('@hierarchie/groupe/supprimer_groupe.html.twig', array(
                        'list' => $data
            ));
        } else {
            $data = $em->getRepository('HierarchieBundle:Hierarchie')->getHieForGroupAddGroup($request->request->get('grpId'));
            return $this->render('@hierarchie/groupe/ajouter_groupe.html.twig', array(
                        'list' => $data
            ));
        }
    }

    /**
     * @Route("/groupe/groupe/{hieId}/{grpId}")
     * @Method({"GET"})
     */
    public function getGroupToAddAction($hieId, $grpId) {
        $em = $this->getDoctrine()->getManager("hierarchie_bundle");
        $data = $em->getRepository('HierarchieBundle:Hierarchie')->getGroupsForHierarchy($hieId);
        $tree = '';

        foreach ($data as $i => $row) {
            if ($row['gpfIdpere'] == 0) {
                $first = $row['gpfIdpere'];
            }
        }
        if (!empty($data)) {
            $tree = $this->indentedTree($data, $first);
        }
        return $this->render('@hierarchie/groupe/groupe_pere.html.twig', array(
                    'list' => $tree,
        ));
    }

    /**
     * @Route("/groupe/modifier/{grpId}/{hieId}")
     * @Method({"GET"})
     */
    public function getGroupToModify($grpId, $hieId) {
        $em = $this->getDoctrine()->getManager("hierarchie_bundle");
        $data = $em->getRepository('HierarchieBundle:Hierarchie')->getGroupsForHierarchy($hieId);

        foreach ($data as $i => $row) {
            if ($row['gpfIdpere'] == 0) {
                $first = $row['gpfIdpere'];
            }
        }
        if (!empty($data)) {
            $tree = $this->indentedTree($data, $first);
        }

        $isChild = $this->childArray($data, $grpId);
        $parent = $this->getParentId($data, $grpId);

        return $this->render('@hierarchie/groupe/groupe_pere_desactive.html.twig', array(
                    'list' => $tree,
                    'selected' => $grpId,
                    'isChild' => $isChild,
                    'parent' => $parent,
        ));
    }

    /**
     * @Route("/groupe/supprimer-groupe/{hieId}/{grpId}")
     * @Method({"GET"})
     */
    public function getGroupToDeleteAction($hieId, $grpId) {
        $em = $this->getDoctrine()->getManager("hierarchie_bundle");
        $data = $em->getRepository('HierarchieBundle:Hierarchie')->getGroupParent($hieId, $grpId);

        return new Response('<kbd>' . $data . '</kbd>');
    }

    /**
     * @Route("/groupe/modifier-groupe", name="groupe-modifier-groupe")
     * @Method({"POST"})
     */
    public function modifyAction() {
        $em = $this->getDoctrine()->getManager("hierarchie_bundle");
        $request = $this->getRequest();
        $action = $request->request->get('action');
        $row = $em->getRepository('HierarchieBundle:GroupePereFils')->findOneBy(
                array(
                    'gpfIdfils' => $request->request->get('groupChild'),
                    'hieId' => $request->request->get('groupHierarchy'),
                )
        );

        if ($action == 'add') {
            $hieIn = $em->getRepository('HierarchieBundle:Hierarchie')->findOneBy(array('hieId' => $request->request->get('groupHierarchy')));
            $grpIn = $em->getRepository('HierarchieBundle:Groupe')->findOneBy(array('grpId' => $request->request->get('groupChild')));

            if (!empty($row)) {
                $this->get('session')->getFlashBag()->add('myError', 'Le groupe est deja dans cette hiérarchie.');
                return $this->redirect($this->generateUrl('groupe-gerer'));
            }
            if (!empty($hieIn) && !empty($grpIn)) {
                try {
                    $gpfg = new \Administration\HierarchieBundle\Entity\GroupePereFils();
                    $gpfg->setGpfIdfils($request->request->get('groupChild'));
                    $gpfg->setGpfIdpere($request->request->get('groupParent'));
                    $gpfg->setHieId($request->request->get('groupHierarchy'));
                    $em->persist($gpfg);
                    $em->flush();
                    $this->get('session')->getFlashBag()->add('success', 'La group ' . $grpIn->getGrpLibelle() . ' a bien été ajoutée à la Hiérarchie ' . $hieIn->getHieLibelle() . '.');
                } catch (\Doctrine\DBAL\DBALException $e) {
                    $this->get('session')->getFlashBag()->add('myError', 'Le groupe est deja dans cette hiérarchie.');
                }
            } else {
                $this->get('session')->getFlashBag()->add('myError', 'Le groupe ou la hierarchié n\'existe pas.');
            }
        }
        if ($action == 'modify') {
            if (!empty($row)) {
                $row->setGpfIdpere($request->request->get('groupParent'));
                $em->flush();
                $childGroup = $em->getRepository('HierarchieBundle:Groupe')->findOneBy(array('grpId' => $request->request->get('groupChild')));
                $hierarchy = $em->getRepository('HierarchieBundle:Hierarchie')->findOneBy(array('hieId' => $request->request->get('groupHierarchy')));
                $this->get('session')->getFlashBag()->add('success', 'Le groupe ' . $childGroup->getGrpLibelle() . ' a bien été modifié dans la Hiérarchie ' . $hierarchy->getHieLibelle() . '.');
            } else {
                $this->get('session')->getFlashBag()->add('myError', 'La modification a ecoueé.');
            }
        }
        if ($action == 'delete') {
            if (!empty($row)) {
                // delete all child groups
                $deleteCpfgChilds = $this->deleteGpfgChildGroups($row->getGpfIdfils(), $request->request->get('groupHierarchy'));
                $childGroup = $em->getRepository('HierarchieBundle:Groupe')->findOneBy(array('grpId' => $request->request->get('groupChild')));
                $hierarchy = $em->getRepository('HierarchieBundle:Hierarchie')->findOneBy(array('hieId' => $request->request->get('groupHierarchy')));
                // delete this group
                $em->remove($row);
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', 'Le groupe ' . $childGroup->getGrpLibelle() . ' a bien été supprimé dans la Hiérarchie ' . $hierarchy->getHieLibelle() . '.');
            } else {
                $this->get('session')->getFlashBag()->add('myError', 'La suppresion a ecoueé.');
            }
        }
        return $this->redirect($this->generateUrl('groupe-gerer'));
    }

}
