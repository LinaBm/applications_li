<?php

namespace Administration\HierarchieBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UsrRoleGroupe
 *
 * @ORM\Table(name="usr_role_groupe", indexes={@ORM\Index(name="fk_usr_role_groupe_roles_rol1_idx", columns={"rol_id"}), @ORM\Index(name="fk_usr_role_groupe_groupes_grp1_idx", columns={"grp_id"}), @ORM\Index(name="fk_usr_role_groupe_utilisateurs_usr1_idx", columns={"usr_matricule"})})
 * @ORM\Entity
 */
class UsrRoleGroupe
{
    /**
     * @var string
     *
     * @ORM\Column(name="usr_matricule", type="string", length=6)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $usrMatricule;

    /**
     * @var integer
     *
     * @ORM\Column(name="rol_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $rolId;

    /**
     * @var integer
     *
     * @ORM\Column(name="grp_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $grpId;

    /**
     * @var integer
     *
     * @ORM\Column(name="hie_id", type="integer", nullable=false)
     */
    private $hieId;



    /**
     * Set usrMatricule
     *
     * @param string $usrMatricule
     * @return UsrRoleGroupe
     */
    public function setUsrMatricule($usrMatricule)
    {
        $this->usrMatricule = $usrMatricule;

        return $this;
    }

    /**
     * Get usrMatricule
     *
     * @return string 
     */
    public function getUsrMatricule()
    {
        return $this->usrMatricule;
    }

    /**
     * Set rolId
     *
     * @param integer $rolId
     * @return UsrRoleGroupe
     */
    public function setRolId($rolId)
    {
        $this->rolId = $rolId;

        return $this;
    }

    /**
     * Get rolId
     *
     * @return integer 
     */
    public function getRolId()
    {
        return $this->rolId;
    }

    /**
     * Set grpId
     *
     * @param integer $grpId
     * @return UsrRoleGroupe
     */
    public function setGrpId($grpId)
    {
        $this->grpId = $grpId;

        return $this;
    }

    /**
     * Get grpId
     *
     * @return integer 
     */
    public function getGrpId()
    {
        return $this->grpId;
    }

    /**
     * Set hieId
     *
     * @param integer $hieId
     * @return UsrRoleGroupe
     */
    public function setHieId($hieId)
    {
        $this->hieId = $hieId;

        return $this;
    }

    /**
     * Get hieId
     *
     * @return integer 
     */
    public function getHieId()
    {
        return $this->hieId;
    }
}
