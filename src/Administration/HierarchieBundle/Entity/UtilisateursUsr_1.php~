<?php

namespace HierarchyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UtilisateursUsr
 *
 * @ORM\Table(name="utilisateurs_usr", uniqueConstraints={@ORM\UniqueConstraint(name="usr_matricule", columns={"usr_matricule"})})
 * @ORM\Entity
 */
class UtilisateursUsr
{
    /**
     * @var string
     *
     * @ORM\Column(name="usr_matricule", type="string", length=6)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $usrMatricule;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_nom", type="string", length=45, nullable=false)
     */
    private $usrNom;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_prenom", type="string", length=45, nullable=false)
     */
    private $usrPrenom;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_email", type="string", length=45, nullable=true)
     */
    private $usrEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_login", type="string", length=45, nullable=false)
     */
    private $usrLogin;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_emailnoz", type="string", length=45, nullable=false)
     */
    private $usrEmailnoz;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_societe", type="string", length=6, nullable=false)
     */
    private $usrSociete;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_numvoie", type="string", length=10, nullable=false)
     */
    private $usrNumvoie;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_adresse", type="string", length=45, nullable=false)
     */
    private $usrAdresse;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_codepostal", type="string", length=5, nullable=false)
     */
    private $usrCodepostal;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_ville", type="string", length=45, nullable=false)
     */
    private $usrVille;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_ddn", type="string", length=45, nullable=false)
     */
    private $usrDdn;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_nationalite", type="string", length=45, nullable=false)
     */
    private $usrNationalite;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_telephone_fixe", type="string", length=45, nullable=false)
     */
    private $usrTelephoneFixe;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_telephone_port", type="string", length=45, nullable=true)
     */
    private $usrTelephonePort;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_situation_familiale", type="string", length=45, nullable=true)
     */
    private $usrSituationFamiliale;

    /**
     * @var integer
     *
     * @ORM\Column(name="usr_nb_enfants", type="integer", nullable=true)
     */
    private $usrNbEnfants;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_type_contrat", type="string", length=45, nullable=false)
     */
    private $usrTypeContrat;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_horaire", type="string", length=45, nullable=false)
     */
    private $usrHoraire;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_categ_sociopro", type="string", length=45, nullable=false)
     */
    private $usrCategSociopro;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_remuneration", type="string", length=45, nullable=false)
     */
    private $usrRemuneration;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_poste", type="string", length=100, nullable=false)
     */
    private $usrPoste;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_service", type="string", length=200, nullable=false)
     */
    private $usrService;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_date_anciennete", type="string", length=45, nullable=false)
     */
    private $usrDateAnciennete;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_date_entree", type="string", length=45, nullable=false)
     */
    private $usrDateEntree;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_date_sortie", type="string", length=45, nullable=true)
     */
    private $usrDateSortie;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_code_sap", type="string", length=6, nullable=true)
     */
    private $usrCodeSap;

    /**
     * @var boolean
     *
     * @ORM\Column(name="usr_2014", type="boolean", nullable=true)
     */
    private $usr2014;

    /**
     * @var boolean
     *
     * @ORM\Column(name="usr_mail_status_2015", type="boolean", nullable=false)
     */
    private $usrMailStatus2015;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_status", type="string", length=50, nullable=true)
     */
    private $usrStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_commentaires", type="text", length=65535, nullable=true)
     */
    private $usrCommentaires;

    /**
     * @var boolean
     *
     * @ORM\Column(name="usr_checked", type="boolean", nullable=true)
     */
    private $usrChecked;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_demandee", type="date", nullable=false)
     */
    private $dateDemandee;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_accepte", type="date", nullable=false)
     */
    private $dateAccepte;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_collab", type="date", nullable=false)
     */
    private $dateCollab;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_manager", type="date", nullable=false)
     */
    private $dateManager;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_valider", type="date", nullable=false)
     */
    private $dateValider;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_comentaires", type="string", length=255, nullable=true)
     */
    private $usrComentaires;


}
