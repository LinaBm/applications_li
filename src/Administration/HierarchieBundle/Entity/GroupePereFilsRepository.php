<?php

namespace Administration\HierarchieBundle\Entity;

use Doctrine\ORM\EntityRepository;

class GroupePereFilsRepository extends EntityRepository {

    public function getGroupToModify($id) {
        $data = $this->createQueryBuilder('gpfg')
                        ->select('grp.grpLibelle', 'hie.hieLibelle', 'gpfg.gpfIdpere','hie.hieId')
                        ->leftJoin('HierarchieBundle:Groupe', 'grp', 'WITH', 'grp.grpId = gpfg.gpfIdfils')
                        ->leftJoin('HierarchieBundle:Hierarchie', 'hie', 'WITH', 'hie.hieId = gpfg.hieId')
                        ->where('grp.grpId = :id')
                        ->setParameter('id', $id)
                        ->groupBy('gpfg.hieId')
                        ->orderBy('hie.hieLibelle')
                        ->getQuery()->getResult();
        return $data;
    }

}
