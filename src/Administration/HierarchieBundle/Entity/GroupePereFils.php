<?php

namespace Administration\HierarchieBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GroupePereFils
 *
 * @ORM\Table(name="groupes_pere_fils_gpf", uniqueConstraints={@ORM\UniqueConstraint(name="id", columns={"id"})})
 * @ORM\Entity(repositoryClass="Administration\HierarchieBundle\Entity\GroupePereFilsRepository")
 */
class GroupePereFils {

    /**
     * @var integer
     *
     * @ORM\Column(name="gpf_idpere", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $gpfIdpere;

    /**
     * @var integer
     *
     * @ORM\Column(name="gpf_idfils", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $gpfIdfils;

    /**
     * @var integer
     *
     * @ORM\Column(name="hie_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $hieId;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     */
    private $id;
    
    /**
     * Set gpfIdpere
     *
     * @param integer $gpfIdpere
     * @return GroupePereFils
     */
    public function setGpfIdpere($gpfIdpere) {
        $this->gpfIdpere = $gpfIdpere;

        return $this;
    }

    /**
     * Get gpfIdpere
     *
     * @return integer 
     */
    public function getGpfIdpere() {
        return $this->gpfIdpere;
    }

    /**
     * Set gpfIdfils
     *
     * @param integer $gpfIdfils
     * @return GroupePereFils
     */
    public function setGpfIdfils($gpfIdfils) {
        $this->gpfIdfils = $gpfIdfils;

        return $this;
    }

    /**
     * Get gpfIdfils
     *
     * @return integer 
     */
    public function getGpfIdfils() {
        return $this->gpfIdfils;
    }

    /**
     * Set hieId
     *
     * @param integer $hieId
     * @return GroupePereFils
     */
    public function setHieId($hieId) {
        $this->hieId = $hieId;

        return $this;
    }

    /**
     * Get hieId
     *
     * @return integer 
     */
    public function getHieId() {
        return $this->hieId;
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return GroupePereFils
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }
    
}
