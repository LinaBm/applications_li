<?php

namespace Administration\HierarchieBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Utilisateur
 *
 * @ORM\Table(name="utilisateurs_usr", uniqueConstraints={@ORM\UniqueConstraint(name="usr_matricule", columns={"usr_matricule"})})
 * @ORM\Entity(repositoryClass="Administration\HierarchieBundle\Entity\UtilisateurRepository")
 */
class Utilisateur {

    /**
     * @var string
     *
     * @ORM\Column(name="usr_matricule", type="string", length=6)
     * @ORM\Id
     */
    private $usrMatricule;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_nom", type="string", length=45, nullable=false)
     */
    private $usrNom;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_prenom", type="string", length=45, nullable=false)
     */
    private $usrPrenom;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_email", type="string", length=45, nullable=true)
     */
    private $usrEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_login", type="string", length=45, nullable=false)
     */
    private $usrLogin;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_emailnoz", type="string", length=45, nullable=false)
     */
    private $usrEmailnoz;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_societe", type="string", length=6, nullable=false)
     */
    private $usrSociete;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_numvoie", type="string", length=10, nullable=false)
     */
    private $usrNumvoie;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_adresse", type="string", length=45, nullable=false)
     */
    private $usrAdresse;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_codepostal", type="string", length=5, nullable=false)
     */
    private $usrCodepostal;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_ville", type="string", length=45, nullable=false)
     */
    private $usrVille;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_ddn", type="string", length=45, nullable=false)
     */
    private $usrDdn;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_nationalite", type="string", length=45, nullable=false)
     */
    private $usrNationalite;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_telephone_fixe", type="string", length=45, nullable=false)
     */
    private $usrTelephoneFixe;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_telephone_port", type="string", length=45, nullable=true)
     */
    private $usrTelephonePort;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_situation_familiale", type="string", length=45, nullable=true)
     */
    private $usrSituationFamiliale;

    /**
     * @var integer
     *
     * @ORM\Column(name="usr_nb_enfants", type="integer", nullable=true)
     */
    private $usrNbEnfants;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_type_contrat", type="string", length=45, nullable=false)
     */
    private $usrTypeContrat;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_horaire", type="string", length=45, nullable=false)
     */
    private $usrHoraire;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_categ_sociopro", type="string", length=45, nullable=false)
     */
    private $usrCategSociopro;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_remuneration", type="string", length=45, nullable=false)
     */
    private $usrRemuneration;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_poste", type="string", length=100, nullable=false)
     */
    private $usrPoste;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_service", type="string", length=200, nullable=false)
     */
    private $usrService;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_date_anciennete", type="string", length=45, nullable=false)
     */
    private $usrDateAnciennete;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_date_entree", type="string", length=45, nullable=false)
     */
    private $usrDateEntree;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_date_sortie", type="string", length=45, nullable=true)
     */
    private $usrDateSortie;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_code_sap", type="string", length=6, nullable=true)
     */
    private $usrCodeSap;

    /**
     * @var boolean
     *
     * @ORM\Column(name="usr_2014", type="boolean", nullable=true)
     */
    private $usr2014;

    /**
     * @var boolean
     *
     * @ORM\Column(name="usr_mail_status_2015", type="boolean", nullable=false)
     */
    private $usrMailStatus2015;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_status", type="string", length=50, nullable=true)
     */
    private $usrStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_commentaires", type="text", length=65535, nullable=true)
     */
    private $usrCommentaires;

    /**
     * @var boolean
     *
     * @ORM\Column(name="usr_checked", type="boolean", nullable=true)
     */
    private $usrChecked;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_demandee", type="date", nullable=false)
     */
    private $dateDemandee;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_accepte", type="date", nullable=false)
     */
    private $dateAccepte;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_collab", type="date", nullable=false)
     */
    private $dateCollab;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_manager", type="date", nullable=false)
     */
    private $dateManager;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_valider", type="date", nullable=false)
     */
    private $dateValider;

    /**
     * @var string
     *
     * @ORM\Column(name="usr_comentaires", type="string", length=255, nullable=true)
     */
    private $usrComentaires;

    /**
     * Set usrMatricule
     *
     * @param string $usrMatricule
     * @return Utilisateur
     */
    public function setUsrMatricule($usrMatricule) {
        $this->usrMatricule = $usrMatricule;

        return $this;
    }

    /**
     * Get usrMatricule
     *
     * @return string 
     */
    public function getUsrMatricule() {
        return $this->usrMatricule;
    }

    /**
     * Set usrNom
     *
     * @param string $usrNom
     * @return Utilisateur
     */
    public function setUsrNom($usrNom) {
        $this->usrNom = $usrNom;

        return $this;
    }

    /**
     * Get usrNom
     *
     * @return string 
     */
    public function getUsrNom() {
        return $this->usrNom;
    }

    /**
     * Set usrPrenom
     *
     * @param string $usrPrenom
     * @return Utilisateur
     */
    public function setUsrPrenom($usrPrenom) {
        $this->usrPrenom = $usrPrenom;

        return $this;
    }

    /**
     * Get usrPrenom
     *
     * @return string 
     */
    public function getUsrPrenom() {
        return $this->usrPrenom;
    }

    /**
     * Set usrEmail
     *
     * @param string $usrEmail
     * @return Utilisateur
     */
    public function setUsrEmail($usrEmail) {
        $this->usrEmail = $usrEmail;

        return $this;
    }

    /**
     * Get usrEmail
     *
     * @return string 
     */
    public function getUsrEmail() {
        return $this->usrEmail;
    }

    /**
     * Set usrLogin
     *
     * @param string $usrLogin
     * @return Utilisateur
     */
    public function setUsrLogin($usrLogin) {
        $this->usrLogin = $usrLogin;

        return $this;
    }

    /**
     * Get usrLogin
     *
     * @return string 
     */
    public function getUsrLogin() {
        return $this->usrLogin;
    }

    /**
     * Set usrEmailnoz
     *
     * @param string $usrEmailnoz
     * @return Utilisateur
     */
    public function setUsrEmailnoz($usrEmailnoz) {
        $this->usrEmailnoz = $usrEmailnoz;

        return $this;
    }

    /**
     * Get usrEmailnoz
     *
     * @return string 
     */
    public function getUsrEmailnoz() {
        return $this->usrEmailnoz;
    }

    /**
     * Set usrSociete
     *
     * @param string $usrSociete
     * @return Utilisateur
     */
    public function setUsrSociete($usrSociete) {
        $this->usrSociete = $usrSociete;

        return $this;
    }

    /**
     * Get usrSociete
     *
     * @return string 
     */
    public function getUsrSociete() {
        return $this->usrSociete;
    }

    /**
     * Set usrNumvoie
     *
     * @param string $usrNumvoie
     * @return Utilisateur
     */
    public function setUsrNumvoie($usrNumvoie) {
        $this->usrNumvoie = $usrNumvoie;

        return $this;
    }

    /**
     * Get usrNumvoie
     *
     * @return string 
     */
    public function getUsrNumvoie() {
        return $this->usrNumvoie;
    }

    /**
     * Set usrAdresse
     *
     * @param string $usrAdresse
     * @return Utilisateur
     */
    public function setUsrAdresse($usrAdresse) {
        $this->usrAdresse = $usrAdresse;

        return $this;
    }

    /**
     * Get usrAdresse
     *
     * @return string 
     */
    public function getUsrAdresse() {
        return $this->usrAdresse;
    }

    /**
     * Set usrCodepostal
     *
     * @param string $usrCodepostal
     * @return Utilisateur
     */
    public function setUsrCodepostal($usrCodepostal) {
        $this->usrCodepostal = $usrCodepostal;

        return $this;
    }

    /**
     * Get usrCodepostal
     *
     * @return string 
     */
    public function getUsrCodepostal() {
        return $this->usrCodepostal;
    }

    /**
     * Set usrVille
     *
     * @param string $usrVille
     * @return Utilisateur
     */
    public function setUsrVille($usrVille) {
        $this->usrVille = $usrVille;

        return $this;
    }

    /**
     * Get usrVille
     *
     * @return string 
     */
    public function getUsrVille() {
        return $this->usrVille;
    }

    /**
     * Set usrDdn
     *
     * @param string $usrDdn
     * @return Utilisateur
     */
    public function setUsrDdn($usrDdn) {
        $this->usrDdn = $usrDdn;

        return $this;
    }

    /**
     * Get usrDdn
     *
     * @return string 
     */
    public function getUsrDdn() {
        return $this->usrDdn;
    }

    /**
     * Set usrNationalite
     *
     * @param string $usrNationalite
     * @return Utilisateur
     */
    public function setUsrNationalite($usrNationalite) {
        $this->usrNationalite = $usrNationalite;

        return $this;
    }

    /**
     * Get usrNationalite
     *
     * @return string 
     */
    public function getUsrNationalite() {
        return $this->usrNationalite;
    }

    /**
     * Set usrTelephoneFixe
     *
     * @param string $usrTelephoneFixe
     * @return Utilisateur
     */
    public function setUsrTelephoneFixe($usrTelephoneFixe) {
        $this->usrTelephoneFixe = $usrTelephoneFixe;

        return $this;
    }

    /**
     * Get usrTelephoneFixe
     *
     * @return string 
     */
    public function getUsrTelephoneFixe() {
        return $this->usrTelephoneFixe;
    }

    /**
     * Set usrTelephonePort
     *
     * @param string $usrTelephonePort
     * @return Utilisateur
     */
    public function setUsrTelephonePort($usrTelephonePort) {
        $this->usrTelephonePort = $usrTelephonePort;

        return $this;
    }

    /**
     * Get usrTelephonePort
     *
     * @return string 
     */
    public function getUsrTelephonePort() {
        return $this->usrTelephonePort;
    }

    /**
     * Set usrSituationFamiliale
     *
     * @param string $usrSituationFamiliale
     * @return Utilisateur
     */
    public function setUsrSituationFamiliale($usrSituationFamiliale) {
        $this->usrSituationFamiliale = $usrSituationFamiliale;

        return $this;
    }

    /**
     * Get usrSituationFamiliale
     *
     * @return string 
     */
    public function getUsrSituationFamiliale() {
        return $this->usrSituationFamiliale;
    }

    /**
     * Set usrNbEnfants
     *
     * @param integer $usrNbEnfants
     * @return Utilisateur
     */
    public function setUsrNbEnfants($usrNbEnfants) {
        $this->usrNbEnfants = $usrNbEnfants;

        return $this;
    }

    /**
     * Get usrNbEnfants
     *
     * @return integer 
     */
    public function getUsrNbEnfants() {
        return $this->usrNbEnfants;
    }

    /**
     * Set usrTypeContrat
     *
     * @param string $usrTypeContrat
     * @return Utilisateur
     */
    public function setUsrTypeContrat($usrTypeContrat) {
        $this->usrTypeContrat = $usrTypeContrat;

        return $this;
    }

    /**
     * Get usrTypeContrat
     *
     * @return string 
     */
    public function getUsrTypeContrat() {
        return $this->usrTypeContrat;
    }

    /**
     * Set usrHoraire
     *
     * @param string $usrHoraire
     * @return Utilisateur
     */
    public function setUsrHoraire($usrHoraire) {
        $this->usrHoraire = $usrHoraire;

        return $this;
    }

    /**
     * Get usrHoraire
     *
     * @return string 
     */
    public function getUsrHoraire() {
        return $this->usrHoraire;
    }

    /**
     * Set usrCategSociopro
     *
     * @param string $usrCategSociopro
     * @return Utilisateur
     */
    public function setUsrCategSociopro($usrCategSociopro) {
        $this->usrCategSociopro = $usrCategSociopro;

        return $this;
    }

    /**
     * Get usrCategSociopro
     *
     * @return string 
     */
    public function getUsrCategSociopro() {
        return $this->usrCategSociopro;
    }

    /**
     * Set usrRemuneration
     *
     * @param string $usrRemuneration
     * @return Utilisateur
     */
    public function setUsrRemuneration($usrRemuneration) {
        $this->usrRemuneration = $usrRemuneration;

        return $this;
    }

    /**
     * Get usrRemuneration
     *
     * @return string 
     */
    public function getUsrRemuneration() {
        return $this->usrRemuneration;
    }

    /**
     * Set usrPoste
     *
     * @param string $usrPoste
     * @return Utilisateur
     */
    public function setUsrPoste($usrPoste) {
        $this->usrPoste = $usrPoste;

        return $this;
    }

    /**
     * Get usrPoste
     *
     * @return string 
     */
    public function getUsrPoste() {
        return $this->usrPoste;
    }

    /**
     * Set usrService
     *
     * @param string $usrService
     * @return Utilisateur
     */
    public function setUsrService($usrService) {
        $this->usrService = $usrService;

        return $this;
    }

    /**
     * Get usrService
     *
     * @return string 
     */
    public function getUsrService() {
        return $this->usrService;
    }

    /**
     * Set usrDateAnciennete
     *
     * @param string $usrDateAnciennete
     * @return Utilisateur
     */
    public function setUsrDateAnciennete($usrDateAnciennete) {
        $this->usrDateAnciennete = $usrDateAnciennete;

        return $this;
    }

    /**
     * Get usrDateAnciennete
     *
     * @return string 
     */
    public function getUsrDateAnciennete() {
        return $this->usrDateAnciennete;
    }

    /**
     * Set usrDateEntree
     *
     * @param string $usrDateEntree
     * @return Utilisateur
     */
    public function setUsrDateEntree($usrDateEntree) {
        $this->usrDateEntree = $usrDateEntree;

        return $this;
    }

    /**
     * Get usrDateEntree
     *
     * @return string 
     */
    public function getUsrDateEntree() {
        return $this->usrDateEntree;
    }

    /**
     * Set usrDateSortie
     *
     * @param string $usrDateSortie
     * @return Utilisateur
     */
    public function setUsrDateSortie($usrDateSortie) {
        $this->usrDateSortie = $usrDateSortie;

        return $this;
    }

    /**
     * Get usrDateSortie
     *
     * @return string 
     */
    public function getUsrDateSortie() {
        return $this->usrDateSortie;
    }

    /**
     * Set usrCodeSap
     *
     * @param string $usrCodeSap
     * @return Utilisateur
     */
    public function setUsrCodeSap($usrCodeSap) {
        $this->usrCodeSap = $usrCodeSap;

        return $this;
    }

    /**
     * Get usrCodeSap
     *
     * @return string 
     */
    public function getUsrCodeSap() {
        return $this->usrCodeSap;
    }

    /**
     * Set usr2014
     *
     * @param boolean $usr2014
     * @return Utilisateur
     */
    public function setUsr2014($usr2014) {
        $this->usr2014 = $usr2014;

        return $this;
    }

    /**
     * Get usr2014
     *
     * @return boolean 
     */
    public function getUsr2014() {
        return $this->usr2014;
    }

    /**
     * Set usrMailStatus2015
     *
     * @param boolean $usrMailStatus2015
     * @return Utilisateur
     */
    public function setUsrMailStatus2015($usrMailStatus2015) {
        $this->usrMailStatus2015 = $usrMailStatus2015;

        return $this;
    }

    /**
     * Get usrMailStatus2015
     *
     * @return boolean 
     */
    public function getUsrMailStatus2015() {
        return $this->usrMailStatus2015;
    }

    /**
     * Set usrStatus
     *
     * @param string $usrStatus
     * @return Utilisateur
     */
    public function setUsrStatus($usrStatus) {
        $this->usrStatus = $usrStatus;

        return $this;
    }

    /**
     * Get usrStatus
     *
     * @return string 
     */
    public function getUsrStatus() {
        return $this->usrStatus;
    }

    /**
     * Set usrCommentaires
     *
     * @param string $usrCommentaires
     * @return Utilisateur
     */
    public function setUsrCommentaires($usrCommentaires) {
        $this->usrCommentaires = $usrCommentaires;

        return $this;
    }

    /**
     * Get usrCommentaires
     *
     * @return string 
     */
    public function getUsrCommentaires() {
        return $this->usrCommentaires;
    }

    /**
     * Set usrChecked
     *
     * @param boolean $usrChecked
     * @return Utilisateur
     */
    public function setUsrChecked($usrChecked) {
        $this->usrChecked = $usrChecked;

        return $this;
    }

    /**
     * Get usrChecked
     *
     * @return boolean 
     */
    public function getUsrChecked() {
        return $this->usrChecked;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Utilisateur
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Utilisateur
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Set dateDemandee
     *
     * @param \DateTime $dateDemandee
     * @return Utilisateur
     */
    public function setDateDemandee($dateDemandee) {
        $this->dateDemandee = $dateDemandee;

        return $this;
    }

    /**
     * Get dateDemandee
     *
     * @return \DateTime 
     */
    public function getDateDemandee() {
        return $this->dateDemandee;
    }

    /**
     * Set dateAccepte
     *
     * @param \DateTime $dateAccepte
     * @return Utilisateur
     */
    public function setDateAccepte($dateAccepte) {
        $this->dateAccepte = $dateAccepte;

        return $this;
    }

    /**
     * Get dateAccepte
     *
     * @return \DateTime 
     */
    public function getDateAccepte() {
        return $this->dateAccepte;
    }

    /**
     * Set dateCollab
     *
     * @param \DateTime $dateCollab
     * @return Utilisateur
     */
    public function setDateCollab($dateCollab) {
        $this->dateCollab = $dateCollab;

        return $this;
    }

    /**
     * Get dateCollab
     *
     * @return \DateTime 
     */
    public function getDateCollab() {
        return $this->dateCollab;
    }

    /**
     * Set dateManager
     *
     * @param \DateTime $dateManager
     * @return Utilisateur
     */
    public function setDateManager($dateManager) {
        $this->dateManager = $dateManager;

        return $this;
    }

    /**
     * Get dateManager
     *
     * @return \DateTime 
     */
    public function getDateManager() {
        return $this->dateManager;
    }

    /**
     * Set dateValider
     *
     * @param \DateTime $dateValider
     * @return Utilisateur
     */
    public function setDateValider($dateValider) {
        $this->dateValider = $dateValider;

        return $this;
    }

    /**
     * Get dateValider
     *
     * @return \DateTime 
     */
    public function getDateValider() {
        return $this->dateValider;
    }

    /**
     * Set usrComentaires
     *
     * @param string $usrComentaires
     * @return Utilisateur
     */
    public function setUsrComentaires($usrComentaires) {
        $this->usrComentaires = $usrComentaires;

        return $this;
    }

    /**
     * Get usrComentaires
     *
     * @return string 
     */
    public function getUsrComentaires() {
        return $this->usrComentaires;
    }

}
