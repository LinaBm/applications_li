<?php

namespace Administration\DirectionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Service
 *
 * @ORM\Table(name="adm_service")
 * @ORM\Entity(repositoryClass="Administration\DirectionBundle\Entity\ServiceRepository")
 */
class Service
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\ManyToOne(targetEntity="Administration\DirectionBundle\Entity\Direction", inversedBy="services")
     * @ORM\JoinColumn(nullable=false)
     */
    private $direction;
    
    /**
     * @ORM\OneToMany(targetEntity="Administration\DirectionBundle\Entity\Pole", mappedBy="service")
     */
    private $poles;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->poles = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Service
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
    

    /**
     * Set direction
     *
     * @param \Administration\DirectionBundle\Entity\Direction $direction
     * @return Service
     */
    public function setDirection(\Administration\DirectionBundle\Entity\Direction $direction)
    {
        $this->direction = $direction;

        return $this;
    }

    /**
     * Get direction
     *
     * @return \Administration\DirectionBundle\Entity\Direction 
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * Add poles
     *
     * @param \Administration\DirectionBundle\Entity\Pole $poles
     * @return Service
     */
    public function addPole(\Administration\DirectionBundle\Entity\Pole $poles)
    {
        $this->poles[] = $poles;

        return $this;
    }

    /**
     * Remove poles
     *
     * @param \Administration\DirectionBundle\Entity\Pole $poles
     */
    public function removePole(\Administration\DirectionBundle\Entity\Pole $poles)
    {
        $this->poles->removeElement($poles);
    }

    /**
     * Get poles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPoles()
    {
        return $this->poles;
    }
    
    function __toString(){
    	return $this->getLibelle();
    }
}
