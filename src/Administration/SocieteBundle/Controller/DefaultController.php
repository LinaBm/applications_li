<?php

namespace Administration\SocieteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('AdministrationSocieteBundle:Default:index.html.twig', array('name' => $name));
    }
}
