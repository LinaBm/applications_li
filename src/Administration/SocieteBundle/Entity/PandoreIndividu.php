<?php

namespace Administration\SocieteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Administration\SocieteBundle\Entity\PandoreSociete;

/**
 * PandoreIndividu
 *
 * @ORM\Table(name="pandore.individu")
 * @ORM\Entity
 */
class PandoreIndividu
{
	/**
	 * @var string
	 *
	 * @ORM\Column(name="matricule", type="string", length=45, nullable=true)
	 * @ORM\Id
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nom", type="string", length=45, nullable=true)
	 */
	private $nom;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="prenom", type="string", length=45, nullable=true)
	 */
	private $prenom;
	
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="numero_secu", type="string", length=11, nullable=true)
	 */
	private $numeroSecu;

	
	
	
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
	
    /**
     * Set numero_secu
     *
     * @param int $numero_secu
     *
     */
    public function setNumeroSecu($raison)
    {
    	$this->numeroSecu = $numeroSecu;
    
    	return $this;
    }
    
    /**
     * Get numero_secu
     *
     * @return string
     */
    public function getNumeroSecu()
    {
    	return $this->numeroSecu;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * Set nom
     *
     * @param string $nom
     *
     */
    public function setNom($nom)
    {
    	$this->nom = $nom;
    
    	return $this;
    }
    
    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
    	return $this->nom;
    }
    
    
    /**
     * Set prenom
     *
     * @param string $prenom
     *
     */
    public function setPrenom($prenom)
    {
    	$this->prenom = $prenom;
    
    	return $this;
    }
    
    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
    	return $this->prenom;
    }
	
}
