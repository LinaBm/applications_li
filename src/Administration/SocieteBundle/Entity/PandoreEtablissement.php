<?php

namespace Administration\SocieteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Administration\SocieteBundle\Entity\PandoreSociete;

/**
 * PandoreSociete
 *
 * @ORM\Table(name="pandore.etablissement")
 * @ORM\Entity
 */
class PandoreEtablissement
{
	/**
	 * @var string
	 *
	 * @ORM\Column(name="siret", type="string", length=45, nullable=true)
	 * @ORM\Id
	 */
	private $id;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Administration\SocieteBundle\Entity\PandoreSociete")
	 * @ORM\JoinColumn(name="societe_id", referencedColumnName="id")
     */
    private $societe;

    /**
     * @var integer
     *
     * @ORM\Column(name="numero", type="string", nullable=false)
     */
    private $numero;    

    /**
     * @var string
     *
     * @ORM\Column(name="raison", type="string", length=45, nullable=true)
     */
    private $raison;

    /**
     * @var string
     *
     * @ORM\Column(name="bister", type="string", length=1, nullable=true)
     */
    private $bister;
    
    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=45, nullable=true)
     */
    private $ville;    

    /**
     * @var string
     *
     * @ORM\Column(name="numvoie", type="string", length=45, nullable=true)
     */
    private $numvoie;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse1", type="string", length=100, nullable=true)
     */
    private $adresse1;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse2", type="string", length=100, nullable=true)
     */
    private $adresse2;
    
    /**
     * @var string
     *
     * @ORM\Column(name="complementadresse", type="string", length=100, nullable=true)
     */
    private $complementadresse;
        
    /**
     * Get id
     *
     * @return \Administration\SocieteBundle\Entity\PandoreSociete
     */
    public function getSociete()
    {
        return $this->societe;
    }

 /* Mikael LOLIVE pour des raisons techniques, l'id est le numéro de SIRET. les getter sont là pour un code plus lisible  */
    /**
     * Set siret
     *
     * @param string $siret
     *
     * @return DdrcSociete
     */
    public function setSiret($siret)
    {
        $this->id = $siret;

        return $this;
    }

    /**
     * Get siret
     *
     * @return string
     */
    public function getSiret()
    {
        return $this->id;
    }

    /**
     * Get siret
     *
     * @return string
     */
    public function getRaison()
    {
    	return $this->raison;
    }
    
    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
    	return $this->ville;
    }
    
    /**
     * Get formeJuridique
     *
     * @return string
     */
    public function getFormeJuridique()
    {
    	return $this->societe->getFormjur();
    }

    /**
     * Get Capital
     *
     * @return string
     */
    public function getCapital()
    {
    	return $this->societe->getCapital();
    }
    
    /**
     * Get Adresse
     *
     * @return string
     */
    public function getAdresse()
    {
    	return $this->numvoie." ".$this->bister." ".$this->adresse1." ".$this->adresse2." ".$this->complementadresse;
    }
    
    /**
     * Get Code Postal
     *
     * @return string
     */
    public function getCodePostal()
    {
    	return $this->societe->getCodePostal();
    }

    /**
     * Get Numero
     *
     * @return string
     */
    public function getNumero()
    {
    	return $this->numero;
    }    
    
    
    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
    	return $this->societe->getRaison();
    }
}
