<?php

namespace Administration\SocieteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PandoreSociete
 *
 * @ORM\Table(name="pandore.societe")
 * @ORM\Entity
 */
class PandoreSociete
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="raison", type="string", length=45, nullable=true)
     */
    private $raison;

    /**
     * @var string
     *
     * @ORM\Column(name="numvoie", type="string", length=45, nullable=true)
     */
    private $numvoie;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse1", type="string", length=100, nullable=true)
     */
    private $adresse1;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse2", type="string", length=100, nullable=true)
     */
    private $adresse2;

    /**
     * @var string
     *
     * @ORM\Column(name="codepostal", type="string", length=5, nullable=true)
     */
    private $codepostal;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=45, nullable=true)
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="pays", type="string", length=45, nullable=true)
     */
    private $pays;

    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=45, nullable=true)
     */
    private $fax;

    /**
     * @var string
     *
     * @ORM\Column(name="formjur", type="string", length=45, nullable=true)
     */
    private $formjur;

    /**
     * @var string
     *
     * @ORM\Column(name="capital", type="string", length=45, nullable=true)
     */
    private $capital;

    /**
     * @var string
     *
     * @ORM\Column(name="siren", type="string", length=45, nullable=true)
     */
    private $siren;

    /**
     * @var string
     *
     * @ORM\Column(name="naf", type="string", length=45, nullable=true)
     */
    private $naf;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set raison
     *
     * @param string $raison
     *
     * @return DdrcSociete
     */
    public function setRaison($raison)
    {
        $this->raison = $raison;

        return $this;
    }

    /**
     * Get raison
     *
     * @return string
     */
    public function getRaison()
    {
        return $this->raison;
    }

    /**
     * Set numvoie
     *
     * @param string $numvoie
     *
     * @return DdrcSociete
     */
    public function setNumvoie($numvoie)
    {
        $this->numvoie = $numvoie;

        return $this;
    }

    /**
     * Get numvoie
     *
     * @return string
     */
    public function getNumvoie()
    {
        return $this->numvoie;
    }

    /**
     * Set adresse1
     *
     * @param string $adresse1
     *
     * @return DdrcSociete
     */
    public function setAdresse1($adresse1)
    {
        $this->adresse1 = $adresse1;

        return $this;
    }

    /**
     * Get adresse1
     *
     * @return string
     */
    public function getAdresse1()
    {
        return $this->adresse1;
    }

    /**
     * Set adresse2
     *
     * @param string $adresse2
     *
     * @return DdrcSociete
     */
    public function setAdresse2($adresse2)
    {
        $this->adresse2 = $adresse2;

        return $this;
    }

    /**
     * Get adresse2
     *
     * @return string
     */
    public function getAdresse2()
    {
        return $this->adresse2;
    }

    /**
     * Set codepostal
     *
     * @param string $codepostal
     *
     * @return DdrcSociete
     */
    public function setCodepostal($codepostal)
    {
        $this->codepostal = $codepostal;

        return $this;
    }

    /**
     * Get codepostal
     *
     * @return string
     */
    public function getCodepostal()
    {
        return $this->codepostal;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return DdrcSociete
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set pays
     *
     * @param string $pays
     *
     * @return DdrcSociete
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set fax
     *
     * @param string $fax
     *
     * @return DdrcSociete
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set formjur
     *
     * @param string $formjur
     *
     * @return DdrcSociete
     */
    public function setFormjur($formjur)
    {
        $this->formjur = $formjur;

        return $this;
    }

    /**
     * Get formjur
     *
     * @return string
     */
    public function getFormjur()
    {
        return $this->formjur;
    }

    /**
     * Set capital
     *
     * @param string $capital
     *
     * @return DdrcSociete
     */
    public function setCapital($capital)
    {
        $this->capital = $capital;

        return $this;
    }

    /**
     * Get capital
     *
     * @return string
     */
    public function getCapital()
    {
        return $this->capital;
    }

    /**
     * Set siren
     *
     * @param string $siren
     *
     * @return DdrcSociete
     */
    public function setSiren($siren)
    {
        $this->siren = $siren;

        return $this;
    }

    /**
     * Get siren
     *
     * @return string
     */
    public function getSiren()
    {
        return $this->siren;
    }

    /**
     * Set naf
     *
     * @param string $naf
     *
     * @return DdrcSociete
     */
    public function setNaf($naf)
    {
        $this->naf = $naf;

        return $this;
    }

    /**
     * Get naf
     *
     * @return string
     */
    public function getNaf()
    {
        return $this->naf;
    }
	
	function __toString(){
    	return $this->getRaison();
    }
}
