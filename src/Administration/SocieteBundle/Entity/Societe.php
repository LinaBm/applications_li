<?php

namespace Administration\SocieteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Societe
 *
 * @ORM\Table(name="adm_societe")
 * @ORM\Entity(repositoryClass="Administration\SocieteBundle\Entity\SocieteRepository")
 */
class Societe
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\Column(name="reference", type="string", length=4)
     */
    private $reference;
    
    /**
     * @ORM\ManyToOne(targetEntity="Administration\DirectionBundle\Entity\Direction" , inversedBy="societes")
     */
    private $direction;

	public function getUniqueName()
	{
		return sprintf('%s - %s', $this->reference, $this->libelle);
	}
	
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Societe
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    
        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }
    
    /**
     * Set reference
     *
     * @param string $reference
     * @return Societe
     */
    public function setReference($reference)
    {
    	$this->reference = $reference;
    
    	return $this;
    }
    
    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
    	return $this->reference;
    }
    
    /**
     * Set direction
     *
     * @param \Administration\DirectionBundle\Entity\Direction $direction
     * @return Service
     */
    public function setDirection(\Administration\DirectionBundle\Entity\Direction $direction)
    {
    	$this->direction = $direction;
    
    	return $this;
    }
    
    /**
     * Get direction
     *
     * @return \Administration\DirectionBundle\Entity\Direction
     */
    public function getDirection()
    {
    	return $this->direction;
    }
    
    public function __toString()
    {
    	return $this->getLibelle();
    }
}
