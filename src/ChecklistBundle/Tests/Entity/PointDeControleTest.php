<?php

namespace ChecklistBundle\Tests\Entity;

use ChecklistBundle\Entity\PointDeControle;
use \DateTime;

class PointDeControleTest extends \PHPUnit_Framework_TestCase
{
	public function testGetDureeTotale(){
		$pointDeControleTest = new PointDeControle();
		$pointDeControleTest->setDuree(new DateTime(date('Y-m-d 00:01:00')));
		$pointDeControleTest->setDureeParCaisse(new DateTime(date('Y-m-d 00:01:00')));
		$pointDeControleTest->setCaisses('true');
		$result= $pointDeControleTest->getdureeTotale(5);
		 
		$this->assertEquals(new DateTime(date('Y-m-d 00:06:00')), $result);
	}
}