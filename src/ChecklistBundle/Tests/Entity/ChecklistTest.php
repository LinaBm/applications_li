<?php

namespace ChecklistBundle\Tests\Entity;

use ChecklistBundle\Entity\Checklist;
use ChecklistBundle\Entity\PointDeControle;
use \DateTime;

class ChecklistTest extends \PHPUnit_Framework_TestCase
{
	 public function testTempsTheorique(){
	 	$checklistTest=new Checklist();
	 	$pointDeControle1 = new PointDeControle();
	 	$pointDeControle1->setDuree(new DateTime(date('Y-m-d 00:00:01')));
	 	$pointDeControle1->setDureeParCaisse(new DateTime(date('Y-m-d 00:00:10')));
	 	$pointDeControle1->setCaisses('true');
	 	$pointDeControle2 = new PointDeControle();
	 	$pointDeControle2->setDuree(new DateTime(date('Y-m-d 00:01:00')));
	 	$pointDeControle2->setDureeParCaisse(new DateTime(date('Y-m-d 00:10:00')));
	 	$pointDeControle2->setCaisses('');
	 	$checklistTest->addPointsDeControle($pointDeControle1);
	 	$checklistTest->addPointsDeControle($pointDeControle2);
	 	
	 	$result= $checklistTest->getTempsTheorique(5);
	 	
	 	$this->assertEquals(new DateTime(date('Y-m-d 00:01:51')), $result);
	 }
}