<?php

namespace ChecklistBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use ChecklistBundle\Entity\Resultat;

class ResultatRepository extends EntityRepository
{
	public function verifyResults($idVisite, $idPointDeControle)
	{
		$idVisite = (int)$idVisite;
		$idPointDeControle = (int)$idPointDeControle;
		$query = $this->getEntityManager()->createQuery(
			'SELECT r FROM ChecklistBundle:Resultat r
			WHERE r.visites = ' . $idVisite . '
			AND r.pointsDeControle = ' . $idPointDeControle . '
			ORDER BY r.id ASC'
		);
		return $query->getResult();
//		->setParameter('visits', $idVisite)
//		->setParameter('pointsDeControle', $idPointDeControle);
//		var_dump($query->getSQL());
//		die('here');
		// to get just one result:
		// $product = $query->setMaxResults(1)->getOneOrNullResult();
	}

}
