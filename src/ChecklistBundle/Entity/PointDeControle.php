<?php

namespace ChecklistBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \DateTime;

/**
 * PointDeControle
 *
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="ChecklistBundle\Entity\PointDeControleRepository")
 * @ORM\Table(name="chk_points_de_controle")
 */
class PointDeControle
{
    /**
     * @ORM\ManyToMany(targetEntity="Checklist", mappedBy="pointsDeControle")
     **/
    private $checklists;

    public function __construct() {
        $this->checklists = new \Doctrine\Common\Collections\ArrayCollection();
    }

    const TYPE_VISUEL   					= 1;
    const TYPE_INSTALLATION_LOGICIEL        = 2;
    const TYPE_ACTION    					= 3;
    const TYPE_RETOUR_INFO 					= 4;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var smallint
     *
     * @ORM\Column(name="type", type="smallint")
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="action", type="string", length=255, nullable=true)
     */
    private $action;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255, nullable=true)
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="question", type="string", length=255)
     */
    private $question;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="duree", type="time")
     */
    private $duree;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="duree_par_caisse", type="time")
     */
    private $dureeParCaisse;

    /**
     * @var string
     *
     * @ORM\Column(name="fische_de_procedure", type="string", length=255, nullable=true)
     */
    private $fischeDeProcedure;

    /**
     * @var boolean
     *
     * @ORM\Column(name="caisses", type="boolean")
     */
    private $caisses;

    /**
     * @var boolean
     *
     * @ORM\Column(name="desactive", type="boolean", options={"default":false})
     */
    private $desactive;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return PointDeControle
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return PointDeControle
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set action
     *
     * @param string $action
     * @return PointDeControle
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set question
     *
     * @param string $question
     * @return PointDeControle
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set duree
     *
     * @param \DateTime $duree
     * @return PointDeControle
     */
    public function setDuree($duree)
    {
        $this->duree = $duree;

        return $this;
    }

    /**
     * Get duree
     *
     * @return \DateTime
     */
    public function getDuree()
    {
        return $this->duree;
    }

    /**
     * Set dureeParCaisse
     *
     * @param \DateTime $dureeParCaisse
     * @return PointDeControle
     */
    public function setDureeParCaisse($dureeParCaisse)
    {
        $this->dureeParCaisse = $dureeParCaisse;

        return $this;
    }

    /**
     * Get dureeParCaisse
     *
     * @return \DateTime
     */
    public function getDureeParCaisse()
    {
        return $this->dureeParCaisse;
    }

    /**
     * Set fischeDeProcedure
     *
     * @param string $fischeDeProcedure
     * @return PointDeControle
     */
    public function setFischeDeProcedure($fischeDeProcedure)
    {
        $this->fischeDeProcedure = $fischeDeProcedure;

        return $this;
    }

    /**
     * Get fischeDeProcedure
     *
     * @return string
     */
    public function getFischeDeProcedure()
    {
        return $this->fischeDeProcedure;
    }

    /**
     * Set caisses
     *
     * @param boolean $caisses
     * @return PointDeControle
     */
    public function setCaisses($caisses)
    {
        $this->caisses = $caisses;

        return $this;
    }

    /**
     * Get caisses
     *
     * @return boolean
     */
    public function getCaisses()
    {
        return $this->caisses;
    }

    /**
     * Get dureeTotale
     *
     * @return boolean
     */
    public function getdureeTotale($nombreCaisse)
    {
    	$dureeTotale= new DateTime(date('Y-m-d 00:00:00'));
    	$temporary = new DateTime(date('Y-m-d 00:00:00'));
    	$dureeTotale->add($temporary->diff($this->getDuree()));
    	if ($this->caisses){
			for ($i=0; $i < $nombreCaisse; $i++){
    			$temporary = new DateTime(date('Y-m-d 00:00:00'));
    			$dureeTotale->add($temporary->diff($this->getDureeParCaisse()));
    		}
    	}
    	return $dureeTotale;
    }



    /**
     * Set desactive
     *
     * @param boolean $desactive
     * @return PointDeControle
     */
    public function setDesactive($desactive)
    {
        $this->desactive = $desactive;

        return $this;
    }

    /**
     * Get desactive
     *
     * @return boolean
     */
    public function getDesactive()
    {
        return $this->desactive;
    }


    /**
     * Set libelle
     *
     * @param string $libelle
     * @return PointDeControle
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Add checklists
     *
     * @param \ChecklistBundle\Entity\Checklist $checklists
     * @return PointDeControle
     */
    public function addChecklist(\ChecklistBundle\Entity\Checklist $checklists)
    {
        $this->checklists[] = $checklists;

        return $this;
    }

    /**
     * Remove checklists
     *
     * @param \ChecklistBundle\Entity\Checklist $checklists
     */
    public function removeChecklist(\ChecklistBundle\Entity\Checklist $checklists)
    {
        $this->checklists->removeElement($checklists);
    }

    /**
     * Get checklists
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChecklists()
    {
        return $this->checklists;
    }
}
