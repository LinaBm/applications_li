<?php
namespace ChecklistBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ChecklistBundle\Entity\PointDeControle;

/**
 * Resultat
 *
 * @ORM\Entity(repositoryClass="ChecklistBundle\Entity\ResultatRepository")
 * @ORM\Table(name="chk_resultat")
 */
class Resultat
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="ChecklistBundle\Entity\PointDeControle")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $pointsDeControle;

	/**
	 * @ORM\ManyToOne(targetEntity="ChecklistBundle\Entity\Visites")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $visites;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="flag_accomplie", type="boolean", nullable=true)
	 */
	private $flagAccomplie;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="reponse", type="boolean", nullable=true)
	 */
	private $reponse;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="fichierUn", type="string", nullable=true)
	 */
	private $fichierUn;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="email", type="boolean", nullable=true)
	 */
	private $email;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="date", type="datetime", nullable=true)
	 */
	private $date;

	/**
	 * @var text
	 *
	 * @ORM\Column(name="resultat", type="text", nullable=true)
	 */
	private $resultat;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="sources", type="boolean", nullable=true)
	 */
	private $sources;	

	/**
	 * @var string
	 *
	 * @ORM\Column(name="fichierDeux", type="string", nullable=true)
	 */
	private $fichierDeux;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="valide", type="boolean", nullable=true)
	 */
	private $valide;

	/**
	 * @var text
	 *
	 * @ORM\Column(name="numeroDuCaisse", type="text", nullable=true)
	 */
	private $numeroDuCaisse;
	
	/**
	 * Set numeroDuCaisse
	 *
	 * @param text $numeroDuCaisse
	 * @return ControleAvecRetourDInfo
	 */
	public function setNumeroDuCaisse($numeroDuCaisse)
	{
		$this->numeroDuCaisse = $numeroDuCaisse;	
		return $this;
	}
	
	/**
	 * Get numeroDuCaisse
	 *
	 * @return text
	 */
	public function getNumeroDuCaisse()
	{
		return $this->numeroDuCaisse;
	}
	
	/**
	 * Set resultat
	 *
	 * @param text $resultat
	 * @return ControleAvecRetourDInfo
	 */
	public function setResultat($resultat)
	{
		$this->resultat = $resultat;	
		return $this;
	}
	
	/**
	 * Get resultat
	 *
	 * @return text
	 */
	public function getResultat()
	{
		return $this->resultat;
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set pointsDeControle
	 *
	 * @param integer $pointsDeControle
	 * @return ActionAMener
	 */
	public function setPointsDeControle($pointsDeControle)
	{
		$this->pointsDeControle = $pointsDeControle;
		return $this;
	}

	/**
	 * Get pointsDeControle
	 *
	 * @return integer
	 */
	public function getPointsDeControle()
	{
		return $this->pointsDeControle;
	}

	/**
	 * Set visites
	 *
	 * @param integer $visites
	 * @return ActionAMener
	 */
	public function setVisites($visites)
	{
		$this->visites = $visites;
		return $this;
	}

	/**
	 * Get visites
	 *
	 * @return integer
	 */
	public function getVisites()
	{
		return $this->visites;
	}

	/**
	 * Set flagAccomplie
	 *
	 * @param boolean $flagAccomplie
	 * @return Resultat
	 */
	public function setFlagAccomplie($flagAccomplie)
	{
		$this->flagAccomplie = $flagAccomplie;
		return $this;
	}

	/**
	 * Get flagAccomplie
	 *
	 * @return boolean
	 */
	public function getFlagAccomplie()
	{
		return $this->flagAccomplie;
	}

	/**
	 * Set reponse
	 *
	 * @param smallint $reponse
	 * @return Resultat
	 */
	public function setReponse($reponse)
	{
		$this->reponse = $reponse;
		return $this;
	}

	/**
	 * Get reponse
	 *
	 * @return smallint
	 */
	public function getReponse()
	{
		return $this->reponse;
	}

	/**
	 * Set email
	 *
	 * @param boolean $email
	 * @return Resultat
	 */
	public function setEmail($email)
	{
		$this->email = $email;
		return $this;
	}

	/**
	 * Get email
	 *
	 * @return boolean
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * Set valide
	 *
	 * @param boolean $valide
	 * @return Resultat
	 */
	public function setValide($valide)
	{
		$this->valide = $valide;
		return $this;
	}

	/**
	 * Get valide
	 *
	 * @return boolean
	 */
	public function getValide()
	{
		return $this->valide;
	}

	/**
	 * Set date
	 *
	 * @param integer $date
	 * @return ActionAMener
	 */
	public function setDate($date)
	{
		$this->date = $date;
		return $this;
	}

	/**
	 * Get date
	 *
	 * @return integer
	 */
	public function getDate()
	{
		return $this->date;
	}

    /**
     * Set fichierUn
     *
     * @param string $fichierUn
     * @return Resultat
     */
    public function setFichierUn($fichierUn)
    {
		if ($fichierUn != null) {
			$this->fichierUn = $fichierUn;
		}
        return $this;
    }

    /**
     * Get fichierUn
     *
     * @return string 
     */
    public function getFichierUn()
    {
			return $this->fichierUn;
    }

    /**
     * Set sources
     *
     * @param integer $sources
     * @return Resultat
     */
    public function setSources($sources)
    {
        $this->sources = $sources;

        return $this;
    }

    /**
     * Get sources
     *
     * @return integer 
     */
    public function getSources()
    {
        return $this->sources;
    }

    /**
     * Set fichierDeux
     *
     * @param string $fichierDeux
     * @return Resultat
     */
    public function setFichierDeux($fichierDeux)
    {
		if ($fichierDeux != null) {
			$this->fichierDeux = $fichierDeux;
		}
        return $this;
    }

    /**
     * Get fichierDeux
     *
     * @return string 
     */
    public function getFichierDeux()
    {	
		return $this->fichierDeux;
    }
    
    public function verification(){
    	switch ($this->pointsDeControle->getType()) {
    		case  PointDeControle::TYPE_VISUEL:
    			return $this->flagAccomplie;
    			break;
    		case PointDeControle::TYPE_INSTALLATION_LOGICIEL:
    			if ($this->flagAccomplie && $this->getReponse()==1){
    				return true;
    			} else {
    				return false;
    			}
    			break;
    		case PointDeControle::TYPE_ACTION:
    			if ($this->flagAccomplie && $this->getReponse()==1){
    				return true;
    			} else {
    				return false;
    			}
    			break;
    		case PointDeControle::TYPE_RETOUR_INFO:
    			if ($this->flagAccomplie && $this->getReponse()==1 && $this->getResultat()){
    				return true;
    			} else {
    				return false;
    			}
    			break;
    	}
    }
}
