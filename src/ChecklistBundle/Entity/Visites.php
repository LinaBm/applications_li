<?php

namespace ChecklistBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ChecklistBundle\Entity\Magasin as magasin;
use \DateTime;

/**
 * Visites
 *
 * @ORM\Entity(repositoryClass="ChecklistBundle\Entity\VisitesRepository")
 * @ORM\Table(name="chk_visites")
 */
class Visites
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ChecklistBundle\Entity\Checklist")
     * @ORM\JoinColumn(nullable=true)
     */
    private $checklists;

    /**
     * @ORM\ManyToOne(targetEntity="ChecklistBundle\Entity\Magasin")
     * @ORM\JoinColumn(nullable=false)
     */
    private $magasin;

    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $technicien;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="heure_de_arivee", type="time")
     */
    private $heureDeArivee;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="heure_de_depart", type="time")
     */
    private $heureDeDepart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="temps_de_interuption", type="time")
     */
    private $tempsDeInteruption;

    /**
     * @var integer
     *
     * @ORM\Column(name="flag_accomplie", type="smallint")
     */
    private $flagAccomplie;

    /**
     * @var text
     *
     * @ORM\Column(name="commentaire", type="text")
     */
    private $commentaire;

    /**
     * @var smallint
     *
     * @ORM\Column(name="commencer", type="smallint")
     */
    private $commencer;

    /**
     * @var smallint
     *
     * @ORM\Column(name="terminer", type="smallint")
     */
    private $terminer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Checklists
     *
     * @param integer $Checklists
     * @return Visites
     */
    public function setChecklists($checklists)
    {
        $this->checklists = $checklists;

        return $this;
    }

    /**
     * Get Checklists
     *
     * @return \ChecklistBundle\Entity\Checklist 
     */
    public function getChecklists()
    { 
        return $this->checklists;
    }

    /**
     * Set idMagasins
     *
     * @param integer $idMagasins
     * @return Visites
     */
    public function setMagasins($idMagasins)
    {
        $this->magasin = $idMagasins;

        return $this;
    }

    /**
     * Get idMagasins
     *
     * @return integer 
     */
    public function getMagasins()
    {
        return $this->magasin;
    }

    /**
     * Set technicien
     *
     * @param integer $technicien
     * @return Visites
     */
    public function setTechnicien($technicien)
    {
        $this->technicien = $technicien;

        return $this;
    }

    /**
     * Get getTechnicien
     *
     * @return \User\UserBundle\Entity\User 
     */
    public function getTechnicien()
    {
        return $this->technicien;
    }

    /**
     * Set heureDeArivee
     *
     * @param \DateTime $heureDeArivee
     * @return Visites
     */
    public function setHeureDeArivee($heureDeArivee)
    {
        $this->heureDeArivee = $heureDeArivee;

        return $this;
    }

    /**
     * Get heureDeArivee
     *
     * @return \DateTime 
     */
    public function getHeureDeArivee()
    {
        return $this->heureDeArivee;
    }

    /**
     * Set heureDeDepart
     *
     * @param \DateTime $heureDeDepart
     * @return Visites
     */
    public function setHeureDeDepart($heureDeDepart)
    {
        $this->heureDeDepart = $heureDeDepart;

        return $this;
    }

    /**
     * Get heureDeDepart
     *
     * @return \DateTime 
     */
    public function getHeureDeDepart()
    {
        return $this->heureDeDepart;
    }

    /**
     * Set tempsDeInteruption
     *
     * @param \DateTime $tempsDeInteruption
     * @return Visites
     */
    public function setTempsDeInteruption($tempsDeInteruption)
    {
        $this->tempsDeInteruption = $tempsDeInteruption;

        return $this;
    }

    /**
     * Get tempsDeInteruption
     *
     * @return \DateTime 
     */
    public function getTempsDeInteruption()
    {
        return $this->tempsDeInteruption;
    }

    /**
     * Set flagAccomplie
     *
     * @param integer $flagAccomplie
     * @return Visites
     */
    public function setFlagAccomplie($flagAccomplie)
    {
        $this->flagAccomplie = $flagAccomplie;

        return $this;
    }

    /**
     * Get flagAccomplie
     *
     * @return integer 
     */
    public function getFlagAccomplie()
    {
        return $this->flagAccomplie;
    }

    /**
     * Set commentaire
     *
     * @param text $commentaire
     * @return Visites
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * Get commentaire
     *
     * @return text 
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * Set commencer
     *
     * @param smallint $commencer
     * @return Visites
     */
    public function setCommencer($commencer)
    {
        $this->commencer = $commencer;

        return $this;
    }

    /**
     * Get commencer
     *
     * @return smallint 
     */
    public function getCommencer()
    {
        return $this->commencer;
    }

    /**
     * Set terminer
     *
     * @param smallint $terminer
     * @return Visites
     */
    public function setTerminer($terminer)
    {
        $this->terminer = $terminer;

        return $this;
    }

    /**
     * Get terminer
     *
     * @return smallint 
     */
    public function getTerminer()
    {
        return $this->terminer;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Visites
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
	}
	

    /**
     * Set magasin
     *
     * @param \ChecklistBundle\Entity\Magasin $magasin
     * @return Visites
     */
    public function setMagasin(\ChecklistBundle\Entity\Magasin $magasin)
    {
        $this->magasin = $magasin;

        return $this;
    }

    /**
     * Get magasin
     *
     * @return \ChecklistBundle\Entity\Magasin 
     */
    public function getMagasin()
    {
        return $this->magasin;
    }
    
    /**
     * Get tempsTheorique
     *
     * @return \ChecklistBundle\Entity\Magasin
     */
    public function getTempsTheorique()
    {
    	return $this->getChecklists()->getTempsTheorique($this->getMagasin()->getNombreDeCaisses());
    }
    
    /**
     * Get tempsReel
     */
    public function getTempsReel()
    { 	$temporary = new DateTime(date('Y-m-d 00:00:00'));
    	$pause = $temporary->diff($this->getTempsDeInteruption());
    	$dureeTotale=$temporary->add($this->getHeureDeArivee()->diff($this->getHeureDeDepart()));
    	return $dureeTotale->sub($pause);
    }
       
}
