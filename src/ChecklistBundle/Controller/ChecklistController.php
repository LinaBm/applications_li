<?php

namespace ChecklistBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ChecklistBundle\Entity\Checklist;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use ChecklistBundle\Form\ChecklistType;
use \DateTime;
use ChecklistBundle\Entity\PointDeControle;

class ChecklistController extends Controller
{	
	/**
	 * @Route("/checklist/getDuree", name="checklist_checklist_get_duree")
	 */
	public function getDuree()
	{
		$request = $this->container->get('request');
		$checkpoints = $request->request->get('checkpoints');
		$estimee = new DateTime(date('Y-m-d 00:00:00'));
		$parCaisse = new DateTime(date('Y-m-d 00:00:00'));
		if($checkpoints){
			foreach ($checkpoints as $idPointDeControle){
				$pointDeControle=$this->getDoctrine()
					->getRepository('ChecklistBundle:PointDeControle')
					->find($idPointDeControle);
				$temporary = new DateTime(date('Y-m-d 00:00:00'));
				$estimee->add($temporary->diff($pointDeControle->getDuree()));
				if ($pointDeControle->getCaisses()){
					$parCaisse->add($temporary->diff($pointDeControle->getDureeParCaisse()));
				}
			}
		}
		$tabDuree=array("estimee"=>$estimee, "parCaisse"=>$parCaisse);
		return new JsonResponse($tabDuree);
	}
	
    /**
     * @Route("/checklist/editer/{id}", name="checklist_checklist_editer")
     */
	public function editerChecklistAction($id, Request $request, $message = '')
	{
		$em = $this->getDoctrine()->getManager();
		$checklist = $em->getRepository('ChecklistBundle:Checklist')->find($id);
		if (!$checklist) {
			throw $this->createNotFoundException(
				'Trouvé aucun checklist pour id '.$id
			);
		}
		$form = $this->createForm(new ChecklistType, $checklist);
		$form->handleRequest($request);
		if ($form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$em->persist($checklist);
			$em->flush();
			$message = 'L’opération s’est bien déroulée!';
			$this->get('session')->getFlashBag()->add('checklistSuccess', $message);
			return $this->redirect($this->generateUrl('checklist_checklist_editer',
				array( 'id' => $id )
			));
		}
		$estimee = new DateTime(date('Y-m-d 00:00:00'));
		$parCaisse = new DateTime(date('Y-m-d 00:00:00'));
		foreach ($checklist->getPointsDeControle() as $pointDeControle) {
			$temporary = new DateTime(date('Y-m-d 00:00:00'));				
			$estimee->add($temporary->diff($pointDeControle->getDuree()));								
			if ($pointDeControle->getCaisses()){
				$parCaisse->add($temporary->diff($pointDeControle->getDureeParCaisse()));				
			}
		}
        return $this->render('ChecklistBundle:checklist:ajouterChecklist.html.twig', array(
            'form' => $form->createView(),
			'title' => 'Modifiée checklist',
			'message' => $message,
        	'estimee' => $estimee,
        	'parCaisse' => $parCaisse
        ));
	}
		
	/**
	 * @Route("/verifChecklist", name="checklist_checklist_verifier")
	 */
	public function verifChecklist()
	{;
		$request = $this->container->get('request');
		$checklistId = $request->request->get('checklistId');
		$visites=$this->getDoctrine()
			->getRepository('ChecklistBundle:Visites')
			->getVisitesChecklist($checklistId);
		if ($visites){				
			return new JsonResponse($visites[0]->getMagasin()->getNom());
		} else {
			return new JsonResponse('pas de visite prevue');
		}
	}

    /**
     * @Route("/checklist/desactiver/{id}", name="checklist_checklist_desactiver")
     */
	public function desactiverChecklistAction($id)
	{
		$checklist = $this->getDoctrine()
			->getRepository('ChecklistBundle:Checklist')
			->find($id);
		if (!$checklist) {
			throw $this->createNotFoundException(
				'Trouvé aucune checklist pour id '.$id
			);
		}
		$visites = $this->getDoctrine()
			->getRepository('ChecklistBundle:Visites')
			->getProchaineVisitesChecklist($id);
		if($visites){
			throw $this->createNotFoundException(
				'Il y aura de visites par checklist id '. $id . '. Vous devez les annuler!'
			);
		}
		$em = $this->getDoctrine()->getManager();
		$checklist->setDesactive(!$checklist->getDesactive());
		$em->persist($checklist);
		$em->flush();
		return $this->redirect($this->generateUrl('checklist_checklist_checklists'));
	}

    /**
     * @Route("/checklist/ajouter", name="checklist_checklist_ajouter")
     */
	public function ajouterChecklistAction(Request $request, $message = '')
	{
        $checklist = new Checklist();
        $form = $this->createForm(new ChecklistType, $checklist);
		$form->handleRequest($request);
		$estimee = new DateTime(date('Y-m-d 00:00:00'));
		$parCaisse = new DateTime(date('Y-m-d 00:00:00'));
		foreach ($checklist->getPointsDeControle() as $pointDeControle) {
			$temmporary = new DateTime(date('Y-m-d 00:00:00'));
			$estimee->add($temmporary->diff($pointDeControle->getDuree()));
			if ($pointDeControle->getCaisses()){
				$parCaisse->add($temmporary->diff($pointDeControle->getDureeParCaisse()));
			}
		}
		if ($form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$checklist->setDesactive(0);
			$em->persist($checklist);
			$em->flush();
			$message = 'L’opération s’est bien déroulée!';
			$this->get('session')->getFlashBag()->add('checklistSuccess', $message);
			return $this->redirect($this->generateUrl('checklist_checklist_editer',
				array('id' => $checklist->getId() )
			));
		}
        return $this->render('ChecklistBundle:checklist:ajouterChecklist.html.twig', array(
            'form' => $form->createView(),
			'title' => 'Ajouter checklist',
			'message' => $message,
        	'estimee' => $estimee,
        	'parCaisse' => $parCaisse
        ));
	}

    /**
     * @Route("/checklist/{id}", name="checklist_checklist_checklist")
     */
	public function checklistAction($id)
	{
		$checklist = $this->getDoctrine()
			->getRepository('ChecklistBundle:Checklist')
			->find($id);
		if (!$checklist) {
			throw $this->createNotFoundException(
				'Trouvé aucune checklist pour id '.$id
			);
		}
		$pointsDeControle = $checklist->getPointsDeControle();
		$estimee = new DateTime(date('Y-m-d 00:00:00'));
		$parCaisse = new DateTime(date('Y-m-d 00:00:00'));

        return $this->render('ChecklistBundle:checklist:checklist.html.twig',
			array(
				'checklist' => $checklist,
				'title' => 'Checklist ' . $checklist->getNom(),
				'estimee' => $estimee,
				'parCaisse' => $parCaisse,
			)
		);
	}

    /**
     * @Route("/checklists", name="checklist_checklist_checklists")
     */
    public function checklistsAction(Request $request)
    {
    	$form = $this->createFormBuilder()
    	->add('afficheDesactive', 'checkbox', array(
    			'label'     => 'Afficher les checkllists désactivées ?',
    			'required' => false
    	))
    	->add('save', 'submit', array(
    			'label' => 'rechercher',
    			'attr' => array('class' => 'btn btn-info', 'aria-label' => 'Left Align', 'type' => 'button'),
    	))
    	->getForm();
    	$form->handleRequest($request);
    	if($form["afficheDesactive"]->getData()){
    		$checklists = $this->getDoctrine()
    		->getRepository('ChecklistBundle:Checklist')
    		->findAll();
    	} else {
    		$checklists = $this->getDoctrine()
    		->getRepository('ChecklistBundle:Checklist')
    		->findBy( array('desactive' => false));
    	}
		$temps = array();
		$caisse = array();
		foreach($checklists as $checklist){
			$pointsDeControle = $checklist->getPointsDeControle();
			$estimee = new DateTime(date('Y-m-d 00:00:00'));
			$parCaisse = new DateTime(date('Y-m-d 00:00:00'));
			foreach ($pointsDeControle as $pointDeControle) {
				$temmporary = new DateTime(date('Y-m-d 00:00:00'));
				$estimee->add($temmporary->diff($pointDeControle->getDuree()));
				if ($pointDeControle->getCaisses()){
					$parCaisse->add($temmporary->diff($pointDeControle->getDureeParCaisse()));
				}
			}
			$temps[$checklist->getId()] = $estimee;
			$caisse[$checklist->getId()] = $parCaisse;
		}
        return $this->render('ChecklistBundle:checklist:checklists.html.twig',
				array(
					'form' => $form->createView(),
					'checklists' => $checklists,
					'temps' => $temps,
					'caisse' => $caisse,
				)
		);
    }
}