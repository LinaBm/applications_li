<?php

namespace ChecklistBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ChecklistBundle\Entity\PointDeControle;
use Symfony\Component\HttpFoundation\Request;
use ChecklistBundle\Form\PointDeControleType;

class PointDeControleController extends Controller
{
    /**
     * @Route("/point-de-controle/editer/{id}", name="checklist_point_de_controle_editer")
     */
	public function editerPointDeControleAction($id, Request $request, $message = '')
	{
		$em = $this->getDoctrine()->getManager();
		$pointDeControle = $em->getRepository('ChecklistBundle:PointDeControle')->find($id);
		if (empty($pointDeControle)) {
			throw $this->createNotFoundException(
				'Aucun point de Contrôle trouvé.'
			);
		}
		$form= $this->createForm(new PointDeControleType, $pointDeControle);
		$form->handleRequest($request);
		if ($form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$em->persist($pointDeControle);
			$em->flush();
			$message = 'L’opération s’est bien déroulée!';
			$this->get('session')->getFlashBag()->add('pointSuccess', $message);
			return $this->redirect($this->generateUrl('checklist_point_de_controle_editer',
				array( 'id' => $id )
			));
		}
        return $this->render('ChecklistBundle:pointDeControle:ajouterPointDeControle.html.twig', array(
            'form' => $form->createView(), 'title' => 'Editer point de contrôle ' . $pointDeControle->getNom(),
			'action' => ($pointDeControle->getType() === 'Contrôles Visuels' ? true : false),
			'message' => $message
        ));
	}

    /**
     * @Route("/point-de-controle/desactiver/{id}", name="checklist_point_de_controle_desactiver")
     */
	public function desactiverPointDeControleAction($id)
	{
		$pointDeControle = $this->getDoctrine()
			->getRepository('ChecklistBundle:PointDeControle')
			->find($id);
		if (!$pointDeControle) {
			throw $this->createNotFoundException(
				'Trouvée aucune pointDeControle pour id '.$id
			);
		}
		$em = $this->getDoctrine()->getManager();
		$pointDeControle->setDesactive(!$pointDeControle->getDesactive());
		$em->persist($pointDeControle);
		$em->flush();
		return $this->redirect($this->generateUrl('checklist_point_de_controle_points_de_controle'));
	}

    /**
     * @Route("/point-de-controle/ajouter", name="checklist_point_de_controle_ajouter")
     */
	public function ajouterPointDeControleAction(Request $request, $message = '')
	{
        $pointDeControle = new PointDeControle();
        $form= $this->createForm(new PointDeControleType, $pointDeControle);
		$form->handleRequest($request);
		if ($form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$pointDeControle->setDesactive(0);
			$em->persist($pointDeControle);
			$em->flush();
			$message = 'L’opération s’est bien déroulée!';
			$this->get('session')->getFlashBag()->add('pointSuccess', $message);
			return $this->redirect($this->generateUrl('checklist_point_de_controle_editer',
					array( 'id' => $pointDeControle->getId() )
			));
		}
        return $this->render('ChecklistBundle:pointDeControle:ajouterPointDeControle.html.twig', array(
            'form' => $form->createView(),
			'title' => 'Ajouter point de contrôle',
			'action' => true,
			'message' => $message
        ));
	}

    /**
     * @Route("/point-de-controle/{id}", name="checklist_point_de_controle_point_de_controle")
     */
	public function pointDeControleAction($id)
	{
		$pointDeControle = $this->getDoctrine()
			->getRepository('ChecklistBundle:PointDeControle')
			->find($id);
		if (!$pointDeControle) {
			throw $this->createNotFoundException(
				'Trouvé aucune pointDeControle pour id '.$id
			);
		}
        return $this->render('ChecklistBundle:pointDeControle:pointDeControle.html.twig', array(
			'pointDeControle' => $pointDeControle, 'title' => 'Point de contrôle ' . $pointDeControle->getNom()
		));
	}

    /**
     * @Route("/point-de-controle-resultats/{id}", name="checklist_point_de_controle_resultats")
     */
	public function pointDeControleResultatsAction($id)
	{
		$pointDeControle = $this->getDoctrine()
			->getRepository('ChecklistBundle:PointDeControle')
			->find($id);
		if (!$pointDeControle) {
			throw $this->createNotFoundException(
				'Trouvé aucune point de controle pour id '.$id
			);
		} else {
			$listeResultats = $this->getDoctrine()
			->getRepository('ChecklistBundle:Resultat')
			->findBy(
					array('pointsDeControle' => $id)
			);
		}
        return $this->render('ChecklistBundle:pointDeControle:resultatPointDeControle.html.twig', array(
			'pointDeControle' => $pointDeControle, 'title' => 'Point de contrôle ' . $pointDeControle->getNom(),
        	'listeResultats' => $listeResultats
		));
	}

    /**
     * @Route("/points-de-controle", name="checklist_point_de_controle_points_de_controle")
     */
    public function pointsDeControleAction(Request $request)
    {
    	$form = $this->createFormBuilder()
			->add('afficheDesactive', 'checkbox', array(
				'label'     => 'Afficher les points de contrôle désactivés ?',
				'required' => false
			))
			->add('save', 'submit', array(
				'label' => 'rechercher',
				'attr' => array('class' => 'btn btn-info', 'aria-label' => 'Left Align', 'type' => 'button'),
			))
			->getForm();
    	$form->handleRequest($request);
    	if($form["afficheDesactive"]->getData()){
    		$pointsDeControle = $this->getDoctrine()
    		->getRepository('ChecklistBundle:PointDeControle')
    		->findAll();
    	} else {
    		$pointsDeControle = $this->getDoctrine()
    		->getRepository('ChecklistBundle:PointDeControle')
    		->findBy( array('desactive' => false));
    	}
        return $this->render('ChecklistBundle:pointDeControle:pointsDeControle.html.twig', array(
        		'pointsDeControle' => $pointsDeControle,
        		'form' => $form->createView()
        ));
    }
}