<?php

namespace ChecklistBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class AccueilController extends Controller
{		
    /**
     * @Route("/", name="checklist_homepage")
     */
	public function homepageAction()
	{	
		$user=$this->getUser();
		if ($user->isGranted("ROLE_CHK_ADMIN")){
			 return $this->render('ChecklistBundle::accueil.html.twig');
		} else if ($user->isGranted("ROLE_CHK_TECH"))
		{
			return $this->redirect($this->generateUrl('checklist_planning_planning'));
		} else
		 {
			return $this->redirect($this->generateUrl('checklist_magasin_magasins_hotline'));
		}
	}
	
    /**
     * @Route("/accueil", name="checklist_accueil")
     */
	public function accueilAction()
	{
        return $this->homepageAction();
	}
	
	// TEST ROUTE FOR ACL
    /**
     * @Route("/admin")
     */
    public function adminAction()
    {
        return new Response('Admin page!');
    }
}