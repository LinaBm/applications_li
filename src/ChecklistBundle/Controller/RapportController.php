<?php

namespace ChecklistBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class RapportController extends Controller
{
    /**
     * @Route("/rapport/{id}", name="checklist_rapport_rapport")
     */
	public function rapportAction($id)
	{
		$rapport = $this->getDoctrine()
			->getRepository('ChecklistBundle:Resultat')
			->findBy(array('visites' => $id));
		if (!$rapport) {
			throw $this->createNotFoundException(
				'Trouvé aucun rapport'
			);
		}
        return $this->render('ChecklistBundle:rapport:rapport.html.twig', array('rapport' => $rapport));
	}

    /**
     * @Route("/rapports", name="checklist_rapport_rapports")
     */
    public function rapportsAction()
    {
		$rapports = $this->getDoctrine()
			->getRepository('ChecklistBundle:Visites')
			->getRapports();
		if (!$rapports) {
			throw $this->createNotFoundException(
				'Trouvé aucun rapport.'
			);
		}
        return $this->render('ChecklistBundle:rapport:rapports.html.twig', array('rapports' => $rapports));
    }

    /**
     * @Route("/rapportHotline/{id}/{page}", name="checklist_rapport_rapport_hotline")
     */
    public function rapportHotlineAction($id,$page)
    {
    	$visite = $this->getDoctrine()
    	->getRepository('ChecklistBundle:Visites')
    	->getVisitesMagasin($id, $page);
    	if (count($visite) == 0) {
			throw $this->createNotFoundException(
				'Trouvé aucune rapport'
			);
		}
		$rapport = $this->getDoctrine()
    	->getRepository('ChecklistBundle:Visites')
    	->getRapport( $visite->getIterator()[0]->getId());
    	return $this->render('ChecklistBundle:rapport:rapportHotline.html.twig', array(
			'visite' => $visite->getIterator()[0],
			'rapport' => $rapport,
			'page' => $page,
			'nbPages' => count($visite)
    	));
    }
}