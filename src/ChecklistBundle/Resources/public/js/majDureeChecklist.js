$(document).ready(function() {
	$(".duree-update").change(function() {
		var checkpoints=$(".duree-update").val();
		$.ajax({
			dataType: "json",
	        type: "POST",
	  		url: url_get_duree,
	        data: { checkpoints : checkpoints},  
	        async:false,
	        cache: false,
	        error: function(jqXHR,textStatus, errorThrown ) {
	        	alert(jqXHR.responseText); return false;
	        },
	        success: function(duree){
	        	var heureEstimee  = duree.estimee.date.substring(11,13);
	        	var minEstimee  = duree.estimee.date.substring(14,16);
	        	var heureParCaisse  = duree.parCaisse.date.substring(11,13);
	        	var minParCaisse  = duree.parCaisse.date.substring(14,16);
	        	$("#duree").html(heureEstimee + "h" + minEstimee + " + " + heureParCaisse  + "h" + minParCaisse + " par caisse.");
	        }                                                                            
		});
	});
});