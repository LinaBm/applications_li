

$(document).ready(function() {
	hideShowDureeParCaisse();
	
	$('#checklist_point_de_controle_caisses').change(function() {
		hideShowDureeParCaisse();
	});
});

function hideShowDureeParCaisse(){
	if  ($('#checklist_point_de_controle_caisses').is(':checked')){
		$('.ligneDureeParCaisse').show();
	} else {
		$('.ligneDureeParCaisse').hide();
	}
}