<?php

namespace ChecklistBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;	
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use DateTime;


class VisitesType extends AbstractType
{
	private $em;
	
	public function __construct($em){
		$this->em = $em;
	}
	
	
	
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->add('date', 'date', array(
					'widget' => 'single_text',
					'format' => 'dd/MM/yyyy',
					'invalid_message' => 'la date de recette n\'est pas valide',
					'constraints' => array(
						new GreaterThanOrEqual(array('value' => new DateTime(date('Y-m-d 00:00:00'))))
					))
				)
	
			->add(
					'checklists',
					'entity',
					array(
							'class' => 'ChecklistBundle\Entity\Checklist',
							'property' => 'nom',
							'multiple' => false,
							'expanded' => false,
							'required' => true,
							'query_builder' => function(\Doctrine\ORM\EntityRepository $er) {
								return $er->createQueryBuilder('c')
								->where("c.desactive = FALSE");
							},
							'label' => 'Checklist',
							'constraints' => array(
									new NotBlank(array('message' => 'Le nom ne peut pas être vide!'))
							)
				)
			)
			
			->add(
					'technicien',
					'entity',
					array(
							'class' => 'UserUserBundle:User',
							'property' => 'displayname',
							'multiple' => false,
							'expanded' => false,
							'required' => true,
							'label' => 'Utilisateur',
							'query_builder'=> $this->em->getRepository('UserUserBundle:User')
									->getByRole('ROLE_CHK_TECH'),
							'constraints' => array(
									new NotBlank(array('message' => 'Vous devez renseigner le nom du technicien !'))
							)
					)
			)			

            ->add('save', 'submit', array(
				'label' => 'Enregistrer', 
				'attr' => array('class' => 'btn btn-info', 'aria-label' => 'Left Align', 'type' => 'button'),
			));
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'ChecklistBundle\Entity\Visites'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'checklist_visites';
	}
}

