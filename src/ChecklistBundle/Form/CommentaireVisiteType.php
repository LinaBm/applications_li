<?php

namespace ChecklistBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CommentaireVisiteType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('commentaire', 'textarea', array(
					'label' => 'Commentaire',
					'required' => false,
			))
			->add('save', 'submit', array(
					'label' => 'Enregistrer',
					'attr' => array('class' => 'btn btn-info', 'aria-label' => 'Left Align', 'type' => 'button'),
			))
			
			->getForm();
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
				'data_class' => 'ChecklistBundle\Entity\Visites'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'commentaire_visite';
	}
}
