<?php

namespace ChecklistBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;	
use Symfony\Component\Validator\Constraints\File;

class InstallationDeLogicielType extends AbstractType
{	
	private $code = 1;
	
    public function __construct($code){
        $this->code = $code;
    }
	
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{			
		if( ($options['data']->getId() !== NULL && $options['data']->getNumeroDuCaisse() !== NULL)
				|| $this->code > 1 ){
			$builder
				->add('numeroDuCaisse', 'text', array(
					'required'  => false,
					'label' => "Numéro de la caisse"
				));
		}
		$builder
			->add('flagAccomplie', 'checkbox', array(
//				'attr' => array('checked'   => 'checked'),
			))
			->add('sources', 'checkbox', array(
				'label'     => 'Sources?',
				'required'  => false,
//				'attr' => array('checked'   => 'checked'),
			))	
			->add('fichierUn', 'file', array(
//				'data_class' => '\Symfony\Component\HttpFoundation\File\File',
				'data_class' => '\Symfony\Component\HttpFoundation\File\UploadedFile',
//                'data_class' => null,
				'property_path' => 'fichierUn',
				'required'  => false,
				'mapped'=>false,
				'constraints' => array(
					new File(array(
						'maxSize' => "128k",
						'maxSizeMessage' => "Maximum 128k",
//							'mimeTypes' => array("application/pdf", "application/x-pdf"),
//							'mimeTypesMessage' => "Please upload a valid PDF"
					))
				),
			))
			->add('fichierDeux', 'file', array(
//				'data_class' => '\Symfony\Component\HttpFoundation\File\File',
				'data_class' => '\Symfony\Component\HttpFoundation\File\UploadedFile',
//                'data_class' => null,
				'property_path' => 'fichierDeux',
				'required'  => false,
				'mapped'=>false,
				'constraints' => array(
					new File(array(
						'maxSize' => "128k",
						'maxSizeMessage' => "Maximum 128k",
//							'mimeTypes' => array("application/pdf", "application/x-pdf"),
//							'mimeTypesMessage' => "Please upload a valid PDF"
					))
				),
			))	
			->add('reponse', 'choice', array(
				'choices'   => array(
					'1' => 'Oui',
					'0' => 'Non',
				),
//					'data'=>'0',
//				'required'  => true,
//				'constraints' => array(
//					new NotBlank(array('message' => 'Le reponse ne peut pas être vide!'))
//				),
				'multiple' => false,
				'expanded' => true,
			));
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'ChecklistBundle\Entity\Resultat'
		));
	}

	/**
	 * @return string
	 */
	public function getName() {
		return 'installation_de_logiciel';
	}

}