<?php

namespace ChecklistBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use ChecklistBundle\Entity\Resultat;
use ChecklistBundle\Entity\PointDeControle;
use Symfony\Component\Validator\Constraints\GreaterThan;


class ResultatsType extends AbstractType
{	
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{	
		if(isset($options['data']['noResults'])){
			$nbForms = $options['data']['noResults'];
		} else {
			$nbForms = sizeof($options['data']);
		}		
		$i=0;
		foreach ($options['data'] as $resultat){			
			if(is_object($resultat)){
				switch ($resultat->getPointsDeControle()->getType()) {
					case PointDeControle::TYPE_VISUEL:
						$builder ->add('resultat_'.$i, new ControleVisuelType($nbForms), array('data' => $resultat));
						break;
					case PointDeControle::TYPE_ACTION:
						$builder ->add('resultat_'.$i, new ActionAMenerType($nbForms), array('data' => $resultat));
						break;
					case PointDeControle::TYPE_INSTALLATION_LOGICIEL:
						$builder ->add('resultat_'.$i, new InstallationDeLogicielType($nbForms), array('data' => $resultat));
						break;
					case PointDeControle::TYPE_RETOUR_INFO:	
						$builder ->add('resultat_'.$i, new ControleAvecRetourDeInfoType($nbForms), array('data' => $resultat));
						break;
				}
				$i++;
			} 
		}		
		$builder
		
			->add('email', 'checkbox', array(
					'label'     => 'Envoyer les résultats par email au manager?',
					'required'  => false,
			))
			->add('save', 'submit', array(
					'label' => 'Enregistrer',
					'attr' => array('class' => 'btn btn-info', 'aria-label' => 'Left Align', 'type' => 'button'),
			))
			->add('finaliser', 'submit', array(
				'label' => 'Finaliser',
				'attr' => array('class' => 'btn btn-success', 'aria-label' => 'Left Align', 'type' => 'button'),
			))

			->getForm();
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => null
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'resultats_type';
	}
}