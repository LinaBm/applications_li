<?php

namespace Direction\DirectionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Suivi\EtudesBundle\Entity\DirectionDir;
use Symfony\Component\HttpFoundation\JsonResponse;

class DirectionController extends Controller
{
    
    public function  selectionnerAction()
    {
    	$form = $this->createForm(new DemandeType);
    	 

    	return $this->render('DirectionDirectionBundle:Direction:selectionner.html.twig', array(
            'listeDirections' => $listeDirections
    	));
    }
    
    public function getservicesAction()
    {
    	$request = $this->container->get('request');
    	$direction='';
    	if($request->isXmlHttpRequest())
  	 	{
    		$direction = $request->request->get('direction');
  	 	} else {
  	 		$direction = 1;
  	 	}	
  	 	if($direction != ''){
  	 		$listeService = $this->getDoctrine() ->getManager('archi') 
  	 		->getRepository('DirectionDirectionBundle:ServiceSrv') 
  	 		->findBy(array('dir' => $direction));
  	 		$data= array();
  	 		$i=0;
  	 		foreach ($listeService as $service){
  	 			$data[$i]=array("srv_id" =>$service->getSrvId(),"srv_libelle"=>$service->getSrvLibelle());
  	 			$i++;
  	 		}
  	 		//var_dump($data);
  	 		return new JsonResponse($data);
  	 		
  	 	//	return new JsonResponse(array('services' => $arrayService));
  	 	//	return $this->render('DirectionDirectionBundle:Direction:selectionner.html.twig', array(
  	 		//		'services'			  => $data
  	 	//	));

  	 	}
    }
    
}
