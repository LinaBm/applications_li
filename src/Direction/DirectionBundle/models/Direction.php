<?php

namespace Direction\DirectionBundle\models;


class Direction extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->hasColumn('dir_id', 'int', 11);
        $this->hasColumn('dir_libelle', 'varchar', 45);
    }
}

?>