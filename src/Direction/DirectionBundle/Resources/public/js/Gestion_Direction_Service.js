/**
 * FICHIER JQUERY POUR LA GESTION DU LIEN DIRECTION -SERVICEE
 * 
 * @author lolive_mi
 * @date : 28.10.2014
 * 
 */

function displayServices() {
	
	$(".ld-service").empty();
	$(".ld-service").removeAttr('disabled');
	var direction = $('.ld-direction option:selected').val();
	if (direction != "") {
		var type = "service";
		$.ajax({                                                                                                        
			dataType: "json",
			type: "POST",
			url: url_gestion_dir_srv,
			data: { type: type, direction: direction},  
			async:false,
			cache: false,
			error: function(jqXHR,textStatus, errorThrown ) {
				alert(jqXHR.responseText);
			},
			success: function(services){
				if (services != null && services.length != 0) {
					$(".ld-service").append('<option value=""></option');
				for(var i=0;i<services.length;i++) {
					$(".ld-service").append('<option value='+services[i].srv_id+'>'+services[i].srv_libelle+'</option');
					}
				}
				else {
					$(".ld-service").append('<option value="0">Pas de service</option');
					$(".ld-service").attr('disabled', true);
				}
			}
		});
	}
	else {
		$(".ld-service").append('<option value="">-- Choisir une direction --</option');
		$(".ld-service").attr('disabled', 'disabled');
	}
}


//Initialisation de l'affichage
$(document).ready(function() { 	
	displayServices();
	if($('#demande_direction').length >0){
		var id_direction=($('#demande_direction').html()).trim();
		$('.ld-direction').val(id_direction);
	}
	
	$('.ld-direction').on("change", function() {
	    displayServices();
	});
	
	if($('#demande_service').length >0){
		$('.ld-service').disabled=false;
	    displayServices();
		var id_service=($('#demande_service').html()).trim();
		$('.ld-service').val(id_service);
	}
});