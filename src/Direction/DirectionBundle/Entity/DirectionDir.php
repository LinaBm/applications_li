<?php

namespace Direction\DirectionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DirectionDir
 *
 * @ORM\Table(name="direction_dir")
 * @ORM\Entity(repositoryClass="Direction\DirectionBundle\Entity\DirectionDirRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class DirectionDir
{
    /**
     * @var integer
     *
     * @ORM\Column(name="dir_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $dirId;

    /**
     * @var string
     *
     * @ORM\Column(name="dir_libelle", type="string", length=45, nullable=false)
     */
    private $dirLibelle;



    /**
     * Get dirId
     *
     * @return integer 
     */
    public function getDirId()
    {
        return $this->dirId;
    }

    /**
     * Set dirLibelle
     *
     * @param string $dirLibelle
     * @return DirectionDir
     */
    public function setDirLibelle($dirLibelle)
    {
        $this->dirLibelle = $dirLibelle;
    
        return $this;
    }

    /**
     * Get dirLibelle
     *
     * @return string 
     */
    public function getDirLibelle()
    {
        return $this->dirLibelle;
    }
    
    public function getService(){
    	return $this->getDoctrine()->getRepository('DirectionDirectionBundle:ServiceSrv')->getServiceByDirection($this->getDirId());
    }
    
}
