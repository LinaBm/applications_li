<?php

namespace Direction\DirectionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PolePol
 *
 * @ORM\Table(name="pole_pol", indexes={@ORM\Index(name="fk_pole_pol_service_srv1_idx", columns={"srv_id"})})
 * @ORM\Entity
 */
class PolePol
{
    /**
     * @var integer
     *
     * @ORM\Column(name="pol_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $polId;

    /**
     * @var string
     *
     * @ORM\Column(name="pol_libelle", type="string", length=45, nullable=false)
     */
    private $polLibelle;

    /**
     * @var \ServiceSrv
     *
     * @ORM\ManyToOne(targetEntity="ServiceSrv")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="srv_id", referencedColumnName="srv_id")
     * })
     */
    private $srv;



    /**
     * Get polId
     *
     * @return integer 
     */
    public function getPolId()
    {
        return $this->polId;
    }

    /**
     * Set polLibelle
     *
     * @param string $polLibelle
     * @return PolePol
     */
    public function setPolLibelle($polLibelle)
    {
        $this->polLibelle = $polLibelle;
    
        return $this;
    }

    /**
     * Get polLibelle
     *
     * @return string 
     */
    public function getPolLibelle()
    {
        return $this->polLibelle;
    }

    /**
     * Set srv
     *
     * @param \Direction\DirectionBundle\Entity\ServiceSrv $srv
     * @return PolePol
     */
    public function setSrv(\Direction\DirectionBundle\Entity\ServiceSrv $srv = null)
    {
        $this->srv = $srv;
    
        return $this;
    }

    /**
     * Get srv
     *
     * @return \Direction\DirectionBundle\Entity\ServiceSrv 
     */
    public function getSrv()
    {
        return $this->srv;
    }
}
