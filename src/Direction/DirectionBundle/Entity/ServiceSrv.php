<?php

namespace Direction\DirectionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ServiceSrv
 *
 * @ORM\Table(name="service_srv", indexes={@ORM\Index(name="fk_service_srv_direction_dir1_idx", columns={"dir_id"})})
 * @ORM\Entity(repositoryClass="Direction\DirectionBundle\Entity\ServiceSrvRepository")
 */
class ServiceSrv
{
    /**
     * @var integer
     *
     * @ORM\Column(name="srv_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $srvId;

    /**
     * @var string
     *
     * @ORM\Column(name="srv_libelle", type="string", length=45, nullable=false)
     */
    private $srvLibelle;

    /**
     * @var \DirectionDir
     *
     * @ORM\ManyToOne(targetEntity="DirectionDir")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dir_id", referencedColumnName="dir_id")
     * })
     */
    private $dir;



    /**
     * Get srvId
     *
     * @return integer 
     */
    public function getSrvId()
    {
        return $this->srvId;
    }

    /**
     * Set srvLibelle
     *
     * @param string $srvLibelle
     * @return ServiceSrv
     */
    public function setSrvLibelle($srvLibelle)
    {
        $this->srvLibelle = $srvLibelle;
    
        return $this;
    }

    /**
     * Get srvLibelle
     *
     * @return string 
     */
    public function getSrvLibelle()
    {
        return $this->srvLibelle;
    }

    /**
     * Set dir
     *
     * @param \Direction\DirectionBundle\Entity\DirectionDir $dir
     * @return ServiceSrv
     */
    public function setDir(\Direction\DirectionBundle\Entity\DirectionDir $dir = null)
    {
        $this->dir = $dir;
    
        return $this;
    }

    /**
     * Get dir
     *
     * @return \Direction\DirectionBundle\Entity\DirectionDir 
     */
    public function getDir()
    {
        return $this->dir;
    }
}
