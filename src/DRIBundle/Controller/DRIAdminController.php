<?php

namespace DRIBundle\Controller;

use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use DRIBundle\Entity\User;
use DRIBundle\Entity\Status;
use DRIBundle\Entity\Computedstatus;
use DRIBundle\Entity\Materials;
use DRIBundle\Entity\Computers;
use DRIBundle\Entity\Phones;
use DRIBundle\Entity\Peripherals;
use DRIBundle\Entity\Printers;
use DRIBundle\Entity\Monitors;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class DRIAdminController extends Controller {

    public function getConn() {
        return $this->get('database_connection');
    }

    public function getConnClient() {
        return $this->get('doctrine.dbal.client_connection');
    }

    public function indexAction() {

        //return new Response("<p>Ok</p>");
    }

//    public function getnouveausersAction(Request $request){
//         
//         
////            $statement = $this->getConn()->executeQuery('SELECT u.*, s.status_label, cs.*  FROM user u
////                                                         INNER JOIN dri_computedstatus cs
////                                                         ON u.id = cs.user_id
////                                                         INNER JOIN dri_status s
////                                                         ON cs.computedstatus = s.id
////                                                         WHERE cs.computedstatus =?',array(1));    
////
////            $results = $statement->fetchAll();
//            
//                        
//                   // $cstatus = new Computedtatus();
//
//                    //$user = $em->getRepository("DRIBundle:User")->find($request->request->get('userid')[$i]);
//          //$results = $em->getRepository("DRIBundle:User")->findBy($cstatuses);    
//         
//             $em = $this->getDoctrine()->getManager();
//             $results = array();
//             $cstatuses = $em->getRepository("DRIBundle:Computedstatus")->findBy(array("computedstatus"=>1));  
//               
//            foreach($cstatuses as $c){
//                echo $c->getUser()->getUsername();
//                $results[] = $em->getRepository("DRIBundle:User")->find($c->getUser()->getId()); 
//            }
//                    
//            // var_dump($results);die();
//            $paginator  = $this->get('knp_paginator');
//            $users = $paginator->paginate(
//                            $results, 
//                            $request->query->getInt('page', 1)/*page number*/,
//                            25/*limit per page*/
//                        );
//            
//            
//             // var_dump($users);die();
//            $users->setTemplate('DRIBundle:Pagination:twitter_bootstrap_v3_pagination.html.php ');
// 
//
//
//    // parameters to template
//    return $this->render('DRIBundle:DRIAdmin:listusers.html.php', array('users'=>$users,
//                                                                       'user'=>$this->getUser(),
//                                                                        'em' =>$this->getDoctrine()->getManager(),
//                                                                        'conn'=>$this->getConnClient(),
//                                                                        'lconn' => $this->getConn()
//                                                                    ));
//    }
//    
//    
//    //Return users en attente (3) 
//    public function getenattenteusersAction(Request $request){
//        
//           $em = $this->getDoctrine()->getManager();
//           $results = array();
//            
//           $cstatuses = $em->getRepository("DRIBundle:Computedstatus")->findBy(array("computedstatus"=>3));  
//               
//            foreach($cstatuses as $c){
//                echo $c->getUser()->getUsername();
//                $results[] = $em->getRepository("DRIBundle:User")->find($c->getUser()->getId()); 
//            }
//                    
//            // var_dump($results);die();
//            $paginator  = $this->get('knp_paginator');
//            $users = $paginator->paginate(
//                            $results, 
//                            $request->query->getInt('page', 1)/*page number*/,
//                            25/*limit per page*/
//                        );
//            
//            
//             // var_dump($users);die();
//            $users->setTemplate('DRIBundle:Pagination:twitter_bootstrap_v3_pagination.html.php ');
// 
//
//
//    // parameters to template
//    return $this->render('DRIBundle:DRIAdmin:listusers.html.php', array('users'=>$users,
//                                                                       'user'=>$this->getUser(),
//                                                                        'em' =>$this->getDoctrine()->getManager(),
//                                                                        'conn'=>$this->getConnClient(),
//                                                                        'lconn' => $this->getConn()
//                                                                    ));
//    }
//    
//    
//     //Return users litige (2) 
//    public function getlitigeusersAction(Request $request){
//        
//            $em = $this->getDoctrine()->getManager();
//            $results = array();
//             
//            $cstatuses = $em->getRepository("DRIBundle:Computedstatus")->findBy(array("computedstatus"=>2));  
//               
//            foreach($cstatuses as $c){
//                echo $c->getUser()->getUsername();
//                $results[] = $em->getRepository("DRIBundle:User")->find($c->getUser()->getId()); 
//            }
//                    
//            // var_dump($results);die();
//            $paginator  = $this->get('knp_paginator');
//            $users = $paginator->paginate(
//                            $results, 
//                            $request->query->getInt('page', 1)/*page number*/,
//                            25/*limit per page*/
//                        );
//            
//            
//             // var_dump($users);die();
//            $users->setTemplate('DRIBundle:Pagination:twitter_bootstrap_v3_pagination.html.php ');
// 
//
//
//    // parameters to template
//    return $this->render('DRIBundle:DRIAdmin:listusers.html.php', array('users'=>$users,
//                                                                       'user'=>$this->getUser(),
//                                                                        'em' =>$this->getDoctrine()->getManager(),
//                                                                        'conn'=>$this->getConnClient(),
//                                                                        'lconn' => $this->getConn()
//                                                                    ));
//    }
//    
//    public function getermineusersAction(Request $request){
//            
//            $em = $this->getDoctrine()->getManager();
//            $results = array();
//            
//            $cstatuses = $em->getRepository("DRIBundle:Computedstatus")->findBy(array("computedstatus"=>4));  
//               
//            foreach($cstatuses as $c){
//                echo $c->getUser()->getUsername();
//                $results[] = $em->getRepository("DRIBundle:User")->find($c->getUser()->getId()); 
//            }
//                    
//            // var_dump($results);die();
//            $paginator  = $this->get('knp_paginator');
//            $users = $paginator->paginate(
//                            $results, 
//                            $request->query->getInt('page', 1)/*page number*/,
//                            25/*limit per page*/
//                        );
//            
//            
//             // var_dump($users);die();
//            $users->setTemplate('DRIBundle:Pagination:twitter_bootstrap_v3_pagination.html.php ');
// 
//
//
//    // parameters to template
//    return $this->render('DRIBundle:DRIAdmin:listusers.html.php', array('users'=>$users,
//                                                                       'user'=>$this->getUser(),
//                                                                        'em' =>$this->getDoctrine()->getManager(),
//                                                                        'conn'=>$this->getConnClient(),
//                                                                        'lconn' => $this->getConn()
//                                                                    ));
//    }


    public function listusersAction(Request $request) {

        // var_dump($request->request->all());die();

        $em = $this->getDoctrine()->getManager();
        $results = array();
        $flag = 0;




        if ($request->request->get('nouveausers')) {
            $flag = 1;
        } elseif ($request->request->get('enattenteusers')) {
            $flag = 3;
        } elseif ($request->request->get('litigeusers')) {
            $flag = 2;
        } elseif ($request->request->get('termineusers')) {
            $flag = 4;
        }

        if ($flag == 1 || $flag == 2 || $flag == 3 || $flag == 4) {

            $cstatuses = $em->getRepository("DRIBundle:Computedstatus")
                    ->findBy(array("computedstatus" => $flag), array('id' => 'DESC'));

            foreach ($cstatuses as $c) {

                $results[] = $em->getRepository("DRIBundle:User")->find($c->getUser()->getId());
            }
        }



        if ($request->request->get('searchbyuname')) {

            $nume = $request->request->get('cautareuname');

            $results = $this->getDoctrine()
                    ->getRepository('DRIBundle:User')
                    ->findBy(array('username' => $nume));

            $flag = 5;
        }

        if ($request->request->get('cautaresocietate')) {

            $societate = $request->request->get('cautaresocietate');

            $repository = $em->getRepository('DRIBundle:User');
            $users = $repository->findUsersOnSociete($societate, $this->getConnClient());


            if (count($users)) {

                foreach ($users as $u) {
                    $result = $this->getDoctrine()
                            ->getRepository('DRIBundle:User')
                            ->findBy(array('username' => $u['username']));

                    if (count($result)) {
                        foreach ($result as $res) {

                            $cstat = $this->getDoctrine()
                                    ->getRepository('DRIBundle:Computedstatus')
                                    ->find($res->getId());

                            if (count($cstat)) {
                                $results[] = $res;
                            }
                        }
                    }
                }
            }


            $flag = 6;
        }

        if ($request->request->get('searchbydate')) {

            $date1 = $request->request->get('usersbydate1');
            $date2 = $request->request->get('usersbydate2');

            //$repository = $em->getRepository('DRIBundle:Materials');    

            $dt1 = new \DateTime($date1);
            $dt2 = new \DateTime($date2);


            //$materials = $repository->findBy(array("restitutiondate"=>$dt1));


            $query = $em->createQuery("SELECT m 
                                          FROM DRIBundle\Entity\Materials m 
                                          WHERE m.restitutiondate BETWEEN ?1 AND ?2 
                                          ORDER BY m.id DESC");
            $query->setParameter(1, $dt1);
            $query->setParameter(2, $dt2);
            $materials = $query->execute();
            // var_dump($result);die();

            $unames = array();
            if (count($materials)) {
                foreach ($materials as $material) {
                    //echo $material->getUsername();
                    $unames[] = $material->getUsername();
                }
            }

            $unames = array_unique($unames);


            //$users = $repository->findUsersOnSociete($societate, $this->getConnClient());
            // var_dump($users);die();
            if (count($unames)) {
                foreach ($unames as $u) {
                    $result = $this->getDoctrine()
                            ->getRepository('DRIBundle:User')
                            ->findBy(array('username' => $u));


                    if (count($result)) {
                        foreach ($result as $res)
                            $results[] = $res;
                    }
                }
            }

            //var_dump($results);die();
            $flag = 7;
        }

        if ($flag == 0) {

            $query = $em->createQuery("SELECT c 
                                       FROM DRIBundle\Entity\Computedstatus c ORDER BY c.id DESC");

            $cstatuses = $query->execute();

            foreach ($cstatuses as $res) {
                $result = $this->getDoctrine()
                        ->getRepository('DRIBundle:User')
                        ->findBy(array("id" => $res->getUserId()));

                if (count($result)) {
                    foreach ($result as $res)
                        $results[] = $res;
                }
            }
        }


        $userswithcstatuses = array();

        $repository = $em->getRepository('DRIBundle:Computedstatus');
        $cstatuses = $repository->findAll();

        foreach ($cstatuses as $res) {
            $result = $em->getRepository('DRIBundle:User')
                    ->findBy(array("id" => $res->getUserId()));

            if (count($result)) {
                foreach ($result as $res)
                    $userswithcstatuses[] = $res;
            }
        }


        $paginator = $this->get('knp_paginator');
        $users = $paginator->paginate(
                $results, $request->query->getInt('page', 1)/* page number */, 25/* limit per page */
        );

//var_dump( $users);die();

        $users->setTemplate('DRIBundle:Pagination:twitter_bootstrap_v3_pagination.html.php ');



        // parameters to template
        return $this->render('DRIBundle:DRIAdmin:listusers.html.php', array(
                    'users' => $results,
                    //'users'=>$users,
                    'user' => $this->getUser(),
                    'userswithcstatuses' => $userswithcstatuses,
                    'em' => $this->getDoctrine()->getManager(),
                    'conn' => $this->getConnClient(),
                    'lconn' => $this->getConn()
        ));
    }

    public function listuserdetailsAction($id) {
        $materials = array();
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository("DRIBundle:User")->find($id);
        $materials = $user->getMaterials();
//        foreach($user->getMaterials() as $mat){
//            echo $mat->getUsername()."/";
//        }
        //     var_dump($user);die();

        $repository = $em->getRepository('DRIBundle:User');
        $societe = array();

        if ($user->getUsername()) {

            $societe = $repository->findUsersAndSociete($user->getUsername(), $this->getConnClient());
        }



        return $this->render('DRIBundle:DRIAdmin:userdetails.html.php', array(
                    'user' => $user,
                    'materials' => $materials,
                    'societe' => $societe,
        ));
    }

    public function savedetailsmaterialsAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $enquireuser = $em->getRepository("DRIBundle:User")->find($request->request->get("uid"));
#################       
//       var_dump($request->request->all());die(); 
        //var_dump($request->request->get("uid"));die(); 
        
      // var_dump($enquireuser->getSurname());die();
//        
//        $f = $this->createFormBuilder($material)->getForm();
//        $f->handleRequest($request);
//        
//        
//        var_dump($f);die(); 
//        if($f->isValid()){
//            die("ok");
//        }
//         die("out");
#################   
        $litigerecords = array();
        
        if ($request->request->get("updatedetails")) {
    
            for ($i = 0; $i < count($request->request->get("status")); $i++) {

                if ($request->request->get("status")[$i] == 2) {

                    $query = $em->createQuery("SELECT m FROM DRIBundle\Entity\Materials m WHERE m.id=?1");
                    $query->setParameter(1, $request->request->get("id")[$i]);
                    $result = $query->execute();

                    // var_dump($result);die();

                    if (is_object($result[0]) && $result[0]->getStatus()->getId() != 2) {
                        //send mail here
                        //var_dump($result[0]->getStatus()->getId());die();                       
                        $litigerecords[] = $result[0];
                    }
                }

                $query = $em->createQuery("UPDATE DRIBundle\Entity\Materials m SET m.status =?1,m.comment=?2  WHERE m.id=?3");
                $query->setParameter(1, $request->request->get("status")[$i]);
                $query->setParameter(2, $request->request->get("tareacomment")[$i]);
                $query->setParameter(3, $request->request->get("id")[$i]);
                $numUpdated = $query->execute();

                $em->clear();

                $nouveau = false;
                $litige = false;
                $enattente = false;
                $termine = false;

                $user = $em->getRepository("DRIBundle:User")->find($request->request->get("uid"));
                // $em->refresh($user);

                $materials = $user->getMaterials();

                // $em->flush();
                foreach ($materials as $m) {
                    //echo $m->getStatus()->getId();

                    if ($m->getStatus()->getId() == 1) {
                        //$nouveau++; 
                        $nouveau = true;
                    }

                    if ($m->getStatus()->getId() == 2) {
                        //$litige++; 
                        $litige = true;
                    }

                    if ($m->getStatus()->getId() == 3) {
                        // $enattente++; 
                        $enattente = true;
                    }

                    if ($m->getStatus()->getId() == 4) {
                        // $termine++; 
                        $termine = true;
                    }
                }

                // die();
                // var_dump($nouveau,$litige,$enattente,$termine);
                // $compstatus = 1;

                if ($litige == true) {
                    $compstatus = 2;
                } elseif ($litige == false && $enattente == true) {
                    $compstatus = 3;
                } elseif ($termine == true && $litige == false && $enattente == false && $nouveau == false) {
                    $compstatus = 4;
                } elseif ($termine == false && $litige == false && $enattente == false && $nouveau == true) {
                    $compstatus = 1;
                } elseif (( ($nouveau == false && $litige == false && $enattente == false && $termine == false) || ($nouveau == true && $litige == false && $enattente == false && $termine == true))) {
                    $compstatus = 3;
                }



                $statement = $this->getConn()->executeQuery('SELECT * FROM dri_computedstatus
                                                         WHERE user_id=?', array($request->request->get("uid")));

                $cstat = $statement->fetch();

                if (count($cstat)) {
                    $this->getConn()->update('dri_computedstatus', array('computedstatus' => $compstatus), array('user_id' => $request->request->get("uid")));
                }                                  
            }
            
                 ############## start trimitere mail litige ########
              
//                var_dump($litigerecords);
//                die();
                

                
                if(count($litigerecords))
                {   
                    
                    if($this->get("kernel")->getEnvironment() == "recette")
                    {
                        $cc = array('agheonea@veoworldwide.ro', 'andrei.alexandru.dan2004@gmail.com'); 
                        $to = "JBUREAU@noz.fr";
                    }     
                    elseif($this->get("kernel")->getEnvironment() == "local")
                    {
                        $cc = array();
                        $to = "andrei.alexandru.dan2004@gmail.com";
                    }
                    else
                    {
                        $cc = array();
                        $to = "materiel@noz.fr";
                    }
                    
                    $mailer = $this->get('mailer');

                    $message = $mailer->createMessage()
                                      ->setSubject("DRI : Un élément n’a pas était restitué")
                                      ->setFrom("admindri@noz.fr")
                                      ->setTo($to)
                                      //->attach(\Swift_Attachment::fromPath($attachment))
                                      ->setCc($cc)
                                      // ->setBcc($bcc)
                                      ->setBody(
                                            $this->renderView(
                                                    'DRIBundle:Emails:litigemail.html.php', 
                                                    array('materials' => $litigerecords,
                                                          'enquireuser'=>$enquireuser
                                                         )
                                                    ), 
                                                    'text/html'
                                        );

                    $mailer->send($message);                   
                }            
        }

        return $this->redirect($this->generateUrl('_adminlistuserdetails', array('id' => $request->request->get('uid'))));
    }

    public function edituserdetailsAction(Request $request, $id) {

        //  var_dump($request->request->all());die();

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository("DRIBundle:User")->find($id);


        $statement = $this->getConn()->executeQuery('SELECT u.username   FROM user u', array());

        $results = $statement->fetchAll();

        $newarray = array();
        foreach ($results as $r) {
            $newarray[$r['username']] = $r['username'];
        }

        //  var_dump($results);die();
        $form = $this->createFormBuilder($user)
                        //->setAction($this->generateUrl('_adduser'))
                        // ->setMethod('POST')
                        // ->create('username')
                        ->add('username', 'text', array('label' => 'User Name'))
                        ->add('email', 'text', array('label' => 'Email'))
                        ->add('givenname', 'text', array('label' => 'Given Name'))
                        ->add('surname', 'text', array('label' => 'Surname'))
                        ->add('save', 'submit', array('label' => 'Create User'))
                        ->add('gender', 'choice', array(
                            'choices' => array(
                                'm' => 'Male',
                                'f' => 'Female'
                            ),
                            'required' => false,
                            'empty_value' => 'Choose your gender',
                            'empty_data' => null,
                            'mapped' => false
                        ))
                        ->add('users', 'choice', array(
                            'choices' => $newarray,
                            'required' => true,
                            'empty_value' => 'Choose ',
                            'empty_data' => true,
                            'mapped' => false
                        ))->getForm();
        //->getForm();


        $validator = $this->get('validator');
        $errors = $validator->validate($user);


        $form->handleRequest($request);

        //$errors = array();

//        if (!$form->isValid()) {
//            // perform some action, such as saving the task to the database
//
//          //  return $this->redirect($this->generateUrl('task_success'));
//              //var_dump($errors);die(); 
//           
//        }
////        
//       if (count($errors) > 0) {
//        /*
//         * Uses a __toString method on the $errors variable which is a
//         * ConstraintViolationList object. This gives us a nice string
//         * for debugging
//         */
//       // $errorsString = (string) $errors;
//
//     //   return new Response($errorsString);
//    }

        if ($form->isSubmitted()) {
            // var_dump($form->getData());die();
            if (count($errors) == 0) {
                die("valid");
            }

            if ($form->isValid()) {
                // the validation passed, do something with the $author object
                //  $request->query->get("username");
//                echo "<pre>";
//                 var_dump($form->getData());
//                 //$form->getData();
//                echo "</pre>";  die();
//              $request->getSession()->getFlashBag()->add(
//                          'notice',
//                           'Your changes were saved!'
//                        );
//                $em = $this->getDoctrine()->getManager();
//                $em->persist($user);
//                $em->flush();
                //$form->getData()->getUsername();
                //echo "</pre>";  
                die("valid");
                //return $this->redirectToRoute('task_success');
                return $this->redirect($this->generateUrl('_adminlistusers'));
            }

//           return $this->render('DRIBundle:DRIAdmin:edituser.html.php', array(
//                                       'form' => $form->createView(),
//                                       'user' => $user,
//                                       'errors'=>$errors
//                                    ));
        }


        return $this->render('DRIBundle:DRIAdmin:edituser.html.php', array(
                    'form' => $form->createView(),
                    'user' => $user,
                    'errors' => $errors
        ));
    }

}
