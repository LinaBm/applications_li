<?php

namespace DRIBundle\Controller;

use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use DRIBundle\Entity\User;
use DRIBundle\Entity\Status;

use DRIBundle\Entity\Materials;

use DRIBundle\Entity\Computers;
use DRIBundle\Entity\Phones;
use DRIBundle\Entity\Peripherals;
use DRIBundle\Entity\Printers;
use DRIBundle\Entity\Monitors;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class DRIController extends Controller
{
    
   public function getConn(){
       return $this->get('database_connection'); 
    }
    
   public function getConnClient(){
       return $this->get('doctrine.dbal.client_connection'); 
    }
    
    public function sendMail($subjectstring, $from, $to, $view, $request, $attachment,$cc, $bcc)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('DRIBundle:User');
        $societe = array();
        if(isset($request->request->get('username')[0])){

            $societe = $repository->findUsersAndSociete($request->request->get('username')[0], $this->getConnClient());  
   
        }
       // var_dump($societe);die();
        
        $mailer = $this->get('mailer');

        $message = $mailer->createMessage()
                ->setSubject($subjectstring)
                ->setFrom($from)
                ->setTo($to)
                ->attach(\Swift_Attachment::fromPath($attachment))
                //->setCc(array('andrei.alexandru.dan2004@gmail.com'))
                ->setCc($cc)
                ->setBcc($bcc)
                ->setBody(
                        $this->renderView(                                  
                                  $view,
                                   array('materiels' => $request->request->all(),
                                         'loggeduser'=>$this->getUser(),
                                         'societe'=>$societe)
                              ),
                              'text/html'
                );

        $mailer->send($message);
    }
    
    
    public function indexAction(Request $request)
    { 

         return $this->usersmaterialsAction($request);
    }
    
    
    public function usersmaterialsAction(Request $request)
    {      
         $materials = array();
         $computers = array();
         $monitors = array();      
         $phones = array();
         $printers = array();
         $peripherals = array();

         $em = $this->getDoctrine()->getManager();
         $repository = $em->getRepository('DRIBundle:User');
             
         $results = $repository->findAll();     
         
         $user = new User();
                

         $form = $this->createFormBuilder($user)
                      ->getForm();
         $form->handleRequest($request);
         
   
             
            $driuser = $repository->findById($request->request->get("users")); 
               
          if(count($driuser)){
//                $societe = $repository->findUsersAndSociete($driuser[0]->getUsername(), $this->getConnClient());  
//                
//                var_dump($societe);die();
                $computers = $repository->findComputers($driuser[0]->getUsername(), $this->getConnClient());
                $monitors = $repository->findMonitors($driuser[0]->getUsername(), $this->getConnClient());
                $phones = $repository->findPhones($driuser[0]->getUsername(), $this->getConnClient());
                $printers = $repository->findPrinters($driuser[0]->getUsername(), $this->getConnClient());//P838
                $peripherals = $repository->findPeripherals($driuser[0]->getUsername(), $this->getConnClient());//p834/p474  
                
                 //var_dump($peripherals);die();
                 
                $materials = array_merge($computers, $monitors, $phones, $printers, $peripherals);
            
            }

         
//var_dump($materials);die();
         
        return $this->render('DRIBundle:DRI:listusersmaterials.html.php', array('users'=>$results,
                                                                                'driuser' => $driuser,
                                                                                'user'=>$this->getUser(),
                                                                                'form' => $form->createView(),
                                                                                'materials' => $materials,
                                                                               ));

    
    }
    
    
    public function savedetailsmaterialsAction(Request $request)
    {
        
        $uploadir = $this->container->getParameter("kernel.root_dir")."/../web/bundles/dri/pdf/";      
        $em = $this->getDoctrine()->getManager();
       // var_dump($request->request->all());die();
        //if($request->request->get('chkfairedemande')) die("set");die();
        //var_dump($this->getUser()->getEmail());die();
        //var_dump($this->get("kernel")->getEnvironment());die();
        $flagmaterial = 0;
        
        $repository = $em->getRepository('DRIBundle:User');
        
        $societe = null;
        
        if(isset($request->request->get('username')[0])){

             $societe = $repository->findUsersAndSociete($request->request->get('username')[0], $this->getConnClient());  
   
        }
        
        $societe = isset($societe[0]['societe'])?$societe[0]['societe']:"";
       // var_dump($societe);die();
    if(count($request->request->get('nom'))){

      
       for($i=0;$i<count($request->request->get('nom'));$i++){
 
           
          $materielexists = $this->getDoctrine()
                                 ->getRepository('DRIBundle:Materials')
                                 ->findBy( array(
                                       'nom' => $request->request->get('nom')[$i],
                                       'prenom' => $request->request->get('prenom')[$i],
                                       'evaluateur'=>$this->getUser()->getUsername(),
                                       'evaluateurId'=>$this->getUser()->getId(),
                                       'numserie' => $request->request->get('numserie')[$i],
                                       'userIdGlpi' => $request->request->get('uid')[$i],
                                       'username' => $request->request->get('username')[$i],
                                       'usernameglpi' => $request->request->get('usernameglpi')[$i],
                                       'manufacturer' => $request->request->get('manufacturer')[$i],
                                       'model' => $request->request->get('model')[$i],
                                       'codebarre' => $request->request->get('codebarre')[$i],
                                       'designation' => $request->request->get('designation')[$i],
                                      // 'materialstatus' => $request->request->get('statut')[$i],
                                      // 'dateRestitution' => new \DateTime($request->request->get('restitutiondate')),
                                        )
                                  );
//var_dump($materielexists);die();
                  if(!count($materielexists)){
                      
                     $flagmaterial++;   
                     
                    $materiel = new Materials();
                   // $status = new Status();

                    $user = $em->getRepository("DRIBundle:User")->find($request->request->get('userid')[$i]);
                    $status = $em->getRepository("DRIBundle:Status")->find(3);  //3 pt en attente

                    $materiel->setNom($request->request->get('nom')[$i]);
                    $materiel->setPrenom($request->request->get('prenom')[$i]);
                    
                    $materiel->setEvaluateur($this->getUser()->getUsername());
                    $materiel->setEvaluateurId($this->getUser()->getId());
                    
                    $materiel->setNumserie($request->request->get('numserie')[$i]);
                    $materiel->setUser($user);
                    $materiel->setUserIdGlpi($request->request->get('uid')[$i]);
                    $materiel->setStatus($status);
                    $materiel->setUsername($request->request->get('username')[$i]);
                    $materiel->setUsernameglpi($request->request->get('usernameglpi')[$i]); 
                    $materiel->setManufacturer($request->request->get('manufacturer')[$i]);
                    $materiel->setModel($request->request->get('model')[$i]);
                    $materiel->setCodebarre($request->request->get('codebarre')[$i]);
                    $materiel->setDesignation($request->request->get('designation')[$i]);
                    $materiel->setRestitutiondate(new \DateTime($request->request->get('restitutiondate')));
                    $materiel->setForwardemail($request->request->get('fwdemail'));
                    $materiel->setSociete($societe);
                    $materiel->setMaterialstatus($request->request->get('statut')[$i]);
                    $em->persist($materiel);

                    $em->flush();

                    $request->getSession()->getFlashBag()->add(
                                   'notice',
                                   'Votre demande a bien été prise en compte!'
                                 );
                   }else{
                    
                       $request->getSession()->getFlashBag()->add(
                                   'notice',
                                   "L'information existe déjà dans la base de données!"
                                 );
                       
                   } 

         }
                  
        }
        

        
         //de pus in entitate  
        //cand inserez in computed status trebuie sa fie nouveau
        if(isset($request->request->get('userid')[0])){
            
            $statement = $this->getConn()->executeQuery('SELECT * FROM dri_computedstatus
                                          WHERE user_id=?',array($request->request->get('userid')[0]));    

            $cstat = $statement->fetchAll();

            if(!count($cstat)){

                $this->getConn()->insert('dri_computedstatus', 
                                            array(
                                                 'user_id' => $request->request->get('userid')[0],
                                                 'computedstatus' => 1,
                                                  )); 
            }
        }
        
		if( $flagmaterial>0 )
        {
            $pdf = $this->get("white_october.tcpdf")->create(
						PDF_PAGE_ORIENTATION, PDF_UNIT, 'A4 PORTRAIT', true, 'UTF-8', false
                   );

            $pdf->SetFont('Helvetica', '', 12);

            $pdf->AddPage();

            $pdf->writeHTMLCell(
                        $w = 0,
                        $h = 0,
                        $x = '',
                        $y = '',
                        $this->renderView('DRIBundle:Pdf:pdfcontent.html.php', 
                                           array(
                                                 "data"=>$request->request->all(),
                                                 "loggeduser"=>$this->getUser(),
                                                 "societe"=>$societe,
                                                )
                                         ),
                    $border = 0,
                    $ln = 1,
                    $fill = 0,
                    $reseth = true,
                    $align = '',
                    $autopadding = true
             );
		
			$pdf->SetFillColor(215, 235, 255);

			$filename = "DRI_".date("d-m-Y")."_".$request->request->get("nom")[0]."_".$request->request->get("prenom")[0].".pdf";
			
			$pdf->Output($uploadir . $filename, 'F');
			//die;

			//## MAIL ##
			$from = "formulairedri@noz.fr";
			$emaildemandeur = $this->getUser()->getEmail();
			$to = $emaildemandeur; //'Cause Bug on line 322 on the else of condition : if($request->request->get('chkautreadressetext') != "" )
			$view = 'DRIBundle:Emails:materialsmail.html.php';
			$attachment = $uploadir . $filename;
			
			//if it's in local or test
			if(($this->get("kernel")->getEnvironment() == "recette") || ($this->get("kernel")->getEnvironment() == "local"))
			{
				$subjectstring = 'TEST DRI : '.$request->request->get('username')[0].' - '.date("d-m-Y");
				
				//if it's local
				if($this->get("kernel")->getEnvironment() == "local"){
					$cc = array('agheonea@veoworldwide.ro', 'andrei.alexandru.dan2004@gmail.com');
				}
			}
			//else it's in production
			else
			{
				$subjectstring = 'DRI : '.$request->request->get('username')[0].' - '.date("d-m-Y");
				$cc = array("materiel@noz.fr");
			}
			
			//if the user(the asker) want an other adress
			if($request->request->get('chkautreadressetext') != "" ) //Bug here --> Condition fail for the else
			{
				if($request->request->get('chkautreadressetext') != "ascuns")
				{
				   $to = $request->request->get('chkautreadressetext');
				}
			}
			/* else
			{
				$to = $emaildemandeur;
			} */
			
			//the user in cc if chkbox
			if($request->request->get('chkfairedemande'))
			{
				$cc = array($request->request->get('useremail'));
			}
			
			//poleweb
			$bcc = array('poleweb@noz.fr');
			
			//var_dump($to);die();
			$this->sendMail($subjectstring, $from, trim($to), $view, $request,$attachment, $cc, $bcc); 
			//## END MAIL ##
		}
		return $this->redirect($this->generateUrl('_glpiusersmaterials'));      
    }
    
    
 
    public function userscomputersAction(Request $request){
         $driuser = array();
         $computers = array();
         
         $em = $this->getDoctrine()->getManager();
         $repository = $em->getRepository('DRIBundle:User');
               
         $results = $repository->findAll();     
         
         $user = new User();

         $form = $this->createFormBuilder($user)
                      ->getForm();
         $form->handleRequest($request);
         
         if ($form->isSubmitted()){
             
            $driuser = $repository->findById($request->request->get("users")); 
          
          if(count($driuser)){
                          
                $computers = $repository->findComputers($driuser[0]->getUsername(), $this->getConnClient());
           
            }

         }
               //var_dump($computers);die();
        return $this->render('DRIBundle:DRI:listuserscomputers.html.php', array('users'=>$results,
                                                                                'driuser' => $driuser,
                                                                                'user'=>$this->getUser(),
                                                                                'form' => $form->createView(),
                                                                                'computers' => $computers,                                                     
                                                                               ));
         
    }
    

    
    
    public function savedetailscomputersAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
      
        //var_dump($request->request->all());die();
        
       
        
        if(count($request->request->get('nom'))){

            $subjectstring = 'List of computer parts for : '.$request->request->get('nom')[0].' '.$request->request->get('prenom')[0];
            $from = "dri@application.com";
            $to = 'andrei.alexandru.dan2004@gmail.com';
            //$to = "materiel@noz.fr";
            $view = 'DRIBundle:Emails:computeremail.html.php';
            $this->sendMail($subjectstring, $from, $to, $view, $request);

        

       for($i=0;$i<count($request->request->get('nom'));$i++){
           
            $materielexists = $this->getDoctrine()
                                   ->getRepository('DRIBundle:Computers')
                                   ->findBy( array(
                                         'nom' => $request->request->get('nom')[$i],
                                         'prenom' => $request->request->get('prenom')[$i],
                                         'numserie' => $request->request->get('numserie')[$i],
                                         'userIdGlpi' => $request->request->get('uid')[$i],
                                         'username' => $request->request->get('username')[$i],
                                         'usernameglpi' => $request->request->get('usernameglpi')[$i],
                                         'manufacturer' => $request->request->get('manufacturer')[$i],
                                         'computermodel' => $request->request->get('computermodel')[$i],
                                         'codebarre' => $request->request->get('codebarre')[$i],
                                         'comment' => $request->request->get('comment')[$i],
                                        // 'dateRestitution' => new \DateTime($request->request->get('restitutiondate')),
                                          )
                                    );
            
           if(!count($materielexists)){
                    $materiel = new Computers();
                    $status = new Status();
                    
                    $user = $em->getRepository("DRIBundle:User")->find($request->request->get('userid')[$i]);
                    $status = $em->getRepository("DRIBundle:Status")->find(1);  
                    
                    $materiel->setNom($request->request->get('nom')[$i]);
                    $materiel->setPrenom($request->request->get('prenom')[$i]);
                    $materiel->setNumserie($request->request->get('numserie')[$i]);
                    $materiel->setUser($user);
                    $materiel->setUserIdGlpi($request->request->get('uid')[$i]);
                    $materiel->setStatus($status);
                    $materiel->setUsername($request->request->get('username')[$i]);
                    $materiel->setUsernameglpi($request->request->get('usernameglpi')[$i]); 
                    $materiel->setManufacturer($request->request->get('manufacturer')[$i]);
                    $materiel->setComputermodel($request->request->get('computermodel')[$i]);
                    $materiel->setCodebarre($request->request->get('codebarre')[$i]);
                    $materiel->setComment($request->request->get('comment')[$i]);
                    $materiel->setRestitutiondate(new \DateTime($request->request->get('restitutiondate')));
                    $materiel->setForwardemail($request->request->get('fwdemail'));
                    $em->persist($materiel);
                          
                    $em->flush();
                    
                    $request->getSession()->getFlashBag()->add(
                            'notice',
                             'Your changes were saved!'
                          );
           }

       }

     }
          return $this->redirect($this->generateUrl('_glpiuserscomputers'));
    }
    
    public function usersphonesAction(Request $request){
         $driuser = array();
         $phones = array();
         
         $em = $this->getDoctrine()->getManager();
         $repository = $em->getRepository('DRIBundle:User');
               
         $results = $repository->findAll();     
         
         $user = new User();

         $form = $this->createFormBuilder($user)
                      ->getForm();
         $form->handleRequest($request);
         
         if ($form->isSubmitted()){
             
            $driuser = $repository->findById($request->request->get("users")); 
          
          if(count($driuser)){
                          
                $phones = $repository->findPhones($driuser[0]->getUsername(), $this->getConnClient());
           
            }
              //var_dump($phones);die(); 
         }
               
        return $this->render('DRIBundle:DRI:listusersphones.html.php', array('users'=>$results,
                                                                                'driuser' => $driuser,
                                                                                'user'=>$this->getUser(),
                                                                                'form' => $form->createView(),
                                                                                'phones' => $phones,                                                     
                                                                               ));
         
    }
    
    public function savedetailsphonesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

       // var_dump($request->request->all());die();
         if(count($request->request->get('nom'))){

            $subjectstring = 'List of phones or phone parts for : '.$request->request->get('nom')[0].' '.$request->request->get('prenom')[0];
            $from = "test@app.com";
            $to = 'andrei.alexandru.dan2004@gmail.com';
            //$to = "materiel@noz.fr";
            $view = 'DRIBundle:Emails:phonesemail.html.php';
            $this->sendMail($subjectstring, $from, $to, $view, $request);

            for($i=0;$i<count($request->request->get('nom'));$i++){

//               $this->getConn()->insert('dri_phones', 
//                                         array(
//                                              'nom' => $request->request->get('nom')[$i],
//                                              'prenom' => $request->request->get('prenom')[$i],
//                                              'numserie' => $request->request->get('numserie')[$i],
//                                              'user_id' => $request->request->get('userid')[$i],
//                                              'user_id_glpi' => $request->request->get('uid')[$i],
//                                              'status_id' => 1,
//                                              'username' => $request->request->get('username')[$i],
//                                              'usernameglpi' => $request->request->get('usernameglpi')[$i],
//                                              'manufacturer' => $request->request->get('manufacturer')[$i],
//                                              'phonemodel' => $request->request->get('phonemodel')[$i],
//                                              'codebarre' => $request->request->get('codebarre')[$i],
//                                              'comment' => $request->request->get('comment')[$i],                                       
//                                              'restitutiondate' => $request->request->get('restitutiondate'),
//                                               )
//                    );   
                $materielexists = $this->getDoctrine()
                                        ->getRepository('DRIBundle:Phones')
                                        ->findBy( array(
                                              'nom' => $request->request->get('nom')[$i],
                                              'prenom' => $request->request->get('prenom')[$i],
                                              'numserie' => $request->request->get('numserie')[$i],
                                              'userIdGlpi' => $request->request->get('uid')[$i],
                                              'username' => $request->request->get('username')[$i],
                                              'usernameglpi' => $request->request->get('usernameglpi')[$i],
                                              'manufacturer' => $request->request->get('manufacturer')[$i],
                                              'phonemodel' => $request->request->get('phonemodel')[$i],
                                              'codebarre' => $request->request->get('codebarre')[$i],
                                              'comment' => $request->request->get('comment')[$i],
                                             // 'dateRestitution' => new \DateTime($request->request->get('restitutiondate')),
                                               )
                                         );
//var_dump($materielexists);die();
                  if(!count($materielexists)){
                           $materiel = new Phones();
                           $status = new Status();

                           $user = $em->getRepository("DRIBundle:User")->find($request->request->get('userid')[$i]);
                           $status = $em->getRepository("DRIBundle:Status")->find(1);  

                           $materiel->setNom($request->request->get('nom')[$i]);
                           $materiel->setPrenom($request->request->get('prenom')[$i]);
                           $materiel->setNumserie($request->request->get('numserie')[$i]);
                           $materiel->setUser($user);
                           $materiel->setUserIdGlpi($request->request->get('uid')[$i]);
                           $materiel->setStatus($status);
                           $materiel->setUsername($request->request->get('username')[$i]);
                           $materiel->setUsernameglpi($request->request->get('usernameglpi')[$i]); 
                           $materiel->setManufacturer($request->request->get('manufacturer')[$i]);
                           $materiel->setPhonemodel($request->request->get('phonemodel')[$i]);
                           $materiel->setCodebarre($request->request->get('codebarre')[$i]);
                           $materiel->setComment($request->request->get('comment')[$i]);
                           $materiel->setRestitutiondate(new \DateTime($request->request->get('restitutiondate')));
                           $materiel->setForwardemail($request->request->get('fwdemail'));
                           $em->persist($materiel);

                           $em->flush();

                           $request->getSession()->getFlashBag()->add(
                                   'notice',
                                    'Your changes were saved!'
                                 );
                   }  
             }
                
      }
          return $this->redirect($this->generateUrl('_glpiusersphones'));
    }
    
    
    public function usersperipheralsAction(Request $request){
         $driuser = array();
         $peripherals = array();
         
         $em = $this->getDoctrine()->getManager();
         $repository = $em->getRepository('DRIBundle:User');
               
         $results = $repository->findAll();     
         
         $user = new User();

         $form = $this->createFormBuilder($user)
                      ->getForm();
         $form->handleRequest($request);
         
         if ($form->isSubmitted()){
             
            $driuser = $repository->findById($request->request->get("users")); 
          
          if(count($driuser)){
                  //test cu user p834/p474     //$driuser[0]->getUsername()  
                 $peripherals = $repository->findPeripherals($driuser[0]->getUsername() , $this->getConnClient());
           
            }

         }
                
        return $this->render('DRIBundle:DRI:listusersperipherals.html.php',  array('users'=>$results,
                                                                                   'driuser' => $driuser,
                                                                                   'user'=>$this->getUser(),
                                                                                   'form' => $form->createView(),
                                                                                   'peripherals' => $peripherals,                                                     
                                                                               ));
         
    }
    
    public function savedetailsperipheralsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        if(count($request->request->get('nom'))){

            $subjectstring = 'List of peripherals for : '.$request->request->get('nom')[0].' '.$request->request->get('prenom')[0];
            $from = "test@app.com";
            $to = 'andrei.alexandru.dan2004@gmail.com';
            //$to = "materiel@noz.fr";
            $view = 'DRIBundle:Emails:peripheralsemail.html.php';
            $this->sendMail($subjectstring, $from, $to, $view, $request);
      
       for($i=0;$i<count($request->request->get('nom'));$i++){
           
//          $this->getConn()->insert('dri_peripherals', 
//                                    array(
//                                         'nom' => $request->request->get('nom')[$i],
//                                         'prenom' => $request->request->get('prenom')[$i],
//                                         'numserie' => $request->request->get('numserie')[$i],
//                                         'user_id' => $request->request->get('userid')[$i],
//                                         'user_id_glpi' => $request->request->get('uid')[$i],
//                                         'status_id' => 1,
//                                         'username' => $request->request->get('username')[$i],
//                                         'usernameglpi' => $request->request->get('usernameglpi')[$i],
//                                         'manufacturer' => $request->request->get('manufacturer')[$i],
//                                         'peripheralmodel' => $request->request->get('peripheralmodel')[$i],
//                                         'codebarre' => $request->request->get('codebarre')[$i],
//                                         'comment' => $request->request->get('comment')[$i],                                       
//                                         'restitutiondate' => $request->request->get('restitutiondate'),
//                                         'forwardemail' => $request->request->get('fwdemail'),
//                                          )
//               );   
           
                    $materielexists = $this->getDoctrine()
                                 ->getRepository('DRIBundle:Peripherals')
                                 ->findBy( array(
                                       'nom' => $request->request->get('nom')[$i],
                                       'prenom' => $request->request->get('prenom')[$i],
                                       'numserie' => $request->request->get('numserie')[$i],
                                       'userIdGlpi' => $request->request->get('uid')[$i],
                                       'username' => $request->request->get('username')[$i],
                                       'usernameglpi' => $request->request->get('usernameglpi')[$i],
                                       'manufacturer' => $request->request->get('manufacturer')[$i],
                                       'peripheralmodel' => $request->request->get('peripheralmodel')[$i],
                                       'codebarre' => $request->request->get('codebarre')[$i],
                                       'comment' => $request->request->get('comment')[$i],
                                      // 'dateRestitution' => new \DateTime($request->request->get('restitutiondate')),
                                        )
                                  );
//var_dump($materielexists);die();
                  if(!count($materielexists)){
                           $materiel = new Peripherals();
                           $status = new Status();

                           $user = $em->getRepository("DRIBundle:User")->find($request->request->get('userid')[$i]);
                           $status = $em->getRepository("DRIBundle:Status")->find(1);  

                           $materiel->setNom($request->request->get('nom')[$i]);
                           $materiel->setPrenom($request->request->get('prenom')[$i]);
                           $materiel->setNumserie($request->request->get('numserie')[$i]);
                           $materiel->setUser($user);
                           $materiel->setUserIdGlpi($request->request->get('uid')[$i]);
                           $materiel->setStatus($status);
                           $materiel->setUsername($request->request->get('username')[$i]);
                           $materiel->setUsernameglpi($request->request->get('usernameglpi')[$i]); 
                           $materiel->setManufacturer($request->request->get('manufacturer')[$i]);
                           $materiel->setPeripheralmodel($request->request->get('peripheralmodel')[$i]);
                           $materiel->setCodebarre($request->request->get('codebarre')[$i]);
                           $materiel->setComment($request->request->get('comment')[$i]);
                           $materiel->setRestitutiondate(new \DateTime($request->request->get('restitutiondate')));
                           $materiel->setForwardemail($request->request->get('fwdemail'));
                           $em->persist($materiel);

                           $em->flush();

                           $request->getSession()->getFlashBag()->add(
                                   'notice',
                                    'Your changes were saved!'
                                 );
                   }  
          
        }

      }
          return $this->redirect($this->generateUrl('_glpiusersperipherals'));
    }
    
    
    public function usersprintersAction(Request $request){
         $driuser = array();
         $printers = array();
         
         $em = $this->getDoctrine()->getManager();
         $repository = $em->getRepository('DRIBundle:User');
               
         $results = $repository->findAll();     
         
         $user = new User();

         $form = $this->createFormBuilder($user)
                      ->getForm();
         $form->handleRequest($request);
         
         if ($form->isSubmitted()){
             
            $driuser = $repository->findById($request->request->get("users")); 
          
          if(count($driuser)){
                 //$driuser[0]->getUsername()   P838       
                $printers = $repository->findPrinters($driuser[0]->getUsername(), $this->getConnClient());
           
            }

         }
             
        return $this->render('DRIBundle:DRI:listusersprinters.html.php', array('users'=>$results,
                                                                                'driuser' => $driuser,
                                                                                'user'=>$this->getUser(),
                                                                                'form' => $form->createView(),
                                                                                'printers' => $printers,                                                     
                                                                               ));
         
    }
    
    public function savedetailsprintersAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        if(count($request->request->get('nom'))){

            $subjectstring = 'List of printers for : '.$request->request->get('nom')[0].' '.$request->request->get('prenom')[0];
            $from = "test@app.com";
            $to = 'andrei.alexandru.dan2004@gmail.com';
            //$to = "materiel@noz.fr";
            $view = 'DRIBundle:Emails:printersemail.html.php';
            $this->sendMail($subjectstring, $from, $to, $view, $request);

                 
        
       for($i=0;$i<count($request->request->get('nom'));$i++){
           
//          $this->getConn()->insert('dri_printers', 
//                                    array(
//                                         'nom' => $request->request->get('nom')[$i],
//                                         'prenom' => $request->request->get('prenom')[$i],
//                                         'numserie' => $request->request->get('numserie')[$i],
//                                         'user_id' => $request->request->get('userid')[$i],
//                                         'user_id_glpi' => $request->request->get('uid')[$i],
//                                         'status_id' => 1,
//                                         'username' => $request->request->get('username')[$i],
//                                         'usernameglpi' => $request->request->get('usernameglpi')[$i],
//                                         'manufacturer' => $request->request->get('manufacturer')[$i],
//                                         'printermodel' => $request->request->get('printermodel')[$i],
//                                         'codebarre' => $request->request->get('codebarre')[$i],
//                                         'comment' => $request->request->get('comment')[$i],                                       
//                                         'restitutiondate' => $request->request->get('restitutiondate'),
//                                         'forwardemail' => $request->request->get('fwdemail'),
//                                          )
//               );   
           
                    $materielexists = $this->getDoctrine()
                                 ->getRepository('DRIBundle:Printers')
                                 ->findBy( array(
                                       'nom' => $request->request->get('nom')[$i],
                                       'prenom' => $request->request->get('prenom')[$i],
                                       'numserie' => $request->request->get('numserie')[$i],
                                       'userIdGlpi' => $request->request->get('uid')[$i],
                                       'username' => $request->request->get('username')[$i],
                                       'usernameglpi' => $request->request->get('usernameglpi')[$i],
                                       'manufacturer' => $request->request->get('manufacturer')[$i],
                                       'printermodel' => $request->request->get('printermodel')[$i],
                                       'codebarre' => $request->request->get('codebarre')[$i],
                                       'comment' => $request->request->get('comment')[$i],
                                      // 'dateRestitution' => new \DateTime($request->request->get('restitutiondate')),
                                        )
                                  );
//var_dump($materielexists);die();
                  if(!count($materielexists)){
                           $materiel = new Printers();
                           $status = new Status();

                           $user = $em->getRepository("DRIBundle:User")->find($request->request->get('userid')[$i]);
                           $status = $em->getRepository("DRIBundle:Status")->find(1);  

                           $materiel->setNom($request->request->get('nom')[$i]);
                           $materiel->setPrenom($request->request->get('prenom')[$i]);
                           $materiel->setNumserie($request->request->get('numserie')[$i]);
                           $materiel->setUser($user);
                           $materiel->setUserIdGlpi($request->request->get('uid')[$i]);
                           $materiel->setStatus($status);
                           $materiel->setUsername($request->request->get('username')[$i]);
                           $materiel->setUsernameglpi($request->request->get('usernameglpi')[$i]); 
                           $materiel->setManufacturer($request->request->get('manufacturer')[$i]);
                           $materiel->setPrintermodel($request->request->get('printermodel')[$i]);
                           $materiel->setCodebarre($request->request->get('codebarre')[$i]);
                           $materiel->setComment($request->request->get('comment')[$i]);
                           $materiel->setRestitutiondate(new \DateTime($request->request->get('restitutiondate')));
                           $materiel->setForwardemail($request->request->get('fwdemail'));
                           $em->persist($materiel);

                           $em->flush();

                           $request->getSession()->getFlashBag()->add(
                                   'notice',
                                    'Your changes were saved!'
                                 );
                   }  
          
        }

     } 
          return $this->redirect($this->generateUrl('_glpiusersprinters'));
    }
    
    
    public function usersmonitorsAction(Request $request){
          $driuser = array();
          $monitors = array();
         
         $em = $this->getDoctrine()->getManager();
         $repository = $em->getRepository('DRIBundle:User');
               
         $results = $repository->findAll();     
         
         $user = new User();

         $form = $this->createFormBuilder($user)
                      ->getForm();
         $form->handleRequest($request);
         
         if ($form->isSubmitted()){
             
            $driuser = $repository->findById($request->request->get("users")); 
          
          if(count($driuser)){
                    $monitors = $repository->findMonitors($driuser[0]->getUsername(), $this->getConnClient());     
            }

         }
             
        return $this->render('DRIBundle:DRI:listusersmonitors.html.php', array('users'=>$results,
                                                                                'driuser' => $driuser,
                                                                                'user'=>$this->getUser(),
                                                                                'form' => $form->createView(),
                                                                                'monitors' =>  $monitors,                                                     
                                                                               ));
         
    }
    
    public function savedetailsmonitorsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        if(count($request->request->get('nom'))){

            $subjectstring = 'List of monitors for : '.$request->request->get('nom')[0].' '.$request->request->get('prenom')[0];
            $from = "test@app.com";
            $to = 'andrei.alexandru.dan2004@gmail.com';
            //$to = "materiel@noz.fr";
            $view = 'DRIBundle:Emails:monitorsemail.html.php';
            $this->sendMail($subjectstring, $from, $to, $view, $request);
            
       
       for($i=0;$i<count($request->request->get('nom'));$i++){
           
//          $this->getConn()->insert('dri_monitors', 
//                                    array(
//                                         'nom' => $request->request->get('nom')[$i],
//                                         'prenom' => $request->request->get('prenom')[$i],
//                                         'numserie' => $request->request->get('numserie')[$i],
//                                         'user_id' => $request->request->get('userid')[$i],
//                                         'user_id_glpi' => $request->request->get('uid')[$i],
//                                         'status_id' => 1,
//                                         'username' => $request->request->get('username')[$i],
//                                         'usernameglpi' => $request->request->get('usernameglpi')[$i],
//                                         'manufacturer' => $request->request->get('manufacturer')[$i],
//                                         'monitormodel' => $request->request->get('monitormodel')[$i],
//                                         'codebarre' => $request->request->get('codebarre')[$i],
//                                         'comment' => $request->request->get('comment')[$i],                                       
//                                         'restitutiondate' => $request->request->get('restitutiondate'),
//                                          )
//                );   
           
          $materielexists = $this->getDoctrine()
                                 ->getRepository('DRIBundle:Monitors')
                                 ->findBy( array(
                                       'nom' => $request->request->get('nom')[$i],
                                       'prenom' => $request->request->get('prenom')[$i],
                                       'numserie' => $request->request->get('numserie')[$i],
                                       'userIdGlpi' => $request->request->get('uid')[$i],
                                       'username' => $request->request->get('username')[$i],
                                       'usernameglpi' => $request->request->get('usernameglpi')[$i],
                                       'manufacturer' => $request->request->get('manufacturer')[$i],
                                       'monitormodel' => $request->request->get('monitormodel')[$i],
                                       'codebarre' => $request->request->get('codebarre')[$i],
                                       'comment' => $request->request->get('comment')[$i],
                                      // 'dateRestitution' => new \DateTime($request->request->get('restitutiondate')),
                                        )
                                  );
//var_dump($materielexists);die();
                  if(!count($materielexists)){
                           $materiel = new Monitors();
                           $status = new Status();

                           $user = $em->getRepository("DRIBundle:User")->find($request->request->get('userid')[$i]);
                           $status = $em->getRepository("DRIBundle:Status")->find(1);  

                           $materiel->setNom($request->request->get('nom')[$i]);
                           $materiel->setPrenom($request->request->get('prenom')[$i]);
                           $materiel->setNumserie($request->request->get('numserie')[$i]);
                           $materiel->setUser($user);
                           $materiel->setUserIdGlpi($request->request->get('uid')[$i]);
                           $materiel->setStatus($status);
                           $materiel->setUsername($request->request->get('username')[$i]);
                           $materiel->setUsernameglpi($request->request->get('usernameglpi')[$i]); 
                           $materiel->setManufacturer($request->request->get('manufacturer')[$i]);
                           $materiel->setMonitormodel($request->request->get('monitormodel')[$i]);
                           $materiel->setCodebarre($request->request->get('codebarre')[$i]);
                           $materiel->setComment($request->request->get('comment')[$i]);
                           $materiel->setRestitutiondate(new \DateTime($request->request->get('restitutiondate')));
                           $materiel->setForwardemail($request->request->get('fwdemail'));
                           $em->persist($materiel);

                           $em->flush();

                           $request->getSession()->getFlashBag()->add(
                                   'notice',
                                    'Your changes were saved!'
                                 );
                   } 

         }
           
        } 
          return $this->redirect($this->generateUrl('_glpiusersmonitors'));
    }
    
    
         
   
    
   
    public function dridetailsAction($id, Request $request)
    {
         $materials = array();
         
         
         
//          $statement = $this->getConn()->executeQuery('SELECT u.displayname, drim.*, dris.status_label FROM user u 
//                                                       INNER JOIN dri_materiel drim                                        
//                                                       ON u.id = drim.user_id
//                                                       INNER JOIN dri_status dris
//                                                       ON drim.status_id = dris.id
//                                                       WHERE u.id=? ORDER BY drim.id ASC',array($id));
             
            //$materials = $this->usershardwareAction($request->request->get("users"));
          //  $materials = $statement->fetchAll();
            
//               $materials = $this->getDoctrine()
//                                 ->getRepository('DRIBundle:User')
//                                 ->findAll(); 
         
                    $em = $this->getDoctrine()->getManager();
                    $repository = $em->getRepository('DRIBundle:User');
                    $materials = $repository->find($id);
            
                    
//                    foreach($materials->getMateriels() as $m){
//                         echo "<pre>";
//                            var_dump($m->getCodebarre(), 
//                                     $m->getStatus()->getStatusLabel(),
//                                     $m->getUser()->getDisplayname());
//                         echo "<pre>";
//                    }
//                    
//                    
//                    die();
               
            $form = $this->createFormBuilder()
                ->add('submitul', 'submit', array('label' => 'Save'))
               // ->setAction($this->generateUrl('_savedetails'))
               // ->setMethod("post")                
                ->getForm();

            
            return $this->render('DRIBundle:DRI:dridetails.html.php', 
                                  array(
                                        'user' => $this->getUser(),
                                        'materials' => $materials,                                      
                                        'form' => $form->createView()
                                       ));

              
    }
    
    public function modifydridetailsAction(Request $request)
    {
        
         $em = $this->getDoctrine()->getManager();
         
         if($request->request->get("updatedetails")){
            //var_dump($request->request->get('uid'));die();
//              $this->getConn()->update('dri_materiel', array('status_id' => $request->request->get("statuses")), 
//                                                       array('id' => $request->request->get("id")));
            
            $query = $em->createQuery("UPDATE DRIBundle\Entity\Materiel m SET m.status =?1  WHERE m.id=?2");
            $query->setParameter(1, $request->request->get("statuses"));
            $query->setParameter(2, $request->request->get("id"));
            $numUpdated = $query->execute();
         }
         
         if($request->request->get("deletedetails")){
             
             //$this->getConn()->delete('dri_materiel', array('id' => $request->request->get("id")));
             $query = $em->createQuery('DELETE FROM DRIBundle\Entity\Materiel m  WHERE m.id=?1');
             $query->setParameter(1, $request->request->get("id"));
             $numDeleted = $query->execute();

         }
    
        return $this->redirect($this->generateUrl('_dridetails',array('id'=>$request->request->get('uid'))));
         
    }
    
    public function savedetailsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
//        var_dump($request->request->get('nom')[0]);
  //      var_dump($request->request->all());die();
        
       for($i=0;$i<count($request->request->get('nom'));$i++){
           
//          $this->getConn()->insert('dri_materiel', 
//                                    array(
//                                         'user_id' => $request->request->get('userid')[$i],
//                                         'user_id_glpi' => $request->request->get('uid')[$i],
//                                         'status_id' => 1,
//                                         'username' => $request->request->get('username')[$i],
//                                         'username_glpi' => $request->request->get('usernameglpi')[$i],
//                                         'manufacturer' => $request->request->get('manufacturer')[$i],
//                                         'computermodel' => $request->request->get('computermodel')[$i],
//                                         'monitormodel' => $request->request->get('monitormodel')[$i],
//                                         'peripheralmodel' => $request->request->get('peripheralmodel')[$i],
//                                         'printermodel' => $request->request->get('printermodel')[$i],
//                                         'phonemodel' => $request->request->get('phonemodel')[$i],
//                                         'designation' => $request->request->get('designation')[$i],
//                                         'numserie' => $request->request->get('numserie')[$i],
//                                         'codebarre' => $request->request->get('codebarre')[$i],
//                                         'date_restitution' => $request->request->get('restitutiondate'),
//                                          )
//                                   );    
// var_dump($request->request->get('userid')[$i]);die();
            $materielexists = $this->getDoctrine()
                                   ->getRepository('DRIBundle:Materiel')
                                   ->findBy( array(
                                         'userIdGlpi' => $request->request->get('uid')[$i],
                                         'username' => $request->request->get('username')[$i],
                                         'usernameGlpi' => $request->request->get('usernameglpi')[$i],
                                         'manufacturer' => $request->request->get('manufacturer')[$i],
                                         'computermodel' => $request->request->get('computermodel')[$i],
                                         'monitormodel' => $request->request->get('monitormodel')[$i],
                                         'peripheralmodel' => $request->request->get('peripheralmodel')[$i],
                                         'printermodel' => $request->request->get('printermodel')[$i],
                                         'phonemodel' => $request->request->get('phonemodel')[$i],
                                         'designation' => $request->request->get('designation')[$i],
                                         'numserie' => $request->request->get('numserie')[$i],
                                         'codebarre' => $request->request->get('codebarre')[$i],
                                        // 'dateRestitution' => new \DateTime($request->request->get('restitutiondate')),
                                          )
                                    );

           if(!count($materielexists)){
                    $materiel = new Materiel();
                    $status = new Status();
                    $user = $em->getRepository("DRIBundle:User")->find($request->request->get('userid')[$i]);
                    $status = $em->getRepository("DRIBundle:Status")->find(1);       
                    $materiel->setUser($user);
                    $materiel->setUserIdGlpi($request->request->get('uid')[$i]);
                    $materiel->setStatus($status);
                    $materiel->setUsername($request->request->get('username')[$i]);
                    $materiel->setUsernameGlpi($request->request->get('usernameglpi')[$i]); 
                    $materiel->setManufacturer($request->request->get('manufacturer')[$i]);
                    $materiel->setComputermodel($request->request->get('computermodel')[$i]);
                    $materiel->setMonitormodel($request->request->get('monitormodel')[$i]);
                    $materiel->setPeripheralmodel($request->request->get('peripheralmodel')[$i]);
                    $materiel->setPrintermodel($request->request->get('printermodel')[$i]);
                    $materiel->setPhonemodel($request->request->get('phonemodel')[$i]); 
                    $materiel->setDesignation($request->request->get('designation')[$i]);
                    $materiel->setNumserie($request->request->get('numserie')[$i]);                  
                    $materiel->setCodebarre($request->request->get('codebarre')[$i]);
               
                    $materiel->setDateRestitution(new \DateTime($request->request->get('restitutiondate')));

                    $em->persist($materiel);
                          
                    $em->flush();                    
                                  
           }

       }
       
       
       

       
            $request->getSession()->getFlashBag()->add(
                 'notice',
                  'Your changes were saved!'
               );
     
          return $this->redirect($this->generateUrl('_glpiusersmaterials'));

    }
    
    
    public function drilistAction(Request $request)
    {
        
     $paginator  = $this->get('knp_paginator');
     // var_dump($this->getUser());die();
     $em = $this->getDoctrine()->getManager();
     $repository = $em->getRepository('DRIBundle:User');
     $results = $repository->findAll();

//               echo "<pre>";
//                  var_dump($results);
//               echo "<pre>";die();
               
     
//     return $this->render('DRIBundle:Default:listusers.html.php', array('users'=>$users,'user'=>$this->getUser()));
        
//     $em    = $this->get('doctrine.orm.entity_manager');
//    $dql   = "SELECT a FROM DRIBundle:User a";
//    $query = $em->createQuery($dql);
        
//        $statement = $this->getConn()->executeQuery('SELECT u.displayname, spu.*, spuc.* FROM user u 
//                                  LEFT JOIN se_projet_user spu                                        
//                                  ON u.id = spu.user_id
//                                  LEFT JOIN se_projet_user_capacite spuc
//                                  ON spu.user_id = spuc.user_id 
//                                  WHERE u.id=?',array(2));
//        $result= $statement->fetchAll();
     
     
       if(count($results)){
           
//               $statement = $this->getConn()->executeQuery('SELECT u.displayname, spu.*, spuc.* FROM user u 
//                                                            LEFT JOIN se_projet_user spu                                        
//                                                            ON u.id = spu.user_id
//                                                            LEFT JOIN se_projet_user_capacite spuc
//                                                            ON spu.user_id = spuc.user_id 
//                                                            WHERE u.id=?',array(2));
//                $result= $statement->fetchAll();
//           
     
                $users = $paginator->paginate(
                                        $results, 
                                        $request->query->getInt('page', 1)/*page number*/,
                                        10/*limit per page*/
                                    );

                 $users->setTemplate('DRIBundle:Pagination:twitter_bootstrap_v3_pagination.html.php ');


            //       echo "<pre>";
            //         var_dump($users);
            //       echo "</pre>";  die();

                // parameters to template
                return $this->render('DRIBundle:DRI:listdri.html.php', array('users'=>$users,
                                                                             'user'=>$this->getUser(),
                                                                                ));
            }
            
            return new Response("<p>There are no results </p>");
    }
    

}