  <!DOCTYPE html>
<html>
  <head>

  </head>
   <body >
    <?php if(count($materiels['nom']) && $loggeduser):?>
       
       <p>Bonjour,</p>
        <p>
            <?php 
                 $soc = isset($societe[0]['societe'])?$societe[0]['societe']:"";
                 $nom = isset($materiels['nom'][0])?$materiels['nom'][0]:"";
                 $prenom = isset($materiels['prenom'][0])?$materiels['prenom'][0]:"";
                                      
            ?>
        <?php echo $loggeduser->getGivenname()." ".$loggeduser->getSurname();?> vient de réaliser une DRI pour l’utilisateur :
        <?php echo  "<strong>".$nom." ".$prenom."</strong>"; ?>
        (société : <?php echo  "<strong>".$soc."</strong>"; ?>).
        <br/>
        Vous trouverez ci-dessous, la liste de matériel que l’utilisateur doit restituer au plus tard le 
            <?php 
           if($materiels['restitutiondate'])
              echo "<strong>".date("d-m-Y", strtotime($materiels['restitutiondate']))."</strong>";
           else
              echo "<strong>".date("d-m-Y")."</strong>";

           ?>.

        <?php if($materiels['fwdemail']):?>
           La boite mail de l’utilisateur doit être transféré vers <?php echo $materiels['fwdemail'];?>
        <?php endif;?>
        </p>

    <div class="wrapper">
                  <div>
                    <table style="border:1px solid black;" >
                      <thead style="border:1px solid black;">
                        <tr>
                          <th style="border:1px solid black;">Statut</th>  
                          <th style="border:1px solid black;">Code barre</th>
                          <th style="border:1px solid black;">Nom utilisateur</th>
                          <th style="border:1px solid black;">Fabriquant</th>
                          <th style="border:1px solid black;">Modèle</th>
                          <th style="border:1px solid black;">Numéro de série</th>                       
                          <th style="border:1px solid black;">Désignation</th> 
                                             
<!--                          <th style="border:1px solid black;">Forward to</th> -->
                        </tr>
                      </thead>
                                          
                      <tbody>
                         <?php for($i=0; $i<count($materiels['nom']);$i++): ?>
                          
                            <tr style="border:1px solid black;">
                                <td style="border:1px solid black;">
                                    <?php 
                                     $statut = isset($materiels['statut'][$i])?$materiels['statut'][$i]:""; 
                                     
                                            if($statut == "En fonction"){

                                                echo "<span style='background-color:#00A65A;
                                                                   color:#fff'>";   
                                                 echo $statut;
                                                echo  "</span>";

                                             }else                                
                                               echo  $statut; 
                                    ?>
                                </td>
                                <td style="border:1px solid black;">
                                    <?php echo  $codebarre = isset($materiels['codebarre'][$i])?$materiels['codebarre'][$i]:""; ?>
                                </td>
                                <td style="border:1px solid black;">
                                    <?php
                                       $nom = isset($materiels['nom'][$i])?$materiels['nom'][$i]:"";
                                       $prenom = isset($materiels['prenom'][$i])?$materiels['prenom'][$i]:"";
                                       
                                       echo  $nom." ".$prenom;
                                    ?>
                                </td>
                                <td style="border:1px solid black;">
                                    <?php echo  $manufacturer = isset($materiels['manufacturer'][$i])?$materiels['manufacturer'][$i]:""; ?>
                                </td>
                                <td style="border:1px solid black;">
                                    <?php echo  $model = isset($materiels['model'][$i])?$materiels['model'][$i]:""; ?>
                                </td> 
                                <td style="border:1px solid black;">
                                    <?php echo  $numserie = isset($materiels['numserie'][$i])?$materiels['numserie'][$i]:""; ?>
                                </td>

                                <td style="border:1px solid black;">
                                    <?php echo  $designation = isset($materiels['designation'][$i])?$materiels['designation'][$i]:""; ?>
                                </td>

<!--                                <td style="border:1px solid black;"><?php //echo  $materiels['fwdemail']; ?></td>-->
                            </tr>
                           
                         <?php endfor; ?>
                    </tbody>
                    </table>
                  </div><!-- /.table-responsive -->
                </div><!-- /.box-body -->
                
        <?php endif;?>
  </body>
</html>