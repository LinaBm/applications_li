  <!DOCTYPE html>
<html>
  <head>

   <link rel="stylesheet" href="<?php echo $view['assets']->getUrl('bundles/dri/adminlte230/bootstrap/css/bootstrap.min.css') ?>">
   
  </head>
   <body >
    <div class="wrapper">
                  <div>
                    <table style="border:1px solid black;" >
                      <thead style="border:1px solid black;">
                        <tr>
                          <th style="border:1px solid black;">User name</th>
                          <th style="border:1px solid black;">Manufacturer</th>
                          <th style="border:1px solid black;">Phone Model</th>
                          <th style="border:1px solid black;">Numserie</th>
                          <th style="border:1px solid black;">Code Barre</th>
                          <th style="border:1px solid black;">Comment</th>
                          <th style="border:1px solid black;">Restitution date</th>
                          <th style="border:1px solid black;">Forward to</th> 
                        </tr>
                      </thead>
                                          
                      <tbody>
                         <?php for($i=0; $i<count($materiels['nom']);$i++): ?>
                          
                            <tr style="border:1px solid black;">    
                                <td style="border:1px solid black;"><?php echo  $materiels['nom'][$i]." ".$materiels['prenom'][$i]; ?></td>
                                <td style="border:1px solid black;"><?php echo  $materiels['manufacturer'][$i]; ?></td>
                                <td style="border:1px solid black;"><?php echo  $materiels['phonemodel'][$i]; ?></td> 
                                <td style="border:1px solid black;"><?php echo  $materiels['numserie'][$i]; ?></td>
                                <td style="border:1px solid black;"><?php echo  $materiels['codebarre'][$i]; ?></td>
                                <td style="border:1px solid black;"><?php echo  $materiels['comment'][$i]; ?></td>
                                <td style="border:1px solid black;"><?php echo  $materiels['restitutiondate']; ?></td>
                                <td style="border:1px solid black;"><?php echo  $materiels['fwdemail']; ?></td>
                            </tr>
                           
                         <?php endfor; ?>
                    </tbody>
                    </table>
                  </div><!-- /.table-responsive -->
                </div><!-- /.box-body -->
  </body>
</html>