  <!DOCTYPE html>
<html>
  <head>

  </head>
   <body >
       
    <?php if(count($materials)):?>

       
       <p>Bonjour,</p>
        <p>
          L'utilisateur <?php echo  "<strong>".$enquireuser->getGivenname()." ".$enquireuser->getSurname()."</strong>"; ?> n'a pas rendu :          
            <?php foreach($materials as $material):?>
              <br/> <?php echo  "<strong> - ".$material->getCodebarre()."</strong>"; ?>.     
            <?php endforeach;?>
          <br/><br/>
          Ce dernier aurait dû être restitué depuis le             
          <?php 
           if($material->getRestitutiondate())
              echo "<strong>".$material->getRestitutiondate()->format("d-m-Y")."</strong>";
           else
              echo "<strong>".date("d-m-Y")."</strong>";
           ?>.
          <br/>      
          S'il n'est pas retourné, le matériel sera facturé à votre société.
          <br/><br/>
          Cordialement,
        </p>
    <?php endif;?>
   </body>
</html>
