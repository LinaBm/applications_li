
<?php $view->extend('DRIBundle::base.html.php') ?>

<?php $view['slots']->set('title', 'Users') ?>

<?php  $view['slots']->start('listusers') ?>
   <div class="count">
    <?php echo $users->getTotalItemCount(); ?>
</div>             
                        
    <div class="col-md-12">           
            <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Users</h3>

                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Username</th>
                          <th>Email</th>
                          <th>Status</th>                      
                        </tr>
                      </thead>
                                          
                      <tbody>
                       <?php foreach ($users as $user): ?>
                            <tr>
                             <td><a href="<?php echo $view['router']->generate('_userdetails', array(
                                                                               'id' => $user->getId()
                                                                              )) ?>"
                                 >  
                                    <?php echo  $user->getGivenname()." ".$user->getSurname();?>
                                 </a>
                             </td>
                       
                             <td><?php echo  $user->getUsername(); ?></td>
                             <td><?php echo  $user->getEmail(); ?></td>
                             <td><span class="label label-success">ok</span></td>                        
                           </tr>
                        <?php endforeach; ?>
                         <div class="navigation">
                            <?php echo $view['knp_pagination']->render($users); ?>
                         </div>     
                      </tbody>
                    </table>
                  </div><!-- /.table-responsive -->
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                  <a href="javascript::;" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>
                  <a href="javascript::;" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
                </div><!-- /.box-footer -->
       </div><!-- /.box -->
    </div>          
 
<?php $view['slots']->stop() ?>

