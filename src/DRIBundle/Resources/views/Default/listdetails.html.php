
<?php $view->extend('DRIBundle::base.html.php') ?>

<?php $view['slots']->set('title', 'Details about user') ?>

<?php  $view['slots']->start('listdetails') ?>
                
                        
    <div class="col-md-12">           
            <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">User Details</h3>

                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>poste</th>
                          <th>pole</th>
                          <th>taux horaire</th>
                          <th>capacite</th>    
                          <th>period</th>
                          <th>pole</th>
                          <th>capacity</th>
                          <th>remaining hours</th>  
                        </tr>
                      </thead>

                      
                      <tbody>

                       <?php foreach ($attributes as $attribute): ?>
                            <tr>                   
                             <td><?php echo  $attribute['poste']; ?></td>
                             <td><?php echo  $attribute['pole']; ?></td>
                             <td><?php echo  $attribute['taux_horaire']; ?></td>
                             <td><?php echo  $attribute['capacite']; ?></td>
                             <td><?php echo  $attribute['period']; ?></td>
                             <td><?php echo  $attribute['pole']; ?></td>
                             <td><?php echo  $attribute['hours']; ?></td>
                             <td><?php echo  $attribute['remaining_hours']; ?></td>
                                  
                           </tr>
                        <?php endforeach; ?>
                              
                      </tbody>
                    </table>
                  </div><!-- /.table-responsive -->
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
               
                  <a href="<?php echo $view['router']->generate('_userlist');?>" 
                     class="btn btn-sm btn-info btn-flat pull-left"
                  >
                      View All Users
                  </a>
                </div><!-- /.box-footer -->
       </div><!-- /.box -->
    </div>          
 
<?php $view['slots']->stop() ?>

