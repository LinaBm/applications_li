<!DOCTYPE html>
<html>
  <head>
       <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
   
  
 
    
    <title><?php $view['slots']->output('title', 'Paneau Administration') ?></title>
    
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    
    <!-- Bootstrap 3.3.5 -->
      
    <link rel="stylesheet" href="<?php echo $view['assets']->getUrl('bundles/dri/adminlte230/bootstrap/css/bootstrap.min.css') ?>">
    
    <!-- Font Awesome -->
<!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">-->
    
    <!-- Ionicons -->
<!--    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">-->
    
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $view['assets']->getUrl('bundles/dri/adminlte230/dist/css/AdminLTE.min.css') ?>">
    
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" 
          href="<?php echo $view['assets']->getUrl('bundles/dri/adminlte230/dist/css/skins/skin-blue.min.css') ?>"
    >
    
    <!-- iCheck -->
    <link rel="stylesheet" 
          href="<?php echo $view['assets']->getUrl('bundles/dri/adminlte230/plugins/iCheck/flat/blue.css') ?>"
    >
    
    <!-- Morris chart -->
    <link rel="stylesheet" 
          href="<?php echo $view['assets']->getUrl('bundles/dri/adminlte230/plugins/morris/morris.css') ?>"
    >
    
    <!-- jvectormap -->
    <link rel="stylesheet" 
          href="<?php echo $view['assets']->getUrl('bundles/dri/adminlte230/plugins/jvectormap/jquery-jvectormap-1.2.2.css') ?>"
    >
    
    
    <!-- Date Picker -->
    <link rel="stylesheet" 
          href="<?php echo $view['assets']->getUrl('bundles/dri/adminlte230/plugins/datepicker/datepicker3.css') ?>"
    >
    
    
    <!-- Daterange picker -->
    <link rel="stylesheet" href=""
          href="<?php echo $view['assets']->getUrl('bundles/dri/adminlte230/plugins/daterangepicker/daterangepicker-bs3.css') ?>"
    >
    
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet"
          href="<?php echo $view['assets']->getUrl('bundles/dri/adminlte230/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') ?>"
    >
    
    <link 
          rel="stylesheet" 
          type="text/css" 
          href="<?php echo $view['assets']->getUrl('bundles/dri/DataTables/datatables.css') ?>"
    >
 

    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
    
    <!-- jQuery 2.1.4 -->
    <script 
            src="<?php echo $view['assets']->getUrl('bundles/dri/adminlte230/plugins/jQuery/jQuery-2.1.4.min.js') ?>"           
    >
    </script>
    
    <script type="text/javascript" 
            charset="utf8" 
            src="<?php echo $view['assets']->getUrl('bundles/dri/DataTables/datatables.js') ?>"
    >
    </script>
    
    <!-- jQuery UI 1.11.4 -->
<!--    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    
    
     Resolve conflict in jQuery UI tooltip with Bootstrap tooltip 
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
     Bootstrap 3.3.5 
    <script src="bootstrap/js/bootstrap.min.js"></script>
     Morris.js charts 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="plugins/morris/morris.min.js"></script>
     Sparkline 
    <script src="plugins/sparkline/jquery.sparkline.min.js"></script>
     jvectormap 
    <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
     jQuery Knob Chart 
    <script src="plugins/knob/jquery.knob.js"></script>
     daterangepicker 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="plugins/daterangepicker/daterangepicker.js"></script>
     datepicker 
    <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
     Bootstrap WYSIHTML5 
    <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
     Slimscroll 
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
     FastClick 
    <script src="plugins/fastclick/fastclick.min.js"></script>
     AdminLTE App 
    <script src="dist/js/app.min.js"></script>
     AdminLTE dashboard demo (This is only for demo purposes) 
    <script src="dist/js/pages/dashboard.js"></script>
     AdminLTE for demo purposes 
    <script src="dist/js/demo.js"></script>-->
    
    <?php if (  $view['slots']->has('listusersmaterials')
              ||$view['slots']->has('adminlistusers')
              ||$view['slots']->has('adminuserdetails')
              ||$view['slots']->has('edituser')
             ): ?> 
        <link rel="stylesheet" href="<?php echo $view['assets']->getUrl('bundles/dri/chosen/chosen.css') ?>">
        <script src="<?php echo $view['assets']->getUrl('bundles/dri/chosen/chosen.jquery.js') ?>" type="text/javascript"></script>
        <script src="<?php echo $view['assets']->getUrl('bundles/dri/chosen/docsupport/prism.js') ?>" type="text/javascript" charset="utf-8"></script>
              
        <script src="<?php echo $view['assets']->getUrl('bundles/dri/js/jquery-ui-1.11.4/jquery-ui.js');?>"></script>
        <link rel="stylesheet" href="<?php echo $view['assets']->getUrl('bundles/dri/js/jquery-ui-1.11.4/jquery-ui.css');?>">
        
        <script type="text/javascript">

            $( document ).ready(function() {
              $(".chosen-select").chosen();
              $(".chosen-select-emails").chosen();
              $(".chosen-select-users").chosen();            
              $(".chosen-select-societe").chosen();
              
                          
              $("#datepicker").datepicker({firstDay: 1,
                                             dateFormat: "dd-mm-yy",
                                             minDate: 0,
                                             beforeShowDay: $.datepicker.noWeekends,
                                             closeText: 'Fermer',
                                             prevText: 'Précédent',
                                             nextText: 'Suivant',
                                             currentText: 'Aujourd\'hui',
                                             monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                                             monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                                             dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                                             dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                                             dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                                             weekHeader: 'Sem.'});
                                         

                      
             $( "#datepicker" ).datepicker("setDate", new Date());
             
             
                $("#datepickeradmin1, #datepickeradmin2").datepicker({
                                    firstDay: 1,
                                    dateFormat: "dd-mm-yy",
                                  //  minDate: 0,
                                    beforeShowDay: $.datepicker.noWeekends,
                                    closeText: 'Fermer',
                                    prevText: 'Précédent',
                                    nextText: 'Suivant',
                                    currentText: 'Aujourd\'hui',
                                    monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                                    monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                                    dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                                    dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                                    dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                                    weekHeader: 'Sem.'});
                                         

                      
             $( "#datepickeradmin1, #datepickeradmin2" ).datepicker("setDate", new Date());  
             
            
          $('#fwdemail').tooltip()
          

           // $('[data-toggle="tooltip"]').tooltip({'placement': 'top'});
            $('#adminuserlist').DataTable({
                        bInfo : false,
                         language: {
                                     "lengthMenu": "Nombre de lignes :  _MENU_ ",
                                     "search":         "Rechercher : ",
                                      paginate: {
                                            "first":      "Premier",
                                            "last":       "Dernier",
                                            "next":       "Suivant",
                                            "previous":   "Antérieur"
                                        },
                                    }
                    });
                    
            $('#ml').DataTable({
                       searching: false,
                       paging: false,
                       bInfo : false
                    });
                    
                    
                $(".errormsgchktext").hide();   
                $("#chkautreadresseinnertext").val("ascuns");
                       //console.log($('#chkautreadresse'));
                $("#chkautreadressetext").hide();
                
                $('#chkautreadresse').on('click',function() 
                {
                   // 
                        if ($('#chkautreadresse').is(':checked')) {
                            
                             $(".errormsgchktext").hide(); 
                             
                            $("#chkautreadresseinnertext").val("");
                            $("#chkautreadressetext").show();
                            
                        } else {
                           $("#chkautreadresseinnertext").val("ascuns");
                            $("#chkautreadressetext").hide();
                        } 
                }); 
//                 $("#details").on("click", function(){
//
//                                alert("hhdfh");            
//                          });
   
                
              $("#detailsform").submit(function(){
                    var emailaddress = $("#chkautreadresseinnertext").val().trim();
                    var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
                   // alert( $("#chkautreadresseinnertext").val());  
                     //
                    if(emailaddress == "ascuns"){
                     //   alert( $("#chkautreadresseinnertext").val());
                        return true;
                   }                       
                   else{
                        if(!pattern.test(emailaddress) || emailaddress == ""){
                          $(".errormsgchktext").show();
                          return false;
                        } 
                   }
                   
                   return true;
              
              });
              

           //return pattern.test(emailAddress);
                
                

           });
                 
        </script>
        
        
        <style type="text/css">
            .table-responsive{
                
                overflow-x: hidden !important;
            }
            
            .content-wrapper, .right-side, .main-footer{
                margin-left:0px !important;
            }
            .main-sidebar{
                width:0px !important;
            }
        </style>
    <?php endif;?>
 
  </head>
  
  <body class="hold-transition skin-blue sidebar-mini" style="background-color: #ECF0F5">
    <div class="wrapper">
  
      <header class="main-header">
        <!-- Logo -->
        <a href="" class="logo">
          <span class="logo-lg">DRI</span>
        </a>
        
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
        
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                  
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    
<!--                  <img 
                       src="<?php //echo $view['assets']->getUrl('bundles/dri/adminlte230/dist/img/user2-160x160.jpg') ?>" 
                                  class="user-image" alt="User Image"
                  >-->
                  
                  <span class="hidden-xs"><?php echo $user->getGivenname()." ".$user->getSurname();?></span>
                </a>

              </li>
 
            </ul>
          </div>
        </nav>
      </header>
        
      <!-- Left side column. contains the logo and sidebar -->
      <div class="main-sidebar" >
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
<!--              <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">-->
            </div>

          </div>
          <!-- search form -->
<!--          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>-->
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
<!--          <ul class="sidebar-menu">-->
<!--            <li class="header">NAVIGATION</li>-->
            
<!--            <li class="treeview">
              <a href="<?php //echo $view['router']->generate('_glpiusersmonitors') ?>">
                <span>Users and monitors</span>          
              </a>
            </li>-->

<!--            <li class="treeview">
              <a href="<?php //echo $view['router']->generate('_glpiusersprinters') ?>">
                <span>Users and printers</span>          
              </a>
            </li>-->
            
<!--            <li class="treeview">
              <a href="<?php //echo $view['router']->generate('_glpiusersperipherals') ?>">
                <span>Users and peripherals</span>          
              </a>
            </li>-->
            
<!--           <li class="treeview">
              <a href="<?php //echo $view['router']->generate('_glpiusersphones') ?>">
                <span>Users and phones</span>          
              </a>
            </li>-->
            
<!--           <li class="treeview">
              <a href="<?php //echo $view['router']->generate('_glpiuserscomputers') ?>">
                <span>Users and computers</span>          
              </a>
            </li>-->
           
<!--            <li class="treeview">
              <a href="<?php //echo $view['router']->generate('_glpiusersmaterials') ?>">
                <span>Materials</span>          
              </a>
            </li>-->
<!--             
            <li class="treeview">
              <a href="<?php //echo $view['router']->generate('_drilist') ?>">
                <span>Dri List</span>          
              </a>
            </li>-->
<!--          </ul>-->
        </section>
        <!-- /.sidebar -->
      </div>
      

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper" >
        <!-- Content Header (Page header) -->
        <section class="content-header">
<!--          <h1>
            Dashboard
            <small>Control panel</small>
          </h1>-->
            
<!--          <ol class="breadcrumb">
              
            <li><a href="#">
                    <i class="fa fa-dashboard"></i> Home</a>
            </li>
            
            <li class="active">Dashboard</li>
          </ol>-->
        </section>
              
        <!-- Main content -->
        <section class="content">       
          <!-- Main row -->
          <div class="row">
              
            <!-- Left col -->
            <section class="col-lg-12">
                
                 <?php 
                    $errors = array();
                    $errors = $view['session']->getFlash('notice'); 
                  ?>
                
               <?php if(count($errors)>0): ?>
                    <div class="col-md-12">           
                      <div class="box">
                            <div class="flash-notice" style="padding:20px;font-size: 16px; color:#00a157;">
                                <?php echo "<div class='flash-error'>$errors[0]</div>" ?>
                            </div>
                       </div>
                     </div>
                <?php endif; ?>
                
                <?php foreach ($view['session']->getFlash('notice') as $message): ?>
                    <div class="col-md-12">           
                      <div class="box">
                            <div class="flash-notice" style="padding:20px;font-size: 16px; color:#00a157;">
                                <?php echo "<div class='flash-error'>$message</div>" ?>
                            </div>
                       </div>
                     </div>
                <?php endforeach ?>
                              
                <?php if ($view['slots']->has('listusersmonitors')): ?>
                     <?php $view['slots']->output('listusersmonitors') ?> 
            
                <?php elseif ($view['slots']->has('edituser')): ?>
                     <?php $view['slots']->output('edituser') ?> 
                
                <?php elseif ($view['slots']->has('listusersmaterials')): ?>
                     <?php $view['slots']->output('listusersmaterials') ?> 
                
                <?php elseif ($view['slots']->has('adminlistusers')): ?>
                     <?php $view['slots']->output('adminlistusers') ?> 
                     
               <?php elseif ($view['slots']->has('adminuserdetails')): ?>
                     <?php $view['slots']->output('adminuserdetails') ?>
                     

                <?php endif ?>


                </section><!-- /.Left col -->

          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      
<!--    <footer class="main-footer" >
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.b
        </div>
        <strong>Copyright &copy; 2016 <a href="#">NOZ</a>.</strong> All rights reserved.
     </footer> -->


            <footer class="main-footer" 
                     style="
                           // float:left;
                            position: fixed;
                           // margin:0 auto;
                            //left: 0;
                           // top:20px;
                            bottom: 0;
                            max-height: 50px;
                            width: 100%;
                            //overflow:hidden;
                            "
                    >
                <i>Cette application a été développée par le Pôle WEB - Univers NOZ © <?php echo date('Y');?>.
                    Pour nous contacter : <a href="mailto:poleweb@noz.fr">envoyez-nous un mail</a></i>
                <span class='pull-right'>Version actuelle : 1.0.b</span>
            </footer>


      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
      
    </div><!-- ./wrapper -->


  </body>
</html>


