<?php $view->extend('DRIBundle::base.html.php') ?>

<?php $view['slots']->set('title', "Details Utilisateur") ?>

<?php  $view['slots']->start('adminuserdetails') ?>
                
 <?php //var_dump($user);die();?>                       
    <div class="col-md-12">           
            <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Details Utilisateur</h3>

                </div><!-- /.box-header -->
                
      <div class="box-body" style="min-height:500px;">
          
                <?php if($user->getUsername()):?>
            <div><strong>Login : </strong><?php echo $user->getUsername();?></div>
                <?php endif;?>
                    
                <?php if($user->getGivenname() && $user->getSurname()):?>
                    <div><strong>Nom User : </strong><?php echo $user->getGivenname()." ".$user->getSurname()?></div>
                <?php endif;?>    
                                      
                <?php if(isset($societe[0]['societe'])):?>
                    <div><strong>Societe : </strong><?php echo $societe[0]['societe']?></div>
                <?php endif;?>    
                
                <?php if($user->getMatricule()):?>
                    <div><strong>Matricule : </strong><?php echo $user->getMatricule();?></div>
                <?php endif;?>  
                          
                <?php if(count($materials)): ?>    
           
                  <div class="col-md-12"  >
                      
                    <?php echo "<form action='".$view['router']->generate('_adminsavedetailsmaterials')."'  method='post' name='dridetails'>";  ?>   
                      
                    <table class="table no-margin" >
                                         
                      <thead>
                        <tr>
                          <th>Id</th>  
                          <th>Num. série</th>                  
                          <th>Fabricant</th>
                          <th>Model</th>
                          <th>Codebarre</th>
                          <th>Designation</th>
                          <th>Commentaire</th>
                          <th>Date de réstitution</th>
                          <th>Mails transferé vers</th>
                          <th>Status</th>
                      
                        </tr>
                      </thead>      
                      
                
                      <tbody>                  
                       <?php foreach ($materials as $hardware): ?>
                                                 
                       <?php 
//echo "<form action='".$view['router']->generate('_savedetails', array("id"=> $hardware['NUMSERIE']))."'  method='post' name='details'>"; 
                       ?>
                            <tr>   
                          
                             <td><?php echo  $hardware->getId(); ?></td> 
                             <td><?php echo  $hardware->getManufacturer(); ?></td>
                             <td><?php echo  $hardware->getNumserie(); ?></td>
                             <td><?php echo  $hardware->getModel(); ?></td>
                             <td><?php echo  $hardware->getCodebarre(); ?></td>
                             <td><?php echo  $hardware->getDesignation(); ?></td>
                             <td>
                                 <textarea name="tareacomment[]" rows="3" cols="24" maxlength="500" ><?php echo  $hardware->getComment(); ?></textarea>
                             </td>
                             <td><?php echo  $hardware->getRestitutiondate()->format('d-m-Y'); ?></td>
                             <td><?php echo  $hardware->getForwardemail(); ?></td>
<!--                             <td><?php //echo  $hardware->getStatus()->getStatusLabel(); ?></td>-->
                        

<!--                             <td><?php //die();?></td>-->
                             
                      <?php //echo $view['form']->start($form); ?>
                             
                          
                            <input type="hidden" name="nom" value="<?php //echo  $hardware['NOM_USER'];?>">
                            <input type="hidden" name="prenom" value="<?php //echo  $hardware['PRENOM_USER'];?>">
                            <input type="hidden" name="numserie" value="<?php //echo  $hardware['NUMSERIE'];?>">
                            
                            <input type="hidden" name="uid" value="<?php echo  $hardware->getUser()->getId();?>">
                            <input type="hidden" name="id[]" value="<?php echo  $hardware->getId();?>">
                            <td>
                                
                                <select name='status[]' class="chosen-select"   > 
                                    
                                    <option value="3" >en attente</option>
                                          
                                     <option value="2" <?php if($hardware->getStatus()->getId() == 2) echo "selected";?>>litige</option>
                                  
                                     <option value="4" <?php if($hardware->getStatus()->getId() == 4) echo "selected";?>>termine</option> 
                                 </select>
   
                            </td>
<!--                            <td>
                                 <?php
        //                        echo $view['form']->widget($form['submitul'], array(
        //                                         'attr' => array('class'=>"btn btn-sm btn-default btn-flat pull-right")
        //                                          )); 
                                ?>
                               <input type='submit' class="btn btn-sm btn-default btn-flat pull-right" name='updatedetails' value='Save'> 
                            </td>-->
<!--                            <td>

                               <input type='submit' class="btn btn-sm btn-default btn-flat pull-right" name='deletedetails' value='Delete'> 
                            </td>-->
                                 <?php //echo $view['form']->end($form) ?>  
                                <?php //echo "</form>"; ?>  
                           </tr>
                                                   
                        <?php endforeach; ?>   

                      </tbody>
                    </table>
                          <input type='submit' class="btn btn-md btn-default btn-flat pull-right" name='updatedetails' value='Sauvegarder'> 
                          <?php echo "</form>"; ?>  
                          
                          
                            <a href="<?php 
               
                                echo $view['router']->generate('_adminlistusers');
                              ?>" 
                                 class="btn btn-md btn-info btn-flat pull-left"
                              >
                                  Retour
                            </a>
                  </div><!-- /.table-responsive -->
                  <?php else:?>
                     <?php echo "No results";?>
                  <?php endif;?>
                 
                </div><!-- /.box-body -->
                               
                <div class="box-footer clearfix">
               

                </div>
                <!-- /.box-footer -->
       </div><!-- /.box -->
    </div>          
 
<?php $view['slots']->stop() ?>

