<?php $view->extend('DRIBundle::base.html.php') ?>
<?php $view['slots']->set('title', 'Administration') ?>

<?php  $view['slots']->start('adminlistusers') ?>
           
    <div class="col-md-12">           
            <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">DRI Paneau Administration</h3>

                </div><!-- /.box-header -->
                <div class="box-body" style="min-height:500px;">

                    <div class='col-md-12 '>
                        <?php echo "<form action='".$view['router']->generate('_adminlistusers')."'  method='post' name='drinouveausers'>";  ?> 
                           
                           <input type='submit' class="btn btn-sm btn-default btn-flat label-primary pull-left" name='nouveausers' value='Nouveau' >                      
                           <input type='submit' class="btn btn-sm btn-default btn-flat pull-left label-warning " name='enattenteusers' value='En Attente'>            
                           <input type='submit' class="btn btn-sm btn-default btn-flat pull-left label-danger" name='litigeusers' value='Litige'>           
                           <input type='submit' class="btn btn-sm btn-default btn-flat pull-left label-success" name='termineusers' value='Termine'> 
                           <input type='submit' class="btn btn-sm btn-default btn-flat pull-left" name='allusers' value='Tous'> 
                                                  
                        <?php echo "</form>";?>   
                    </div> 
<!--                    <div style="clear: both;"></div>-->
          
                          <div class='col-md-12' style="margin:10px 0px 0px 0px"></div> 
                    <?php 
                        #################### START DROP DOWN USERI ################
                    
                       // $allusers = $em->getRepository('DRIBundle:User')->findAll();

                    ?>

                   <?php if(count($userswithcstatuses)):?>  
                       <div class='col-md-12' >
                    <?php       
                        echo "<form action='".$view['router']->generate('_adminlistusers')."'  method='post' name='drinouveausers'>";  
                    ?>    

                           <select name="cautareuname" class="chosen-select-users" style="width:296px">
                               <option value="">-- Sélectionner un utilisateur -- </option>
                               
                                <?php foreach ($userswithcstatuses as $user): ?>
                                   <option value="<?php echo $user->getUsername();?>"><?php echo  $user->getGivenname()." ".$user->getSurname();?></option>
                                <?php endforeach; ?>

                           </select>
                         
                           <input type='submit' class="btn btn-sm btn-default btn-flat " name='searchbyuname' value='Voir'> 
                        <?php echo "</form>";?>  
                       </div>
                       <div class='col-md-12' style="margin:10px 0px 0px 0px"></div>
                    <?php endif;
                     #################### END DROP DOWN USERI ################
                    ?>  
                           
                           
                    <?php 
                       #################### START DROP DOWN SOCIETATI ################
                       // $societe = array();
                         $repository = $em->getRepository('DRIBundle:User');

                         $societe = $repository->findSocietes($conn);   
                      // var_dump($societe);die();
                    ?> 

                   <?php if(count($societe)):?>  
                      <div class='col-md-12 '>
                    <?php                                     
                       echo "<form action='".$view['router']->generate('_adminlistusers')."'  method='post' name='drinouveausers'>";  
                    ?>  

                       <select name="cautaresocietate" class="chosen-select-societe" style="width:296px">
                           <option value="">-- Sélectionner une societe -- </option>

                            <?php foreach ($societe as $soc): ?>
                               <?php if(isset($soc['name'])):?>
                                 <option value="<?php echo $soc['name'];?>"><?php echo  $soc['name'];?></option>
                               <?php endif;?>
                            <?php endforeach; ?>

                       </select>

                       <input type='submit' class="btn btn-sm btn-default btn-flat" name='searchvalue' value='Voir'> 
                    <?php echo "</form>";?> 
                    </div>
                         <div class='col-md-12' style="margin:10px 0px 0px 0px"></div> 
                 <?php endif;
                 #################### END DROP DOWN SOCIETATI ################
                 ?>
                       
                    <div class=' col-md-12'>    
                        <?php  
                          #################### START DATEPICKER ################

                         echo "<form action='".$view['router']->generate('_adminlistusers')."'  method='post' name='drinouveausers'>";  
                        ?>
                        <span style="width:296px">
                           <input type="text" id="datepickeradmin1" name="usersbydate1" value=""/>
                           <input type="text" id="datepickeradmin2" name="usersbydate2" value=""/>
                        </span> 
                          <input type='submit' class="btn btn-sm btn-default btn-flat " name='searchbydate' value='Voir'> 

                       <?php echo "</form>";
                        #################### END DATEPICKER ################
                       ?>      

                    </div>  
                         <div class='col-md-12' style="overflow: visible; margin:20px 0px 20px 0px;">    
                </div>
                 <?php if(count($users)):?>
                   
                  <div class="table-responsive  col-md-12" >
                                                  
                    <table class="table no-margin display" id="adminuserlist">
                      <thead>
                        <tr>
                          <th>Uid</th>
                          <th>Nom</th>
                          <th>Login</th>
                          <th>Email</th>                         
                          <th>Societe</th>   
                          <th>Status</th> 
                          <th>Date de restitution</th>
<!--                      <th>Edit User</th>-->
                        </tr>
                      </thead>
                        <?php //die("ok");?>                  
                      <tbody>
                       <?php foreach ($users as $user): ?>
                            <tr>
                             <td>
                                 <?php  echo $user->getId();?>
                             </td>
                             
                             <td><a href="<?php  echo $view['router']->generate('_adminlistuserdetails', array(
                                                                                'id' => $user->getId()
                                                                               )) ?>" 
                                      
                                 >  
                                    <?php echo  $user->getGivenname()." ".$user->getSurname();?>
                                 </a>
                             </td>
                             
                          
                             
                             <td><?php echo  $user->getUsername(); ?></td>
                             <td><?php echo  $user->getEmail(); ?></td>
                             <td>
                                 <?php 
                                      
                                        $repository = $em->getRepository('DRIBundle:User');
                                     // $user->getUsername(); 
                                        $societe = $repository->findUsersAndSociete($user->getUsername(), $conn); 
                                        //var_dump($societe);die();
                                     // echo $repository->get
                                        if(count($societe)){
                                            echo $societe[0]['societe'];
                                        }
                                  ?>
                             </td>
                             <td>
                                
                            <?php 

                               $statement = $lconn->executeQuery('SELECT * FROM dri_computedstatus
                                                                            WHERE user_id=?', array($user->getId()));

                               $cstat = $statement->fetchAll();

                                      
                                $label = "";
                                $status = "";
                                
                               if(count($cstat)){
                                           
                                           
                                 //echo $cstat[0]['computedstatus'];
                                          
                                  if($cstat[0]['computedstatus'] == 1){
                                     $label = "label-primary";
                                     $status = "nouveau";
                                 }
                                 elseif($cstat[0]['computedstatus'] == 2){
                                     $label = "label-danger";
                                     $status = "litige";
                                 }
                                 elseif($cstat[0]['computedstatus'] == 3){
                                     $label = "label-warning";
                                     $status = "en attente";
                                 }
                                 elseif($cstat[0]['computedstatus'] == 4){
                                     $label = "label-success";
                                     $status = "termine";
                                 }
                                 
                               echo   "<span class='".$label."'>";   
                               echo $status;
                               echo  "</span>";
 
                            }else{
                                
                             echo   "<span class='label-primary'>";   
                             echo "nouveau";
                             echo  "</span>";
                             
                            }
                                          
                                        //echo "OK";
                                         //var_dump($user->getMaterials()[0]->getStatus()->getId());die(); 
                                    ?>
                             </td>     
                             <td>
                                 <?php
                                  //var_dump($user->getId());die();
                                    $statement = $lconn->executeQuery('SELECT restitutiondate FROM dri_materials
                                                                       WHERE user_id=?', array($user->getId()));

                                    $restitutiondate = $statement->fetch();
                                   // var_dump($restitutiondate);die();
//                                    foreach($restitutiondates as $rd)
//                                        $restdate = $rd;
                                    
                                    if(isset($restitutiondate['restitutiondate']))
                                      // echo date("d-m-Y",strtotime($restitutiondate['restitutiondate']));
                                       echo date("Y-m-d",strtotime($restitutiondate['restitutiondate']));
                                   // var_dump( $restdate );die();
                                 ?>
                             </td>
                             
<!--                            <td><a href="<?php // echo $view['router']->generate('_edituserdetails', array('id' => $user->getId())) ?>" >  
                                             <?php // echo "Edit";?>
                                 </a>
                             </td>
                             -->
                           </tr>
                        <?php endforeach; ?>

                      </tbody>
                    </table>
                  </div><!-- /.table-responsive -->
                <div class="col-md-12">  
                   <div class="navigation">
                      <?php //echo $view['knp_pagination']->render($users); ?>
                   </div>     
                 </div>  
                  <?php else:?>
                   <div class="col-md-4 callout alert alert-info alert-dismissible" style="font-size:18px;">  
                       
<!--                       <h4>-->
                            <?php echo "Rien a afficher";?>
<!--                       </h4>-->
                   </div>
                 <?php endif;?>
                  
              
                </div><!-- /.box-body -->

       </div><!-- /.box -->
    </div>          
 
<?php $view['slots']->stop() ?>

