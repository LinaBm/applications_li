<?php $view->extend('DRIBundle::base.html.php') ?>

<?php $view['slots']->set('title', 'Details about user') ?>

<?php  $view['slots']->start('edituser') ?>
                
                        
    <div class="col-md-12">           
            <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Utilisateur</h3>

                </div><!-- /.box-header -->
                
      <div class="box-body" style="min-height:500px;">
          <h3>Following errors</h3>
<ul>

</ul>

             
    <?php //echo $view['form']->start($form, array('attr' => array('novalidate' => 'novalidate'),)) ?>
    <?php echo "<form action='".$view['router']->generate('_edituserdetails', array(
                                                                                'id' => 3
                                                                               ))."'  
                                                                               method='post' name='dridetails' 
                                                                               novalidate>";  ?> 

    <div>
        <?php 
       // var_dump($form->vars['value']->getUsername());die();
        
        echo $view['form']->label($form['username']) ?>
        
        <?php foreach ($form['username']->vars['errors'] as $error): ?>
            <?php echo $error->getMessage();?>
        <?php endforeach; ?>
        
        <?php //echo "<input type='text' name='username' value='".$form->vars['value']->getUsername()."'>"; ?>
          <?php echo $view['form']->widget($form['username']) ?>
    </div>

   <div>
        <?php echo $view['form']->label($form['email']) ?>
        
        <?php foreach ($form['email']->vars['errors'] as $error): ?>
            <?php echo $error->getMessage();?>
        <?php endforeach; ?>
        
        <?php echo $view['form']->widget($form['email']) ?>
    </div>


    <div>
        <?php echo $view['form']->label($form['givenname']) ?>
        
        <?php foreach ($form['givenname']->vars['errors'] as $error): ?>
            <?php echo $error->getMessage();?>
        <?php endforeach; ?>
        
        <?php echo $view['form']->widget($form['givenname']) ?>
    </div>

    <div>
        <?php echo $view['form']->label($form['surname']) ?>
        <?php foreach ($form['surname']->vars['errors'] as $error): ?>
            <?php echo $error->getMessage();?>
        <?php endforeach; ?>
        <?php echo $view['form']->widget($form['surname']) ?>
    </div>
    
     <div>
        <?php echo $view['form']->widget($form['gender']) ?>
    </div>
          
    <div>
        <?php 
        if(1){
           echo $view['form']->widget($form['users']); 
        }
         ?>
    </div>      
    <div>
        <?php echo $view['form']->widget($form['save']) ?>
    </div>

<?php //echo $view['form']->end($form) ?>
    <?php echo "</form>"; ?>  
                  
                  
      </div><!-- /.box-body -->
                
                
                <div class="box-footer clearfix">
               
                  <a href="<?php 
               
                    echo $view['router']->generate('_adminlistusers');
                  ?>" 
                     class="btn btn-sm btn-info btn-flat pull-left"
                  >
                      Return
                  </a>
                </div><!-- /.box-footer -->
       </div><!-- /.box -->
    </div>          
 
<?php $view['slots']->stop() ?>


