
<?php if ($pageCount > 1): ?>
    <ul class="pagination">

    <?php if(isset($previous)): ?>
        <li>
            <a href="<?php echo $view['router']->generate($route, array(
                                                                         'page' =>$previous
                                                                       )) ?>
                    ">&laquo;&nbsp;Previous</a>
        </li>
    <?php else: ?>
        <li class="disabled">
            <span>&laquo;&nbsp;Previous</span>
        </li>
    <?php endif;?>

    <?php if ($startPage > 1): ?>
        <li>
            <a href="<?php echo $view['router']->generate($route, array(
                                                                         'page' =>1
                                                                       )) ?>"
            >1</a>
        </li>
        <?php if ($startPage == 3): ?>
            <li>
                <a href="<?php echo $view['router']->generate($route, array(
                                                                         'page' =>2
                                                             )) ?>"
                >2</a>
            </li>
        <?php elseif ($startPage != 2): ?>
        <li class="disabled">
            <span>&hellip;</span>
        </li>
          <?php endif; ?>
    <?php endif; ?>

    <?php foreach($pagesInRange as $page): ?>
        <?php if ($page != $current): ?>
            <li>
                <a href="<?php echo $view['router']->generate($route, array(
                                                                         'page' =>$page
                                                                       )) ?>"><?php echo $page;?></a>
            </li>
        <?php else: ?>
            <li class="active">
                <span><?php echo $page;?></span>
            </li>
        <?php endif; ?>

    <?php endforeach; ?>

    <?php if ($pageCount > $endPage): ?>
       <?php if ($pageCount > ($endPage + 1)): ?>
            <?php if ($pageCount > ($endPage + 2)): ?>
                <li class="disabled">
                    <span>&hellip;</span>
                </li>
            <?php else: ?>
                <li>
                    <a href="<?php echo $view['router']->generate($route, array(
                                                                         'page' =>$pageCount-1
                                                                       )) ?>"
                    ><?php echo $pageCount-1;?></a>
                </li>
            <?php endif; ?>
        <?php endif; ?>
        <li>
            <a href="<?php echo $view['router']->generate($route, array(
                                                                         'page' =>$pageCount
                                                                       )) ?>"
            ><?php echo $pageCount;?></a>
        </li>
    <?php endif; ?>

    <?php if (isset($next)): ?>
        <li>
            <a href="<?php echo $view['router']->generate($route, array(
                                                                         'page' =>$next
                                                                       )) ?>
               "
            >Next &nbsp;&raquo;</a>
        </li>
    <?php else:?>
        <li class="disabled">
            <span>Next &nbsp;&raquo;</span>
        </li>
    <?php endif; ?>
    </ul>
<?php endif; ?>
