<?php $view->extend('DRIBundle::base.html.php') ?>

<?php $view['slots']->set('title', 'DRI Users') ?>

<?php  $view['slots']->start('drilist') ?>
   <div class="count">
    <?php //echo $users->getTotalItemCount(); ?>
</div>             
                        
    <div class="col-md-12">           
            <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">DRI Users</h3>

                </div><!-- /.box-header -->
                <div class="box-body" style="min-height:250px;">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Username</th>
                          <th>Email</th>
                          <th>Status</th>                      
                        </tr>
                      </thead>
                                          
                      <tbody>
                       <?php foreach ($users as $user): ?>
                            <tr>
                             <td><a href="<?php echo $view['router']->generate('_dridetails', array(
                                                                               'id' => $user->getId()
                                                                              )) ?>"
                                 >  
                                    <?php echo  $user->getGivenname()." ".$user->getSurname();?>
                                 </a>
                             </td>
                       
                             <td><?php echo  $user->getUsername(); ?></td>
                             <td><?php echo  $user->getEmail(); ?></td>
                             <td><span class="label label-success">ok</span></td>                        
                           </tr>
                        <?php endforeach; ?>
                         <div class="navigation">
                            <?php echo $view['knp_pagination']->render($users); ?>
                         </div>     
                      </tbody>
                    </table>
                  </div><!-- /.table-responsive -->
                </div><!-- /.box-body -->

       </div><!-- /.box -->
    </div>      

                                <?php   
                                // var_dump($materials);

//                                 if($material['status_id'] == 1){
//                                     $label = "label-primary";
//                                 }
//                                 elseif($material['status_id'] == 2){
//                                     $label = "label-danger";
//                                 }
//                                 elseif($material['status_id'] == 3){
//                                     $label = "label-success";
//                                 }
//                                 elseif($material['status_id'] == 4){
//                                     $label = "label-warning";
//                                 }
                                ?> 
 
<?php $view['slots']->stop() ?>



