
<?php $view->extend('DRIBundle::base.html.php') ?>

<?php $view['slots']->set('title', 'Users and Printers') ?>

<?php  $view['slots']->start('listusersprinters') ?>
           

    <div class="col-md-12">           
            <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Printers</h3>

                </div><!-- /.box-header -->
                <div class="box-body" style="min-height:420px;">
                    
               <?php echo $view['form']->start($form); ?>
                    
                  <select name='users' class="chosen-select">
                       <option value=""> -- Select user -- </option>
                    <?php foreach ($users as $user): ?>
                   
                       <option value="<?php echo  $user->getId();?>"><?php echo  $user->getGivenname()." ".$user->getSurname();?></option>
                 
                    <?php endforeach; ?>
                   </select>
                                    
                    <input type='submit' class="btn btn-sm btn-default btn-flat" name='dropdownvalues' value='View'
                                         style="margin-left: 15px;width:100px;">
                    
                <?php echo $view['form']->end($form) ?>    
                    
                
                    
                <?php if(count($printers)): ?>   
                    
                    <?php echo  "<form action='".$view['router']->generate('_savedetailsprinters', array())."'  method='post' name='details'>";?>
                      <div style="margin:35px 0px 20px 10px;">  Restitution Date: <input type="text" id="datepicker" name="restitutiondate" value=""></div>
                     
                      <div style="margin:20px 0px 20px 10px;">     
                        Forward to : 
                        <select name='fwdemail' class="chosen-select-emails">
                             <option value=""> -- Select email -- </option>
                              <?php foreach ($users as $user): ?>

                                 <option value="<?php echo  $user->getEmailCanonical();?>"><?php echo  $user->getEmailCanonical();?></option>

                              <?php endforeach; ?>
                         </select> 
                      </div>
                      
                  <div class="table-responsive" >
                    <table class="table no-margin" >
                      <thead>
                        <tr>
                          <th>Nom User</th>                    
                          <th>Manufacturer</th>
                          <th>Printer Model</th>
                          <th>Numserie</th>
                          <th>Code Barre</th>
                          <th>Comment<th>                         
                        </tr>
                      </thead>                                         
                      <tbody>
                          
              
                       <?php foreach ($printers as $material): ?>
                          
                            <tr>                     
                             <td><?php echo  $material['NOM_USER']." ".$material['PRENOM_USER']; ?></td>
                             <td><?php echo  $material['manufacturer']; ?></td>
                             <td><?php echo  $material['printermodel']; ?></td> 
                             <td><?php echo  $material['NUMSERIE']; ?></td>
                             <td><?php echo  $material['CODE_BARRE']; ?></td>
                             <td><?php echo  $material['comment']; ?></td>


                                                                                  
                            <input type="hidden" name="nom[]" value="<?php echo  $material['NOM_USER'];?>">
                            <input type="hidden" name="prenom[]" value="<?php echo  $material['PRENOM_USER'];?>">
                            <input type="hidden" name="numserie[]" value="<?php echo  $material['NUMSERIE'];?>">
                            <input type="hidden" name="codebarre[]" value="<?php echo  $material['CODE_BARRE'];?>">
                            <input type="hidden" name="uid[]" value="<?php echo  $material['user_id'];?>">
                            <input type="hidden" name="usernameglpi[]" value="<?php echo  $material['username'];?>">
                             
                          <?php if(isset($driuser) && count($driuser)): ?>  
                            <input type="hidden" name="userid[]" value="<?php echo  $driuser[0]->getId();?>">  
                            <input type="hidden" name="username[]" value="<?php echo  $driuser[0]->getUsername();?>">
                          <?php endif;?>
                            
                            <input type="hidden" name="manufacturer[]" value="<?php echo  $material['manufacturer'];?>">
                            <input type="hidden" name="printermodel[]" value="<?php echo  $material['printermodel'];?>">
                            <input type="hidden" name="comment[]" value="<?php echo  $material['comment'];?>">
                           
                             <td><span class="label <?php// echo $label;?>">
                                  <?php //echo  $material['status_label']; ?></span>
                             </td>
                             <td><?php //echo  $material['date_restitution']; ?></td>  
                           </tr>
                           
                         <?php endforeach; ?>
                           
                                                                 
<!--                          <tr> 
                              <td style="border-top: 0px;">
                                  Restitution Date: <input type="text" id="datepicker" name="restitutiondate" value="">
                                  
                              </td>  
                          </tr>-->
                         <tr>
                           <td style="border-top: 0px;">
                            
                                <input type='submit' class="btn btn-success btn-flat" 
                                       
                                       style="width:100px; 
                                              display: block; 
                                              margin: 10px auto;"
                                              
                                name='details' 
                                value='Save'>
                            
                           </td>
                          </tr>
                                                                   

                       <?php echo "</form>";?>
                      </tbody>
                    </table>
                  </div><!-- /.table-responsive -->
                  
                  <?php //elseif(isset($materials) && count($materials) == 0):?>
                     <?php //echo "No results";?>
                  <?php endif;?>
                </div><!-- /.box-body -->

       </div><!-- /.box -->
    </div>          
 
<?php $view['slots']->stop() ?>

