
<?php $view->extend('DRIBundle::base.html.php') ?>

<?php $view['slots']->set('title', 'Details about user') ?>

<?php  $view['slots']->start('dridetails') ?>
                
                        
    <div class="col-md-12">           
            <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">DRI Details</h3>

                </div><!-- /.box-header -->
                
      <div class="box-body" style="min-height:250px;">

                                                                       
                <?php if(count($materials)): ?>    
                    
                  <div class="table-responsive" >
                    <table class="table no-margin" >
                      <thead>
                        <tr>
                          <th>id</th>  
                          <th>NOM USER</th>                  
                          <th>manufacturer</th>
                          <th>computermodel</th>
                          <th>monitormodel</th>
                          <th>peripheralmodel</th>
                          <th>printermodel</th>
                          <th>phonemodel</th>
                          <th>designation</th>
                          <th>codebarre</th>
                          <th>date_restitution</th>

                          <th>option</th>
                          <th>save</th>
                        </tr>
                      </thead>                                          
                      <tbody>
                       <?php foreach ($materials->getMateriels() as $hardware): ?>
                                                 
                       <?php 
//echo "<form action='".$view['router']->generate('_savedetails', array("id"=> $hardware['NUMSERIE']))."'  method='post' name='details'>"; 
                       ?>
                            <tr>   
                             <td><?php echo  $hardware->getId(); ?></td> 
                             <td><?php echo  $hardware->getUser()->getDisplayname(); ?></td>
                             <td><?php echo  $hardware->getManufacturer(); ?></td>
                             <td><?php echo  $hardware->getComputermodel(); ?></td>       
                             <td><?php echo  $hardware->getMonitormodel(); ?></td>
                             <td><?php echo  $hardware->getPeripheralmodel(); ?></td>
                             <td><?php echo  $hardware->getPrintermodel(); ?></td>
                             <td><?php echo  $hardware->getPhonemodel(); ?></td> 
                             <td><?php echo  $hardware->getDesignation(); ?></td>
                             <td><?php echo  $hardware->getCodebarre(); ?></td>
                             <td><?php echo  $hardware->getDateRestitution()->format('Y-m-d'); ?></td>

<!--                             <td><?php //echo  $hardware['CODE_BARRE']; ?></td>-->
                             
                      <?php //echo $view['form']->start($form); ?>
                             
                        <?php echo "<form action='".$view['router']->generate('_modifydridetails')."'  method='post' name='dridetails'>";  ?>    
                            <input type="hidden" name="nom" value="<?php //echo  $hardware['NOM_USER'];?>">
                            <input type="hidden" name="prenom" value="<?php //echo  $hardware['PRENOM_USER'];?>">
                            <input type="hidden" name="numserie" value="<?php //echo  $hardware['NUMSERIE'];?>">
                            <input type="hidden" name="uid" value="<?php echo  $hardware->getUser()->getId();?>">
                            <input type="hidden" name="id" value="<?php echo  $hardware->getId();?>">
                            <td>
                                
                                <select name='statuses' class="chosen-select">                                   
                                    <option value="1"  >nouveau</option>
                                     <option value="2" <?php if($hardware->getStatus()->getId() == 2) echo "selected";?>>litige</option>
                                     <option value="3" <?php if($hardware->getStatus()->getId() == 3) echo "selected";?>>en attente</option>
                                     <option value="4" <?php if($hardware->getStatus()->getId() == 4) echo "selected";?>>termine</option> 
                                 </select>
   
                            </td>
                            <td>
                        <?php
//                        echo $view['form']->widget($form['submitul'], array(
//                                         'attr' => array('class'=>"btn btn-sm btn-default btn-flat pull-right")
//                                          )); 
                                ?>
                               <input type='submit' class="btn btn-sm btn-default btn-flat pull-right" name='updatedetails' value='Save'> 
                            </td>
                            <td>

                               <input type='submit' class="btn btn-sm btn-default btn-flat pull-right" name='deletedetails' value='Delete'> 
                            </td>
                                 <?php //echo $view['form']->end($form) ?>                                                                      
                           </tr>
                         
                           <?php echo "</form>"; ?>  
                        <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div><!-- /.table-responsive -->
                  <?php //elseif(isset($hardwares) && count($hardwares) == 0):?>
                     <?php //echo "No results";?>
                  <?php endif;?>
                </div><!-- /.box-body -->
                
                
                <div class="box-footer clearfix">
               
                  <a href="<?php 
               
                    echo $view['router']->generate('_drilist');
                  ?>" 
                     class="btn btn-sm btn-info btn-flat pull-left"
                  >
                      Return
                  </a>
                </div><!-- /.box-footer -->
       </div><!-- /.box -->
    </div>          
 
<?php $view['slots']->stop() ?>

