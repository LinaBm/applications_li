
<?php $view->extend('DRIBundle::base.html.php') ?>

<?php $view['slots']->set('title', 'Users and materials') ?>

<?php  $view['slots']->start('listusersmaterials') ?>

    <div class="col-md-12 ">           
            <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Matériels</h3>

                </div><!-- /.box-header -->
                <div class="box-body" style="min-height:500px;">
                 
               <?php echo $view['form']->start($form); ?>
                  <select name='users' class="chosen-select">
                       <option value=""> -- Sélectionner un utilisateur -- </option>
                    <?php foreach ($users as $user): ?>
                   
                       <option value="<?php echo  $user->getId();?>"><?php echo  $user->getGivenname()." ".$user->getSurname();?></option>
                 
                    <?php endforeach; ?>
                   </select>
                                    
                    <input type='submit' class="btn btn-sm btn-default btn-flat" name='dropdownvalues' value='Voir'
                                         style="margin-left: 15px;width:100px;">
                <?php echo $view['form']->end($form) ?>    
        

                <?php if(count($materials)): ?>    
                    
                 <?php echo  "<form action='".$view['router']->generate('_savedetailsmaterials', array())."'  
                                    method='post' 
                                    name='details'
                                    id='detailsform'>";?>                    
                    
                <div style="margin:35px 0px 20px 10px;">                       
                    <input type="checkbox" id="chkfairedemande" name="chkfairedemande" value='cfd'/>
                     &nbsp; : &nbsp; Faire suivre la demande a l'utilisateur
                </div> 
                
                <div style="margin:25px 0px 20px 10px;">                       
                    <input type="checkbox" id="chkautreadresse" name="chkautreadresse" value='caa'/>
                     &nbsp; : &nbsp; Faire suivre la demande a une autre adresse que la votre <br/>                   
                </div> 
                 

                <div style="margin:20px 0px 30px 10px;" id="chkautreadressetext"> 
                    Precisez la nouvelle adresse &nbsp; : &nbsp; 
                    <input type="text" id="chkautreadresseinnertext" name="chkautreadressetext" value="">
                    
                    <span style="margin:20px 0px 30px 10px;color:red;" class="errormsgchktext">                     
                     Adresse email invalide!
                    </span>
                    
                </div>
                    
                <div style="margin:35px 0px 20px 10px;">                       
                    Date de restitution &nbsp; : &nbsp; <input type="text" id="datepicker" name="restitutiondate" value="">
                </div>
                    
                <div style="margin:20px 0px 20px 10px;">     
                  <span>Transférer les futurs mails vers  &nbsp; : &nbsp;</span>
                  <span data-toggle="tooltip" 
                        data-placement="right"  
                        title="Permet de renseigner la boite mail vers laquelle seront transférer les mails de l’utilisateur."               
                        id="fwdemail"
                        class="glyphicon glyphicon-question-sign "
                        aria-hidden="true"
                        style="color:#3C8DBC"
                  ></span>
                  <select name='fwdemail' class="chosen-select-emails">
                       <option value=""> -- Sélectionner un utilisateur -- </option>
                        <?php foreach ($users as $user): ?>

                           <option value="<?php echo  $user->getEmailCanonical();?>">
                               <?php echo  $user->getGivenname()." ".$user->getSurname();?>
                           </option>

                        <?php endforeach; ?>
                   </select> 
                </div>
                    
                <div class="table-responsive col-md-12" >
                    <table class="table no-margin display" id="ml">
                      <thead>
                        <tr>
                          <th>Nom de l'utilisateur</th>                    
                          <th>Fabriquant</th>
                          <th>Modèle</th>
                          <th>Num. série</th>
                          <th>Code barre</th>
                          <th>Designation</th>
                          <th>Statut</th>
                        </tr>
                      </thead>
                                          
                      <tbody>
                                       
                       <?php foreach ($materials as $material): ?>
                          
                            <tr>                     
                             <td><?php echo  utf8_encode($material['NOM_USER'])." ".utf8_encode($material['PRENOM_USER']); ?></td>
                             <td><?php echo  utf8_encode($material['manufacturer']); ?></td>
                             <td><?php echo  utf8_encode($material['model']); ?></td> 
                             <td><?php echo  utf8_encode($material['NUMSERIE']); ?></td>
                             <td><?php echo  utf8_encode($material['CODE_BARRE']); ?></td>
                             <td><?php echo  utf8_encode($material['comment']); ?> </td>
                             <td>
                               <?php 
                                 if($material['statut'] == "En fonction"){
                                     
                                    echo   "<span class='label-success'>";   
                                    echo utf8_encode($material['statut']);
                                    echo  "</span>";

                                 }else                                
                                   echo  utf8_encode($material['statut']); 
                                 ?>
                             </td>
                             
                            <input type="hidden" name="nom[]" value="<?php echo  utf8_encode($material['NOM_USER']);?>">
                            <input type="hidden" name="prenom[]" value="<?php echo  utf8_encode($material['PRENOM_USER']);?>">
                            <input type="hidden" name="numserie[]" value="<?php echo  utf8_encode($material['NUMSERIE']);?>">
                            <input type="hidden" name="codebarre[]" value="<?php echo  utf8_encode($material['CODE_BARRE']);?>">
                            <input type="hidden" name="uid[]" value="<?php echo  utf8_encode($material['user_id']);?>">
                            <input type="hidden" name="usernameglpi[]" value="<?php echo  utf8_encode($material['username']);?>">
                            <input type="hidden" name="statut[]" value="<?php echo  utf8_encode($material['statut']);?>">
                            
                          <?php if(isset($driuser) && count($driuser)): ?>  
                            <input type="hidden" name="userid[]" value="<?php echo  $driuser[0]->getId();?>">  
                            <input type="hidden" name="username[]" value="<?php echo  $driuser[0]->getUsername();?>">
                             <input type="hidden" name="useremail" value="<?php echo  $driuser[0]->getEmail();?>">
                          <?php endif;?>
                            
                            <input type="hidden" name="manufacturer[]" value="<?php echo  utf8_encode($material['manufacturer']);?>">
                            <input type="hidden" name="model[]" value="<?php echo  utf8_encode($material['model']);?>">
                            <input type="hidden" name="designation[]" value="<?php echo  utf8_encode($material['comment']);?>">

                           </tr>
                     
                         <?php endforeach; ?>
                      </tbody>
                    </table>
                  
                  </div><!-- /.table-responsive -->
    
                <div class="table-responsive col-md-12" >        
             
                     <input type='submit' class="btn btn-success btn-flat" 

                            style="width:100px; 
                                   display: block; 
                                   margin: 10px auto;"
                                   
                            id='details' 
                            name='details' 
                            value='Envoyer'>
               
                </div>
                <?php echo "</form>";?>
                  
                  <?php elseif(isset($materials) && count($materials) == 0):?>
                  
                     <?php //echo "No results";?>
                  
                  <?php endif;?>
                </div><!-- /.box-body -->

       </div><!-- /.box -->
    </div>          

<?php $view['slots']->stop() ?>

