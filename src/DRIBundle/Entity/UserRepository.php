<?php

namespace DRIBundle\Entity;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{

    public function findUsersAndSociete($username, $conn){
              $statement = $conn->executeQuery(
                    "select `u`.`id` AS `user_id`,
                            `u`.`name` AS `username`,
                            `u`.`realname` AS `NOM_USER`,
                            `u`.`firstname` AS `PRENOM_USER`,  
                            `uc`.`name` AS `societe` 
                    from                          
                            `glpi_users` `u`     
                    left join 
                            `glpi_usercategories` `uc` on `u`.`usercategories_id` = `uc`.`id` 
                     
                   where  `u`.`name` = ?",
              
                        array($username)); 
            
               return $statement->fetchAll();
    }
    
    
    public function findUsersOnSociete($societe, $conn){
              $statement = $conn->executeQuery(
                    "select 
                            `u`.`name` AS `username`,
                            `u`.`realname` AS `NOM_USER`,
                            `u`.`firstname` AS `PRENOM_USER`,  
                            `uc`.`name` AS `societe` 
                    from                          
                            `glpi_users` `u`     
                    left join 
                            `glpi_usercategories` `uc` on `u`.`usercategories_id` = `uc`.`id` 
                     
                   where  `uc`.`name` = ?",
              
                        array($societe)); 
            
               return $statement->fetchAll();
    }
    
    public function findSocietes($conn){
            
              $statement = $conn->executeQuery(
                    "select `name` 
                     from                          
                           `glpi_usercategories` ",
              
                        array()); 
            
               return $statement->fetchAll();
               
    }
    
    public function findUsersAndMaterials($username, $conn){
           
      $statement = $conn->executeQuery(
                    "select `u`.`id` AS `user_id`,
                            `u`.`name` AS `username`,
                            `u`.`realname` AS `NOM_USER`,
                            `u`.`firstname` AS `PRENOM_USER`,      
                            `f`.`name` manufacturer, 
                            `cm`.`name` computermodel,
                            `mm`.`name` monitormodel,
                            `pem`.`name` peripheralmodel,
                            `prm`.`name` printermodel,
                            `phm`.`name` phonemodel,  
                            `ph`.`name` phonename,
                            `c`.`comment` AS `DESIGNATION`,
                            `c`.`serial` AS `NUMSERIE`,
                            `c`.`name` AS `CODE_BARRE`,
                            `s`.`name` AS `STATUT`,
                            `c`.`is_deleted` AS `is_deleted`,
                            `uc`.`name` AS `societe` 
                    from                          
                        `glpi_users` `u` 
      
                        left join `glpi_computers` `c` on `c`.`users_id` = `u`.`id` 
                        
                        left join `glpi_computermodels` `cm` on `cm`.`id` = `c`.`computermodels_id` 
                        left join `glpi_manufacturers` `f` on `f`.`id` = `c`.`manufacturers_id`

                        left join `glpi_states` `s` on `s`.`id` = `c`.`states_id` 
                        left join `glpi_usercategories` `uc` on `u`.`usercategories_id` = `uc`.`id` 

                        left join `glpi_monitors` `m` on `u`.`id` = `m`.`users_id`
                        left join `glpi_monitormodels` `mm` on `mm`.`id` = `m`.`monitormodels_id`

                        left join `glpi_peripherals` `pe` on `u`.`id` = `pe`.`users_id` 
                        left join `glpi_peripheralmodels` `pem` on `pem`.`id` = `pe`.`peripheralmodels_id`

                        left join `glpi_printers` `pr` on `pr`.`users_id` = `u`.`id`
                        left join `glpi_printermodels` `prm` on `prm`.`id` = `pr`.`printermodels_id` 

                        left join `glpi_phones` `ph` on `ph`.`users_id` = `u`.`id` 
                        
                        left join `glpi_phonemodels` `phm` on `phm`.`id` = `ph`.`phonemodels_id`
                        
                         where (`c`.`locations_id` > 0 or 
                                `m`.`locations_id` > 0 or 
                                `pe`.`locations_id` > 0 or 
                                `pr`.`locations_id` > 0 or 
                                `ph`.`locations_id` > 0) 
                        and `u`.`name` = ?",
              
                        array($username)); 
            
               return $statement->fetchAll();
    }
    //`cm`.`name` AS `computermodel`
    //
    //   `b`.`name`  AS `societyname`
    //    left join `glpi_infocoms` `i` on `i`.`items_id` = `c`.`id`
    //    left join `glpi_budgets` `b` on `b`.`id` = `i`.`budgets_id`
    public function findComputers($username, $conn){
              $statement = $conn->executeQuery(
                    "select `u`.`id` AS `user_id`,
                            `u`.`name` AS `username`,
                            `u`.`realname` AS `NOM_USER`,
                            `u`.`firstname` AS `PRENOM_USER`,      
                            `f`.`name` manufacturer, 
             
                            `c`.`comment` AS `comment`,
                            `c`.`serial` AS `NUMSERIE`,
                            `c`.`name` AS `CODE_BARRE`,
                            `c`.`is_deleted` AS `is_deleted`,
                            `cm`.`name` AS `model`,
                            `s`.`name` AS `statut`
                                                                             
                    from                          
                        `glpi_users` `u` 
      
                        left join `glpi_computers` `c` on `c`.`users_id` = `u`.`id` 
                        
                        left join `glpi_states` `s` on `c`.`states_id` = `s`.`id`
                        
                        left join `glpi_computermodels` `cm` on `cm`.`id` = `c`.`computermodels_id` 
                        
                        left join `glpi_manufacturers` `f` on `f`.`id` = `c`.`manufacturers_id`
                                               
                                          
                    where `c`.`locations_id` > 0 
                                
                    and `u`.`name` = ?",
              
                    array($username)); 
            
               return $statement->fetchAll();
    }
    //`mm`.`name` AS `monitormodel`
    public function findMonitors($username, $conn){
        $statement = $conn->executeQuery(
                    "select `u`.`id` AS `user_id`,
                            `u`.`name` AS `username`,
                            `u`.`realname` AS `NOM_USER`,
                            `u`.`firstname` AS `PRENOM_USER`,    
                            
                            `f`.`name` manufacturer, 
             
                            `m`.`comment` AS `comment`,
                            `m`.`serial` AS `NUMSERIE`,
                            `m`.`name` AS `CODE_BARRE`,
                            `m`.`is_deleted` AS `is_deleted`,
                            `mm`.`name` AS `model`,
                            `s`.`name` AS `statut`
                    from
                    
                       `glpi_users` `u` 
      
                        left join `glpi_monitors` `m` on `u`.`id` = `m`.`users_id`
                        
                        left join `glpi_states` `s` on `m`.`states_id` = `s`.`id`
                        
                        left join `glpi_monitormodels` `mm` on `mm`.`id` = `m`.`monitormodels_id`
                        left join `glpi_manufacturers` `f` on `f`.`id` = `m`.`manufacturers_id`

                                         
                    where `m`.`locations_id` > 0
                                
                    and `u`.`name` = ?",
              
                    array($username)); 
            
        return $statement->fetchAll();
    }
    
    //`phm`.`name` AS `phonemodel`
     public function findPhones($username, $conn){
        $statement = $conn->executeQuery(
                    "select `u`.`id` AS `user_id`,
                            `u`.`name` AS `username`,
                            `u`.`realname` AS `NOM_USER`,
                            `u`.`firstname` AS `PRENOM_USER`,    
                            
                            `f`.`name` manufacturer, 
             
                            `ph`.`comment` AS `comment`,
                            `ph`.`serial` AS `NUMSERIE`,
                            `ph`.`name` AS `CODE_BARRE`,
                            `ph`.`is_deleted` AS `is_deleted`,
                            `phm`.`name` AS `model`,
                            `s`.`name` AS `statut`
                            
                    from
                    
                       `glpi_users` `u` 
                      
                        left join `glpi_phones` `ph` on `ph`.`users_id` = `u`.`id` 
                        
                        left join `glpi_states` `s` on `ph`.`states_id` = `s`.`id`
                        
                        left join `glpi_phonemodels` `phm` on `phm`.`id` = `ph`.`phonemodels_id`
                        
                        left join `glpi_manufacturers` `f` on `f`.`id` = `ph`.`manufacturers_id`
                                                                
                    where `ph`.`locations_id` > 0
                                
                    and `u`.`name` = ?",
              
                    array($username)); 
            
        return $statement->fetchAll();
    }
    
    //`pm`.`name` AS `printermodel`
     public function findPrinters($username, $conn){
        $statement = $conn->executeQuery(
                    "select `u`.`id` AS `user_id`,
                            `u`.`name` AS `username`,
                            `u`.`realname` AS `NOM_USER`,
                            `u`.`firstname` AS `PRENOM_USER`,    
                            
                            `f`.`name` manufacturer, 
             
                            `p`.`comment` AS `comment`,
                            `p`.`serial` AS `NUMSERIE`,
                            `p`.`name` AS `CODE_BARRE`,
                            `p`.`is_deleted` AS `is_deleted`,
                            `pm`.`name` AS `model`,
                            `s`.`name` AS `statut`
                    from
                    
                       `glpi_users` `u` 
                      
                        left join `glpi_printers` `p` on `p`.`users_id` = `u`.`id` 
                        
                        left join `glpi_states` `s` on `p`.`states_id` = `s`.`id`

                        left join `glpi_printermodels` `pm` on `pm`.`id` = `p`.`printermodels_id`
                        
                        left join `glpi_manufacturers` `f` on `f`.`id` = `p`.`manufacturers_id`
                                                                 
                    where `p`.`locations_id` > 0
                                
                    and `u`.`name` = ?",
              
                    array($username)); 
            
        return $statement->fetchAll();
    }
    
    //`pm`.`name`  AS `peripheralmodel`
    public function findPeripherals($username, $conn){
        $statement = $conn->executeQuery(
                    "select `u`.`id` AS `user_id`,
                            `u`.`name` AS `username`,
                            `u`.`realname` AS `NOM_USER`,
                            `u`.`firstname` AS `PRENOM_USER`,    
                            
                            `f`.`name` manufacturer, 
             
                            `p`.`comment` AS `comment`,
                            `p`.`serial` AS `NUMSERIE`,
                            `p`.`name` AS `CODE_BARRE`,
                            `p`.`is_deleted` AS `is_deleted`,
                            `pm`.`name`  AS `model`,
                            `s`.`name` AS `statut`
                    from
                    
                       `glpi_users` `u` 
                      
                        left join `glpi_peripherals` `p` on `p`.`users_id` = `u`.`id` 
                        
                        left join `glpi_states` `s` on `p`.`states_id` = `s`.`id`
                        
                        left join `glpi_peripheralmodels` `pm` on `pm`.`id` = `p`.`peripheralmodels_id`
                        
                        left join `glpi_manufacturers` `f` on `f`.`id` = `p`.`manufacturers_id`

                        
                    where `p`.`locations_id` > 0
                                
                    and `u`.`name` = ?",
              
                    array($username)); 
            
        return $statement->fetchAll();
    }
    
}

