<?php

namespace DRIBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;

/**
 * Materials
 *
 * @ORM\Table(name="dri_materials")
 * @ORM\Entity
 */
class Materials
{
    
    
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('comment', new NotBlank());

    }
    
     /**
     * @var string
     *
     * @ORM\Column(name="evaluateur", type="string", length=255)
     */
    private $evaluateur;
    
    /**
     * Set evaluateur
     *
     * @param string $evaluateur
     *
     * @return Materials
     */
    public function setEvaluateur($evaluateur)
    {
        $this->evaluateur= $evaluateur;
    
        return $this;
    }
    
    
    /**
     * Get evaluateur
     *
     * @return string
     */
    public function getEvaluateur()
    {
        return $this->evaluateur;
    }
    
    
 
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="evaluateur_id", type="integer")
     */
    private $evaluateurId;
    
    
     /**
     * Set evaluateurId
     *
     * @param integer $evaluateurId
     *
     * @return Materials
     */
    public function setEvaluateurId($evaluateurId)
    {
        $this->evaluateurId = $evaluateurId;
    
        return $this;
    }

    /**
     * Get evaluateurId
     *
     * @return integer
     */
    public function getEvaluateurId()
    {
        return $this->evaluateurId;
    }
   
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="materials")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
    
    
    /**
     * @ORM\ManyToOne(targetEntity="Status", inversedBy="materials")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id")
     */
    protected $status;
    
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="numserie", type="string", length=255)
     */
    private $numserie;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id_glpi", type="integer")
     */
    private $userIdGlpi;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="usernameglpi", type="string", length=255)
     */
    private $usernameglpi;

    /**
     * @var string
     *
     * @ORM\Column(name="manufacturer", type="string", length=255)
     */
    private $manufacturer;

    /**
     * @var string
     *
     * @ORM\Column(name="model", type="string", length=255)
     */
    private $model;

    /**
     * @var string
     *
     * @ORM\Column(name="codebarre", type="string", length=255)
     */
    private $codebarre;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=255)
     */
    private $comment;
    
     /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255)
     */
    private $designation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="restitutiondate", type="date")
     */
    private $restitutiondate;

    /**
     * @var string
     *
     * @ORM\Column(name="forwardemail", type="string", length=255)
     */
    private $forwardemail;
    
    
     /**
     * @var string
     *
     * @ORM\Column(name="societe", type="string", length=255)
     */
    private $societe;
    
    /**
     * @var string
     *
     * @ORM\Column(name="materialstatus", type="string", length=255)
     */
    private $materialstatus;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Materials
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Materials
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    
        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set numserie
     *
     * @param string $numserie
     *
     * @return Materials
     */
    public function setNumserie($numserie)
    {
        $this->numserie = $numserie;
    
        return $this;
    }

    /**
     * Get numserie
     *
     * @return string
     */
    public function getNumserie()
    {
        return $this->numserie;
    }

    /**
     * Set userIdGlpi
     *
     * @param integer $userIdGlpi
     *
     * @return Materials
     */
    public function setUserIdGlpi($userIdGlpi)
    {
        $this->userIdGlpi = $userIdGlpi;
    
        return $this;
    }

    /**
     * Get userIdGlpi
     *
     * @return integer
     */
    public function getUserIdGlpi()
    {
        return $this->userIdGlpi;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return Materials
     */
    public function setUsername($username)
    {
        $this->username = $username;
    
        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set usernameglpi
     *
     * @param string $usernameglpi
     *
     * @return Materials
     */
    public function setUsernameglpi($usernameglpi)
    {
        $this->usernameglpi = $usernameglpi;
    
        return $this;
    }

    /**
     * Get usernameglpi
     *
     * @return string
     */
    public function getUsernameglpi()
    {
        return $this->usernameglpi;
    }

    /**
     * Set manufacturer
     *
     * @param string $manufacturer
     *
     * @return Materials
     */
    public function setManufacturer($manufacturer)
    {
        $this->manufacturer = $manufacturer;
    
        return $this;
    }

    /**
     * Get manufacturer
     *
     * @return string
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * Set model
     *
     * @param string $model
     *
     * @return Materials
     */
    public function setModel($model)
    {
        $this->model = $model;
    
        return $this;
    }

    /**
     * Get model
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set codebarre
     *
     * @param string $codebarre
     *
     * @return Materials
     */
    public function setCodebarre($codebarre)
    {
        $this->codebarre = $codebarre;
    
        return $this;
    }

    /**
     * Get codebarre
     *
     * @return string
     */
    public function getCodebarre()
    {
        return $this->codebarre;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Materials
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set restitutiondate
     *
     * @param \DateTime $restitutiondate
     *
     * @return Materials
     */
    public function setRestitutiondate($restitutiondate)
    {
        $this->restitutiondate = $restitutiondate;
    
        return $this;
    }

    /**
     * Get restitutiondate
     *
     * @return \DateTime
     */
    public function getRestitutiondate()
    {
        return $this->restitutiondate;
    }

    /**
     * Set forwardemail
     *
     * @param string $forwardemail
     *
     * @return Materials
     */
    public function setForwardemail($forwardemail)
    {
        $this->forwardemail = $forwardemail;
    
        return $this;
    }

    /**
     * Get forwardemail
     *
     * @return string
     */
    public function getForwardemail()
    {
        return $this->forwardemail;
    }

    /**
     * Set user
     *
     * @param \DRIBundle\Entity\User $user
     *
     * @return Materials
     */
    public function setUser(\DRIBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \DRIBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set status
     *
     * @param \DRIBundle\Entity\Status $status
     *
     * @return Materials
     */
    public function setStatus(\DRIBundle\Entity\Status $status = null)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return \DRIBundle\Entity\Status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set designation
     *
     * @param string $designation
     *
     * @return Materials
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;
    
        return $this;
    }

    /**
     * Get designation
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * Set societe
     *
     * @param string $societe
     *
     * @return Materials
     */
    public function setSociete($societe)
    {
        $this->societe = $societe;
    
        return $this;
    }

    /**
     * Get societe
     *
     * @return string
     */
    public function getSociete()
    {
        return $this->societe;
    }

    /**
     * Set materialstatus
     *
     * @param string $materialstatus
     *
     * @return Materials
     */
    public function setMaterialstatus($materialstatus)
    {
        $this->materialstatus = $materialstatus;
    
        return $this;
    }

    /**
     * Get materialstatus
     *
     * @return string
     */
    public function getMaterialstatus()
    {
        return $this->materialstatus;
    }
}
