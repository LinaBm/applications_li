<?php

namespace DRIBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Phones
 *
 * @ORM\Table(name="dri_phones")
 * @ORM\Entity
 */
class Phones
{
     /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="phones")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
    
    
    /**
     * @ORM\ManyToOne(targetEntity="Status", inversedBy="phones")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id")
     */
    protected $status;
    
    
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="numserie", type="string", length=255)
     */
    private $numserie;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id_glpi", type="integer")
     */
    private $userIdGlpi;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="usernameglpi", type="string", length=255)
     */
    private $usernameglpi;

    /**
     * @var string
     *
     * @ORM\Column(name="manufacturer", type="string", length=255)
     */
    private $manufacturer;

    /**
     * @var string
     *
     * @ORM\Column(name="phonemodel", type="string", length=255)
     */
    private $phonemodel;

    /**
     * @var string
     *
     * @ORM\Column(name="codebarre", type="string", length=255)
     */
    private $codebarre;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=255)
     */
    private $comment;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="restitutiondate", type="date")
     */
    private $restitutiondate;

    /**
     * @var string
     *
     * @ORM\Column(name="forwardemail", type="string", length=255)
     */
    private $forwardemail;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Phones
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Phones
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    
        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set numserie
     *
     * @param string $numserie
     *
     * @return Phones
     */
    public function setNumserie($numserie)
    {
        $this->numserie = $numserie;
    
        return $this;
    }

    /**
     * Get numserie
     *
     * @return string
     */
    public function getNumserie()
    {
        return $this->numserie;
    }

    /**
     * Set userIdGlpi
     *
     * @param integer $userIdGlpi
     *
     * @return Phones
     */
    public function setUserIdGlpi($userIdGlpi)
    {
        $this->userIdGlpi = $userIdGlpi;
    
        return $this;
    }

    /**
     * Get userIdGlpi
     *
     * @return integer
     */
    public function getUserIdGlpi()
    {
        return $this->userIdGlpi;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return Phones
     */
    public function setUsername($username)
    {
        $this->username = $username;
    
        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set usernameglpi
     *
     * @param string $usernameglpi
     *
     * @return Phones
     */
    public function setUsernameglpi($usernameglpi)
    {
        $this->usernameglpi = $usernameglpi;
    
        return $this;
    }

    /**
     * Get usernameglpi
     *
     * @return string
     */
    public function getUsernameglpi()
    {
        return $this->usernameglpi;
    }

    /**
     * Set manufacturer
     *
     * @param string $manufacturer
     *
     * @return Phones
     */
    public function setManufacturer($manufacturer)
    {
        $this->manufacturer = $manufacturer;
    
        return $this;
    }

    /**
     * Get manufacturer
     *
     * @return string
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * Set phonemodel
     *
     * @param string $phonemodel
     *
     * @return Phones
     */
    public function setPhonemodel($phonemodel)
    {
        $this->phonemodel = $phonemodel;
    
        return $this;
    }

    /**
     * Get phonemodel
     *
     * @return string
     */
    public function getPhonemodel()
    {
        return $this->phonemodel;
    }

    /**
     * Set codebarre
     *
     * @param string $codebarre
     *
     * @return Phones
     */
    public function setCodebarre($codebarre)
    {
        $this->codebarre = $codebarre;
    
        return $this;
    }

    /**
     * Get codebarre
     *
     * @return string
     */
    public function getCodebarre()
    {
        return $this->codebarre;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Phones
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set restitutiondate
     *
     * @param \DateTime $restitutiondate
     *
     * @return Phones
     */
    public function setRestitutiondate($restitutiondate)
    {
        $this->restitutiondate = $restitutiondate;
    
        return $this;
    }

    /**
     * Get restitutiondate
     *
     * @return \DateTime
     */
    public function getRestitutiondate()
    {
        return $this->restitutiondate;
    }

    /**
     * Set forwardemail
     *
     * @param string $forwardemail
     *
     * @return Phones
     */
    public function setForwardemail($forwardemail)
    {
        $this->forwardemail = $forwardemail;
    
        return $this;
    }

    /**
     * Get forwardemail
     *
     * @return string
     */
    public function getForwardemail()
    {
        return $this->forwardemail;
    }

    /**
     * Set user
     *
     * @param \DRIBundle\Entity\User $user
     *
     * @return Phones
     */
    public function setUser(\DRIBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \DRIBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set status
     *
     * @param \DRIBundle\Entity\Status $status
     *
     * @return Phones
     */
    public function setStatus(\DRIBundle\Entity\Status $status = null)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return \DRIBundle\Entity\Status
     */
    public function getStatus()
    {
        return $this->status;
    }
}
