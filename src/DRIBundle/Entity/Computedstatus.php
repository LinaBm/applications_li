<?php

namespace DRIBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Computedstatus
 *
 * @ORM\Table(name="dri_computedstatus")
 * @ORM\Entity
 */
class Computedstatus
{
    
    
    /**
     * @ORM\OneToOne(targetEntity="User", inversedBy="computedstatus")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
    
    
    
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $userId;
    
    

    /**
     * @var integer
     *
     * @ORM\Column(name="computedstatus", type="integer")
     */
    private $computedstatus;
    
    


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Computedstatus
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    
        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set computedstatus
     *
     * @param integer $computedstatus
     *
     * @return Computedstatus
     */
    public function setComputedstatus($computedstatus)
    {
        $this->computedstatus = $computedstatus;
    
        return $this;
    }

    /**
     * Get computedstatus
     *
     * @return integer
     */
    public function getComputedstatus()
    {
        return $this->computedstatus;
    }

    /**
     * Set user
     *
     * @param \DRIBundle\Entity\User $user
     *
     * @return Computedstatus
     */
    public function setUser(\DRIBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \DRIBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
