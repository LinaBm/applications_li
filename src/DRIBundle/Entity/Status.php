<?php

namespace DRIBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Status
 *
 * @ORM\Table(name="dri_status")
 * @ORM\Entity
 */
class Status
{
    
        /**
        * @ORM\OneToMany(targetEntity="Materials", mappedBy="status")
        */
        protected $materials;
       
       
    
        /**
        * @ORM\OneToMany(targetEntity="Materiel", mappedBy="status")
        */
       protected $materiels;
       
       
        /**
        * @ORM\OneToMany(targetEntity="Computers", mappedBy="status")
        */
       protected $computers;
       
        /**
        * @ORM\OneToMany(targetEntity="Phones", mappedBy="status")
        */
       protected $phones;
       
        /**
        * @ORM\OneToMany(targetEntity="Peripherals", mappedBy="status")
        */
       protected $peripherals;
       
       /**
        * @ORM\OneToMany(targetEntity="Printers", mappedBy="status")
        */
       protected $printers;
       
        /**
        * @ORM\OneToMany(targetEntity="Monitors", mappedBy="status")
        */
       protected $monitors;

       public function __construct()
       {   
           $this->materials = new ArrayCollection();
           
           $this->computers = new ArrayCollection();
           $this->phones = new ArrayCollection();
           $this->peripherals = new ArrayCollection();
           $this->printers = new ArrayCollection();
           $this->monitors = new ArrayCollection();
           
       }
    
    
       
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="status_label", type="string", length=255)
     */
    private $statusLabel;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set statusLabel
     *
     * @param string $statusLabel
     *
     * @return Status
     */
    public function setStatusLabel($statusLabel)
    {
        $this->statusLabel = $statusLabel;
    
        return $this;
    }

    /**
     * Get statusLabel
     *
     * @return string
     */
    public function getStatusLabel()
    {
        return $this->statusLabel;
    }

    /**
     * Add materiel
     *
     * @param \DRIBundle\Entity\Materiel $materiel
     *
     * @return Status
     */
    public function addMateriel(\DRIBundle\Entity\Materiel $materiel)
    {
        $this->materiels[] = $materiel;
    
        return $this;
    }

    /**
     * Remove materiel
     *
     * @param \DRIBundle\Entity\Materiel $materiel
     */
    public function removeMateriel(\DRIBundle\Entity\Materiel $materiel)
    {
        $this->materiels->removeElement($materiel);
    }

    /**
     * Get materiels
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMateriels()
    {
        return $this->materiels;
    }

    /**
     * Add computer
     *
     * @param \DRIBundle\Entity\Computers $computer
     *
     * @return Status
     */
    public function addComputer(\DRIBundle\Entity\Computers $computer)
    {
        $this->computers[] = $computer;
    
        return $this;
    }

    /**
     * Remove computer
     *
     * @param \DRIBundle\Entity\Computers $computer
     */
    public function removeComputer(\DRIBundle\Entity\Computers $computer)
    {
        $this->computers->removeElement($computer);
    }

    /**
     * Get computers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComputers()
    {
        return $this->computers;
    }

    /**
     * Add phone
     *
     * @param \DRIBundle\Entity\Phones $phone
     *
     * @return Status
     */
    public function addPhone(\DRIBundle\Entity\Phones $phone)
    {
        $this->phones[] = $phone;
    
        return $this;
    }

    /**
     * Remove phone
     *
     * @param \DRIBundle\Entity\Phones $phone
     */
    public function removePhone(\DRIBundle\Entity\Phones $phone)
    {
        $this->phones->removeElement($phone);
    }

    /**
     * Get phones
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhones()
    {
        return $this->phones;
    }

    /**
     * Add peripheral
     *
     * @param \DRIBundle\Entity\Peripherals $peripheral
     *
     * @return Status
     */
    public function addPeripheral(\DRIBundle\Entity\Peripherals $peripheral)
    {
        $this->peripherals[] = $peripheral;
    
        return $this;
    }

    /**
     * Remove peripheral
     *
     * @param \DRIBundle\Entity\Peripherals $peripheral
     */
    public function removePeripheral(\DRIBundle\Entity\Peripherals $peripheral)
    {
        $this->peripherals->removeElement($peripheral);
    }

    /**
     * Get peripherals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPeripherals()
    {
        return $this->peripherals;
    }

    /**
     * Add printer
     *
     * @param \DRIBundle\Entity\Printers $printer
     *
     * @return Status
     */
    public function addPrinter(\DRIBundle\Entity\Printers $printer)
    {
        $this->printers[] = $printer;
    
        return $this;
    }

    /**
     * Remove printer
     *
     * @param \DRIBundle\Entity\Printers $printer
     */
    public function removePrinter(\DRIBundle\Entity\Printers $printer)
    {
        $this->printers->removeElement($printer);
    }

    /**
     * Get printers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrinters()
    {
        return $this->printers;
    }

    /**
     * Add monitor
     *
     * @param \DRIBundle\Entity\Monitors $monitor
     *
     * @return Status
     */
    public function addMonitor(\DRIBundle\Entity\Monitors $monitor)
    {
        $this->monitors[] = $monitor;
    
        return $this;
    }

    /**
     * Remove monitor
     *
     * @param \DRIBundle\Entity\Monitors $monitor
     */
    public function removeMonitor(\DRIBundle\Entity\Monitors $monitor)
    {
        $this->monitors->removeElement($monitor);
    }

    /**
     * Get monitors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMonitors()
    {
        return $this->monitors;
    }

    /**
     * Add material
     *
     * @param \DRIBundle\Entity\Materiel $material
     *
     * @return Status
     */
    public function addMaterial(\DRIBundle\Entity\Materiel $material)
    {
        $this->materials[] = $material;
    
        return $this;
    }

    /**
     * Remove material
     *
     * @param \DRIBundle\Entity\Materiel $material
     */
    public function removeMaterial(\DRIBundle\Entity\Materiel $material)
    {
        $this->materials->removeElement($material);
    }

    /**
     * Get materials
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMaterials()
    {
        return $this->materials;
    }
}
