<?php 

// src/AppBundle/EventListener/ExceptionListener.php
namespace Indicateurs\DAMBundle\EventListener;

use Symfony\Component\Templating\EngineInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class ExceptionListener
{
	
	protected $templating;
	
	public function __construct(EngineInterface $templating)
	{
		$this->templating = $templating;
	}
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
    	$request = $event->getRequest();
    	// Matched route
    	$route  = $request->attributes->get('_route');
    	if(strpos($route, 'affichage_tv')===0){
    		$response = new Response();
    		$response->setContent($this->templating->renderResponse('IndicateursDAMBundle:TvDam:error.html.twig',array(
    			'route'=>$route
    		)));
    		// Send the modified response object to the event
    		$event->setResponse($response);    		
    	}

    }
}