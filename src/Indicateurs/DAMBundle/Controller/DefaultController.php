<?php

namespace Indicateurs\DAMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('IndicateursDAMBundle:Default:index.html.twig', array('name' => $name));
    }
}
