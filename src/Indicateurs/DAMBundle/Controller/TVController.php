<?php

namespace Indicateurs\DAMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ob\HighchartsBundle\Highcharts\Highchart;


class TVController extends Controller
{
	public $data= array();
	
	public function getDataDAM(){
		$page = "";
		$url =  $this->get('kernel')->getRootDir()."/../web/data_dam/";
		$fp = fopen($url."alpha_var.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$objDAM=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
		$this->data['objectif_DAM']=$objDAM;
		$realisation=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$realisation = (Double)$realisation;
		$neg=stripos($realisation, "-");
		if ($neg!=false)
		{
			$arr2 = explode("-",$realisation);
			$realisation=$arr2[0]*-1;
		}
		$this->data['realisation_DAM']=intval($realisation);
		$this->data['objectif_dam']=intval($objDAM-$realisation);
	}

	public function getDataEquipes(){
		$url =  $this->get('kernel')->getRootDir()."/../web/data_dam/";
		$page="";
		$fp = fopen($url."TERUK.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$this->data['objectif_UK']=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
		$this->data['objectif_terrain']=$this->data['objectif_UK'];
		
		$uk =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($uk, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$uk);
			$uk=$arr2[0]*-1;
		}
		$this->data['realisation_UK']=$uk;
		$this->data['objectif_terrain']=$this->data['objectif_UK'];
		$this->data['realisation_terrain']=$uk;
		/*************** RECUPERATION DES DOSSIERS DE L'EQUIPE UK ***************/
		
		$page = "";
		$page = file($url."acheteurs_TERUK.txt");
		$total_dossier_deposes=0;
		$total_objectif_dossier=0;
		//pour toutes lignes du tableaux
		foreach ($page as $line_num => $line) {
			// création d'un tableau
			$arr1 = explode(":",$line);
			if (count($arr1) == 5) {
				$total_objectif_dossier= $total_objectif_dossier +  (int)ltrim($arr1[3]);
				$total_dossier_deposes= $total_dossier_deposes+ (int)substr(ltrim($arr1[4]),0,stripos(ltrim($arr1[4]), " "));
			} //fin if
		}//fin foreach
		$this->data['dossier_UK']=$total_dossier_deposes;
		$this->data['obj_dossier_UK']=$total_objectif_dossier;
		
		
		/*************** RECUPERATION DES DONNEES DE L'EQUIPE ALLEMAGNE ***************/
		$page="";
		$fp = fopen($url."TERALLEMAGNE.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$this->data['objectif_ALL']=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
		
		$allemagne =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($allemagne, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$allemagne);
			$allemagne=$arr2[0]*-1;
		}
		$this->data['realisation_ALL']=$allemagne;
		$this->data['objectif_terrain']=$this->data['objectif_terrain'] + $this->data['objectif_ALL'];
		$this->data['realisation_terrain']=$this->data['realisation_terrain']+$allemagne;
		/*************** RECUPERATION DES DOSSIERS DE L'EQUIPE ALLEMAGNE ***************/
		
		$page = "";
		$page = file($url."acheteurs_TERALLEMAGNE.txt");
		$total_dossier_deposes=0;
		$total_objectif_dossier=0;
		//pour toutes lignes du tableaux
		foreach ($page as $line_num => $line) {
			// création d'un tableau
			$arr1 = explode(":",$line);
			if (count($arr1) == 5) {
				$total_objectif_dossier= $total_objectif_dossier +  (int)ltrim($arr1[3]);
				$total_dossier_deposes= $total_dossier_deposes + (int)substr(ltrim($arr1[4]),0,stripos(ltrim($arr1[4]), " "));
			} //fin if
		}//fin foreach
		$this->data['dossier_ALL']=$total_dossier_deposes;
		$this->data['obj_dossier_ALL']=$total_objectif_dossier;
		
		
		
		/*****************************************************************************/
		
		/*************** RECUPERATION DES DONNEES DE L'EQUIPE ESPAGHNE ***************/
		
		$page="";
		$fp = fopen($url."TERESPAGNE.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$this->data['objectif_esp']=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
		$espagne =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($espagne, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$espagne);
			$espagne=$arr2[0]*-1;
		}
		$this->data['realisation_esp']=$espagne;
		$this->data['objectif_terrain']=$this->data['objectif_terrain'] + $this->data['objectif_esp'];
		$this->data['realisation_terrain']=$this->data['realisation_terrain']+$espagne;
		/*****************************************************************************/
		
		/*************** RECUPERATION DES DOSSIERS DE L'EQUIPE ALLEMAGNE ***************/
		
		$page = "";
		$page = file($url."acheteurs_TERESPAGNE.txt");
		$total_dossier_deposes=0;
		$total_objectif_dossier=0;
		//pour toutes lignes du tableaux
		foreach ($page as $line_num => $line) {
			// création d'un tableau
			$arr1 = explode(":",$line);
			if (count($arr1) == 5) {
				$total_objectif_dossier= $total_objectif_dossier +  (int)ltrim($arr1[3]);
				$total_dossier_deposes= $total_dossier_deposes+ (int)substr(ltrim($arr1[4]),0,stripos(ltrim($arr1[4]), " "));
			} //fin if
		}//fin foreach
		$this->data['dossier_ESP']=$total_dossier_deposes;
		$this->data['obj_dossier_ESP']=$total_objectif_dossier;
		
		/*************** RECUPERATION DES DONNEES DE L'EQUIPE ITALIE ***************/
		
		$page="";
		$fp = fopen($url."TERITALIE.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$objita=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
		
		$italie =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($italie, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$italie);
			$italie=$arr2[0]*-1;
		}
		$this->data['objectif_ita']=$objita;
		$this->data['realisation_ita']=$italie;
		$this->data['objectif_terrain']=$this->data['objectif_terrain'] + $objita;
		$this->data['realisation_terrain']=$this->data['realisation_terrain']+$italie;
		/*************** RECUPERATION DES DOSSIERS DE L'EQUIPE italie ***************/
		
		$page = "";
		$page = file($url."acheteurs_TERITALIE.txt");
		$total_dossier_deposes=0;
		$total_objectif_dossier=0;
		//pour toutes lignes du tableaux
		foreach ($page as $line_num => $line) {
			// création d'un tableau
			$arr1 = explode(":",$line);
			if (count($arr1) == 5) {
				$total_objectif_dossier= $total_objectif_dossier +  (int)ltrim($arr1[3]);
				$total_dossier_deposes= $total_dossier_deposes+ (int)substr(ltrim($arr1[4]),0,stripos(ltrim($arr1[4]), " "));
			} //fin if
		}//fin foreach
		$this->data['dossier_ITA']=$total_dossier_deposes;
		$this->data['obj_dossier_ITA']=$total_objectif_dossier;
		
		
		/*************** RECUPERATION DES DONNEES DE L'EQUIPE FRANCE OUEST ***************/
		$page="";
		$fp = fopen($url."FRANCEOUEST.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$objfro=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
		
		$france_ouest =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($france_ouest, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$france_ouest);
			$france_ouest=$arr2[0]*-1;
		}
		$this->data['objectif_fro']=$objfro;
		$this->data['realisation_fro']=$france_ouest;
		$this->data['objectif_terrain']=$this->data['objectif_terrain'] + $objfro;
		$this->data['realisation_terrain']=$this->data['realisation_terrain']+$france_ouest;
		/*****************************************************************************/
		
		/*************** RECUPERATION DES DOSSIERS DE L'EQUIPE FRANCE OUEST ***************/
		
		$page = "";
		$page = file($url."acheteurs_FRANCEOUEST.txt");
		$total_dossier_deposes=0;
		$total_objectif_dossier=0;
		//pour toutes lignes du tableaux
		foreach ($page as $line_num => $line) {
			// création d'un tableau
			$arr1 = explode(":",$line);
			if (count($arr1) == 5) {
				$total_objectif_dossier= $total_objectif_dossier +  (int)ltrim($arr1[3]);
				$total_dossier_deposes= $total_dossier_deposes+ (int)substr(ltrim($arr1[4]),0,stripos(ltrim($arr1[4]), " "));
			} //fin if
		}//fin foreach
		$this->data['dossier_FRAOUEST']=$total_dossier_deposes;
		$this->data['obj_dossier_FRAOUEST']=$total_objectif_dossier;
		
		/*************** RECUPERATION DES DONNEES DE L'EQUIPE FRANCE EST ***************/
		$page="";
		$fp = fopen($url."FRANCEEST.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$objfre=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
		
		$france_est =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($france_est, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$france_est);
			$france_est=$arr2[0]*-1;
		}
		$this->data['objectif_fre']=$objfre;
		$this->data['realisation_fre']=$france_est;
		$this->data['objectif_terrain']=$this->data['objectif_terrain'] + $objfre;
		$this->data['realisation_terrain']=$this->data['realisation_terrain']+$france_est;
		
		/*****************************************************************************/
		/*************** RECUPERATION DES DOSSIERS DE L'EQUIPE FRANCE EST ***************/
		
		$page = "";
		$page = file($url."acheteurs_FRANCEEST.txt");
		$total_dossier_deposes=0;
		$total_objectif_dossier=0;
		//pour toutes lignes du tableaux
		foreach ($page as $line_num => $line) {
			// création d'un tableau
			$arr1 = explode(":",$line);
			if (count($arr1) == 5) {
				$total_objectif_dossier= $total_objectif_dossier +  (int)ltrim($arr1[3]);
				$total_dossier_deposes= $total_dossier_deposes+ (int)substr(ltrim($arr1[4]),0,stripos(ltrim($arr1[4]), " "));
			} //fin if
		}//fin foreach
		$this->data['dossier_FRAEST']=$total_dossier_deposes;
		$this->data['obj_dossier_FRAEST']=$total_objectif_dossier;
		
		/*************** RECUPERATION DES DONNEES DE L'EQUIPE FRANCE SUD ***************/
		$page="";
		$fp = fopen($url."FRANCESUD.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$objfrs=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
		
		$france_sud =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($france_sud, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$france_sud);
			$france_sud=$arr2[0]*-1;
		}
		$this->data['objectif_frs']=$objfrs;
		$this->data['realisation_frs']=$france_sud;
		$this->data['objectif_terrain']=$this->data['objectif_terrain'] + $objfrs;
		$this->data['realisation_terrain']=$this->data['realisation_terrain']+$france_sud;
		/*****************************************************************************/
		
		/*************** RECUPERATION DES DOSSIERS DE L'EQUIPE FRANCE SUD ***************/
		
		$page = "";
		$page = file($url."acheteurs_FRANCESUD.txt");
		$total_dossier_deposes=0;
		$total_objectif_dossier=0;
		//pour toutes lignes du tableaux
		foreach ($page as $line_num => $line) {
			// création d'un tableau
			$arr1 = explode(":",$line);
			if (count($arr1) == 5) {
				$total_objectif_dossier= $total_objectif_dossier +  (int)ltrim($arr1[3]);
				$total_dossier_deposes= $total_dossier_deposes+ (int)substr(ltrim($arr1[4]),0,stripos(ltrim($arr1[4]), " "));
			} //fin if
		}//fin foreach
		$this->data['dossier_FRASUD']=$total_dossier_deposes;
		$this->data['obj_dossier_FRASUD']=$total_objectif_dossier;		
		
		/*************** RECUPERATION DES DONNEES SEDENTAIRE MARKETING ***************/
		$page="";
		$fp = fopen($url."/SEDENTAIRETEXTILE.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$objmark=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
	
		$marketing =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($marketing, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$marketing);
			$marketing=$arr2[0]*-1;
		}
		$this->data['objectif_textile']=$objmark;
		$this->data['realisation_textile']=$marketing;
		$this->data['objectif_sedentaire']=$objmark;
		$this->data['realisation_sedentaire']=$marketing;
	
		/*************** RECUPERATION DES DONNEES SEDENTAIRES GC ***************/
		$page="";
		$fp = fopen($url."/SEDENTAIREGC.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$objgc=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
	
		$grandcompte =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($grandcompte, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$grandcompte);
			$grandcompte=$arr2[0]*-1;
		}
		$this->data['objectif_gc']=$objgc;
		$this->data['realisation_gc']=$grandcompte;
		$this->data['objectif_sedentaire']=$this->data['objectif_sedentaire'] + $objgc;
		$this->data['realisation_sedentaire']=$this->data['realisation_sedentaire'] + $grandcompte;
	
		$page="";
		$fp = fopen($url."/SEDENTAIREFRANCEBELGIQUE.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$objbelg=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
	
		$belgique =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($belgique, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$belgique);
			$belgique=$arr2[0]*-1;
		}
		$this->data['objectif_bel']=$objbelg;
		$this->data['realisation_bel']=$belgique;
		$this->data['objectif_sedentaire']=$this->data['objectif_sedentaire'] + $objbelg;
		$this->data['realisation_sedentaire']=$this->data['realisation_sedentaire'] + $belgique;
	
		$page="";
		$fp = fopen($url."/SEDENTAIREMOGADOR1.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$objbelg=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
	
		$belgique =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($belgique, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$belgique);
			$belgique=$arr2[0]*-1;
		}
		$this->data['objectif_mog1']=$objbelg;
		$this->data['realisation_mog1']=$belgique;
		$this->data['objectif_sedentaire']=$this->data['objectif_sedentaire'] + $objbelg;
		$this->data['realisation_sedentaire']=$this->data['realisation_sedentaire'] + $belgique;
	
		$page="";
		$fp = fopen($url."/SEDENTAIREMOGADOR2.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$objbelg=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
	
		$belgique =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($belgique, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$belgique);
			$belgique=$arr2[0]*-1;
		}
		$this->data['objectif_mog2']=$objbelg;
		$this->data['realisation_mog2']=$belgique;
		$this->data['objectif_sedentaire']=$this->data['objectif_sedentaire'] + $objbelg;
		$this->data['realisation_sedentaire']=$this->data['realisation_sedentaire'] + $belgique;
	
		$page="";
		$fp = fopen($url."/SEDENTAIREEUROPE1.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$objeur1=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
	
		$eur1 =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($eur1, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$eur1);
			$eur1=$arr2[0]*-1;
		}
		$this->data['objectif_eur1']=$objeur1;
		$this->data['realisation_eur1']=$eur1;
		$this->data['objectif_sedentaire']=$this->data['objectif_sedentaire'] + $objeur1;
		$this->data['realisation_sedentaire']=$this->data['realisation_sedentaire'] + $eur1;
		/*************** RECUPERATION DES DONNEES SEDENTAIRES Europe2 ***************/
		$page="";
		$fp = fopen($url."/SEDENTAIREEUROPE2.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$objeur2=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
	
		$eur2 =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($eur2, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$eur2);
			$eur2=$arr2[0]*-1;
		}
		$this->data['objectif_eur2']=$objeur2;
		$this->data['realisation_eur2']=$eur2;
		$this->data['objectif_sedentaire']=$this->data['objectif_sedentaire'] + $objeur2;
		$this->data['realisation_sedentaire']=$this->data['realisation_sedentaire'] + $eur2;
		/*****************************************************************************/
		$page="";
		$fp = fopen($url."/SEDENTAIREEUROPE3.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$objeur3=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
	
		$eur3 =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($eur3, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$eur3);
			$eur3=$arr2[0]*-1;
		}
		$this->data['objectif_eur3']=$objeur3;
		$this->data['realisation_eur3']=$eur3;
		$this->data['objectif_sedentaire']=$this->data['objectif_sedentaire'] + $objeur3;
		$this->data['realisation_sedentaire']=$this->data['realisation_sedentaire'] + $eur3;
	
		/*************** RECUPERATION DES DONNEES SEDENTAIRES Europe4 ***************/
		$page="";
		$fp = fopen($url."/SEDENTAIREEUROPE4.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$objeur4=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
	
		$eur4 =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		//$europe2=ltrim(substr(ltrim($arr1[2]),stripos(ltrim($arr1[2]), " "),6));
		$neg=stripos($eur4, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$eur4);
			$eur4=$arr2[0]*-1;
		}
		$this->data['objectif_eur4']=$objeur4;
		$this->data['realisation_eur4']=$eur4;
		$this->data['objectif_sedentaire']=$this->data['objectif_sedentaire'] + $objeur4;
		$this->data['realisation_sedentaire']=$this->data['realisation_sedentaire'] + $eur4;
	
		/*************** RECUPERATION DES DONNEES SEDENTAIRES Europe5 ***************/
		$page="";
		$fp = fopen($url."/SEDENTAIREEUROPE5.txt","r"); //lecture du fichier
		while (!feof($fp)) { //on parcourt toutes les lignes
			$page .= fgets($fp, 4096)."<br>";  // lecture du contenu de la ligne
		}
		$arr1 = explode(":",$page); //premiere division de la ligne
		$objeur5=substr(ltrim($arr1[1]),0,stripos(ltrim($arr1[1]), " "));
	
		$eur5 =substr(ltrim($arr1[2]),0,stripos(ltrim($arr1[2]), " "));
		$neg=stripos($eur5, "-");
		//echo $neg;
		if ($neg!=false)
		{
			$arr2 = explode("-",$eur5);
			$eur5=$arr2[0]*-1;
		}
		$this->data['objectif_eur5']=$objeur5;
		$this->data['realisation_eur5']=$eur5;
		$this->data['objectif_sedentaire']=$this->data['objectif_sedentaire'] + $objeur5;
		$this->data['realisation_sedentaire']=$this->data['realisation_sedentaire'] + $eur5;
		/*****************************************************************************/
	
		/*****************************************************************************/
		$page = "";
		$page = file($url."acheteurs_SEDENTAIREEUROPE5.txt");
		$total_dossier_deposes=0;
		$total_objectif_dossier=0;
		//pour toutes lignes du tableaux
		foreach ($page as $line_num => $line) {
			// création d'un tableau
			$arr1 = explode(":",$line);
			if (count($arr1) == 5) {
				$total_objectif_dossier= $total_objectif_dossier +  (int)ltrim($arr1[3]);
				$total_dossier_deposes= $total_dossier_deposes+ (int)substr(ltrim($arr1[4]),0,stripos(ltrim($arr1[4]), " "));
			} //fin if
		}//fin foreach
		$this->data['dossier_EURO5']=$total_dossier_deposes;
		$this->data['obj_dossier_EURO5']=$total_objectif_dossier;
	}
	

	/// indicateurs/dam/TV/diapos/achat
	public function diapoAchatAction(){
		$this->getDataDAM();
				return $this->render('IndicateursDAMBundle:TvDam:achats.html.twig',array(
				'data'=>$this->data
		));		
	}
	
	/// indicateurs/dam/TV/diapos/challenge
	public function diapoChallengeAction(){
		$this->getDataDAM();
				return $this->render('IndicateursDAMBundle:TvDam:challenge.html.twig',array(
				'data'=>$this->data
		));
	}

	// indicateurs/dam/TV/diapos/pie
	public function diapoPieAction(){
		$this->getDataDAM();
		return $this->render('IndicateursDAMBundle:TvDam:pie.html.twig',array(
				'data'=>$this->data
		));		
	}
	
	// indicateurs/dam/TV/diapos/histogramme
	public function diapoHistogrammeAction(){
		$this->getDataDAM();
		$this->getDataEquipes();
		return $this->render('IndicateursDAMBundle:TvDam:histogramme.html.twig',array(
				'data'=>$this->data
		));
	}

	// indicateurs/dam/TV/diapos/chiffre_terrain	
	public function diapoChiffreTerrainAction(){
		$this->getDataDAM();
		$this->getDataEquipes();
		return $this->render('IndicateursDAMBundle:TvDam:chiffres_terrain.html.twig',array(
				'data'=>$this->data
		));		
	}
	

	// indicateurs/dam/TV/diapos/histogramme_terrain
	public function diapoHistogrammeTerrainAction(){
		$this->getDataDAM();
		$this->getDataEquipes();
		return $this->render('IndicateursDAMBundle:TvDam:histogramme_terrain.html.twig',array(
				'data'=>$this->data
		));
	}
	
	// indicateurs/dam/TV/diapos/chiffres_sedentaire
	public function diapoChiffreSedentaireAction(){
		$this->getDataDAM();
		$this->getDataEquipes();
		return $this->render('IndicateursDAMBundle:TvDam:chiffres_sedentaire.html.twig',array(
				'data'=>$this->data
		));
	}
	

	// indicateurs/dam/TV/diapos/histogramme_sedentaire
	public function diapoHistogrammeSedentaireAction(){
		$this->getDataDAM();
		$this->getDataEquipes();
		return $this->render('IndicateursDAMBundle:TvDam:histogramme_sedentaire.html.twig',array(
				'data'=>$this->data
		));
	}
	
	/// indicateurs/dam/TV/diapos/drh
	public function diapoDRHAction(){
		$this->getDataDAM();
		return $this->render('IndicateursDAMBundle:TvDam:drh.html.twig',array(
				'data'=>$this->data
		));
	}
	
	/// indicateurs/dam/TV/diapos/siege
	public function diapoSiegeAction(){
		$this->getDataDAM();
		return $this->render('IndicateursDAMBundle:TvDam:siege.html.twig',array(
				'data'=>$this->data
		));
	}
}