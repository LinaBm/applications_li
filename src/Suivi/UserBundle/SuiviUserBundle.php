<?php

namespace Suivi\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SuiviUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
