<?php

namespace Suivi\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use IMAG\LdapBundle\User\LdapUserInterface;

use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="Suivi\UserBundle\Entity\UserRepository")
 */
class User extends BaseUser implements LdapUserInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="dn", type="string", length=255)
     */
    protected $dn;
    /**
     * @ORM\Column(name="givenname", type="string", length=255)
     */
    protected $givenname;
    /**
     * @ORM\Column(name="surname", type="string", length=255)
     */
    protected $surname;
     /**
     * @ORM\Column(name="displayname", type="string", length=255)
     */
    protected $displayname;
    
     /**
     * @ORM\Column(name="attributes", type="string", length=255, nullable=true)
     */
    protected $attributes;

     /**
     * @ORM\Column(name="matricule", type="string", length=6, nullable=true)
     */
    protected $matricule;
    
    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    public function getId(){
    	return $this->id;
    }
    
    public function getDn()
    {
        return $this->dn;
    }

    public function setDn($dn)
    {
        $this->dn = $dn;

        return $this;
    }

    public function getCn()
    {
        return $this->username;
    }

    public function setCn($cn)
    {
        $this->username = $cn;

        return $this;
    }
    
    public function getDisplayname()
    {
        return $this->displayname;
    }

    public function setDisplayname($displayname)
    {
        $this->displayname = $displayname;
        return $this;
    }

    public function getGivenname()
    {
        return $this->givenname;
    }

    public function setGivenname($givenname)
    {
        $this->givenname = $givenname;
        return $this;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function setSurname($surname)
    {
        $this->surname = $surname;
        return $this;
    }
    
    public function getAttributes()
    {
        return $this->attributes;
    }

    public function setAttributes(array $attributes)
    {
        $this->attributes = $attributes;

        return $this;
    }

    public function getAttribute($name)
    {
        return isset($this->attributes[$name]) ? $this->attributes[$name] : null;
    }

    public function isEqualTo(UserInterface $user)
    {
        if (!$user instanceof LdapUserInterface
            || $user->getUsername() !== $this->username
            || $user->getEmail() !== $this->email
            || count(array_diff($user->getRoles(), $this->getRoles())) > 0
            || $user->getDn() !== $this->dn
        ) {
            return false;
        }

        return true;
    }

    public function serialize()
    {
        return serialize(array(
            $this->password,
            $this->salt,
            $this->usernameCanonical,
            $this->username,
            $this->emailCanonical,
            $this->email,
            $this->expired,
            $this->locked,
            $this->credentialsExpired,
            $this->enabled,
            $this->id,
            $this->roles,
            $this->dn,
        ));
    }

    public function unserialize($serialized)
    {
        list(
            $this->password,
            $this->salt,
            $this->usernameCanonical,
            $this->username,
            $this->emailCanonical,
            $this->email,
            $this->expired,
            $this->locked,
            $this->credentialsExpired,
            $this->enabled,
            $this->id,
            $this->roles,
            $this->dn,
        ) = unserialize($serialized);
    }
    
   
    

    /**
     * Set matricule
     *
     * @param string $matricule
     * @return User
     */
    public function setMatricule($matricule)
    {
        $this->matricule = $matricule;

        return $this;
    }

    /**
     * Get matricule
     *
     * @return string 
     */
    public function getMatricule()
    {
        return $this->matricule;
    }
    
    /**
     * isGranted
     *
     * @return boolean 
     */
    public function isGranted($role)
    {
        return in_array($role, $this->getRoles());
    }
    
    public function __toString()
    {
        return $this->displayname();
    }
}
