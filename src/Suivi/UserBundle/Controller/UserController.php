<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Suivi\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserController extends Controller
{
	public function ajaxsuiviAction(Request $request)
	{
		$listeUser = $this->getDoctrine() ->getManager() 
  	 		->getRepository('SuiviUserBundle:User') 
  	 		->findAll();
  	 		$data= array();
  	 		$i=0;
  	 		foreach ($listeUser as $user){
  	 			$data[$i]=array("user" =>$user ->getDisplayname());
  	 			$i++;
  	 		}
  	 		return new JsonResponse($data);
	}
	
	public function ajaxtechnicienAction(Request $request)
	{
		$listeUser = $this->getDoctrine() ->getManager()
			->getRepository('SuiviUserBundle:User')
			->findByRoles('ROLE_TECH');
			$data= array();
			$i=0;
			foreach ($listeUser as $user){
				$data[$i]=array("user" =>$user ->getDisplayname());
				$i++;
			}
			return new JsonResponse($data);		
	}
}
