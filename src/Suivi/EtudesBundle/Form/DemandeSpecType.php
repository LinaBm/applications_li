<?php

namespace Suivi\EtudesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DemandeSpecType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('choix', 'choice', array(
        			'choices' => array(
         				'upload' => 'Uploadez un fichier',
   					    'reseau' => 'Entrez le chemin du fichier'
    				),
				    'multiple' => false,
				    'expanded' => true,
            		'mapped' => false,
				))
            
            ->add('fichier', 'file', array(
            		'mapped' => false,
            		'required' => false,
            ))

            ->add('chemin', 'text', array(
            		'mapped' => false,
            		'required' => false,
            ))
            
            //->add('Modifier', 'submit')
        ;
    }

    public function getName()
    {
      return 'suivi_etudes_demande_spec';
    }
}
