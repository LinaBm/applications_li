<?php

namespace Suivi\EtudesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class AvancementType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('chargesCPRestante', 'text', array(
                'required' => false,
            ))
            ->add('chargesDevRestante', 'text', array(
                'required' => false,
            ))
            //->add('Modifier', 'submit')
        ;
    }

    public function getName()
    {
      return 'suivi_etudes_demande_avancement';
    }
}