<?php

namespace Suivi\EtudesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EstimationEtudesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('chargesCP', 'text', array(
                'required' => false,

            ))
            ->add('chargesDev', 'text', array(
                'required' => false,

            ))
    
            //->add('Modifier', 'submit')
        ;
    }

    public function getName()
    {
      return 'suivi_etudes_demande_estimation_etudes';
    }
}