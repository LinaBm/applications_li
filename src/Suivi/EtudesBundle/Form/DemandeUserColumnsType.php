<?php

namespace Suivi\EtudesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DemandeUserColumnsType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('title', 'checkbox', array(
                    'label'=> 'Titre',
                    'required' => false,
                ))
                ->add('date', 'checkbox', array(
                    'label'=> 'Date',
                    'required' => false,
                ))
                ->add('type', 'checkbox', array(
                    'label'=> 'Type',
                    'required' => false,
                ))
                ->add('status', 'checkbox', array(
                    'label'=> 'Statut',
                    'required' => false
                ))
                ->add('urgence', 'checkbox', array(
                    'label'=> 'Urgence',
                    'required' => false
                ))
                ->add('application', 'checkbox', array(
                    'label'=> 'Application',
                    'required' => false
                ))
                ->add('applicant', 'checkbox', array(
                    'label'=> 'Demandeur',
                    'required' => false
                ))
                ->add('departament', 'checkbox', array(
                    'label'=> 'Direction',
                    'required' => false
                ))
                ->add('service', 'checkbox', array(
                    'label'=> 'Service',
                    'required' => false
                ))
                ->add('observer', 'checkbox', array(
                    'label'=> 'Observateur',
                    'required' => false
                ))
                ->add('technician', 'checkbox', array(
                    'label'=> 'Technicien',
                    'required' => false
                ))
                ->add('projectManager', 'checkbox', array(
                    'label'=> 'Chef de projet',
                    'required' => false
                ))
                ->add('testDate', 'checkbox', array(
                    'label'=> 'Date recette',
                    'required' => false
                ))
                ->add('prodDate', 'checkbox', array(
                    'label'=> 'Date Production',
                    'required' => false
                ))
                ->add('projectManagerHours', 'checkbox', array(
                    'label'=> 'Charge CP',
                    'required' => false
                ))
                ->add('technicianHours', 'checkbox', array(
                    'label'=> 'Charge Dev',
                    'required' => false
                ))
                ->add('frequency', 'checkbox', array(
                    'label'=> 'Fréquence',
                    'required' => false
                ))
                ->add('origin', 'checkbox', array(
                    'label'=> 'Origine',
                    'required' => false
                ))
                ->add('population', 'checkbox', array(
                    'label'=> 'Population',
                    'required' => false
                ))
                ->add('commentEstimation', 'checkbox', array(
                    'label'=> 'Commentaire. refus estimations',
                    'required' => false
                ))
                ->add('commentTest', 'checkbox', array(
                    'label'=> 'Commentaire recette',
                    'required' => false
                ))
                ->add('commentStandBy', 'checkbox', array(
                    'label'=> 'Commentaire stand-by',
                    'required' => false
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Suivi\EtudesBundle\Entity\DemandeUserColumns',
            'csrf_protection'   => false,
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return null;
    }

}
