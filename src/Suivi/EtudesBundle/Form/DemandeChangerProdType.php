<?php
//  Mikael LOLIVE <mlolive@noz.fr> 						
//  11-12-14 												
//  formulaire de modification de la date de livraison d'une demande 	
namespace Suivi\EtudesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DemandeChangerProdType extends AbstractType
{
    /**
     * Mikael LOLIVE <mlolive@noz.fr>
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	
    	$builder
	    	->add('date_production', 'date', array(
	    			'widget' => 'single_text',
	    			'format' => 'dd/MM/yyyy',))
	    			
	    	->add('description', 'textarea', array(
        			'mapped'=>false,
	    			))
    	;
    }

    public function getName()
    {
      return 'suivi_etudes_modification_date_prod';
    }
}