<?php

namespace Suivi\EtudesBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\Common\Persistence\Doctrine\Common\Persistence\ObjectManager;

class DemandeEditType extends AbstractType
{
	private $demande;
	private  $listeObservateurs;
	private  $listeChefDeProjets;
	private  $listeTechs;
	private $em;
		
	public function __construct($demande, $em){
		$this->demande = $demande;
		$this->em = $em;
		$this->listeObservateurs=$em->getRepository('SuiviEtudesBundle:DemandeUser')->getObservateursbyDemande($demande);
		$this->listeChefDeProjets=$em->getRepository('SuiviEtudesBundle:DemandeUser')->getChefDeProjetbyDemande($demande);
		$this->listeTechs=$em->getRepository('SuiviEtudesBundle:DemandeUser')->getTechniciensbyDemande($demande);
	}
	
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	//$demande = $this->demande;
        $builder        

            ->add('direction', 'entity', array(
                            'class'    => 'AdministrationDirectionBundle:Direction',
                            'property' => 'libelle',
                            'empty_value' => '',
                            'required' => false,
            ))

            ->add('service', 'entity', array(
                            'class'    => 'AdministrationDirectionBundle:Service',
                            'property' => 'libelle',
                            'empty_value' => '',
                            'required' => false,
            ))

            //Pour l'instant, le statut ne sera pas modifiable, c'est pourquoi le champ est désactivé.
            ->add('statut', 'entity', array(
             'class'    => 'SuiviEtudesBundle:Statut',
            		'property' => 'name',
            		'required'	=>	false,
            ))

		->add('suiviMail', 'entity', array(
				'class' => 'UserUserBundle:User',
				'property' => 'displayname',
				'required' => false,
				'multiple' => true,
				'expanded' => false,
				'mapped'=>false,
				'data' => $this->listeObservateurs,
		))
		
		->add('chefdeProjet', 'entity', array(
				'class' => 'UserUserBundle:User',				
				'property' => 'displayname',
				'query_builder'=> $this->em->getRepository('UserUserBundle:User')
					->getByRole('ROLE_TECH'),
				'required' => false,
				'multiple' => true,
				'expanded' => false,
				'mapped'=>false,
				'data' => $this->listeChefDeProjets,
		))

		->add('techniciens', 'entity', array(
				'class' => 'UserUserBundle:User',
				'property' => 'displayname',
				'query_builder'=> $this->em->getRepository('UserUserBundle:User')
					->getByRole('ROLE_TECH'),
				'required' => false,
				'multiple' => true,
				'expanded' => false,
				'mapped'=>false,
				'data' => $this->listeTechs,
		))
            
       //     ->add('titre', 'text')
    //        ->add('description', 'textarea')
            
    /*        ->add('urgence', 'entity', array(
                'class'    => 'SuiviEtudesBundle:Urgence',
                'property' => 'name',
            	'required'	=>	false,
            )) */
            
            -> add('piecejointes', 'collection', array(
            	'type' => new PiecejointeType(),
            	'allow_add' =>true,
            	'allow_delete' =>true,
            	'mapped'=>false,
            ))
            
            ->add('Modifier', 'submit', array(
            	'label'	=>	'Enregistrer les modifications',
            ))
        ;
    }

    public function getName()
    {
      return 'suivi_etudes_demande_edit';
    }
}
