<?php

namespace Suivi\EtudesBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Suivi\EtudesBundle\Entity\Type;

class LoadType extends AbstractFixture implements OrderedFixtureInterface
{

    /**
   * ORDRE DE PASSAGE DU FIXTURES:LOAD
   */
  public function getOrder()
  {
    return 3;
  }


  public function load(ObjectManager $manager)
  {
    // Liste des noms de compétences à ajouter
    $names = array('Evolution', 'Correction', 'Support', 'Demande de projet', 'Anomalie');

    foreach ($names as $name) {
      // On crée la compétence
      $type = new Type();
      $type->setName($name);

      // On la persiste
      $manager->persist($type);
    }

    // On déclenche l'enregistrement de toutes les catégories
    $manager->flush();
  }
}