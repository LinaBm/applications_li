<?php
namespace Suivi\EtudesBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Suivi\EtudesBundle\Entity\Demande;
use Suivi\EtudesBundle\Entity\Statut;
use Suivi\EtudesBundle\Entity\Type;
use Suivi\EtudesBundle\Entity\Urgence;
use Suivi\EtudesBundle\Entity\Application;
use Suivi\UserBundle\Entity\User;


class LoadDemande extends AbstractFixture implements OrderedFixtureInterface
{

  private $manager;


  /**
   * ORDRE DE PASSAGE DU FIXTURES:LOAD
   */
  public function getOrder()
  {
    return 7;
  }


  public function load(ObjectManager $manager)
  {

    $this->manager = $manager;
    //$this->generateEntities();
    //$manager->flush();
  }


  public function generateEntities()
  {
    $listeDemandes = array(
      array(
        'titre'       => 'Demande évolution Selecting People',
        'description' => 'Nous recherchons un développeur Symfony2 débutant sur Lyon. Blabla…',
        'type'        => $this->manager->getRepository('SuiviEtudesBundle:Type')->findOneByName('Evolution'),
        'statut'      => $this->manager->getRepository('SuiviEtudesBundle:Statut')->findOneByName('Nouveau'),
        'urgence'     => $this->manager->getRepository('SuiviEtudesBundle:Urgence')->findOneByName('Basse'),
        'application' => $this->manager->getRepository('SuiviEtudesBundle:Application')->findOneByName('GLPI'),
        'demandeur'	  => $this->manager->getRepository('UserUserBundle:User')->findOneBy(array('username' => 'Alexandre')),
      	'service'	  => 1,
      	'mailing'     => 1,
      ),
      array(
        'titre'       => 'Bug CRM',
        'description' => 'Nous recherchons un webmaster capable de maintenir notre site internet. Blabla…',
        'type'        => $this->manager->getRepository('SuiviEtudesBundle:Type')->findOneByName('Evolution'),
        'statut'      => $this->manager->getRepository('SuiviEtudesBundle:Statut')->findOneByName('Etudes'),
        'urgence'     => $this->manager->getRepository('SuiviEtudesBundle:Urgence')->findOneByName('Basse'),
        'application' => $this->manager->getRepository('SuiviEtudesBundle:Application')->findOneByName('Application WEB'),
        'demandeur'	  => $this->manager->getRepository('UserUserBundle:User')->findOneBy(array('username' => 'Marine')),
      	'service'	  => 2,
      	'mailing'     => 1,
    ),
      array(
        'titre'       => 'Référence Unique',
        'description' => 'Nous recherchons un webmaster capable de maintenir notre site internet. Blabla…',
        'type'        => $this->manager->getRepository('SuiviEtudesBundle:Type')->findOneByName('Support'),
        'statut'      => $this->manager->getRepository('SuiviEtudesBundle:Statut')->findOneByName('En cours - Développeur'),
        'urgence'     => $this->manager->getRepository('SuiviEtudesBundle:Urgence')->findOneByName('Basse'),
        'application' => $this->manager->getRepository('SuiviEtudesBundle:Application')->findOneByName('COGNOS'),
        'demandeur'	  => $this->manager->getRepository('UserUserBundle:User')->findOneBy(array('username' => 'Anna')),
      	'service'	  => 3,
      	'mailing'     => 1,
        ),
      array(
        'titre'       => 'Evolution SAP',
        'description' => 'Nous recherchons un webmaster capable de maintenir notre site internet. Blabla…',
        'type'        => $this->manager->getRepository('SuiviEtudesBundle:Type')->findOneByName('Evolution'),
        'statut'      => $this->manager->getRepository('SuiviEtudesBundle:Statut')->findOneByName('Clos'),
        'urgence'     => $this->manager->getRepository('SuiviEtudesBundle:Urgence')->findOneByName('Très Basse'),
        'application' => $this->manager->getRepository('SuiviEtudesBundle:Application')->findOneByName('GLPI'),
        'demandeur'	  => $this->manager->getRepository('UserUserBundle:User')->findOneBy(array('username' => 'Alexandre')),
      	'service'	  => 4,
      	'mailing'     => 1,
        ),
      array(
        'titre'       => 'Evolution Référence Unique',
        'description' => 'Nous recherchons un webmaster capable de maintenir notre site internet. Blabla…',
        'type'        => $this->manager->getRepository('SuiviEtudesBundle:Type')->findOneByName('Evolution'),
        'statut'      => $this->manager->getRepository('SuiviEtudesBundle:Statut')->findOneByName('Nouveau'),
        'urgence'     => $this->manager->getRepository('SuiviEtudesBundle:Urgence')->findOneByName('Moyenne'),
        'application' => $this->manager->getRepository('SuiviEtudesBundle:Application')->findOneByName('GLPI'),
        'demandeur'	  => $this->manager->getRepository('UserUserBundle:User')->findOneBy(array('username' => 'Marine')),
      	'service'	  => 5,
      	'mailing'     => 1,
       ),
      array(
        'titre'       => 'Correction COGNOS',
        'description' => 'Nous recherchons un webmaster capable de maintenir notre site internet. Blabla…',
        'type'        => $this->manager->getRepository('SuiviEtudesBundle:Type')->findOneByName('Correction'),
        'statut'      => $this->manager->getRepository('SuiviEtudesBundle:Statut')->findOneByName('Nouveau'),
        'urgence'     => $this->manager->getRepository('SuiviEtudesBundle:Urgence')->findOneByName('Haute'),
        'application' => $this->manager->getRepository('SuiviEtudesBundle:Application')->findOneByName('CRM - Selligent'),
        'demandeur'	  => $this->manager->getRepository('UserUserBundle:User')->findOneBy(array('username' => 'Anna')),
      	'service'	  => 6,
      	'mailing'     => 1,
         ),
      array(
        'titre'       => 'Evolution SAP',
        'description' => 'Nous recherchons un webmaster capable de maintenir notre site internet. Blabla…',
        'type'        => $this->manager->getRepository('SuiviEtudesBundle:Type')->findOneByName('Evolution'),
        'statut'      => $this->manager->getRepository('SuiviEtudesBundle:Statut')->findOneByName('Clos'),
        'urgence'     => $this->manager->getRepository('SuiviEtudesBundle:Urgence')->findOneByName('Très Basse'),
        'application' => $this->manager->getRepository('SuiviEtudesBundle:Application')->findOneByName('Application WEB'),
        'demandeur'	  => $this->manager->getRepository('UserUserBundle:User')->findOneBy(array('username' => 'Alexandre')),
      	'service'	  => 7,
      	'mailing'     => 1,
      		    ),
      array(
        'titre'       => 'Evolution Référence Unique',
        'description' => 'Nous recherchons un webmaster capable de maintenir notre site internet. Blabla…',
        'type'        => $this->manager->getRepository('SuiviEtudesBundle:Type')->findOneByName('Evolution'),
        'statut'      => $this->manager->getRepository('SuiviEtudesBundle:Statut')->findOneByName('Nouveau'),
        'urgence'     => $this->manager->getRepository('SuiviEtudesBundle:Urgence')->findOneByName('Moyenne'),
        'application' => $this->manager->getRepository('SuiviEtudesBundle:Application')->findOneByName('COGNOS'),
        'demandeur'	  => $this->manager->getRepository('UserUserBundle:User')->findOneBy(array('username' => 'Marine')),
      	'service'	  => 8,
      	'mailing'     => 1,
      		    ),
      array(
        'titre'       => 'Correction COGNOS',
        'description' => 'Nous recherchons un webmaster capable de maintenir notre site internet. Blabla…',
        'type'        => $this->manager->getRepository('SuiviEtudesBundle:Type')->findOneByName('Correction'),
        'statut'      => $this->manager->getRepository('SuiviEtudesBundle:Statut')->findOneByName('Nouveau'),
        'urgence'     => $this->manager->getRepository('SuiviEtudesBundle:Urgence')->findOneByName('Haute'),
        'application' => $this->manager->getRepository('SuiviEtudesBundle:Application')->findOneByName('Application WEB'),
        'demandeur'	  => $this->manager->getRepository('UserUserBundle:User')->findOneBy(array('username' => 'Anna')),
      	'service'	  => 9,
      	'mailing'     => 1,
      		    ),
      array(
        'titre'       => 'Evolution SAP',
        'description' => 'Nous recherchons un webmaster capable de maintenir notre site internet. Blabla…',
        'type'        => $this->manager->getRepository('SuiviEtudesBundle:Type')->findOneByName('Evolution'),
        'statut'      => $this->manager->getRepository('SuiviEtudesBundle:Statut')->findOneByName('Clos'),
        'urgence'     => $this->manager->getRepository('SuiviEtudesBundle:Urgence')->findOneByName('Très Basse'),
        'application' => $this->manager->getRepository('SuiviEtudesBundle:Application')->findOneByName('GLPI'),
        'demandeur'	  => $this->manager->getRepository('UserUserBundle:User')->findOneBy(array('username' => 'Alexandre')),
      	'service'	  => 10,
      	'mailing'     => 1,
      		    ),
      array(
        'titre'       => 'Evolution Référence Unique',
        'description' => 'Nous recherchons un webmaster capable de maintenir notre site internet. Blabla…',
        'type'        => $this->manager->getRepository('SuiviEtudesBundle:Type')->findOneByName('Evolution'),
        'statut'      => $this->manager->getRepository('SuiviEtudesBundle:Statut')->findOneByName('Nouveau'),
        'urgence'     => $this->manager->getRepository('SuiviEtudesBundle:Urgence')->findOneByName('Moyenne'),
        'application' => $this->manager->getRepository('SuiviEtudesBundle:Application')->findOneByName('CRM - Selligent'),
        'demandeur'	  => $this->manager->getRepository('UserUserBundle:User')->findOneBy(array('username' => 'Marine')),
      	'service'	  => 11,
      	'mailing'     => 1,
      		    ),
      array(
        'titre'       => 'Correction COGNOS',
        'description' => 'Nous recherchons un webmaster capable de maintenir notre site internet. Blabla…',
        'type'        => $this->manager->getRepository('SuiviEtudesBundle:Type')->findOneByName('Correction'),
        'statut'      => $this->manager->getRepository('SuiviEtudesBundle:Statut')->findOneByName('Nouveau'),
        'urgence'     => $this->manager->getRepository('SuiviEtudesBundle:Urgence')->findOneByName('Haute'),
        'application' => $this->manager->getRepository('SuiviEtudesBundle:Application')->findOneByName('COGNOS'),
        'demandeur'	  => $this->manager->getRepository('UserUserBundle:User')->findOneBy(array('username' => 'Anna')),
      	'service'	  => 12,
      	'mailing'     => 1,
	    ),
    );

    foreach($listeDemandes as $demande)
    {
      $this->newEntity($demande);
    }

  }

  public function newEntity($param)
  {

    $demande = new Demande();
    $demande->setTitre($param['titre']);
    $demande->setDescription($param['description']);
    $demande->setType($param['type']);
    $demande->setStatut($param['statut']);
    $demande->setUrgence($param['urgence']);
    $demande->setApplication($param['application']);
    $demande->setDemandeur($param['demandeur']);
    $demande->setService($param['service']);
    $demande->setMailing($param['mailing']);
    $this->manager->persist($demande);

  }


/*      $statut = new Statut();
      $statut->setName("Nouveau");
      $manager->persist($statut);
       $statut$manager->getRepository('SuiviEtudesBundle:Statut')->find(16)
      $urgence = new Urgence();
      $urgence->setName("Très très urgent");
      $manager->persist($urgence);

      $type = new Type();
      $type->setName("Gros bug");
      $manager->persist($type);*/

}