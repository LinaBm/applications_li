<?php

namespace Suivi\EtudesBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Suivi\EtudesBundle\Entity\Application;
use Suivi\EtudesBundle\Entity\Categorie;


class LoadApplication extends AbstractFixture implements OrderedFixtureInterface
{

  private $manager;
  /**
   * ORDRE DE PASSAGE DU FIXTURES:LOAD
   */
  public function getOrder()
  {
    return 5;
  }


  // Dans l'argument de la méthode load, l'objet $manager est l'EntityManager
  public function load(ObjectManager $manager)
  {
    $this->manager = $manager;
    
    $categorieSAP = $this->manager->getRepository('SuiviEtudesBundle:Categorie')->findOneByName('SAP');
    $categorieWEB = $this->manager->getRepository('SuiviEtudesBundle:Categorie')->findOneByName('WEB');
    $categorieDECISIONNEL = $this->manager->getRepository('SuiviEtudesBundle:Categorie')->findOneByName('DECISIONNEL');
    // Liste des noms de statut à ajouter
    $listeApplications = array(
        array(
          'name'      =>'SAP',
          'categorie' => $categorieSAP,
        ),
        array(
          'name'      =>'CRM - Selligent',
          'categorie' => $categorieDECISIONNEL,
        ),
        array(
          'name'      =>'COGNOS',
          'categorie' => $categorieDECISIONNEL,
        ),
        array(
          'name'      =>'GLPI',
          'categorie' => $categorieWEB,
        ),
        array(
          'name'      =>'Affichages des télés',
          'categorie' => $categorieWEB,
        ),
        array(
          'name'      =>'Application WEB',
          'categorie' => $categorieWEB,
        ),
    );

    foreach ($listeApplications as $appli) {
      // On crée le statut
      $application = new Application();
      $application->setName($appli['name']);
      $application->setCategorie($appli['categorie']);

      // On la persiste
      $manager->persist($application);
    }

    // On déclenche l'enregistrement de toutes les catégories
    $manager->flush();
  }
}