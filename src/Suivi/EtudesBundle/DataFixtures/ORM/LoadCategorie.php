<?php

namespace Suivi\EtudesBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Suivi\EtudesBundle\Entity\Categorie;

class LoadCategorie extends AbstractFixture implements OrderedFixtureInterface
{

  /**
   * ORDRE DE PASSAGE DU FIXTURES:LOAD
   */
  public function getOrder()
  {
    return 4;
  }


  // Dans l'argument de la méthode load, l'objet $manager est l'EntityManager
  public function load(ObjectManager $manager)
  {
    // Liste des noms de statut à ajouter
    $names = array(
      'WEB',
      'SAP',
      'DECISIONNEL'
    );

    foreach ($names as $name) {
      // On crée le statut
      $categorie = new Categorie();
      $categorie->setName($name);

      // On la persiste
      $manager->persist($categorie);
    }

    // On déclenche l'enregistrement de toutes les catégories
    $manager->flush();
  }
}