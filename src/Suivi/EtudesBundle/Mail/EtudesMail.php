<?php
// src/Suivi/EtudesBundle/Mail/EtudesMail.php

namespace Suivi\EtudesBundle\Mail;

/**
 * service EtudesMail
 * @author bureau_ju
 * 31/10/14
 */
class EtudesMail
{
	
	private $nomUser;
	private $mailUser;
	private $idDemande;
	private $libNouveauStatut;
	private $urlDemande;
	private $titreDemande;
	private $aObservateur = array();
	
	public function __construct($mailer){
		$this->mailer = $mailer;
	}
	
	//SETTER
	public function setNomUser($nomUser){
		$this->nomUser = $nomUser;
		
		return $this;
	}
	
	public function setMailUser($mailUser){
		$this->mailUser= $mailUser;
		
		return $this;
	}
	
	public function setIdDemande($idDemande){
		$this->idDemande = $idDemande;
		
		return $this;
	}
	
	public function setNouveauStatut($libNouveauStatut){
		$this->libNouveauStatut = $libNouveauStatut;
		
		return $this;
	}
	
	public function setUrlDemande($urlDemande){
		$this->urlDemande = $urlDemande;
		
		return $this;
	}
	
	public function setTitreDemande($titreDemande){
		$this->titreDemande = $titreDemande;
		
		return $this;
	}

	/**
	 * @author Julien BUREAU <jbureau@noz.fr>
	 * - 31/10/14
	 * @param unknown $aObservateurs
	 */
	public function mailUser($aObservateurs) {
		//Déclaration de l'objet, de l'emetteur, du destinataire et du contenu du mail
		
		$message = \Swift_Message::newInstance()
			->setSubject("Votre annonce n°".$this->idDemande." - ".$this->titreDemande." a changée de statut.")
			->setFrom('poleweb@noz.fr')
			->setTo($this->mailUser)
			->setBody('je suis un mail pour l utilisateur.')
		;

		//ajout des observateurs
		foreach ($aObservateurs as $observateur) { // recuperation de la liste des utilisateurs
			$message->setCc($observateur);
		}
		
		//Envoi de l'e-mail
		$this->mailer->send($message);
	}
	
	/**
	 * @author Julien BUREAU <jbureau@noz.fr>
	 * - 31/10/14
	 */
	public function mailManager() {
		//Déclaration de l'objet, de l'emetteur, du destinataire et du contenu du mail
	
		$message = \Swift_Message::newInstance()
		->setSubject("L'annonce n°".$this->idDemande." - ".$this->titreDemande." a changée de statut.")
			
		->setFrom('poleweb@noz.fr')
			
		->setTo($this->mailManager)
			
		->setBody('je suis un mail.');
	
		// 					$this->render('SuiviEtudesBundle:Demande:creationDemande.html.twig', array(
		// 					'nom' => $this->nomUser,
		// 					'identifiant' => $this->idDemande,
		// 					'url' => $this->urlDemande,
		// 					'nouveaustatut' => $this->libNouveauStatut,
		// 			)));
	
		//Envoi de l'e-mail
		$this->mailer->send($message);
	}
	
	/**
	 * @author Julien BUREAU <jbureau@noz.fr>
	 * - 31/10/14
	 */
	public function mailDevelloper() {
		//Déclaration de l'objet, de l'emetteur, du destinataire et du contenu du mail
	
		$message = \Swift_Message::newInstance()
		->setSubject("L'annonce n°".$this->idDemande." - ".$this->titreDemande." a changée de statut.")
			
		->setFrom('poleweb@noz.fr')
			
		->setTo($this->mailDevelloper)
			
		->setBody('je suis un mail.');
	
		// 					$this->render('SuiviEtudesBundle:Demande:creationDemande.html.twig', array(
		// 					'nom' => $this->nomUser,
		// 					'identifiant' => $this->idDemande,
		// 					'url' => $this->urlDemande,
		// 					'nouveaustatut' => $this->libNouveauStatut,
		// 			)));
	
		//Envoi de l'e-mail
		$this->mailer->send($message);
	}
}