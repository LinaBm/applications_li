<?php

namespace Suivi\EtudesBundle\DoctrineListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Suivi\EtudesBundle\Entity\Modification;
use Suivi\EtudesBundle\Entity\Demande;

class ModificationDemande {

    protected $container;
    private $modifs;

    public function __construct($container) {
        $this->container = $container;
    }

    /**
     * 
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Demande) {
            $i = 0;
            $listeModif = array();
            foreach ($args->getEntityChangeSet() as $colonne) {
                if ($colonne[0] instanceof \DateTime) {
                    $colonne[0] = $colonne[0]->format('Y-m-d H:i:s');
                }
                if ($colonne[1] instanceof \DateTime) {
                    $colonne[1] = $colonne[1]->format('Y-m-d H:i:s');
                }
                $modification = new Modification();
                $modification->setDate(new \DateTime());
                $modification->setType(array_keys($args->getEntityChangeSet())[$i]);
                $modification->setAncienneValeur($colonne[0]);
                $modification->setNouvelleValeur($colonne[1]);
                if ((array_keys($args->getEntityChangeSet())[$i] == "chargesCPRestante" && $colonne[0] == NULL) || (array_keys($args->getEntityChangeSet())[$i] == "chargesDevRestante" && $colonne[0] == NULL)) {
                    $modification->setUser(null);
                } else {
                    $modification->setUser($this->container->get('security.context')->getToken()->getUser());
                }
                $modification->setDemande($entity);
                $listeModif[$i] = $modification;
                $i++;
                $this->modif = $modification;
            }
            $this->modifs = $listeModif;
        }
    }

    public function postUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();
        if ($entity instanceof Demande) {
            // new implementation to remove stand by row if status changes and request is not yest validated
            $standBy = $em->getRepository('SuiviEtudesBundle:DemandeStandBy')->findOneBy(array('demande' => $entity, 'validated' => 0));
            if(null !== $standBy) {
                $em->remove($standBy);
            }
            foreach ($this->modifs as $colonne) {
                $em->persist($colonne);
                $em->flush();
            }
        }
    }

}
