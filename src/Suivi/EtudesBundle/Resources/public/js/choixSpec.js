
//affiche les champs correspondant au choix de l'utilisateur
function displayFichier(choix) {
	//alert(choix);
	if(choix=='upload'){
		$(".ch-chemin").hide();
		$(".ch-fichier").show();
	} else if (choix=='reseau') {
		$(".ch-chemin").show();
		$(".ch-fichier").hide();	
	} else {
		$(".ch-chemin").hide();
		$(".ch-fichier").hide();		
	}
}



$(document).ready(function(){
	$('#suivi_etudes_demande_spec_choix_0').prop('checked', true);
	displayFichier('upload');
	$('input[name$="suivi_etudes_demande_spec[choix]"]').on("change",function(){
		displayFichier($(this).val());
	});
});