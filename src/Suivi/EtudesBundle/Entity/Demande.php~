<?php

namespace Suivi\EtudesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Suivi\EtudesBundle\Entity\Statut;
use Suivi\EtudesBundle\Entity\Application;
use Suivi\EtudesBundle\Entity\Piecejointe;
use Suivi\EtudesBundle\Entity\suiviMail;
use Suivi\EtudesBundle\Entity\Modification;
use User\UserBundle\Entity\User;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * Demande
 *
 * @ORM\Table(name="se_demande")
 * @ORM\Entity(repositoryClass="Suivi\EtudesBundle\Entity\DemandeRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @Assert\Callback(methods={"validateDates"})
 */
class Demande
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(name="date_recette", type="datetime", nullable=true)
     * @Assert\DateTime(message="La date de recette n'est pas valide.")
     */
    private $dateRecette;

    /**
     * @ORM\Column(name="charges", type="string", nullable=true)
     * @Assert\Type(type="digit", message="La charge doit être un nombre entier.")
     */
    private $charges;

    /**
     * @ORM\Column(name="charges_cp", type="string", nullable=true)
     * @Assert\Type(type="digit", message="La charge du chef de projet doit être un nombre entier.")
     */
    private $chargesCP;

    /**
     * @ORM\Column(name="charges_dev", type="string", nullable=true)
     * @Assert\Type(type="digit", message="La charge du développeur doit être un nombre entier.")
     */
    private $chargesDev;

    /**
     * @ORM\Column(name="charges_cp_realise", type="string", nullable=true)
     * @Assert\Type(type="digit", message="La charge réalisée du chef de projet doit être un nombre entier.")
     */
    private $chargesCPRealise;

    /**
     * @ORM\Column(name="charges_dev_realise", type="string", nullable=true)
     * @Assert\Type(type="digit", message="La charge réalisée du développeur doit être un nombre entier.")
     */
    private $chargesDevRealise;

    /**
     * @ORM\Column(name="charges_cp_restante", type="string", nullable=true)
     * @Assert\Type(type="digit", message="La charge restante du chef de projet doit être un nombre entier.")
     */
    private $chargesCPRestante;

    /**
     * @ORM\Column(name="charges_dev_restante", type="string", nullable=true)
     * @Assert\Type(type="digit", message="La charge restante du développeur doit être un nombre entier.")
     */
    private $chargesDevRestante;

    /**
     * @ORM\Column(name="charges_etudes", type="string", nullable=true)
     * @Assert\Type(type="digit", message="L'estimation de la phase d'Etudes doit être un nombre entier.")
     */
    private $chargesEtudes;

    /**
     * @ORM\Column(name="charges_etudes_realise", type="string", nullable=true)
     * @Assert\Type(type="digit", message="L'estimation de la phase d'Etudes doit être un nombre entier.")
     */
    private $chargesEtudesRealise;

    /**
     * @ORM\Column(name="date_production", type="datetime", nullable=true)
     * @Assert\DateTime(message="La date de mise en production n'est pas valide.")
     */
    private $dateProduction;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)

     */
    private $updatedAt;

    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\Column(name="date_cloture", type="date", nullable=true)
     */
    private $dateCloture;

    /**
     * @ORM\ManyToOne(targetEntity="Suivi\EtudesBundle\Entity\Statut")
     * @ORM\JoinColumn(nullable=false)
     */
    private $statut;

    /**
     * @ORM\ManyToOne(targetEntity="Suivi\EtudesBundle\Entity\Urgence")
     * @ORM\JoinColumn(nullable=false)
     */
    private $urgence;

    /**
     * @ORM\ManyToOne(targetEntity="Suivi\EtudesBundle\Entity\Type")
     * @ORM\JoinColumn(nullable=false)
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="Suivi\EtudesBundle\Entity\Application")
     * @ORM\JoinColumn(nullable=true)
     */
    private $application;

    /**
     * @ORM\OneToMany(targetEntity="Suivi\EtudesBundle\Entity\Piecejointe", mappedBy="demande", cascade={"persist", "remove"})
     */
    private $piecejointes; //Notez le « s », une demande est liée à plusieurs pièces jointes

    /**
     * @ORM\ManyToOne(targetEntity="Administration\DirectionBundle\Entity\Service")
     * @ORM\JoinColumn(nullable=true)
     */
    private $service;

    /**
     * @ORM\ManyToOne(targetEntity="Administration\DirectionBundle\Entity\Direction")
     * @ORM\JoinColumn(nullable=true)
     */
    private $direction;

    /**
     * @ORM\Column(name="mailing", type="boolean", nullable=true)
     */
    private $mailing;

    /**
     * @ORM\OneToMany(targetEntity="Suivi\EtudesBundle\Entity\DemandeUser", mappedBy="demande", cascade={"persist", "remove"})
     */
    private $demandeUser;

    /**
     * @ORM\Column(name="recette", type="boolean", nullable=true)
     */
    private $recette;

    /**
     * @ORM\OneToOne(targetEntity="Suivi\EtudesBundle\Entity\Piecejointe", mappedBy="demande", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $devis;

    /**
     * @ORM\Column(name="demandeur_id", type="integer")
     */
    private $demandeur;

    /**
     * @ORM\Column(name="modification_date_prod", type="boolean", nullable=true)
     */
    private $modifProd;

    /**
     * @ORM\Column(name="motifRefus", type="string", length=255, nullable=true)
     */
    private $motifRefus;

    /**
     * @ORM\Column(name="descriptionRefus", type="text", nullable=true)
     */
    private $descriptionRefus;

    /**
     * @ORM\Column(name="commentaireRecette", type="text", nullable=true)
     */
    private $commentaireRecette;

    /**
     * @ORM\Column(name="commentaireProd", type="text", nullable=true)
     */
    private $commentaireProd;

    /**
     * @ORM\Column(name="commentaireInfo", type="text", nullable=true)
     */
    private $commentaireInfo;

    /**
     * @ORM\Column(name="spec", type="text", nullable=true)
     */
    private $spec;

    /**
     * @ORM\OneToOne(targetEntity="Suivi\EtudesBundle\Entity\Piecejointe", mappedBy="demande", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $cahierRecette;

    public function __construct()
    {
        $this->date         = new \Datetime();
        //$this->categories   = new ArrayCollection();
        //$this->applications = new ArrayCollection();
        $this->piecejointes = new ArrayCollection();
        $this->demandeUser = new ArrayCollection();
        // On récupère l'EntityManager
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Demande
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Demande
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Demande
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @ORM\PreUpdate
     */
    public function updateDate()
    {
        $this->setUpdatedAt(new \Datetime());
    }

    public function setUpdatedAt(\Datetime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set dateCloture
     *
     * @param \DateTime $dateCloture
     * @return Demande
     */
    public function setDateCloture($dateCloture)
    {
        $this->dateCloture = $dateCloture;

        return $this;
    }

    /**
     * Get dateCloture
     *
     * @return \DateTime
     */
    public function getDateCloture()
    {
        return $this->dateCloture;
    }

    /**
     * Set statut
     *
     * @param \Suivi\EtudesBundle\Entity\Statut $statut
     * @return Demande
     */
    public function setStatut(\Suivi\EtudesBundle\Entity\Statut $statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return \Suivi\EtudesBundle\Entity\Statut
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set urgence
     *
     * @param \Suivi\EtudesBundle\Entity\Urgence $urgence
     * @return Demande
     */
    public function setUrgence(\Suivi\EtudesBundle\Entity\Urgence $urgence)
    {
        $this->urgence = $urgence;

        return $this;
    }

    /**
     * Get urgence
     *
     * @return \Suivi\EtudesBundle\Entity\Urgence
     */
    public function getUrgence()
    {
        return $this->urgence;
    }

    /**
     * Set type
     *
     * @param \Suivi\EtudesBundle\Entity\Type $type
     * @return Demande
     */
    public function setType(\Suivi\EtudesBundle\Entity\Type $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \Suivi\EtudesBundle\Entity\Type
     */
    public function getType()
    {
        return $this->type;
    }

    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }

    /**
     * Set application
     *
     * @param \Suivi\EtudesBundle\Entity\Application $application
     * @return Demande
     */
    public function setApplication(\Suivi\EtudesBundle\Entity\Application $application)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \Suivi\EtudesBundle\Entity\Application
     */
    public function getApplication()
    {
        return $this->application;
    }


    /**
     * Add piecejointes
     *
     * @param \Suivi\EtudesBundle\Entity\Piecejointe $piecejointe
     * @return Demande
     */
    public function addPiecejointe(\Suivi\EtudesBundle\Entity\Piecejointe $piecejointe)
    {
        $this->piecejointes[] = $piecejointe;

        $piecejointe->setDemande($this);

        return $this;
    }

    /**
    * Get piecejointes
    * @return \Doctrine\Common\Collections\Collection
	*/
    public function getPiecejointes()
    {
    	return $this->piecejointes->toArray();
    }

    /**
     * Remove piecejointe
     *
     * @param \Suivi\EtudesBundle\Entity\PieceJointe $piecejointe
     */
    public function removePiecejointe(\Suivi\EtudesBundle\Entity\Piecejointe $piecejointe)
    {
        $this->piecejointes->removeElement($piecejointe);
    }

    /**
     * Set devis
     *
     * @param \Suivi\EtudesBundle\Entity\PieceJointe $piecejointe
     */
    public function setDevis(\Suivi\EtudesBundle\Entity\Piecejointe $devis)
    {
    	if ($this->devis != null){
    		$this->addPiecejointe($this->devis);
    	}
    	$this->devis=$devis;
    }

    /**
     * Get devis
     *
     * @return \Suivi\EtudesBundle\Entity\PieceJointe
     */
    public function getDevis()
    {
    	return $this->devis;
    }


   /**
     * Set service
     *
     * @param \Administration\DirectionBundle\Entity\Service $service
     * @return Demande
     */
    public function setService(\Administration\DirectionBundle\Entity\Service $service)
    {
    	$this->service = $service;

    	return $this;
    }

    /**
     * Get service
     *
     * @return \Administration\DirectionBundle\Entity\Service
     */
    public function getService()
    {
    	return $this->service;
    }

    /**
     * Set direction
     *
     * @param \Administration\DirectionBundle\Entity\Direction $direction
     * @return Demande
     */
    public function setDirection(\Administration\DirectionBundle\Entity\Direction $direction)
    {
    	$this->direction = $direction;

    	return $this;
    }

    /**
     * Get direction
     *
     * @return \Administration\DirectionBundle\Entity\Direction
     */
    public function getDirection()
    {
    	return $this->direction;
    }


    /**
     * Set mailing
     *
     * @param boolean $mailing
     * @return Demande
     */
    public function setMailing($mailing)
    {
        $this->mailing = $mailing;

        return $this;
    }

    /**
     * Get mailing
     *
     * @return boolean
     */
    public function getMailing()
    {
        return $this->mailing;
    }

    /**
     * Add demandeUser
     *
     * @param \Suivi\EtudesBundle\Entity\DemandeUser $demandeUser
     * @return Demande
     */
    public function addDemandeUser(\Suivi\EtudesBundle\Entity\DemandeUser $demandeUser, User $userConnecte=null)
    {
        $this->demandeUser[] = $demandeUser;

        return $this;
    }

    /**
     * Remove demandeUser
     *
     * @param \Suivi\EtudesBundle\Entity\DemandeUser $demandeUser
     */
    public function removeDemandeUser(\Suivi\EtudesBundle\Entity\DemandeUser $demandeUser)
    {
        $this->demandeUser->removeElement($demandeUser);
    }

    /**
     * Get demandeUser
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDemandeUser()
    {
        return $this->demandeUser;
    }

    /**
     * Set dateRecette
     *
     * @param \DateTime £dateRecette
     * @return Demande
     */
    public function setDateRecette($dateRecette)
    {
        $this->dateRecette = $dateRecette;

        return $this;
    }

    /**
     * Get dateRecette
     *
     * @return \DateTime
     */
    public function getDateRecette()
    {
        return $this->dateRecette;
    }

    /**
     * Set charges
     *
     * @param string $charges
     * @return Demande
     */
    public function setCharges($charges)
    {
        $this->charges = $charges;

        return $this;
    }

    /**
     * Get charges
     *
     * @return string
     */
    public function getCharges()
    {
        return $this->charges;
    }


    /**
     * Set chargesCP
     *
     * @param string $chargesCP
     * @return Demande
     */
    public function setChargesCP($chargesCP)
    {
        $this->chargesCP = $chargesCP;

        return $this;
    }

    /**
     * Get chargesCP
     *
     * @return string
     */
    public function getChargesCP()
    {
        return $this->chargesCP;
    }

     /**
     * Set chargesDev
     *
     * @param string $chargesDev
     * @return Demande
     */
    public function setChargesDev($chargesDev)
    {
        $this->chargesDev = $chargesDev;

        return $this;
    }

    /**
     * Get chargesDev
     *
     * @return string
     */
    public function getChargesDev()
    {
        return $this->chargesDev;
    }

    /**
     * Set dateProduction
     *
     * @param \DateTime $dateProduction
     * @return Demande
     */
    public function setDateProduction($dateProduction)
    {
        $this->dateProduction = $dateProduction;

        return $this;
    }

    /**
     * Get dateProduction
     *
     * @return \DateTime
     */
    public function getDateProduction()
    {
        return $this->dateProduction;
    }

    /**
     * Set recette
     *
     * @param boolean $recette
     * @return Demande
     */
    public function setRecette($recette)
    {
    	$this->recette = $recette;

    	return $this;
    }

    /**
     * Get recette
     *
     * @return boolean
     */
    public function getRecette()
    {
    	return $this->recette;
    }


    public function __toString(){
        return $this->id.' - '.$this->titre;
    }


    /**
     * Set demandeur
     *
     * @param integer $demandeur
     * @return Demande
     */
    public function setDemandeur($demandeur)
    {
        $this->demandeur = $demandeur;

        return $this;
    }

    /**
     * Get demandeur
     *
     * @return integer
     */
    public function getDemandeur()
    {
        return $this->demandeur;
    }

    /**
     * Set modifProd
     *
     * @param boolean $modifProd
     * @return Demande
     */
    public function setModifProd($modifProd)
    {
        $this->modifProd = $modifProd;

        return $this;
    }

    /**
     * Get modifProd
     *
     * @return boolean
     */
    public function getModifProd()
    {
        return $this->modifProd;
    }

    /**
     * Set motifRefus
     *
     * @param boolean $motifRefus
     * @return Demande
     */
    public function setMotifRefus($motifRefus)
    {
    	$this->motifRefus = $motifRefus;

    	return $this;
    }

    /**
     * Get motifRefus
     *
     * @return string
     */
    public function getMotifRefus()
    {
    	return $this->motifRefus;
    }

    /**
     * Set motifcommentaireProd
     *
     * @param boolean $motifRefus
     * @return Demande
     */
    public function setCommentaireProd($commentaireProd)
    {
    	$this->commentaireProd = $commentaireProd;

    	return $this;
    }

    /**
     * Get motifCommentaireProd
     *
     * @return string
     */
    public function getCommentaireProd()
    {
    	return $this->commentaireProd;
    }

    /**
     * Set descriptionRefus
     *
     * @param string $descriptionRefus
     * @return Demande
     */
    public function setDescriptionRefus($descriptionRefus)
    {
    	$this->descriptionRefus = $descriptionRefus;

    	return $this;
    }

    /**
     * Get descriptionRefus
     *
     * @return string
     */
    public function getDescriptionRefus()
    {
    	return $this->descriptionRefus;
    }

    /**
     * Set descriptioncommentaireRecette
     *
     * @param string $commentaireRecette
     * @return Demande
     */
    public function setCommentaireRecette($commentaireRecette)
    {
    	$this->commentaireRecette = $commentaireRecette;

    	return $this;
    }

    /**
     * Get commentaireRecette
     *
     * @return string
     */
    public function getCommentaireRecette()
    {
    	return $this->commentaireRecette;
    }

    /**
     * Set spec
     *
     * @param string $spec
     * @return Demande
     */
    public function setSpec($spec)
    {
    	$this->spec = $spec;

    	return $this;
    }

    /**
     * Get spec
     *
     * @return string
     */
    public function getSpec()
    {
    	return $this->spec;
    }

    /**
     * Set CommentaireInfo
     *
     * @param string $commentaireInfo
     * @return Demande
     */
    public function setCommentaireInfo($commentaireInfo)
    {
    	$this->commentaireInfo = $commentaireInfo;

    	return $this;
    }

    /**
     * Get CommentaireInfo
     *
     * @return string
     */
    public function getCommentaireInfo()
    {
    	return $this->commentaireInfo;
    }


    /**
     * Set devis
     *
     * @param \Suivi\EtudesBundle\Entity\PieceJointe $piecejointe
     */
    public function setCahierRecette(\Suivi\EtudesBundle\Entity\Piecejointe $cahierRecette)
    {
    	if ($this->cahierRecette != null){
    		$this->addPiecejointe($this->cahierRecette);
    	}
    	$this->cahierRecette=$cahierRecette;
    }

    /**
     * Get devis
     *
     * @return \Suivi\EtudesBundle\Entity\PieceJointe
     */
    public function getCahierRecette()
    {
    	return $this->cahierRecette;
    }

    /**
     * @Assert\Callback
     */
    public function validateDates(ExecutionContextInterface $context)
    {
    	if ($this->getDateProduction()<$this->getDateRecette())
    	{
    		$context->addViolationAt(
    				'DateRecette',
    				'La date de la recette doit être antérieure à la date de mise en production',
    				array(),
    				null
    		);
    	}
    }

    /**
     * Set chargesCPRealise
     *
     * @param string $chargesCPRealise
     * @return Demande
     */
    public function setChargesCPRealise($chargesCPRealise)
    {
        $this->chargesCPRealise = $chargesCPRealise;

        return $this;
    }

    /**
     * Get chargesCPRealise
     *
     * @return string
     */
    public function getChargesCPRealise()
    {
        return $this->chargesCPRealise;
    }

    /**
     * Set chargesDevRealise
     *
     * @param string $chargesDevRealise
     * @return Demande
     */
    public function setChargesDevRealise($chargesDevRealise)
    {
        $this->chargesDevRealise = $chargesDevRealise;

        return $this;
    }

    /**
     * Get chargesDevRealise
     *
     * @return string
     */
    public function getChargesDevRealise()
    {
        return $this->chargesDevRealise;
    }

    /**
     * Set chargesCPRestante
     *
     * @param string $chargesCPRestante
     * @return Demande
     */
    public function setChargesCPRestante($chargesCPRestante)
    {
        $this->chargesCPRestante = $chargesCPRestante;

        return $this;
    }

    /**
     * Get chargesCPRestante
     *
     * @return string
     */
    public function getChargesCPRestante()
    {
        return $this->chargesCPRestante;
    }

    /**
     * Set chargesDevRestante
     *
     * @param string $chargesDevRestante
     * @return Demande
     */
    public function setChargesDevRestante($chargesDevRestante)
    {
        $this->chargesDevRestante = $chargesDevRestante;

        return $this;
    }

    /**
     * Get chargesDevRestante
     *
     * @return string
     */
    public function getChargesDevRestante()
    {
        return $this->chargesDevRestante;
    }

    /**
     * Set chargesEtudes
     *
     * @param string $chargesEtudes
     * @return Demande
     */
    public function setChargesEtudes($chargesEtudes)
    {
        $this->chargesEtudes = $chargesEtudes;

        return $this;
    }

    /**
     * Get chargesEtudes
     *
     * @return string
     */
    public function getChargesEtudes()
    {
        return $this->chargesEtudes;
    }

    /**
     * Set chargesEtudesRealise
     *
     * @param string $chargesEtudesRealise
     * @return Demande
     */
    public function setChargesEtudesRealise($chargesEtudesRealise)
    {
        $this->chargesEtudesRealise = $chargesEtudesRealise;

        return $this;
    }

    /**
     * Get chargesEtudesRealise
     *
     * @return string
     */
    public function getChargesEtudesRealise()
    {
        return $this->chargesEtudesRealise;
    }
}
