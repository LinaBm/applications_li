<?php

namespace Suivi\EtudesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SeProjetTauxHoraire
 *
 * @ORM\Table(name="se_projet_taux_horaire")
 * @ORM\Entity
 */
class ProjetTauxHoraire {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="period", type="string", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $period;

    /**
     * @var string
     *
     * @ORM\Column(name="job_title", type="string", length=255, nullable=true)
     */
    private $jobTitle;

    /**
     * @var float
     *
     * @ORM\Column(name="salary", type="float", precision=10, scale=0, nullable=true)
     */
    private $salary;

    /**
     * @var float
     *
     * @ORM\Column(name="taux_horaire", type="float", precision=10, scale=0, nullable=true)
     */
    private $tauxHoraire;

    /**
     * @ORM\OneToMany(targetEntity="Suivi\EtudesBundle\Entity\ProjetCharge", mappedBy="tax")
     */
    private $charge;

    public function __construct() {
        $this->charge = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get charge
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCharge() {
        return $this->charge;
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return ProjetTauxHoraire
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return ProjetTauxHoraire
     */
    public function setUserId($userId) {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId() {
        return $this->userId;
    }

    /**
     * Set jobTitle
     *
     * @param string $jobTitle
     *
     * @return ProjetTauxHoraire
     */
    public function setJobTitle($jobTitle) {
        $this->jobTitle = $jobTitle;

        return $this;
    }

    /**
     * Get jobTitle
     *
     * @return string
     */
    public function getJobTitle() {
        return $this->jobTitle;
    }

    /**
     * Set salary
     *
     * @param float $salary
     *
     * @return ProjetTauxHoraire
     */
    public function setSalary($salary) {
        $this->salary = $salary;

        return $this;
    }

    /**
     * Get salary
     *
     * @return float
     */
    public function getSalary() {
        return $this->salary;
    }

    /**
     * Set period
     *
     * @param \DateTime $period
     *
     * @return ProjetTauxHoraire
     */
    public function setPeriod($period) {
        $this->period = $period;

        return $this;
    }

    /**
     * Get period
     *
     * @return \DateTime
     */
    public function getPeriod() {
        return $this->period;
    }

    /**
     * Set tauxHoraire
     *
     * @param float $tauxHoraire
     *
     * @return ProjetTauxHoraire
     */
    public function setTauxHoraire($tauxHoraire) {
        $this->tauxHoraire = $tauxHoraire;

        return $this;
    }

    /**
     * Get tauxHoraire
     *
     * @return float
     */
    public function getTauxHoraire() {
        return $this->tauxHoraire;
    }

}
