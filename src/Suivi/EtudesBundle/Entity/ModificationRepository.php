<?php

namespace Suivi\EtudesBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ModificationRepository extends EntityRepository {

    public function getModificationsParDemande($demande) {
        $qb = $this->createQueryBuilder('m')
                ->andWhere('m.demande = :demande')
                ->setParameter('demande', $demande->getId())
                ->orderBy('m.date', 'DESC');
        return $qb->getQuery()->execute();

        /* $qb = $this->createQueryBuilder('m')
          ->select('COALESCE(SUM(d.chargesCPRestante)/8, 0) AS chargesCPRest')
          ->addSelect('COALESCE(SUM(d.chargesDevRestante)/8, 0) AS chargesDevRest')
          ->innerJoin('d.application', 'a',  'd.application = a.id') //Jointure sur les applications
          ->addSelect('a.id')
          ->addSelect('a.name')
          ->groupBy('d.application');
          return $qb ->getQuery()->getResult(); */
    }

    public function getDemandeValidationDate($demande) {
        $result = $this->createQueryBuilder('m')
                ->select('m.date')
                ->where('m.demande = :demande')
                ->andWhere("m.nouvelleValeur = 'Validation (devis)'")
                ->setParameter('demande', $demande->getId())
                ->orderBy('m.date', 'DESC')
                ->getQuery()
                ->setMaxResults(1)
                ->getResult();
        if (!empty($result)) {
            return $result[0]['date']->format('Y-m-01');
        }
        return null;
    }

}
