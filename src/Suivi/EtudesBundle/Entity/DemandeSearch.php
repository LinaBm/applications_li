<?php

namespace Suivi\EtudesBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * DemandeSearch
 *
 * @ORM\Table(name="se_demandesearch")
 * @ORM\Entity
 */
class DemandeSearch
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    public $user_id;
    
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    public $demande_id;
	
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    public $date;

    /**
    * @ORM\ManyToOne(targetEntity="Suivi\EtudesBundle\Entity\Statut")
    * @ORM\JoinColumn(nullable=true)
    */

    private $statut;
    /**
    * @ORM\ManyToOne(targetEntity="Suivi\EtudesBundle\Entity\Urgence")
    * @ORM\JoinColumn(nullable=true)
    */
    private $urgence;

    /**
    * @ORM\ManyToOne(targetEntity="Suivi\EtudesBundle\Entity\Application")
    * @ORM\JoinColumn(nullable=true)
    */
    private $application;

    /**
    * @ORM\ManyToOne(targetEntity="Suivi\EtudesBundle\Entity\Type")
    * @ORM\JoinColumn(nullable=true)
    */
    private $type;

    /**
    * @ORM\ManyToOne(targetEntity="Administration\DirectionBundle\Entity\Direction")
    * @ORM\JoinColumn(nullable=true)
    */
    private $direction;
    
    /**
    * @ORM\ManyToOne(targetEntity="Administration\DirectionBundle\Entity\Service")
    * @ORM\JoinColumn(nullable=true)
    */
    private $service;
    
    /**
    * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User")
    * @ORM\JoinColumn(nullable=true)
     */
    private $demandeur;
    /**
    * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User")
    * @ORM\JoinColumn(nullable=true)
     */
    private $technicien;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $expression;
    
     /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $booDescription;
    
    /**
     * Set user_id
     *
     * @param boolean $user_id
     * @return DemandeSearch
     */
    
    public function setUserId($user_id)
    {
    	$this->user_id = $user_id;
    
    	return $this;
    }
    
    /**
     * Get user_id
     *
     * @return boolean
     */
    public function getUserId()
    {
    	return $this->user_id;
    }
    
    /**
     * Set statut
     *
     * @param \Suivi\EtudesBundle\Entity\Statut $statut
     * @return DemandeSearch
     */
    public function addStatut(\Suivi\EtudesBundle\Entity\Statut $statut = null)
    {
        $this->statut[] = $statut;
        return $this;
    }

    /**
     * Get statut
     *
     * @return \Suivi\EtudesBundle\Entity\Statut 
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set urgence
     *
     * @param \Suivi\EtudesBundle\Entity\Urgence $urgence
     * @return DemandeSearch
     */
    public function setUrgence(\Suivi\EtudesBundle\Entity\Urgence $urgence = null)
    {
        $this->urgence = $urgence;

        return $this;
    }

    /**
     * Get urgence
     *
     * @return \Suivi\EtudesBundle\Entity\Urgence 
     */
    public function getUrgence()
    {
        return $this->urgence;
    }

    /**
     * Set application
     *
     * @param \Suivi\EtudesBundle\Entity\Application $application
     * @return DemandeSearch
     */
    public function setApplication(\Suivi\EtudesBundle\Entity\Application $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \Suivi\EtudesBundle\Entity\Application 
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set type
     *
     * @param \Suivi\EtudesBundle\Entity\Type $type
     * @return DemandeSearch
     */
    public function setType(\Suivi\EtudesBundle\Entity\Type $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \Suivi\EtudesBundle\Entity\Type 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

     /**
     * Set service
     *
     * @param \Administration\DirectionBundle\Entity\Service $service
     * @return DemandeSearch
     */
    public function setService(\Administration\DirectionBundle\Entity\Service $service = null)
    {
    	$this->service = $service;
    
    	return $this;
    }
    
    /**
     * Get service
     *
     * @return \Administration\DirectionBundle\Entity\Service
     */
    public function getService()
    {
    	return $this->service;
    }

    /**
     * Set direction
     *
     * @param \Administration\DirectionBundle\Entity\Direction $direction
     * @return DemandeSearch
     */
    public function setDirection(\Administration\DirectionBundle\Entity\Direction $direction = null)
    {
    	$this->direction = $direction;
    
    	return $this;
    }
    
    /**
     * Get direction
     *
     * @return \Administration\DirectionBundle\Entity\Direction
     */
    public function getDirection()
    {
    	return $this->direction;
    }

    /**
     * Set demandeur
     *
     * @param \User\UserBundle\Entity\User $demandeur
     * @return DemandeSearch
     */
    public function setDemandeur(\User\UserBundle\Entity\User $demandeur = null)
    {
        $this->demandeur = $demandeur;

        return $this;
    }

    /**
     * Get demandeur
     *
     * @return \User\UserBundle\Entity\User 
     */
    public function getDemandeur()
    {
        return $this->demandeur;
    }

    /**
     * Set technicien
     *
     * @param \User\UserBundle\Entity\User $technicien
     * @return DemandeSearch
     */
    public function setTechnicien(\User\UserBundle\Entity\User $technicien = null)
    {
        $this->technicien = $technicien;

        return $this;
    }

    /**
     * Get technicien
     *
     * @return \User\UserBundle\Entity\User 
     */
    public function getTechnicien()
    {
        return $this->technicien;
    }

    /**
     * Set expression
     *
     * @param string $expression
     * @return DemandeSearch
     */
    public function setExpression($expression = null)
    {
    	$this->expression = $expression;
    
    	return $this;
    }
    
    /**
     * Get expression
     *
     * @return string
     */
    public function getExpression()
    {
    	return $this->expression;
    }

    /**
     * Set expression
     *
     * @param boolean $booDescription
     * @return DemandeSearch
     */
    
    public function setBooDescription($booDescription)
    {
    	$this->booDescription = $booDescription;
    
    	return $this;
    }
    
    /**
     * Get booDescription
     *
     * @return boolean
     */
    public function getBooDescription()
    {
    	return $this->booDescription;
    }
       
    
    public function __construct()
    {
    	$this->statut= new ArrayCollection();
    }
}
