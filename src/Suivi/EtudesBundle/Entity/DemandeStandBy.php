<?php

namespace Suivi\EtudesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DemandeStandBy
 *
 * @ORM\Table(name="se_demande_stand_by", indexes={@ORM\Index(name="demande_id_idx", columns={"demande_id"})})
 * @ORM\Entity
 */
class DemandeStandBy {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reason", type="string", length=255, nullable=false)
     */
    private $reason;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var \Demande
     *
     * @ORM\ManyToOne(targetEntity="Demande", inversedBy="standBy")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="demande_id", referencedColumnName="id")
     * })
     */
    private $demande;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(name="validated", type="integer")
     */
    private $validated;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set reason
     *
     * @param string $reason
     *
     * @return DemandeStandBy
     */
    public function setReason($reason) {
        $this->reason = $reason;

        return $this;
    }

    /**
     * Get reason
     *
     * @return string
     */
    public function getReason() {
        return $this->reason;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return DemandeStandBy
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set demande
     *
     * @param \Suivi\EtudesBundle\Entity\Demande $demande
     *
     * @return DemandeStandBy
     */
    public function setDemande(\Suivi\EtudesBundle\Entity\Demande $demande = null) {
        $this->demande = $demande;

        return $this;
    }

    /**
     * Get demande
     *
     * @return \Suivi\EtudesBundle\Entity\Demande
     */
    public function getDemande() {
        return $this->demande;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     * @return DemandeStandBy
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = new \Datetime();

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return datetime 
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }
    
    /**
     * Set validated
     *
     * @param string $validated
     *
     * @return DemandeStandBy
     */
    public function setValidated($validated) {
        $this->validated = $validated;

        return $this;
    }

    /**
     * Get validated
     *
     * @return int
     */
    public function getValidated() {
        return $this->validated;
    }

}
