<?php

namespace Suivi\EtudesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="se_demandeuser")
 * @ORM\Entity(repositoryClass="Suivi\EtudesBundle\Entity\DemandeUserRepository")
 */
class DemandeUser{

    const DEMANDEUR      = 'DEMANDEUR';
    const OBSERVATEUR    = 'OBSERVATEUR';
    const CHEF_DE_PROJET = 'CHEF_DE_PROJET';
    const TECHNICIEN     = 'TECHNICIEN';
    
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="demande_id", type="integer")
     */
    private $demandeId;

    /**
     * @ORM\ManyToOne(targetEntity="Suivi\EtudesBundle\Entity\Demande", inversedBy="demandeUser", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $demande;

     /**
     * @var integer
     * @ORM\OneToMany(targetEntity="Suivi\EtudesBundle\Entity\ProjetCharge", mappedBy="demandeUser")
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @ORM\ManyToMany(targetEntity="Mail", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="se_DemandeUserMail",
     *      joinColumns={@ORM\JoinColumn(name="demandeUserId", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="mailId", referencedColumnName="id")}
     *      )
     **/
    private $mail;
    
    /**
    * @ORM\Column(name="actif", type="boolean")
    */
    private $actif = true;
    
     /**
     * @ORM\OneToMany(targetEntity="Suivi\EtudesBundle\Entity\ProjetCharge", mappedBy="demandeUser")
     */
    private $charge;
    
    public function __construct() {
    	$this->setActif(true);
    	$this->mail = new \Doctrine\Common\Collections\ArrayCollection();
        $this->charge = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get charge
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCharge() {
        return $this->charge;
    }

    public function getId()
    {
      return $this->id;
    }

    public function getDemandeId() {
        return $this->demandeId;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return DemandeUser
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set mail
     *
     * @param string $mail
     * @return DemandeUser
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string 
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set demande
     *
     * @param \Suivi\EtudesBundle\Entity\Demande $demande
     * @return DemandeUser
     */
    public function setDemande(\Suivi\EtudesBundle\Entity\Demande $demande)
    {
        $this->demande = $demande;

        return $this;
    }

    /**
     * Get demande
     *
     * @return \Suivi\EtudesBundle\Entity\Demande 
     */
    public function getDemande()
    {
        return $this->demande;
    }

    /**
     * Set user
     *
     * @param \User\UserBundle\Entity\User $user
     * @return DemandeUser
     */
    public function setUser(\User\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \User\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    
    public function __toString(){
    	return $this->user->getDisplayname();
    }
    
    /**
     * Set actif
     *
     * @param boolean $actif
     * @return \User\UserBundle\Entity\User
     */    
    public function setActif($actif){
    	$this->actif=$actif;
    	return $this;
    }
    
    /**
     * Get actif
     *
     * @return boolean
     */
    public function getActif()
    {
    	return $this->actif;
    }    

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return DemandeUser
     */
    public function setUserId($userId) {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId() {
        return $this->userId;
    }
}
