<?php

namespace Suivi\EtudesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Modification
 *
 * @ORM\Table(name="se_modification")
 * @ORM\Entity(repositoryClass="Suivi\EtudesBundle\Entity\ModificationRepository")
 */
class Modification
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="Suivi\EtudesBundle\Entity\Demande")
     * @ORM\JoinColumn(nullable=true)
     */
    private $demande;

    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="ancienneValeur", type="text", nullable=true)
     */
    private $ancienneValeur;

    /**
     * @var string
     *
     * @ORM\Column(name="nouvelleValeur", type="text", nullable=true)
     */
    private $nouvelleValeur;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Modification
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set demande
     *
     * @param \stdClass $demande
     * @return Modification
     */
    public function setDemande($demande)
    {
        $this->demande = $demande;
    
        return $this;
    }

    /**
     * Get demande
     *
     * @return \stdClass 
     */
    public function getDemande()
    {
        return $this->demande;
    }

    /**
     * Set user
     *
     * @param \stdClass $user
     * @return Modification
     */
    public function setUser($user)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \stdClass 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Modification
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set ancienneValeur
     *
     * @param string $ancienneValeur
     * @return Modification
     */
    public function setAncienneValeur($ancienneValeur)
    {
        $this->ancienneValeur = $ancienneValeur;
    
        return $this;
    }

    /**
     * Get ancienneValeur
     *
     * @return string 
     */
    public function getAncienneValeur()
    {
        return $this->ancienneValeur;
    }

    /**
     * Set nouvelleValeur
     *
     * @param string $nouvelleValeur
     * @return Modification
     */
    public function setNouvelleValeur($nouvelleValeur)
    {
        $this->nouvelleValeur = $nouvelleValeur;
    
        return $this;
    }

    /**
     * Get nouvelleValeur
     *
     * @return string 
     */
    public function getNouvelleValeur()
    {
        return $this->nouvelleValeur;
    }
    public function __construct()
    {
    	$this->date         = new \DateTime();
    }
}
