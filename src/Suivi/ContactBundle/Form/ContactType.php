<?php

namespace Suivi\ContactBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Suivi\EtudesBundle\Entity\DemandeSearch;

class ContactType extends AbstractType
{
	
	private $criteres;
	
	public function __construct($criteres){
		$this->criteres = $criteres;
	}	
	
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('demande', 'entity', array(
                'class'    => 'SuiviEtudesBundle:Demande',
                'required' => false,
                'empty_value' => '',
            	'query_builder' => function(\Doctrine\ORM\EntityRepository $er) {
            		return $er->getResult($this->criteres);
            	}
            ))  //EVOLUTION : N'AFFICHER QUE LES DEMANDES CONCERNEES PAR LA PERSONNE COURANTE !!!!!
            ->add('objet', 'text')
            ->add('message', 'textarea')
            ->add('Envoyer', 'submit')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Suivi\ContactBundle\Entity\Contact'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'suivi_contactbundle_contact';
    }
}
