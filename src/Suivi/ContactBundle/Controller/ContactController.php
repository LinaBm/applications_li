<?php

namespace Suivi\ContactBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Suivi\ContactBundle\Entity\Contact;
use Suivi\ContactBundle\Form\ContactType;
use Suivi\EtudesBundle\Entity\DemandeSearch;


class ContactController extends Controller
{
    public function contacterAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        //Instanciation de la classe contact
        $contact = new Contact();
        $contact->setUser($this->getUser());
        //récupération des demandes
        $critere = new DemandeSearch();
        $critere->setDemandeur($this->getUser());
        
        //Création du formulaire
        $form = $this->createForm(new ContactType($critere), $contact);
                
        //A CHANGER POUR TEST/DEV/PROD ==> parameters.yml
        $url = $this->container->getParameter('url_serveur');
		
        //Récupération des données de la classe contact
        $mailContent = $em->getRepository('SuiviContactBundle:Contact')->findAll();

		

        //Vérification de la validité du formulaire
        if ($form->handleRequest($request)->isValid()) {
            if($contact->getObjet()===null or $contact->getMessage()===null){
                $this->get('session')->getFlashBag()->add('erreur', 'Erreur ! Vous devez renseigner le titre et le contenu du message.');
            } else {
            	$sujet = $contact->getObjet();
            	if ($this->container->get('kernel')->getEnvironment()!='prod'){
            		$sujet= '[TEST]'.$sujet;
            	}
                //Déclaration de l'objet, de l'emetteur, du destinataire et du contenu du mail
                $message = \Swift_Message::newInstance()
                    ->setContentType('text/html')
                    ->setSubject($sujet)
                    ->setFrom($contact->getUser()->getEmail())
                    ->setTo('poleweb@noz.fr')
                    ->setCc($this->getUser()->getEmail())
                    ->setBody($this->renderView('SuiviContactBundle:Contact:contactEmail.html.twig', array(
                        'contact' => $contact,
                        'url'  => $url,
                    	'environnement'=>$this->container->get('kernel')->getEnvironment(),
                )));

                //Envoi de l'e-mail
                $this->get('mailer')->send($message);
                $request->getSession()->getFlashBag()->add('success', ' Votre mesage a bien été envoyé !');
            }
        } else {
            foreach ($form->getErrors() as $key => $error) {
                $this->get('session')->getFlashBag()->add('erreur', $error->getMessage());
            }
            foreach ($form->all() as $child) {
                if (!$child->isValid()) {
                    foreach ($child->getErrors() as $key => $error) {
                        $this->get('session')->getFlashBag()->add('erreur', $error->getMessage());
                    }
                }
            }
        }

        //Vue du formulaire de contact
        return $this->render('SuiviContactBundle:Contact:contacter.html.twig', array(
            'form'	=>	$form->createView(),
            'mailContent' => $mailContent,
        ));
    }
}