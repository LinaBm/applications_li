<?php

namespace CovoiturageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CovReservation
 *
 * @ORM\Table(name="cov_reservation", indexes={@ORM\Index(name="trajet_id", columns={"trajet_id"}), @ORM\Index(name="passager_id", columns={"passager_id"})})
 * @ORM\Entity
 */
class CovReservation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \User\UserBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="passager_id", referencedColumnName="id")
     * })
     */
    private $passager;

    /**
     * @var \CovoiturageBundle\Entity\CovTrajet
     *
     * @ORM\ManyToOne(targetEntity="CovoiturageBundle\Entity\CovTrajet")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="trajet_id", referencedColumnName="id")
     * })
     */
    private $trajet;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set passager
     *
     * @param \User\UserBundle\Entity\User $passager
     *
     * @return CovReservation
     */
    public function setPassager(\User\UserBundle\Entity\User $passager = null)
    {
        $this->passager = $passager;
    
        return $this;
    }

    /**
     * Get passager
     *
     * @return \User\UserBundle\Entity\User
     */
    public function getPassager()
    {
        return $this->passager;
    }

    /**
     * Set trajet
     *
     * @param \CovoiturageBundle\Entity\CovTrajet $trajet
     *
     * @return CovReservation
     */
    public function setTrajet(\CovoiturageBundle\Entity\CovTrajet $trajet = null)
    {
        $this->trajet = $trajet;
    
        return $this;
    }

    /**
     * Get trajet
     *
     * @return \CovoiturageBundle\Entity\CovTrajet
     */
    public function getTrajet()
    {
        return $this->trajet;
    }
}

