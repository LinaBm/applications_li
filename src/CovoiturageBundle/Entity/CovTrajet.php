<?php

namespace CovoiturageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CovTrajet
 *
 * @ORM\Table(name="cov_trajet", indexes={@ORM\Index(name="cov_trajet_ibfk_2", columns={"trajet_suivant_id"}), @ORM\Index(name="cov_trajet_ibfk_1", columns={"conducteur_id"}), @ORM\Index(name="cov_trajet_ibfk_4", columns={"ville_arrivee"}), @ORM\Index(name="ville_depart", columns={"cov_trajet_ibfk_3"})})
 * @ORM\Entity
 */
class CovTrajet
{
    /**
     * @var string
     *
     * @ORM\Column(name="new_depart", type="string", length=255, nullable=true)
     */
    private $newDepart;

    /**
     * @var string
     *
     * @ORM\Column(name="new_arrivee", type="string", length=255, nullable=true)
     */
    private $newArrivee;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="heure_arrivee", type="string", length=30, nullable=false)
     */
    private $heureArrivee;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="heure_depart", type="string", length=30, nullable=false)
     */
    private $heureDepart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="jour", type="datetime", nullable=false)
     */
    private $jour;

    /**
     * @var integer
     *
     * @ORM\Column(name="nbre_place", type="integer", nullable=false)
     */
    private $nbrePlace;

    /**
     * @var boolean
     *
     * @ORM\Column(name="option_autoroute", type="boolean", nullable=false)
     */
    private $optionAutoroute = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="option_peages", type="boolean", nullable=false)
     */
    private $optionPeages = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="commentaire", type="string", length=255, nullable=false)
     */
    private $commentaire;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \CovoiturageBundle\Entity\CovEtape
     *
     * @ORM\ManyToOne(targetEntity="CovoiturageBundle\Entity\CovEtape")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ville_arrivee", referencedColumnName="id")
     * })
     */
    private $villeArrivee;
    /**
     * @var boolean
     *
     * @ORM\Column(name="type", type="boolean", nullable=false)
     */
    private $type = '0';
    /**
     * @var \CovoiturageBundle\Entity\CovEtape
     *
     * @ORM\ManyToOne(targetEntity="CovoiturageBundle\Entity\CovEtape")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ville_depart", referencedColumnName="id")
     * })
     */
    private $villeDepart;

    /**
     * @var \CovoiturageBundle\Entity\CovTrajet
     *
     * @ORM\ManyToOne(targetEntity="CovoiturageBundle\Entity\CovTrajet")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="trajet_suivant_id", referencedColumnName="id")
     * })
     */
    private $trajetSuivant;

    /**
     * @var \User\UserBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="conducteur_id", referencedColumnName="id")
     * })
     */
    private $conducteur;


    /**
     * Set newDepart
     *
     * @param string $newDepart
     *
     * @return CovTrajet
     */
    public function setNewDepart($newDepart)
    {
        $this->newDepart = $newDepart;
    
        return $this;
    }

    /**
     * Get newDepart
     *
     * @return string
     */
    public function getNewDepart()
    {
        return $this->newDepart;
    }

    /**
     * Set newArrivee
     *
     * @param string $newArrivee
     *
     * @return CovTrajet
     */
    public function setNewArrivee($newArrivee)
    {
        $this->newArrivee = $newArrivee;
    
        return $this;
    }

    /**
     * Get newArrivee
     *
     * @return string
     */
    public function getNewArrivee()
    {
        return $this->newArrivee;
    }

    /**
     * Set heureArrivee
     *
     * @param string $heureArrivee
     *
     * @return CovTrajet
     */
    public function setHeureArrivee($heureArrivee)
    {
        $this->heureArrivee = $heureArrivee;
    
        return $this;
    }

    /**
     * Get heureArrivee
     *
     * @return string
     */
    public function getHeureArrivee()
    {
        return $this->heureArrivee;
    }
    /**
     * Set type
     *
     * @param boolean $type
     *
     * @return CovEtape
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return boolean
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * Set heureDepart
     *
     * @param string $heureDepart
     *
     * @return CovTrajet
     */
    public function setHeureDepart($heureDepart)
    {
        $this->heureDepart = $heureDepart;
    
        return $this;
    }

    /**
     * Get heureDepart
     *
     * @return string
     */
    public function getHeureDepart()
    {
        return $this->heureDepart;
    }

    /**
     * Set jour
     *
     * @param \DateTime $jour
     *
     * @return CovTrajet
     */
    public function setJour($jour)
    {
        $this->jour = $jour;
    
        return $this;
    }

    /**
     * Get jour
     *
     * @return \DateTime
     */
    public function getJour()
    {
        return $this->jour;
    }

    /**
     * Set nbrePlace
     *
     * @param integer $nbrePlace
     *
     * @return CovTrajet
     */
    public function setNbrePlace($nbrePlace)
    {
        $this->nbrePlace = $nbrePlace;
    
        return $this;
    }

    /**
     * Get nbrePlace
     *
     * @return integer
     */
    public function getNbrePlace()
    {
        return $this->nbrePlace;
    }

    /**
     * Set optionAutoroute
     *
     * @param boolean $optionAutoroute
     *
     * @return CovTrajet
     */
    public function setOptionAutoroute($optionAutoroute)
    {
        $this->optionAutoroute = $optionAutoroute;
    
        return $this;
    }

    /**
     * Get optionAutoroute
     *
     * @return boolean
     */
    public function getOptionAutoroute()
    {
        return $this->optionAutoroute;
    }
    /**
     * Set optionPeages
     *
     * @param boolean $optionPeages
     *
     * @return CovTrajet
     */
    public function setOptionPeages($optionPeages)
    {
        $this->optionPeages = $optionPeages;

        return $this;
    }

    /**
     * Get optionPeages
     *
     * @return boolean
     */
    public function getOptionPeages()
    {
        return $this->optionPeages;
    }
    /**
     * Set commentaire
     *
     * @param string $commentaire
     *
     * @return CovTrajet
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;
    
        return $this;
    }

    /**
     * Get commentaire
     *
     * @return string
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set villeArrivee
     *
     * @param \CovoiturageBundle\Entity\CovEtape $villeArrivee
     *
     * @return CovTrajet
     */
    public function setVilleArrivee(\CovoiturageBundle\Entity\CovEtape $villeArrivee = null)
    {
        $this->villeArrivee = $villeArrivee;
    
        return $this;
    }

    /**
     * Get villeArrivee
     *
     * @return \CovoiturageBundle\Entity\CovEtape
     */
    public function getVilleArrivee()
    {
        return $this->villeArrivee;
    }

    /**
     * Set villeDepart
     *
     * @param \CovoiturageBundle\Entity\CovEtape $villeDepart
     *
     * @return CovTrajet
     */
    public function setVilleDepart(\CovoiturageBundle\Entity\CovEtape $villeDepart = null)
    {
        $this->villeDepart = $villeDepart;
    
        return $this;
    }

    /**
     * Get villeDepart
     *
     * @return \CovoiturageBundle\Entity\CovEtape
     */
    public function getVilleDepart()
    {
        return $this->villeDepart;
    }

    /**
     * Set trajetSuivant
     *
     * @param \CovoiturageBundle\Entity\CovTrajet $trajetSuivant
     *
     * @return CovTrajet
     */
    public function setTrajetSuivant(\CovoiturageBundle\Entity\CovTrajet $trajetSuivant = null)
    {
        $this->trajetSuivant = $trajetSuivant;
    
        return $this;
    }

    /**
     * Get trajetSuivant
     *
     * @return \CovoiturageBundle\Entity\CovTrajet
     */
    public function getTrajetSuivant()
    {
        return $this->trajetSuivant;
    }

    /**
     * Set conducteur
     *
     * @param \User\UserBundle\Entity\User $conducteur
     *
     * @return CovTrajet
     */
    public function setConducteur(\User\UserBundle\Entity\User $conducteur = null)
    {
        $this->conducteur = $conducteur;
    
        return $this;
    }

    /**
     * Get conducteur
     *
     * @return \User\UserBundle\Entity\User
     */
    public function getConducteur()
    {
        return $this->conducteur;
    }
}

