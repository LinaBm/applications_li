<?php

namespace CovoiturageBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CovoiturageType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('depart', 'choice', array('label'=>'Adresse Départ',  'mapped'=>false, 'attr'=>array('class'=>'form-control input-sm')))
            ->add('arrivee','choice', array('label'=>'Adresse Arrivée',  'mapped'=>false, 'attr'=>array('class'=>'form-control input-sm') ))
            ->add('autoroutes', 'checkbox', array('mapped' => true, 'label' => 'Eviter les autoroutes','required'=>false, 'attr'=>array('unchecked'=>'unchecked') ))
            ->add('peages', 'checkbox', array('mapped' => true, 'label' => 'Eviter les péages','required'=>false, 'attr'=>array('unchecked'=>'unchecked') ))
            ->add('Comment', 'textarea', array('required'=>false,'mapped' => false, 'label' => '(détails sur votre parcours, sur le lieu précis du départ, sur vos attentes, etc.)', 'attr'=>array('class'=>'form-control')))
            ->add('retour', 'checkbox', array('mapped' => true, 'label' => 'Aller-Retour','required'=>false, 'attr'=>array('unchecked'=>'unchecked') ))
            #validation de formulaire
            ->add('Valider', 'submit', array('attr'=>array('class'=>'btn btn-info pull-right')))
            ->add('Reserver', 'submit', array('attr'=>array('class'=>'btn btn-info pull-right')))
            ->add('Rechercher', 'submit', array('attr'=>array('class'=>'btn btn-info pull-right')))
            ->add('Annuler', 'button', array('attr'=>array('class'=>'btn btn-default')))

            #liste logiciel
        ;
    }
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'allow_extra_fields' => true
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'covoituragebundle_profil';
    }
}
