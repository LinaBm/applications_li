<?php

namespace CovoiturageBundle\Controller;

use CovoiturageBundle\Form\CovoiturageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="covoiturage")
     * @Template("CovoiturageBundle:Default:index.html.twig")
     */
    public function indexAction()
    {

        $em= $this->getDoctrine()->getManager();

        // $trajets=$em->getRepository('CovoiturageBundle:CovTrajet')->findAll();
        //  var_dump($trajets);die;

        $query = $em->createQuery(
            'SELECT p
    FROM CovoiturageBundle:CovTrajet p
    WHERE p.jour >= :jour
    ORDER BY p.jour ASC'
        )->setParameter('jour', new \DateTime("now"));

        $trajets = $query->getResult();
        if(count($trajets)<=0){
            $result='';
        }
        $i=0;
        foreach($trajets as $trajet){

            $result[$i]['id']=$trajet->getId();
            if($trajet->getVilleDepart() != ''){
                $result[$i]['ville_depart']=$trajet->getVilleDepart()->getLibelle();}
            else{
                $result[$i]['ville_depart']=$trajet->getNewDepart();
            }
            if($trajet->getVilleArrivee() != ''){
                $result[$i]['ville_arrivee']=$trajet->getVilleArrivee()->getLibelle();}
            else {
                $result[$i]['ville_Arrivee']=$trajet->getNewArrivee();
            }

            $result[$i]['date']= $trajet->getJour()->format('Y-m-d').' à '.$trajet->getHeureDepart();
            if($trajet->getNbrePlace()== 0){$result[$i]['NbrePlace']='complet';}
            else{$result[$i]['NbrePlace']=$trajet->getNbrePlace();}

            $i++;
        }

        $form = $this->get('form.factory')->create(new CovoiturageType());

        return array('name' => 'Projet Covoiturage','form' => $form->createView(), 'listetrajet'=> $result);
    }


    /**
     * @Route("/listVillePerso")
     *
     */
    public function listVillePersoAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $result = array();
        if ($request->isXmlHttpRequest()) {

            $services = $em->getRepository('CovoiturageBundle:CovEtape')->findBy(array('type'=> '1'));
            $i = 0;
            foreach ($services as $service) {
                $result[$i]['id'] = $service->getId();
                $result[$i]['libelle'] = $service->getLibelle();
                $i++;
            }


            if(count($result)>0){$output = '<option value=""> --- liste --- </option>';
                foreach ($result as $row) {
                    $output .= '<option value="' . $row['id'] . '">' . $row['libelle'] . '</option>';
                } $output .= '<option value="-1">-- AUTRE -- </option>';}
            else{
                $output = '<option value="-1">--Liste Vide--</option>';
            }

            $return = $output;
            $response = new Response();
            $data = json_encode($return);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;

        }
        return new Response('<option value="-1">Pas d\'enregistrement</option>');
    }

    /**
     * @Route("/listVillePro")
     *
     */
    public function listVilleProAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {

            $services = $em->getRepository('CovoiturageBundle:CovEtape')->findBy(array('type'=> '0'));
            $i = 0;
            foreach ($services as $service) {
                $result[$i]['id'] = $service->getId();
                $result[$i]['libelle'] = $service->getLibelle();
                $i++;
            }

            if(count($result)>0){$output = '<option value=""> --- liste --- </option>';
                foreach ($result as $row) {
                    $output .= '<option value="' . $row['id'] . '">' . $row['libelle'] . '</option>';
                } $output .= '<option value="-1">-- AUTRE -- </option>';}
            else{
                $output = '<option value="-1">--Liste Vide--</option>';
            }
            $return = $output;
            $response = new Response();
            $data = json_encode($return);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;

        }
        return new Response('Erreur');
    }

    /**
     * @Route("/listVille")
     *
     */
    public function listVilleAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {

            $services = $em->getRepository('CovoiturageBundle:CovEtape')->findAll();
            $i = 0;
            foreach ($services as $service) {
                $result[$i]['id'] = $service->getId();
                $result[$i]['libelle'] = $service->getLibelle();
                $i++;
            }

            if(count($result)>0){$output = '<option value=""> --- liste --- </option>';
                foreach ($result as $row) {
                    $output .= '<option value="' . $row['id'] . '">' . $row['libelle'] . '</option>';
                } $output .= '<option value="-1">-- AUTRE -- </option>';}
            else{
                $output = '<option value="-1">--Liste Vide--</option>';
            }

            $return = $output;
            $response = new Response();
            $data = json_encode($return);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;

        }
        return new Response('Erreur');
    }
}