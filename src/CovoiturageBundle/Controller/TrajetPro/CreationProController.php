<?php

namespace CovoiturageBundle\Controller\TrajetPro;

use CovoiturageBundle\Entity\CovTrajet;
use CovoiturageBundle\Form\CovoiturageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Zend\Stdlib\DateTime;

class CreationProController extends Controller
{
    /**
     * @Route("/creationPro", name="creation_trajet_pro")
     * @Template("CovoiturageBundle:TrajetPro:creationtrajet.html.twig")
     */
    public function indexAction()
    {
        $form = $this->get('form.factory')->create(new CovoiturageType());

        return array('name' => 'Projet Covoiturage', 'form' => $form->createView());
    }




    /**
     * @Route("/creation_success", name="creation_trajet_pro_success")
     * @Template("CovoiturageBundle:TrajetPro:creationtrajet.html.twig")
     */
    public function TraitementAction()
    {
        // var_dump($_POST);die;

        $request = $this->getRequest();
        if ($request->isMethod('POST')) {
            $this->TraitementCov($_POST);
            $this->variablePrepare();
            $this->get('session')->getFlashBag()->add('success', 'trajet crée');
            return $this->redirect($this->generateUrl('creation_trajet_pro'));
        }
        $this->get('session')->getFlashBag()->add('myError', 'erreur de sauvegarde');
        return $this->redirect($this->generateUrl('creation_trajet_pro'));

    }

    function TraitementCov($donneesPost)
    {
        //On recupere les données
        $this->data = $donneesPost;
    }

    function variablePrepare()
    {
        $em = $this->getDoctrine()->getManager();

        $trajet = new CovTrajet();
        //recuperation adresse depart
        if ($this->data['covoituragebundle_profil']['depart'] >= 1) {
            $this->adresseDepart = $em->getRepository('CovoiturageBundle:CovEtape')->findBy(array('id' => $this->data['covoituragebundle_profil']['depart']));
            $trajet->setVilleDepart($this->adresseDepart[0]);

        } else {
            $this->adresseDepart = $this->data['inputNewDepart'];
            $trajet->setNewDepart($this->adresseDepart);
        }
        //recuperation adresse arrivee
        if ($this->data['covoituragebundle_profil']['arrivee'] >= 1) {
            $this->adresseArrivee = $em->getRepository('CovoiturageBundle:CovEtape')->findBy(array('id' => $this->data['covoituragebundle_profil']['arrivee']));
            $trajet->setVilleArrivee($this->adresseArrivee[0]);

        } else {
            $this->adresseArrivee = $this->data['inputNewArrivee'];
            $trajet->setNewArrivee($this->adresseArrivee);
        }
        //recuperation date aller
        $this->dateAller = DateTime::createFromFormat('d-m-Y', $this->data['date_aller']);


        $trajet->setJour($this->dateAller);
        //recuperation heure depart et heure arrivee
        $this->debut = $this->data['debut'];
        $this->debut1 = $this->data['debut1'];

        //  var_dump($this->debut);die;

        $trajet->setHeureDepart(str_replace(' ', '', $this->debut));
        $trajet->setHeureArrivee(str_replace(' ', '', $this->debut1));

        //recuperation nombre de place disponible
        $this->placedispo = $this->data['demo_vertical2'];
        $trajet->setNbrePlace($this->placedispo);
        //option autoroute et peages
        if (isset($this->data['covoituragebundle_profil']['autoroutes'])) {
            $this->autoroutes = $this->data['covoituragebundle_profil']['autoroutes'];
            $trajet->setOptionAutoroute($this->autoroutes);

        } else {
            $this->autoroutes = 0;
            $trajet->setOptionAutoroute($this->autoroutes);

        }
        if (isset($this->data['covoituragebundle_profil']['peages'])) {
            $this->peages = $this->data['covoituragebundle_profil']['peages'];
            $trajet->setOptionPeages($this->peages);

        } else {
            $this->peages = 0;
            $trajet->setOptionPeages($this->peages);

        }
        //recuperation commentaire
        if (isset($this->data['covoituragebundle_profil']['Comment'])) {
            $this->comment = $this->data['covoituragebundle_profil']['Comment'];
            $trajet->setCommentaire($this->comment);
        } else {
            $this->comment = '';
            $trajet->setCommentaire($this->comment);
        }

//var_dump($this->get('security.context')->getToken()->getUser());die;
        //$this->conducteur = $em->getRepository('UserUserBundle:User')->findBy(array('id' => $this->get('security.context')->getToken()->getUser()->getId()));
        $trajet->setConducteur($this->get('security.context')->getToken()->getUser());
        $em->persist($trajet);
        $em->flush();
        //recuperation date retour
        if (isset($this->data['date_retour'])) {
            if ($this->data['date_retour'] != '') {
                $this->dateRetour = DateTime::createFromFormat('d-m-Y', $this->data['date_retour']);;
                //recuperation heure depart et heure arrivee
                $this->debut2 = $this->data['debut2'];
                $this->debut3=$this->data['debut3'];
                $trajet2 = new CovTrajet();
                if ($trajet->getVilleDepart() != '') {
                    $trajet2->setVilleArrivee($trajet->getVilleDepart());
                } else {
                    $trajet2->setNewArrivee(($trajet->getNewDepart()));
                }
                if ($trajet->getVilleArrivee() != '') {
                    $trajet2->setVilleDepart($trajet->getVilleArrivee());
                } else {
                    $trajet2->setNewDepart(($trajet->getNewArrivee()));
                }
                $trajet2->setHeureDepart(str_replace(' ', '', $this->debut2));
                $trajet2->setHeureArrivee(str_replace(' ', '', $this->debut3));
                $trajet2->setNbrePlace($trajet->getNbrePlace());
                $trajet2->setOptionAutoroute($trajet->getOptionAutoroute());
                $trajet2->setCommentaire($trajet->getCommentaire());
                $trajet2->setJour($this->dateRetour);
                $trajet2->setConducteur($trajet->getConducteur());
                $em->persist($trajet2);
                $em->flush();
                $data2['depart']=$trajet2->getVilleArrivee()->getLibelle();
                $data2['arrivee']=$trajet2->getVilleArrivee()->getLibelle();
                $data2['heure']=$this->data['date_retour'].$this->debut2;
                $this->sendMail($data2);
            }
        }

        if (isset($this->data['date_retour'])) {
            if ($this->data['date_retour'] != '') {
                $trajet->setTrajetSuivant($trajet2);
                $em->persist($trajet);
                $em->flush();
            }
        }
        $data['depart']=$this->adresseDepart[0]->getLibelle();
        $data['arrivee']=$this->adresseArrivee[0]->getLibelle();
        $data['heure']=$this->data['date_aller'].$this->debut;
        $this->sendMail($data);

    }

//envoyer un mail de confirmation

    public function sendMail($data)
    {

        $this->data['senderMail'] = ucfirst($this->get('security.context')->getToken()->getUser()->getEmail());
        $message = \Swift_Message::newInstance()
            ->setSubject("[Covoiturage]: création d'un nouveau trajet")
            ->setFrom('covoiturage@noz.fr')
            ->setTo($this->data['senderMail'])
            ->setBody(
                $this->renderView(
                    'CovoiturageBundle:Mail:creation.html.twig', array('depart' => $data['depart'], 'arrivee' => $data['arrivee'], 'heure' => $data['heure'])
                ),
                'text/html'
            );
        $this->get('mailer')->send($message);
        return 'send mails';
    }
}