<?php

namespace CovoiturageBundle\Controller\TrajetPerso;

use CovoiturageBundle\Form\CovoiturageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class ListePersoController extends Controller
{
    /**
     * @Route("/homePerso", name="liste_trajet_perso")
     * @Template("CovoiturageBundle:TrajetPerso:listetrajet.html.twig")
     */
    public function indexAction()
    {

        $em = $this->getDoctrine()->getManager();

        //  var_dump($trajets);die;

        $query = $em->createQuery(
            'SELECT p
    FROM CovoiturageBundle:CovTrajet p
    WHERE p.jour >= :jour and p.type = 1
    ORDER BY p.jour ASC'
        )->setParameter('jour', new \DateTime("now"));

        $trajets = $query->getResult();
        if (count($trajets) <= 0) {
            $result = '';
        }
        $i = 0;
        foreach ($trajets as $trajet) {
            $result[$i]['id'] = $trajet->getId();
            if ($trajet->getVilleDepart() != '') {
                $result[$i]['ville_depart'] = $trajet->getVilleDepart()->getLibelle();
            } else {
                $result[$i]['ville_depart'] = $trajet->getNewDepart();
            }
            if ($trajet->getVilleArrivee() != '') {
                $result[$i]['ville_arrivee'] = $trajet->getVilleArrivee()->getLibelle();
            } else {
                $result[$i]['ville_Arrivee'] = $trajet->getNewArrivee();
            }

            $result[$i]['date'] = $trajet->getJour()->format('Y-m-d') . ' à ' . $trajet->getHeureDepart();
            if ($trajet->getNbrePlace() == 0) {
                $result[$i]['NbrePlace'] = 'complet';
            } else {
                $result[$i]['NbrePlace'] = $trajet->getNbrePlace();
            }

            $i++;
        }

        $form = $this->get('form.factory')->create(new CovoiturageType());

        return array('name' => 'Projet Covoiturage', 'form' => $form->createView(), 'listetrajet' => $result);
    }
}