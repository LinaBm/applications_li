<?php

namespace CovoiturageBundle\Controller\TrajetPerso;

use CovoiturageBundle\Form\CovoiturageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class FutursPersoController extends Controller
{
    /**
     * @Route("/ProfilPerso", name="futur_trajet_perso")
     * @Template("CovoiturageBundle:TrajetPerso:profilPersonnel.html.twig")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        //conducteur
        $conducteur = $em->getRepository('CovoiturageBundle:CovTrajet')->findBy(array('conducteur' => $this->get('security.context')->getToken()->getUser(), 'type' => 1));
        $i = 0;
        $result='';
        foreach ($conducteur as $trajet) {
            if ($trajet->getJour() >= new \DateTime('now')) {
                $result[$i]['id'] = $trajet->getId();
                if ($trajet->getVilleDepart() != '') {
                    $result[$i]['ville_depart'] = $trajet->getVilleDepart()->getLibelle();
                } else {
                    $result[$i]['ville_depart'] = $trajet->getNewDepart();
                }
                if ($trajet->getVilleArrivee() != '') {
                    $result[$i]['ville_arrivee'] = $trajet->getVilleArrivee()->getLibelle();
                } else {
                    $result[$i]['ville_Arrivee'] = $trajet->getNewArrivee();
                }

                $result[$i]['date'] = $trajet->getJour()->format('Y-m-d') . ' à ' . $trajet->getHeureDepart();
                if ($trajet->getNbrePlace() == 0) {
                    $result[$i]['NbrePlace'] = 'complet';
                } else {
                    $result[$i]['NbrePlace'] = $trajet->getNbrePlace();
                }

                $i++;
            } else {
                $result = '';
            }
        }
        //passager
        $passagers = $em->getRepository('CovoiturageBundle:CovReservation')->findBy(array('passager' => $this->get('security.context')->getToken()->getUser()));
        $i = 0;
        $resulte='';
        foreach ($passagers as $passager) {
            $trajet = $passager->getTrajet();
            if ($trajet->getJour() >= new \DateTime('now') && $trajet->getType() == '1') {
                $resulte[$i]['id'] = $trajet->getId();
                if ($trajet->getVilleDepart() != '') {
                    $resulte[$i]['ville_depart'] = $trajet->getVilleDepart()->getLibelle();
                } else {
                    $resulte[$i]['ville_depart'] = $trajet->getNewDepart();
                }
                if ($trajet->getVilleArrivee() != '') {
                    $resulte[$i]['ville_arrivee'] = $trajet->getVilleArrivee()->getLibelle();
                } else {
                    $resulte[$i]['ville_Arrivee'] = $trajet->getNewArrivee();
                }

                $resulte[$i]['date'] = $trajet->getJour()->format('Y-m-d') . ' à ' . $trajet->getHeureDepart();
                if ($trajet->getNbrePlace() == 0) {
                    $resulte[$i]['NbrePlace'] = 'complet';
                } else {
                    $resulte[$i]['NbrePlace'] = $trajet->getNbrePlace();
                }

                $i++;
            } else {
                $resulte = '';
            }
        }

        $form = $this->get('form.factory')->create(new CovoiturageType());

        return array('name' => 'Projet Covoiturage', 'form' => $form->createView(), 'conducteur' => $result, 'passager' => $resulte);
    }


    /**
     * @Route("/profilPerso/remove/{id}", name="remove_reservation_perso")
     * @Template()
     */
    public function removeAction($id)
    {
        $request = $this->getRequest();

        $em = $this->getDoctrine()->getManager();
        $trajet = $em->getRepository('CovoiturageBundle:CovTrajet')->findBy(array('id' => $id));
        $data['depart'] = $trajet[0]->getVilleDepart()->getLibelle();
        $data['arrivee'] = $trajet[0]->getVilleArrivee()->getLibelle();
        $data['heure'] = $trajet[0]->getJour()->format('Y-m-d') . ' à ' . $trajet[0]->getHeureDepart();
        $reservation = $em->getRepository('CovoiturageBundle:CovReservation')->findBy(array('passager' => $this->get('security.context')->getToken()->getUser(), 'trajet' => $trajet));

        if (count($reservation) > 0) {
            $trajet[0]->setNbrePlace($trajet[0]->getNbrePlace() + 1);
            $em->remove($reservation[0]);
            $em->persist($trajet[0]);
            $em->flush();
            $this->sendMailResevation($data);
            $request->getSession()->getFlashBag()->add(
                'success',
                'Réservation Annulée!'
            );
        } else {
            $request->getSession()->getFlashBag()->add(
                'myError',
                'Vous ne pouvez pas Annuler cette réservation!'
            );
        }

        return $this->redirect($this->generateUrl('futur_trajet_perso'));
    }

    //envoyer un mail pour annuler la réservation

    public function sendMailResevation($data)
    {

        $this->data['senderMail'] = ucfirst($this->get('security.context')->getToken()->getUser()->getEmail());
        $message = \Swift_Message::newInstance()
            ->setSubject("[Covoiturage]: Annuler la réservation")
            ->setFrom('covoiturage@noz.fr')
            ->setTo($this->data['senderMail'])
            ->setBody(
                $this->renderView(
                    'CovoiturageBundle:Mail:creation.html.twig', array('depart' => $data['depart'], 'arrivee' => $data['arrivee'], 'heure' => $data['heure'])
                ),
                'text/html'
            );
        $this->get('mailer')->send($message);
        return 'send mails';
    }


    /**
     * @Route("/profilPerso/remove_trajet/{id}", name="remove_trajet_perso")
     * @Template()
     */
    public function removeTrajetAction($id)
    {
        $request = $this->getRequest();

        $em = $this->getDoctrine()->getManager();
        $trajet = $em->getRepository('CovoiturageBundle:CovTrajet')->findBy(array('id' => $id));

        $data['depart'] = $trajet[0]->getVilleDepart()->getLibelle();
        $data['arrivee'] = $trajet[0]->getVilleArrivee()->getLibelle();
        $data['heure'] = $trajet[0]->getJour()->format('Y-m-d') . ' à ' . $trajet[0]->getHeureDepart();

        $suivant = $em->getRepository('CovoiturageBundle:CovTrajet')->findBy(array('trajetSuivant' => $trajet[0]));
        if (count($suivant) > 0) {
            $suivant[0]->setTrajetSuivant(null);
            $em->persist($suivant[0]);
            $em->flush();
        }
        if (count($trajet[0]->getTrajetSuivant()) > 0) {
            $trajet[0]->setTrajetSuivant(null);
            $em->persist($trajet[0]);
            $em->flush();
        }
        $reservations = $em->getRepository('CovoiturageBundle:CovReservation')->findBy(array('passager' => $this->get('security.context')->getToken()->getUser(), 'trajet' => $trajet));

        if (count($trajet) > 0) {
            foreach ($reservations as $reservation) {
                $em->remove($reservation);
                $em->flush();
                $data2['depart'] = $reservation->getTrajet()->getVilleDepart()->getLibelle();
                $data2['arrivee'] = $reservation->getTrajet()->getVilleArrivee()->getLibelle();
                $data2['heure'] = $reservation->getTrajet()->getJour()->format('Y-m-d') . ' à ' . $reservation->getTrajet()->getHeureDepart();
                $this->sendMailResevation($data2);
            }

            $em->remove($trajet[0]);
            $em->flush();
            $this->sendMailRemove($data);
            $request->getSession()->getFlashBag()->add(
                'success',
                'Trajet Supprimé!'
            );
        } else {
            $request->getSession()->getFlashBag()->add(
                'myError',
                'Vous ne pouvez pas supprimer ce trajet!'
            );
        }

        return $this->redirect($this->generateUrl('futur_trajet_perso'));
    }

    //envoyer un mail d'annulation

    public function sendMailRemove($data)
    {

        $this->data['senderMail'] = ucfirst($this->get('security.context')->getToken()->getUser()->getEmail());
        $message = \Swift_Message::newInstance()
            ->setSubject("[Covoiturage]: Suprimer un trajet")
            ->setFrom('covoiturage@noz.fr')
            ->setTo($this->data['senderMail'])
            ->setBody(
                $this->renderView(
                    'CovoiturageBundle:Mail:creation.html.twig', array('depart' => $data['depart'], 'arrivee' => $data['arrivee'], 'heure' => $data['heure'])
                ),
                'text/html'
            );
        $this->get('mailer')->send($message);
        return 'send mails';
    }

}