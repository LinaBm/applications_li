<?php

namespace CovoiturageBundle\Controller\TrajetPerso;

use CovoiturageBundle\Entity\CovReservation;
use CovoiturageBundle\Form\CovoiturageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;


class DetailsPersoController extends Controller
{
    /**
     * @Route("/detailsPerso/{id}", name="details_trajet_perso")
     * @Template("CovoiturageBundle:TrajetPerso:detailstrajet.html.twig")
     */
    public function indexAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $trajet=$em->getRepository('CovoiturageBundle:CovTrajet')->findBy(array('id'=>$id));
        $result['id']=$trajet[0]->getId();
        if($trajet[0]->getVilleDepart() != ''){
            $result['ville_depart']=$trajet[0]->getVilleDepart()->getLibelle();
            $result['villeDepart']=$trajet[0]->getVilleDepart()->getLibelle().' '.$trajet[0]->getVilleDepart()->getNumero().' '.$trajet[0]->getVilleDepart()->getRue().' '.$trajet[0]->getVilleDepart()->getComplement().' '.$trajet[0]->getVilleDepart()->getVille()->getLibelle().' '.$trajet[0]->getVilleDepart()->getVille()->getCp();
        }
        else{
            $result['ville_depart']=$trajet[0]->getNewDepart();
            $result['villeDepart']=$trajet[0]->getNewDepart();
        }
        if($trajet[0]->getVilleArrivee() != ''){
            $result['ville_arrivee']=$trajet[0]->getVilleArrivee()->getLibelle();
            $result['villeArrivee']=$trajet[0]->getVilleArrivee()->getLibelle().' '.$trajet[0]->getVilleArrivee()->getNumero().' '.$trajet[0]->getVilleArrivee()->getRue().' '.$trajet[0]->getVilleArrivee()->getComplement().' '.$trajet[0]->getVilleArrivee()->getVille()->getLibelle().' '.$trajet[0]->getVilleArrivee()->getVille()->getCp();}
        else {
            $result['ville_Arrivee']=$trajet[0]->getNewArrivee();
            $result['villeArrivee']=$trajet[0]->getNewArrivee();
        }

        $result['date']= $trajet[0]->getJour()->format('Y-m-d').' à '.$trajet[0]->getHeureDepart();
        $result['NbrePlace']=$trajet[0]->getNbrePlace();
        $result['heureDepart']=$trajet[0]->getHeureDepart();
        $result['heureArrivee']=$trajet[0]->getHeureArrivee();
        $result['commentaire']=$trajet[0]->getCommentaire();

        if($trajet[0]->getTrajetSuivant()!= ''){
            $result['retour']=$trajet[0]->getTrajetSuivant()->getId();
        }else{
            $result['retour']='';
        }

        $form = $this->get('form.factory')->create(new CovoiturageType());

        return array('name' => 'Projet Covoiturage','form' => $form->createView(),'result'=>$result);
    }


    /**
     * @Route("/reservation_success", name="reservation_trajet_success")
     * @Template("CovoiturageBundle:TrajetPerso:DetailsPersotrajet.html.twig")
     */
    public function TraitementAction(){
        $request = $this->getRequest();
        $em=$this->getDoctrine()->getManager();
        if ($request->isMethod('POST')) {
            $trajet_id= $em->getRepository('CovoiturageBundle:CovTrajet')->findBy(array('id'=>$_POST['id_trajet']));
            $passager_id=$this->get('security.context')->getToken()->getUser();
            if($trajet_id[0]->getNbrePlace()-1 <0){
                $this->get('session')->getFlashBag()->add('myError', 'Il est trop tard pour réserver sur ce trajet.');
                return $this->redirect($this->generateUrl('liste_trajet_perso'));
            }
            $trajet_id[0]->setNbrePlace($trajet_id[0]->getNbrePlace()-1);
            $reservation= new CovReservation();
            $reservation->setPassager($passager_id);
            $reservation->setTrajet($trajet_id[0]);
            $em->persist($reservation);
            $em->persist($trajet_id[0]);
            $em->flush();
            $data['depart']=$trajet_id[0]->getVilleArrivee()->getLibelle();
            $data['arrivee']=$trajet_id[0]->getVilleArrivee()->getLibelle();
            $data['heure']=$trajet_id[0]->getJour()->format('Y-m-d').' à '.$trajet_id[0]->getHeureDepart();
            $this->sendMailResevation($data);
            $this->get('session')->getFlashBag()->add('success', 'trajet reservée');
            return $this->redirect($this->generateUrl('liste_trajet_perso'));
        }
        $this->get('session')->getFlashBag()->add('myError', 'erreur de sauvegarde');
        return $this->redirect($this->generateUrl('liste_trajet_perso'));

    }
    //envoyer un mail pour confirmer la réservation

    public function sendMailResevation($data)
    {

        $this->data['senderMail'] = ucfirst($this->get('security.context')->getToken()->getUser()->getEmail());
        $message = \Swift_Message::newInstance()
            ->setSubject("[Covoiturage]: Confirmation de réservation")
            ->setFrom('covoiturage@noz.fr')
            ->setTo($this->data['senderMail'])
            ->setBody(
                $this->renderView(
                    'CovoiturageBundle:Mail:creation.html.twig', array('depart' => $data['depart'], 'arrivee' => $data['arrivee'], 'heure' => $data['heure'])
                ),
                'text/html'
            );
        $this->get('mailer')->send($message);
        return 'send mails';
    }


    /**
     * @Route("/getAdressePerso", name="get_address_perso")
     *
     */
    public function getAdresseAction()
    {        $em=$this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $return = array();
        if ($request->isXmlHttpRequest()) {
            $idPack = $request->get('pack');
            $trajet=$em->getRepository('CovoiturageBundle:CovTrajet')->findBy(array('id'=>$idPack));
            if($trajet[0]->getVilleDepart() != ''){
                $result[0]=$trajet[0]->getVilleDepart()->getLibelle().' '.$trajet[0]->getVilleDepart()->getNumero().' '.$trajet[0]->getVilleDepart()->getRue().' '.$trajet[0]->getVilleDepart()->getComplement().' '.$trajet[0]->getVilleDepart()->getVille()->getLibelle().' '.$trajet[0]->getVilleDepart()->getVille()->getCp();
            }
            else{
                $result[0]=$trajet[0]->getNewDepart();
            }
            if($trajet[0]->getVilleArrivee() != ''){
                $result[1]=$trajet[0]->getVilleArrivee()->getLibelle().' '.$trajet[0]->getVilleArrivee()->getNumero().' '.$trajet[0]->getVilleArrivee()->getRue().' '.$trajet[0]->getVilleArrivee()->getComplement().' '.$trajet[0]->getVilleArrivee()->getVille()->getLibelle().' '.$trajet[0]->getVilleArrivee()->getVille()->getCp();}
            else {
                $result[1]=$trajet[0]->getNewArrivee();
            }
           /* $result[0]='new york';
            $result[1]='chicago';*/
            $result[2]=$trajet[0]->getHeureDepart();
            if($trajet[0]->getOptionAutoroute()){
                $result[3]=true;
            }else{
                $result[3]=false;
            }
            if($trajet[0]->getOptionPeages()){
                $result[4]=true;
            }else{
                $result[4]=false;
            }
            $return = $result;
            $response = new Response();
            $data = json_encode($return);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;
        }
        return new Response('Erreur');
    }
}