<?php

namespace CovoiturageBundle\Controller\TrajetPerso;

use CovoiturageBundle\Entity\CovTrajet;
use CovoiturageBundle\Form\CovoiturageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Zend\Stdlib\DateTime;

class CreationPersoController extends Controller
{
    /**
     * @Route("/creationPerso", name="creation_trajet_perso")
     * @Template("CovoiturageBundle:TrajetPerso:creationtrajet.html.twig")
     */
    public function indexAction()
    {
        $form = $this->get('form.factory')->create(new CovoiturageType());

        return array('name' => 'Projet Covoiturage', 'form' => $form->createView());
    }

    /**
     * @Route("/creation_success_perso", name="creation_trajet_perso_success")
     * @Template("CovoiturageBundle:TrajetPerso:creationtrajet.html.twig")
     */
    public function TraitementAction()
    {
        $request = $this->getRequest();
        if ($request->isMethod('POST')) {
            $this->TraitementCov($_POST);
            $this->variablePrepare();
            $this->get('session')->getFlashBag()->add('success', 'trajet crée');
            return $this->redirect($this->generateUrl('creation_trajet_perso'));
        }
        $this->get('session')->getFlashBag()->add('myError', 'erreur de sauvegarde');
        return $this->redirect($this->generateUrl('creation_trajet_perso'));

    }

    function TraitementCov($donneesPost)
    {
        //On recupere les données
        $this->data = $donneesPost;
    }

    function variablePrepare()
    {
        $em = $this->getDoctrine()->getManager();

        $trajet = new CovTrajet();
        //recuperation adresse depart
        if ($this->data['covoituragebundle_profil']['depart'] >= 1) {
            $this->adresseDepart = $em->getRepository('CovoiturageBundle:CovEtape')->findBy(array('id' => $this->data['covoituragebundle_profil']['depart']));
            $trajet->setVilleDepart($this->adresseDepart[0]);

        } else {
            $this->adresseDepart = $this->data['inputNewDepart'];
            $trajet->setNewDepart($this->adresseDepart);
        }
        //recuperation adresse arrivee
        if ($this->data['covoituragebundle_profil']['arrivee'] >= 1) {
            $this->adresseArrivee = $em->getRepository('CovoiturageBundle:CovEtape')->findBy(array('id' => $this->data['covoituragebundle_profil']['arrivee']));
            $trajet->setVilleArrivee($this->adresseArrivee[0]);

        } else {
            $this->adresseArrivee = $this->data['inputNewArrivee'];
            $trajet->setNewArrivee($this->adresseArrivee);
        }
        //recuperation date aller
        $this->dateAller = DateTime::createFromFormat('d-m-Y', $this->data['date_aller']);


        $trajet->setJour($this->dateAller);
        //recuperation heure depart et heure arrivee
        $this->debut = $this->data['debut'];
        //  var_dump($this->debut);die;

        $trajet->setHeureDepart(str_replace(' ', '', $this->debut));
        $trajet->setHeureArrivee('non précis');

        //recuperation nombre de place disponible
        $this->placedispo = $this->data['demo_vertical2'];
        $trajet->setNbrePlace($this->placedispo);
        //option autoroute et peages
        if (isset($this->data['covoituragebundle_profil']['autoroutes'])) {
            $this->autoroutes = $this->data['covoituragebundle_profil']['autoroutes'];
            $trajet->setOptionAutoroute($this->autoroutes);

        } else {
            $this->autoroutes = 0;
            $trajet->setOptionAutoroute($this->autoroutes);

        }
        if (isset($this->data['covoituragebundle_profil']['peages'])) {
            $this->peages = $this->data['covoituragebundle_profil']['peages'];
            $trajet->setOptionPeages($this->peages);

        } else {
            $this->peages = 0;
            $trajet->setOptionPeages($this->peages);

        }
        //recuperation commentaire
        if (isset($this->data['covoituragebundle_profil']['Comment'])) {
            $this->comment = $this->data['covoituragebundle_profil']['Comment'];
            $trajet->setCommentaire($this->comment);
        } else {
            $this->comment = '';
            $trajet->setCommentaire($this->comment);
        }
        $trajet->setType(1);
//var_dump($this->get('security.context')->getToken()->getUser());die;
        //$this->conducteur = $em->getRepository('UserUserBundle:User')->findBy(array('id' => $this->get('security.context')->getToken()->getUser()->getId()));
        $trajet->setConducteur($this->get('security.context')->getToken()->getUser());
        $em->persist($trajet);
        $em->flush();
        //recuperation date retour
        if (isset($this->data['date_retour'])) {
            if ($this->data['date_retour'] != '') {
                $this->dateRetour = DateTime::createFromFormat('d-m-Y', $this->data['date_retour']);;
                //recuperation heure depart et heure arrivee
                $this->debut2 = $this->data['debut2'];
                $trajet2 = new CovTrajet();
                if ($trajet->getVilleDepart() != '') {
                    $trajet2->setVilleArrivee($trajet->getVilleDepart());
                } else {
                    $trajet2->setNewArrivee(($trajet->getNewDepart()));
                }
                if ($trajet->getVilleArrivee() != '') {
                    $trajet2->setVilleDepart($trajet->getVilleArrivee());
                } else {
                    $trajet2->setNewDepart(($trajet->getNewArrivee()));
                }
                $trajet2->setHeureDepart(str_replace(' ', '', $this->debut2));
                $trajet2->setHeureArrivee('non précis');
                $trajet2->setNbrePlace($trajet->getNbrePlace());
                $trajet2->setOptionAutoroute($trajet->getOptionAutoroute());
                $trajet2->setOptionPeages($trajet->getOptionPeages());
                $trajet2->setCommentaire($trajet->getCommentaire());
                $trajet2->setJour($this->dateRetour);
                $trajet2->setConducteur($trajet->getConducteur());
                $trajet2->setType(1);
                $em->persist($trajet2);
                $em->flush();
                $data2['depart'] = $trajet2->getVilleDepart()->getLibelle();
                $data2['arrivee'] = $trajet2->getVilleArrivee()->getLibelle();
                $data2['heure'] = $trajet2->getJour()->format('Y-m-d') . ' à ' . $trajet2->getHeureDepart();
                $this->sendMail($data2);
            }
        }

        if (isset($this->data['date_retour'])) {
            if ($this->data['date_retour'] != '') {
                $trajet->setTrajetSuivant($trajet2);
                $em->persist($trajet);
                $em->flush();
            }
        }

        $data['depart'] = $trajet->getVilleDepart()->getLibelle();
        $data['arrivee'] = $trajet->getVilleArrivee()->getLibelle();
        $data['heure'] = $trajet->getJour()->format('Y-m-d') . ' à ' . $trajet->getHeureDepart();
         $this->sendMail($data);

    }
//envoyer un mail de confirmation

    public function sendMail($data)
    {

        $this->data['senderMail'] = ucfirst($this->get('security.context')->getToken()->getUser()->getEmail());
        $message = \Swift_Message::newInstance()
            ->setSubject("[Covoiturage]: création d'un nouveau trajet")
            ->setFrom('covoiturage@noz.fr')
            ->setTo($this->data['senderMail'])
            ->setBody(
                $this->renderView(
                    'CovoiturageBundle:Mail:creation.html.twig', array('depart' => $data['depart'], 'arrivee' => $data['arrivee'], 'heure' => $data['heure'])
                ),
                'text/html'
            );
        $this->get('mailer')->send($message);
       return 'send mails';
    }

}