function displayPreviousDate() {
    var currentDate = $('#dateDebut').val();
    var tab = [];
    var ch = '';
    $.ajax({
        dataType: "json",
        type: "POST",
        url: "previousDate",
        data: {currentDate: currentDate},
        async: false,
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            $('#hierarchie').empty();
            if (result != '') {
                for (var y = 0; y < result.length; y++) {
                    ch = ch + '<div class="box box-info"><div class="box-header with-border"><h3 class="box-title">' + result[y][0]['manager'] + '</h3> <a type="button" id="relancer" onclick="sendMailRelancer(' + result[y][0]['idManager'] + ')">Relancer</a><div class="box-tools pull-right"> <button type="button" class="btn btn-box-tool" data-widget="collapse"> <i class="fa fa-minus"></i></button></div></div><div class="box-body"> <div class="row"><div class="col-md-12"><table id="' + result[y][0]['idManager'] + '" class="display table table-striped table-bordered table-hover table-responsive" style="padding-left: 2px; width: 100%"><thead> <tr> <th></th> <th>Matricule Salarie</th> <th>Societe</th> <th>Detachement</th><th>Date Debut</th> <th>Date Fin</th> <th>Commentaire RH</th> </tr> </thead> <tbody>';
                    //   tab.push('<div class="box box-default"><div class="box-header with-border"><h3 class="box-title">'+  result[y][0]['manager']+ '</h3> <div class="box-tools pull-right"> <button type="button" class="btn btn-box-tool" data-widget="collapse"> <i class="fa fa-minus"></i></button></div></div><div class="box-body"> <div class="row"><div class="col-md-12"><table class="table table-striped table-bordered table-hover table-responsive" style="padding-left: 2px; width: 100%"><thead> <tr> <th>Matricule Salarie</th> <th>Societe</th> <th>Date Debut</th> <th>Date Fin</th> <th>Commentaire RH</th> </tr> </thead> <tbody>');
                    for (var i = 0; i < result[y].length; i++) {
                        //   tab.push('<tr> <td>'+result[y][i]['nom'] +'</td> <td>'+ result[y][i]['societe'] +'</td> <td >'+ result[y][i]['debut'] +' <input id="dateDebut" value='+ result[y][i]['debut'] +' hidden ></td> <td>'+ result[y][i]['fin'] +'</td> <td>'+ result[y][i]['com_rh'] +'</td> </tr>');
                        ch = ch + '<tr> <td></td> <td>' + result[y][i]['nom'] + '</td>  <td>'+result[y][i]['societe']+' </td><td>';
                        if(result[y][i]['detachement'].length >0){
                            ch=ch+'<div class="col-md-4"><B>Detachement:</B></div> <div class="col-md-6">';
                            for(var x=0; x< result[y][i]['detachement'].length;x++){
                                ch=ch+'<div class="text-muted" style="text-indent:20px;"><B>'+result[y][i]['detachement'][x]['societe_id']+': </B>'+ result[y][i]['detachement'][x]['nb_hours']+' heures travaillées </div>';
                            }}
                        else{
                            ch=ch+'<div class="text-center"><B>Pas de detachement.</B>'
                        }

                        ch=ch+'</div></td><td >' + result[y][i]['debut'] + ' <input id="dateDebut" value=' + result[y][i]['debut'] + ' hidden ></td> <td>' + result[y][i]['fin'] + '</td> <td>' + result[y][i]['com_rh'] + '</td> </tr>';
                    }
                    //  tab.push('</tbody> </table> </div> <!-- /.row --> </div> <!-- /.box-body --> </div> <!-- /.box --> </div>');
                    ch = ch + '</tbody> </table> </div> <!-- /.row --> </div> <!-- /.box-body --> </div> <!-- /.box --> </div>';
                }
            }
            else {
                //  tab.push('<h3 style="text-align: center">Pas d\'enregistrement</h3></div>');
                ch = ch + '<h3 style="text-align: center">Pas d\'enregistrement</h3></div>';
            }

            $('#hierarchie').append(ch);
            displayDatatble();

        }
    })
};
function displayNextDate() {
    var currentDate = $('#dateDebut').val();

    var tab = [];
    var ch = '';
    $.ajax({
        dataType: "json",
        type: "POST",
        url: "nextDate",
        data: {currentDate: currentDate},
        async: false,
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            $('#hierarchie').empty();
            if (result != '') {
                for (var y = 0; y < result.length; y++) {
                    ch = ch + '<div class="box box-info"><div class="box-header with-border"><h3 class="box-title">' + result[y][0]['manager'] + '</h3> <a type="button" id="relancer" onclick="sendMailRelancer(' + result[y][0]['idManager'] + ')">Relancer</a><div class="box-tools pull-right"> <button type="button" class="btn btn-box-tool" data-widget="collapse"> <i class="fa fa-minus"></i></button></div></div><div class="box-body"> <div class="row"><div class="col-md-12"><table id="' + result[y][0]['idManager'] + '" class="display table table-striped table-bordered table-hover table-responsive" style="padding-left: 2px; width: 100%"><thead> <tr> <th></th> <th>Matricule Salarie</th> <th>Societe</th><th>Detachement</th> <th>Date Debut</th> <th>Date Fin</th> <th>Commentaire RH</th> </tr> </thead> <tbody>';
                    //   tab.push('<div class="box box-default"><div class="box-header with-border"><h3 class="box-title">'+  result[y][0]['manager']+ '</h3> <div class="box-tools pull-right"> <button type="button" class="btn btn-box-tool" data-widget="collapse"> <i class="fa fa-minus"></i></button></div></div><div class="box-body"> <div class="row"><div class="col-md-12"><table class="table table-striped table-bordered table-hover table-responsive" style="padding-left: 2px; width: 100%"><thead> <tr> <th>Matricule Salarie</th> <th>Societe</th> <th>Date Debut</th> <th>Date Fin</th> <th>Commentaire RH</th> </tr> </thead> <tbody>');
                    for (var i = 0; i < result[y].length; i++) {
                        //   tab.push('<tr> <td>'+result[y][i]['nom'] +'</td> <td>'+ result[y][i]['societe'] +'</td> <td >'+ result[y][i]['debut'] +' <input id="dateDebut" value='+ result[y][i]['debut'] +' hidden ></td> <td>'+ result[y][i]['fin'] +'</td> <td>'+ result[y][i]['com_rh'] +'</td> </tr>');
                        ch = ch + '<tr><td></td>  <td>' + result[y][i]['nom'] + '</td>  <td>'+result[y][i]['societe']+' </td><td>';
                        if(result[y][i]['detachement'].length >0){
                            ch=ch+'<div class="col-md-4"><B>Detachement:</B></div> <div class="col-md-6">';
                            for(var x=0; x< result[y][i]['detachement'].length;x++){
                                ch=ch+'<div class="text-muted" style="text-indent:20px;"><B>'+result[y][i]['detachement'][x]['societe_id']+': </B>'+ result[y][i]['detachement'][x]['nb_hours']+' heures travaillées </div>';
                            }}
                        else{
                            ch=ch+'<div class="text-center"><B>Pas de detachement.</B>'
                        }

                        ch=ch+'</div></td><td >' + result[y][i]['debut'] + ' <input id="dateDebut" value=' + result[y][i]['debut'] + ' hidden ></td> <td>' + result[y][i]['fin'] + '</td> <td>' + result[y][i]['com_rh'] + '</td> </tr>';
                    }
                    //  tab.push('</tbody> </table> </div> <!-- /.row --> </div> <!-- /.box-body --> </div> <!-- /.box --> </div>');
                    ch = ch + '</tbody> </table> </div> <!-- /.row --> </div> <!-- /.box-body --> </div> <!-- /.box --> </div>';
                }
            }
            else {
                //  tab.push('<h3 style="text-align: center">Pas d\'enregistrement</h3></div>');
                ch = ch + '<h3 style="text-align: center">Pas d\'enregistrement</h3></div>';
            }

            $('#hierarchie').append(ch);
            displayDatatble();

        }
    })
};
function displayDate(){
    var ch = '';
    $.ajax({
        dataType: "json",
        type: "POST",
        url: "currentDate",
        async: false,
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            $('#hierarchie').empty();
            if (result != '') {
                for (var y = 0; y < result.length; y++) {
                    ch = ch + '<div class="box box-warning"><div class="box-header with-border"><h3 class="box-title">' + result[y][0]['manager'] + '</h3> <a type="button" id="relancer" onclick="sendMailRelancer(' + result[y][0]['idManager'] + ')">Relancer</a><div class="box-tools pull-right"> <button type="button" class="btn btn-box-tool" data-widget="collapse"> <i class="fa fa-minus"></i></button></div></div><div class="box-body"> <div class="row"><div class="col-md-12"><table id="' + result[y][0]['idManager'] + '" class="display table table-striped table-bordered table-hover table-responsive" style="padding-left: 2px; width: 100%"><thead> <tr><th></th> <th>Matricule Salarie</th> <th>Societe</th> <th>Detachement</th><th>Date Debut</th> <th>Date Fin</th> <th>Commentaire RH</th> </tr> </thead> <tbody>';
                    //   tab.push('<div class="box box-default"><div class="box-header with-border"><h3 class="box-title">'+  result[y][0]['manager']+ '</h3> <div class="box-tools pull-right"> <button type="button" class="btn btn-box-tool" data-widget="collapse"> <i class="fa fa-minus"></i></button></div></div><div class="box-body"> <div class="row"><div class="col-md-12"><table class="table table-striped table-bordered table-hover table-responsive" style="padding-left: 2px; width: 100%"><thead> <tr> <th>Matricule Salarie</th> <th>Societe</th> <th>Date Debut</th> <th>Date Fin</th> <th>Commentaire RH</th> </tr> </thead> <tbody>');
                    for (var i = 0; i < result[y].length; i++) {
                        //   tab.push('<tr> <td>'+result[y][i]['nom'] +'</td> <td>'+ result[y][i]['societe'] +'</td> <td >'+ result[y][i]['debut'] +' <input id="dateDebut" value='+ result[y][i]['debut'] +' hidden ></td> <td>'+ result[y][i]['fin'] +'</td> <td>'+ result[y][i]['com_rh'] +'</td> </tr>');
                        ch = ch + '<tr> <td></td><td>' + result[y][i]['nom'] + '</td>  <td>'+result[y][i]['societe']+'</td><td>';

                        if(result[y][i]['detachement'].length >0){
                            ch=ch+'<div class="col-md-4"><B>Detachement:</B></div> <div class="col-md-6">';
                            for(var x=0; x< result[y][i]['detachement'].length;x++){
                            ch=ch+'<div class="text-muted" style="text-indent:20px;"><B>'+result[y][i]['detachement'][x]['societe_id']+': </B>'+ result[y][i]['detachement'][x]['nb_hours']+' heures travaillées </div>';
                        }}
                        else{
                            ch=ch+'<div class="text-center"><B>Pas de detachement.</B>'
                        }


                   ch=ch+'</div></td><td>' + result[y][i]['debut'] + ' <input id="dateDebut" value=' + result[y][i]['debut'] + ' hidden ></td><td>' + result[y][i]['fin'] + '</td><td>' + result[y][i]['com_rh'] + '</td></tr>';
                    }
                    //  tab.push('</tbody> </table> </div> <!-- /.row --> </div> <!-- /.box-body --> </div> <!-- /.box --> </div>');
                    ch = ch + '</tbody></table></div> <!-- /.row --> </div> <!-- /.box-body --> </div> <!-- /.box --> </div>';

                }
            }
            else {
                //  tab.push('<h3 style="text-align: center">Pas d\'enregistrement</h3></div>');
                ch = ch + '<h3 style="text-align: center">Pas d\'enregistrement</h3></div>';
            }

            $('#hierarchie').append(ch);

            displayDatatble();
            }

    });
};
function displayDatatble(){
    var dt = $('table.display').DataTable({
        "bProcessing": false,
        "bLengthChange": false,
        "iDisplayLength" : 10,

        "info":false,
        "language": {
            "zeroRecords": "Pas d'enregistrement",
            "infoEmpty": "Pas d'enregistrement",
        },
        "columns": [
            {
                "class": "details-control",
                "orderable": false,
                "data": null,
                "defaultContent": ""
            },
            {"data": "name"},
            {"data": "societe"},
            {"data": "detachement",
                "class":"hidden"},
            {"data": "debut"},
            {"data": "fin"},
            {"data": "com_rh"}
        ],
        "order": [[1, 'asc']]
    });


    // Array to track the ids of the details displayed rows
    var detailRows = [];

    $('table.display tbody').on( 'click', 'tr td.details-control', function () {
        tbl = $(this).parent().parent().parent().DataTable();
        var tr = $(this).closest('tr');
        var row = tbl.row( tr );
        var idx = $.inArray( tr.attr('id'), detailRows );

        if ( row.child.isShown() ) {
            tr.removeClass( 'details' );
            row.child.hide();

            // Remove from the 'open' array
            detailRows.splice( idx, 1 );
        }
        else {
            tr.addClass( 'details' );
            row.child( format(row.data()) ).show();

            // Add to the 'open' array
            if ( idx === -1 ) {
                detailRows.push( tr.attr('id') );
            }
        }
    } );

    // On each draw, loop over the `detailRows` array and show any child rows
    dt.on( 'draw', function () {
        $.each( detailRows, function ( i, id ) {
            $('#'+id+' td.details-control').trigger( 'click' );
        } );
    } );
}

function format(d) {

    return d.detachement;}
function displaySocieteHierarchie() {
    var data = [];
    var ch = '';
    var urlSoc;
    data.push($('#dateDebut').val());
    data.push($('#libelle').val());
    if($('#libelle').val() != '' && $('#dateDebut').val() !='' ){urlSoc = "societe"}else{urlSoc= "currentDate"}

    $.ajax({
        dataType: "json",
        type: "POST",
        url: urlSoc,
        data: {data: data},
        async: false,
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
            $('#hierarchie').empty();
            $('#hierarchie').append('<h3 style="text-align: center">Pas d\'enregistrement</h3></div>');
        },
        success: function (result) {
            $('#hierarchie').empty();

            if(urlSoc=="societe"){
            if (result != '') {

                for (var y = 0; y < result.length; y++) {
                    ch = ch + '<div class="box box-info"><div class="box-header with-border"><h3 class="box-title">' + result[y][0]['manager'] + '</h3> <div class="box-tools pull-right"> <button type="button" class="btn btn-box-tool" data-widget="collapse"> <i class="fa fa-minus"></i></button></div></div><div class="box-body"> <div class="row"><div class="col-md-12"><table class="table table-striped table-bordered table-hover table-responsive" style="padding-left: 2px; width: 100%"><thead> <tr> <th>Matricule Salarie</th> <th>Societe</th> <th>Date Debut</th> <th>Date Fin</th> <th>Commentaire RH</th> </tr> </thead> <tbody>';
                    for (var i = 0; i < result[y].length; i++) {
                        ch = ch + '<tr> <td>' + result[y][i]['nom'] + '</td> <td>' + result[y][i]['societe'] + '</td> <td >' + result[y][i]['debut'] + ' <input id="dateDebut" value=' + result[y][i]['debut'] + ' hidden ></td> <td>' + result[y][i]['fin'] + '</td> <td>' + result[y][i]['com_rh'] + '</td> </tr>';
                    }
                    ch = ch + '</tbody> </table> </div> <!-- /.row --> </div> <!-- /.box-body --> </div> <!-- /.box --> </div>';
                }
            }
            else {

                ch = ch + '<h3 style="text-align: center">Pas d\'enregistrement</h3></div>';
            }
                $('#hierarchie').append(ch);
            }
            else{
                if (result != '') {
                    for (var y = 0; y < result.length; y++) {
                        ch = ch + '<div class="box box-info"><div class="box-header with-border"><h3 class="box-title">' + result[y][0]['manager'] + '</h3> <a type="button" id="relancer" onclick="sendMailRelancer(' + result[y][0]['idManager'] + ')">Relancer</a><div class="box-tools pull-right"> <button type="button" class="btn btn-box-tool" data-widget="collapse"> <i class="fa fa-minus"></i></button></div></div><div class="box-body"> <div class="row"><div class="col-md-12"><table id="' + result[y][0]['idManager'] + '" class="display table table-striped table-bordered table-hover table-responsive" style="padding-left: 2px; width: 100%"><thead> <tr> <th></th> <th>Matricule Salarie</th> <th>Societe</th> <th>Detachement</th><th>Date Debut</th> <th>Date Fin</th> <th>Commentaire RH</th> </tr> </thead> <tbody>';
                        //   tab.push('<div class="box box-default"><div class="box-header with-border"><h3 class="box-title">'+  result[y][0]['manager']+ '</h3> <div class="box-tools pull-right"> <button type="button" class="btn btn-box-tool" data-widget="collapse"> <i class="fa fa-minus"></i></button></div></div><div class="box-body"> <div class="row"><div class="col-md-12"><table class="table table-striped table-bordered table-hover table-responsive" style="padding-left: 2px; width: 100%"><thead> <tr> <th>Matricule Salarie</th> <th>Societe</th> <th>Date Debut</th> <th>Date Fin</th> <th>Commentaire RH</th> </tr> </thead> <tbody>');
                        for (var i = 0; i < result[y].length; i++) {
                            //   tab.push('<tr> <td>'+result[y][i]['nom'] +'</td> <td>'+ result[y][i]['societe'] +'</td> <td >'+ result[y][i]['debut'] +' <input id="dateDebut" value='+ result[y][i]['debut'] +' hidden ></td> <td>'+ result[y][i]['fin'] +'</td> <td>'+ result[y][i]['com_rh'] +'</td> </tr>');
                            ch = ch + '<tr> <td></td> <td>' + result[y][i]['nom'] + '</td>  <td>'+result[y][i]['societe']+' </td><td>';

                            if(result[y][i]['detachement'].length >0){
                                ch=ch+'<div class="col-md-4"><B>Detachement:</B></div> <div class="col-md-6">';
                                for(var x=0; x< result[y][i]['detachement'].length;x++){
                                    ch=ch+'<div class="text-muted" style="text-indent:20px;"><B>'+result[y][i]['detachement'][x]['societe_id']+': </B>'+ result[y][i]['detachement'][x]['nb_hours']+' heures travaillées </div>';
                                }}
                            else{
                                ch=ch+'<div class="text-center"><B>Pas de detachement.</B>'
                            }

                            ch=ch+'</div></td><td >' + result[y][i]['debut'] + ' <input id="dateDebut" value=' + result[y][i]['debut'] + ' hidden ></td> <td>' + result[y][i]['fin'] + '</td> <td>' + result[y][i]['com_rh'] + '</td> </tr>';
                        }
                        //  tab.push('</tbody> </table> </div> <!-- /.row --> </div> <!-- /.box-body --> </div> <!-- /.box --> </div>');
                        ch = ch + '</tbody> </table> </div> <!-- /.row --> </div> <!-- /.box-body --> </div> <!-- /.box --> </div>';
                    }
                }
                else {
                    //  tab.push('<h3 style="text-align: center">Pas d\'enregistrement</h3></div>');
                    ch = ch + '<h3 style="text-align: center">Pas d\'enregistrement</h3></div>';
                }
                $('#hierarchie').append(ch);
                displayDatatble();
            }



        }
    })
};

function sendMailRelancer(id){
    $.ajax({
        dataType: "json",
        type: "POST",
        url: "sendMailRelancer",

        async: false,
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            alert('send mail to'+id);
        }
    })
};
function sendMail(){

    $.ajax({
        dataType: "json",
        type: "POST",
        url: "sendMail",
        async: false,
        cache: false,
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            alert('send monthly mails');
        }
    })
};
function displayslideupdown(){
    $("[data-widget='collapse']").click(function() {
        //Find the box parent........
        var box = $(this).parents(".box").first();
        //Find the body and the footer
        var bf = box.find(".box-body, .box-footer");
        if (!$(this).children().hasClass("fa-plus")) {
            $(this).children(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
            bf.slideUp();
        } else {
            //Convert plus into minus
            $(this).children(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
            bf.slideDown();
        }
    });
}
$(document).ready(function () {
    displayDate();
    $(".chosen-select").chosen({disable_search_threshold: 10});
    displayslideupdown()
    $('#Mois_Precedent').on("click", function () {

        displayPreviousDate();
        displayslideupdown()
        if($('#libelle').val() != '' ){
            displaySocieteHierarchie();
            displayslideupdown()
        }
    });

    $('#Mois_Suivant').on("click", function () {

        displayNextDate();
        displayslideupdown()
        if($('#libelle').val() != '' ){
            displaySocieteHierarchie();
            displayslideupdown()
        }
    });

    $('#libelle').on("change", function () {
        displaySocieteHierarchie();
        displayslideupdown()
    });

$('#importGrp').on("click",function(){
    sendMail();
});
});