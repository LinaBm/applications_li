<?php

namespace SuiviHierarchieBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DetSalarie
 *
 * @ORM\Table(name="det_salarie", indexes={@ORM\Index(name="salarie", columns={"salarie_id"}), @ORM\Index(name="referent", columns={"referent_id"}), @ORM\Index(name="salarie_2", columns={"salarie_id"}), @ORM\Index(name="societe_id", columns={"societe_id"})})
 * @ORM\Entity
 */
class DetSalarie
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="mois", type="date", nullable=false)
     */
    private $mois;


    /**
     * @var boolean
     *
     * @ORM\Column(name="detacher", type="boolean", nullable=false)
     */
    private $detacher = '0';


    /**
     * @var boolean
     *
     * @ORM\Column(name="relancer", type="boolean", nullable=false)
     */
    private $relancer = '0';

    /**
     * @return boolean
     */
    public function getRelancer()
    {
        return $this->relancer;
    }

    /**
     * @param boolean $relancer
     */
    public function setRelancer($relancer)
    {
        $this->relancer = $relancer;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="commentaire_rh", type="string", length=255, nullable=false)
     */
    private $commentaireRh;

    /**
     * @var string
     *
     * @ORM\Column(name="commentaire_manager", type="string", length=255, nullable=false)
     */
    private $commentaireManager;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Administration\SocieteBundle\Entity\PandoreIndividu
     *
     * @ORM\ManyToOne(targetEntity="Administration\SocieteBundle\Entity\PandoreIndividu")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="referent_id", referencedColumnName="matricule")
     * })
     */
    private $referent;

    /**
     * @var \Administration\SocieteBundle\Entity\PandoreIndividu
     *
     * @ORM\ManyToOne(targetEntity="Administration\SocieteBundle\Entity\PandoreIndividu")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="salarie_id", referencedColumnName="matricule")
     * })
     */
    private $salarie;

    /**
     * @var \Administration\SocieteBundle\Entity\PandoreSociete
     *
     * @ORM\ManyToOne(targetEntity="Administration\SocieteBundle\Entity\PandoreSociete")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="societe_id", referencedColumnName="id")
     * })
     */
    private $societe;


    /**
     * Set mois
     *
     * @param \DateTime $mois
     *
     * @return DetSalarie
     */
    public function setMois($mois)
    {
        $this->mois = $mois;
    
        return $this;
    }

    /**
     * Get mois
     *
     * @return \DateTime
     */
    public function getMois()
    {
        return $this->mois;
    }

    /**
     * Set detacher
     *
     * @param boolean $detacher
     *
     * @return DetSalarie
     */
    public function setDetacher($detacher)
    {
        $this->detacher = $detacher;
    
        return $this;
    }

    /**
     * Get detacher
     *
     * @return boolean
     */
    public function getDetacher()
    {
        return $this->detacher;
    }

    /**
     * Set commentaireRh
     *
     * @param string $commentaireRh
     *
     * @return DetSalarie
     */
    public function setCommentaireRh($commentaireRh)
    {
        $this->commentaireRh = $commentaireRh;
    
        return $this;
    }

    /**
     * Get commentaireRh
     *
     * @return string
     */
    public function getCommentaireRh()
    {
        return $this->commentaireRh;
    }

    /**
     * Set commentaireManager
     *
     * @param string $commentaireManager
     *
     * @return DetSalarie
     */
    public function setCommentaireManager($commentaireManager)
    {
        $this->commentaireManager = $commentaireManager;
    
        return $this;
    }

    /**
     * Get commentaireManager
     *
     * @return string
     */
    public function getCommentaireManager()
    {
        return $this->commentaireManager;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set referent
     *
     * @param \Administration\SocieteBundle\Entity\PandoreIndividu $referent
     *
     * @return DetSalarie
     */
    public function setReferent(\Administration\SocieteBundle\Entity\PandoreIndividu $referent = null)
    {
        $this->referent = $referent;
    
        return $this;
    }

    /**
     * Get referent
     *
     * @return \Administration\SocieteBundle\Entity\PandoreIndividu
     */
    public function getReferent()
    {
        return $this->referent;
    }

    /**
     * Set salarie
     *
     * @param \Administration\SocieteBundle\Entity\PandoreIndividu $salarie
     *
     * @return DetSalarie
     */
    public function setSalarie(\Administration\SocieteBundle\Entity\PandoreIndividu $salarie = null)
    {
        $this->salarie = $salarie;
    
        return $this;
    }

    /**
     * Get salarie
     *
     * @return \Administration\SocieteBundle\Entity\PandoreIndividu
     */
    public function getSalarie()
    {
        return $this->salarie;
    }

    /**
     * Set societe
     *
     * @param \Administration\SocieteBundle\Entity\PandoreSociete $societe
     *
     * @return DetSalarie
     */
    public function setSociete(\Administration\SocieteBundle\Entity\PandoreSociete $societe = null)
    {
        $this->societe = $societe;
    
        return $this;
    }

    /**
     * Get societe
     *
     * @return \Administration\SocieteBundle\Entity\PandoreSociete
     */
    public function getSociete()
    {
        return $this->societe;
    }

}

