<?php

namespace SuiviHierarchieBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DetDetachement
 *
 * @ORM\Table(name="det_detachement", indexes={@ORM\Index(name="salarie_id", columns={"salarie_id"}), @ORM\Index(name="societe_id", columns={"societe_id"})})
 * @ORM\Entity
 */
class DetDetachement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="nb_hours", type="integer", nullable=false)
     */
    private $nbHours;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Administration\SocieteBundle\Entity\Societe
     *
     * @ORM\ManyToOne(targetEntity="Administration\SocieteBundle\Entity\Societe")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="societe_id", referencedColumnName="id")
     * })
     */
    private $societe;

    /**
     * @var \SuiviHierarchieBundle\Entity\DetSalarie
     *
     * @ORM\ManyToOne(targetEntity="SuiviHierarchieBundle\Entity\DetSalarie")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="salarie_id", referencedColumnName="id")
     * })
     */
    private $salarie;


    /**
     * Set nbHours
     *
     * @param integer $nbHours
     *
     * @return DetDetachement
     */
    public function setNbHours($nbHours)
    {
        $this->nbHours = $nbHours;
    
        return $this;
    }

    /**
     * Get nbHours
     *
     * @return integer
     */
    public function getNbHours()
    {
        return $this->nbHours;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set societe
     *
     * @param \Administration\SocieteBundle\Entity\Societe $societe
     *
     * @return DetDetachement
     */
    public function setSociete(\Administration\SocieteBundle\Entity\Societe $societe = null)
    {
        $this->societe = $societe;
    
        return $this;
    }

    /**
     * Get societe
     *
     * @return \Administration\SocieteBundle\Entity\Societe
     */
    public function getSociete()
    {
        return $this->societe;
    }

    /**
     * Set salarie
     *
     * @param \SuiviHierarchieBundle\Entity\DetSalarie $salarie
     *
     * @return DetDetachement
     */
    public function setSalarie(\SuiviHierarchieBundle\Entity\DetSalarie $salarie = null)
    {
        $this->salarie = $salarie;
    
        return $this;
    }

    /**
     * Get salarie
     *
     * @return \SuiviHierarchieBundle\Entity\DetSalarie
     */
    public function getSalarie()
    {
        return $this->salarie;
    }
}

