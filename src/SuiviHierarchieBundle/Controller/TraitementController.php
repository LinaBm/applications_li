<?php

namespace SuiviHierarchieBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints\False;
use SuiviHierarchieBundle\Entity\DetSalarie;
use SuiviHierarchieBundle\Entity\DetDetachement;
use Symfony\Component\Validator\Constraints\Null;

class TraitementController extends Controller
{
    /**
     * @Route("/collaborateur_success", name="det_success")
     * @Template()
     */
    public function indexAction()
    {

        $em = $this->getDoctrine()->getManager();
     //var_dump($_POST);die;
        $this->data=$_POST;
        $date= $em->createQuery('SELECT max(t.mois)
                FROM SuiviHierarchieBundle:DetSalarie t
                WHERE t.referent is NULL
                ')
            ->getResult();
        //var_dump($date[0]);die;

        //mise à jour des données de Det_Salarie
         $y=0;
        while(isset($this->data['salarie_id'][$y])){
            if($this->data['com_manag'][$y] != ""){
            $individus = $em->getRepository('AdministrationSocieteBundle:PandoreIndividu')->findBy(
                array('id' => $this->data["salarie_id"][$y])
            );
            $salaries[$y] = $em->getRepository('SuiviHierarchieBundle:DetSalarie')->findBy(array('salarie' => $individus, 'mois' => $date[0]));
            $salaries[$y][0]->setCommentaireManager($this->data['com_manag'][$y]);
                //var_dump($salaries[$y][0]);die;
                $em->persist($salaries[$y][0]);
        }
        $y++;
        }

        //savegarde manager
        $manag= $em->getRepository('SuiviHierarchieBundle:DetSalarie')->findBy(array('id' => $this->data['manager_id']));
        $manag[0]->setRelancer(1);

        //sauvegarde détachement
        $i=0;
        while (isset($this->data['menuTitle'][$i])) {
            $individu = $em->getRepository('AdministrationSocieteBundle:PandoreIndividu')->findBy(
                array('id' => $this->data["identite"][$i])
            );

            $product[$i] = new DetDetachement();
            $salarie = $em->getRepository('SuiviHierarchieBundle:DetSalarie')->findBy(array('salarie' => $individu, 'mois' => $date[0]));
            $salarie[0]->setDetacher(1);
            //$salarie[0]->setRelancer(1);
            $societe = $em->getRepository('AdministrationSocieteBundle:Societe')->findBy(
                array('id' => $this->data["menuTitle"][$i])
            );
          //  var_dump($this->data["qte"][0]);die;x
            $product[$i]->setSalarie($salarie[0]);
            $product[$i]->setSociete($societe[0]);
            $product[$i]->setNbHours($this->data["qte"][$i]);
            $em->persist($product[$i]);
            $em->persist($salarie[0]);
            $i++;
        }
$em->persist($manag[0]);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Sauvegardé avec succés');
        return $this->render('SuiviHierarchieBundle:Default:referent.html.twig');
    }

}
