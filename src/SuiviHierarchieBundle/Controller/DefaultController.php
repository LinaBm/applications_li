<?php

namespace SuiviHierarchieBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints\False;
use SuiviHierarchieBundle\Entity\DetSalarie;
use Symfony\Component\Validator\Constraints\Null;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="home_det_hier")
     * @Template()
     */
    public function indexAction()
    {
        $rows = array();
        $em = $this->getDoctrine()->getManager();
        set_time_limit(300);
        $request = $this->getRequest();
        $form = $this->get('form.factory')->createNamedBuilder('', 'form', null, array())
            ->add('importFile', 'file', array('required' => true))
            ->add('Mois_Precedent', 'button', array('attr' => array('class' => 'btn btn-info pull-left')))
            ->add('Mois_Suivant', 'button', array('attr' => array('class' => 'btn btn-info')))
            ->add('libelle', 'entity', array(
                'class' => 'AdministrationSocieteBundle:PandoreSociete',
                'query_builder' => function (EntityRepository $em) {
                    return $em->createQueryBuilder('p')
                        ->orderBy('p.raison', 'ASC');
                },
                'property' => 'raison',
                'required' => false,
                'empty_value' => 'Choisissez une société',
            ))
            ->getForm();
        $result = array();
        if ($request->isMethod('POST')) {
            $fileType = null;
            // bind form to request to persist data
            $form->bind($request);
            // upload file
            $file = $form->get('importFile')->getData();
            $action = $request->request->get('_action');

            if (!empty($file)) {
                $fileType = $file->getClientMimeType();
            }
            // if is not a excel file do not parse the file
            $fileTypes = array('text/vnd.ms-excel', 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            if (!in_array($fileType, $fileTypes)) {
                $this->get('session')->getFlashBag()->add('myError', 'Ce n\'est pas un fichier excel.');
                return $this->redirect($this->generateUrl('home_det_hier'));
            }

            // Get and set infos
            $hieId = (int)$request->request->get('libelle');
            $dir = $this->get('kernel')->getRootDir() . '/../web/bundles/dmli/uploads/';
            // generate unique file name
            $fileName = uniqid($file->getClientOriginalName());
            $file->move($dir, $fileName);


            /* READ EXCEL FILE */
            $phpExcelObject = \PHPExcel_IOFactory::load($dir . $fileName);
            // only for the first worksheet
            $worksheet = $phpExcelObject->getSheet(0);
            //  Read rows into an array
            $rows = $worksheet->toArray(NULL, NULL, TRUE, FALSE);

            // unset($rows[0]);
            $nb = 0;
            foreach ($rows as $row) {
                $nb++;
                $salarie1 = $em->getRepository('AdministrationSocieteBundle:PandoreIndividu')->findBy(
                    array('id' => $row[0]));
                if ($row[1] != '') {
                    $referant1 = $em->getRepository('AdministrationSocieteBundle:PandoreIndividu')->findBy(
                        array('id' => $row[1])
                    );
                }
                $societePandore = $em->getRepository('AdministrationSocieteBundle:PandoreSociete')->findBy(
                    array('id' => $row[2])
                );
                if ($row[1] == '') {
                    $detsalarie = $em->getRepository('SuiviHierarchieBundle:DetSalarie')->findBy(
                        array('salarie' => $salarie1, 'referent' => NULL, 'societe' => $societePandore, 'mois' => new \DateTime("now")));
                } else {
                    $detsalarie = $em->getRepository('SuiviHierarchieBundle:DetSalarie')->findBy(
                        array('salarie' => $salarie1, 'referent' => $referant1, 'societe' => $societePandore, 'mois' => new \DateTime("now")));
                }

                if (count($detsalarie) > 0) {
                    $this->get('session')->getFlashBag()->add('myError', 'Salarie existant!! veuillez vérifier la ligne ' . $nb . ' de fichier Excel');
                    return $this->redirect($this->generateUrl('home_det_hier'));
                }
                // var_dump($societePandore);die;
                if (count($societePandore) > 0) {
                    $societe1 = $em->getRepository('AdministrationSocieteBundle:Societe')->findBy(
                        array('libelle' => $societePandore[0]->getRaison())
                    );
                    if (count($societe1) == 0) {
                        $this->get('session')->getFlashBag()->add('myError', 'Les données de fichiers Excel sont incorrectes.');
                        return $this->redirect($this->generateUrl('home_det_hier'));
                    }
                }

                if (count($salarie1) == 0) {
                    //var_dump(count($societePandore));die;
                    $this->get('session')->getFlashBag()->add('myError', 'Les données de fichiers Excel sont incorrectes.');
                    return $this->redirect($this->generateUrl('home_det_hier'));
                }
                if ($row[1] != '') {
                    if (count($referant1) == 0) {
                        //var_dump(count($societePandore));die;
                        $this->get('session')->getFlashBag()->add('myError', 'Les données de fichiers Excel sont incorrectes.');
                        return $this->redirect($this->generateUrl('home_det_hier'));
                    }
                }
                if (count($societePandore) == 0) {
                    //var_dump(count($societePandore));die;
                    $this->get('session')->getFlashBag()->add('myError', 'Les données de fichiers Excel sont incorrectes.');
                    return $this->redirect($this->generateUrl('home_det_hier'));
                }
            }


            //save data Det_salarie
            foreach ($rows as $row) {
                $product = new DetSalarie();
                $salarie = $em->getRepository('AdministrationSocieteBundle:PandoreIndividu')->findBy(
                    array('id' => $row[0])
                );


                //  $contrats=$em->getRepository('AdministrationSocieteBundle:PandoreContrat')->findBy(array('user' => $salarie));

                if ($row[1] != '') {
                    $referant = $em->getRepository('AdministrationSocieteBundle:PandoreIndividu')->findBy(
                        array('id' => $row[1])
                    );
                    $product->setReferent($referant[0]);
                }
                $societe = $em->getRepository('AdministrationSocieteBundle:PandoreSociete')->findBy(
                    array('id' => $row[2])
                );
                $product->setSalarie($salarie[0]);
                $product->setMois(new \DateTime("now"));
                $product->setSociete($societe[0]);
                $product->setCommentaireRh($row[3]);
                $em->persist($product);
                $em->flush();
            }
            // date d'importation
            $date = $em->createQuery('SELECT max(t.mois)
                FROM SuiviHierarchieBundle:DetSalarie t
                WHERE t.referent is NULL
                ')
                ->getResult();

            // liste des managers
            $managers = $em->createQuery('SELECT t
                FROM SuiviHierarchieBundle:DetSalarie t
                WHERE t.referent is NULL and t.mois =:id ORDER BY t.id ASC
                ')
                ->setParameter('id', $date)
                ->getResult();


            foreach ($managers as $manag) {

                if (in_array($this->get('kernel')->getEnvironment(), array('test', 'prod'))) {
                    $donnee[0] = $this->container->getParameter('url_serveur') . $this->container->getParameter('url_det_hier_prod') . $manag->getId();
                    $donnee[1] = 'SuiviHierarchieBundle:MailProd:mail.html.twig';
                    $mailProd = $this->container->getParameter('mailThierry_prod');
                } else {
                    $donnee[0] = $this->container->getParameter('url_serveur_recette') . $this->container->getParameter('url_det_hier_dev') . $manag->getId();
                    $donnee[1] = 'SuiviHierarchieBundle:Mail:mail.html.twig';
                    $mailProd = $this->container->getParameter('mailThierry_dev');
                }
                $sql = "SELECT *
				FROM pandore.Individu t where t.matricule= " . $manag->getSalarie()->getId() . "";
                $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
                $query->execute();
                $indiv = $query->fetchAll();

                $user = $em->getRepository('UserUserBundle:User')->findBy(array('givenname' => $indiv[0]['prenom'], 'surname' => $indiv[0]['nom']));

                if (count($user) == 1) {
                    $donnee[3] = '';
                    $donnee[2] = $user[0]->getEmail();
                } else {
                    $donnee[3] = $indiv[0]['prenom'] . $indiv[0]['nom'];
                    $donnee[2] = $mailProd;
                }
                $this->sendMail($donnee);
            }


            // delete file from upload directory
            unlink($dir . $fileName);
            $this->get('session')->getFlashBag()->add('success', 'La hiérarchie a été générée en fonction de votre fichier Excel.');


        }

        //var_dump($count);die;
        return $this->render('SuiviHierarchieBundle:Default:index.html.twig', array(
            'form' => $form->createView()));
    }

    /**
     * @Route("/currentDate")
     * @Template()
     */
    function currentDateAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        //list Manager
        $result = array();
        if ($request->get('data') != '') {
            $data = $request->get('data');

            $date = $data[0];
        } else {
            $date = $em->createQuery('SELECT max(t.mois)
                FROM SuiviHierarchieBundle:DetSalarie t
                WHERE t.referent is NULL
                ')
                ->getResult();
        }

        //var_dump($date);die;
        // $managers= $em->getRepository('SuiviHierarchieBundle:DetSalarie')->findBy(array('referent' => NULL, 'mois' => new \DateTime("now")));
        $managers = $em->createQuery('SELECT t
                FROM SuiviHierarchieBundle:DetSalarie t
                WHERE t.referent is NULL and t.mois =:id ORDER BY t.id ASC
                ')
            ->setParameter('id', $date)
            ->getResult();

        //var_dump($managers);die;

        $i = 0;
        $y = 0;
        $j = 0;
        foreach ($managers as $manager) {
            $salaries = $em->getRepository('SuiviHierarchieBundle:DetSalarie')->findBy(array('referent' => $manager->getSalarie(), 'mois' => $manager->getmois(), 'societe' => $manager->getSociete()));
            //$result=$manager->getSalarie()->getId();
            // var_dump($salarie);die;
            $j = 0;
            foreach ($salaries as $salariee) {
                $result[$i][$y]['idManager'] = $manager->getId();
                $result[$i][$y]['manager'] = $manager->getSalarie()->getId() . ' ' . $manager->getSalarie()->getNom() . ' ' . $manager->getSalarie()->getPrenom();
                $result[$i][$y]['nom'] = $salariee->getSalarie()->getId() . ' ' . $salariee->getSalarie()->getNom() . ' ' . $salariee->getSalarie()->getPrenom();
                $result[$i][$y]['societe'] = $salariee->getSociete()->getRaison();


                $sql = "SELECT *
				FROM det_detachement t where  t.salarie_id = \"" . $salariee->getId() . "\"";
                $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
                $query->execute();
                $detachements = $query->fetchAll();
                if (count($detachements) > 0) {
                    foreach ($detachements as $detachement) {
                        $det = $em->getRepository('AdministrationSocieteBundle:Societe')->findBy(array('id' => $detachement['societe_id']));
                        $result[$i][$y]['detachement'][$j]['societe_id'] = $det[0]->getLibelle();
                        $result[$i][$y]['detachement'][$j]['nb_hours'] = $detachement['nb_hours'];
                        $j++;
                    }
                } else {
                    $result[$i][$y]['detachement'] = '';
                }
                $j = 0;
                $result[$i][$y]['debut'] = $salariee->getMois()->format('Y-m-d');
                $result[$i][$y]['fin'] = date('Y-m-d', strtotime($result[$i][$y]['debut'] . ' + 1 months'));
                $sql2 = 'SELECT *
				FROM pandore.contrat t where  t.matricule = "' . $salariee->getSalarie()->getId() .'"';
                $query2 = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql2);
                $query2->execute();
                $contrats = $query2->fetchAll();
                $test = true;
                $x = 0;
                $datesortie = '';

                    while ($test && ($x < count($contrats))) {
                        foreach ($contrats as $contrat) {
                        $x++;
                        if (($contrat['date_sortie'] == '') && ($contrat['date_fincont'] == '')) {
                            $datesortie = '';
                            $test = false;

                        } elseif ($contrat['date_sortie'] == '' && $contrat['date_fincont'] != '') {
                            if ($datesortie <= $contrat['date_fincont']) {
                                $datesortie = $contrat['date_fincont'];
                            }
                        } elseif ($contrat['date_sortie'] != '') {
                            if ($datesortie <= $contrat['date_sortie']) {
                                $datesortie = $contrat['date_sortie'];
                            }
                        }
                    }
                }
                if ($datesortie != '') {
                    if (strpos($datesortie, '-') == false) {
                        $a = substr($datesortie, 0, 4);
                        $b = substr($datesortie, 4, 2);
                        $c = substr($datesortie, 6, 2);
                        $res = $a . '-' . $b . '-' . $c;
                        $datesortie = $res;
                    }
                    if (date('Y-m-d', strtotime($result[$i][$y]['debut'] . ' + 1 months')) < $datesortie) {
                        $result[$i][$y]['fin'] = date('Y-m-d', strtotime($result[$i][$y]['debut'] . ' + 1 months'));
                    } else if ((date('Y-m-d', strtotime($result[$i][$y]['debut'] . ' + 1 months')) > $datesortie) && ($result[$i][$y]['debut'] < $datesortie)) {
                        $result[$i][$y]['fin'] = $datesortie;
                    } else {
                        $result[$i][$y]['fin'] = 'a quitté la société';

                    }

                } else {
                    $result[$i][$y]['fin'] = date('Y-m-d', strtotime($result[$i][$y]['debut'] . ' + 1 months'));
                }


                if ($salariee->getCommentaireRh() != '') {
                    $result[$i][$y]['com_rh'] = $salariee->getCommentaireRh();
                } else {
                    $result[$i][$y]['com_rh'] = 'Pas de remarque';
                }

                if ($salariee->getCommentaireManager() != '') {
                    $result[$i][$y]['com_manag'] = $salariee->getCommentaireManager();
                } else {
                    $result[$i][$y]['com_manag'] = 'Pas de remarque';
                }

                $y++;


            }
            $y = 0;
            $i++;
        }

        if (count($result) > 0) {

            $return = $result;
            $response = new Response();
            $data = json_encode($return);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;

        }
        return new Response('Erreur');
    }


    /**
     * @Route("/previousDate")
     * @Template()
     */
    function previousDateAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $result = array();
        $currentDate = $request->get('currentDate');
        $date = $em->createQuery('SELECT max(t.mois)
                FROM SuiviHierarchieBundle:DetSalarie t
                WHERE t.referent is NULL and t.mois <:id
                ')
            ->setParameter('id', $currentDate)
            ->getResult();

        $managers = $em->createQuery('SELECT t
                FROM SuiviHierarchieBundle:DetSalarie t
                WHERE t.referent is NULL and t.mois =:id
                ')
            ->setParameter('id', $date)
            ->getResult();

        //var_dump($managers);die;
        $i = 0;
        $y = 0;
        foreach ($managers as $manager) {
            $salaries = $em->getRepository('SuiviHierarchieBundle:DetSalarie')->findBy(array('referent' => $manager->getSalarie(), 'mois' => $manager->getmois(), 'societe' => $manager->getSociete()));
            //$result=$manager->getSalarie()->getId();
            $j = 0;
            foreach ($salaries as $salarie) {
                $result[$i][$y]['idManager'] = $manager->getId();
                $result[$i][$y]['manager'] = $manager->getSalarie()->getId() . ' ' . $manager->getSalarie()->getNom() . ' ' . $manager->getSalarie()->getPrenom();
                $result[$i][$y]['nom'] = $salarie->getSalarie()->getId() . ' ' . $salarie->getSalarie()->getNom() . ' ' . $salarie->getSalarie()->getPrenom();
                $result[$i][$y]['societe'] = $salarie->getSociete()->getRaison();
                $sql = "SELECT *
				FROM det_detachement t where  t.salarie_id = \"" . $salarie->getId() . "\"";
                $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
                $query->execute();
                $detachements = $query->fetchAll();
                if (count($detachements) > 0) {
                    foreach ($detachements as $detachement) {
                        $det = $em->getRepository('AdministrationSocieteBundle:Societe')->findBy(array('id' => $detachement['societe_id']));
                        $result[$i][$y]['detachement'][$j]['societe_id'] = $det[0]->getLibelle();
                        $result[$i][$y]['detachement'][$j]['nb_hours'] = $detachement['nb_hours'];
                        $j++;
                    }
                } else {
                    $result[$i][$y]['detachement'] = '';
                }
                $j = 0;
                $result[$i][$y]['debut'] = $salarie->getMois()->format('Y-m-d');
                $result[$i][$y]['fin'] = date('Y-m-d', strtotime($result[$i][$y]['debut'] . ' + 1 months'));
                $sql2 = 'SELECT *
				FROM pandore.contrat t where  t.matricule = "' . $salarie->getSalarie()->getId() .'"';
                $query2 = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql2);
                $query2->execute();
                $contrats = $query2->fetchAll();
                $test = true;
                $x = 0;
                $datesortie = '';
                while ($test && ($x < count($contrats))) {
                foreach ($contrats as $contrat) {

                        $x++;
                        if ($contrat['date_sortie'] == '' && $contrat['date_fincont'] == '') {
                            $datesortie = '';
                            $test = false;

                        } elseif ($contrat['date_sortie'] == '' && $contrat['date_fincont'] != '') {
                            if ($datesortie <= $contrat['date_fincont']) {
                                $datesortie = $contrat['date_fincont'];
                            }
                        } elseif ($contrat['date_sortie'] != '') {
                            if ($datesortie <= $contrat['date_sortie']) {
                                $datesortie = $contrat['date_sortie'];
                            }
                        }  $result[$i][$y]['contrat'][$x]=$datesortie;
                    }


                }
                if ($datesortie != '') {
                    if (strpos($datesortie, '-') == false) {
                        $a = substr($datesortie, 0, 4);
                        $b = substr($datesortie, 4, 2);
                        $c = substr($datesortie, 6, 2);
                        $res = $a . '-' . $b . '-' . $c;
                        $datesortie = $res;
                    }
                    if (date('Y-m-d', strtotime($result[$i][$y]['debut'] . ' + 1 months')) < $datesortie) {
                        $result[$i][$y]['fin'] = date('Y-m-d', strtotime($result[$i][$y]['debut'] . ' + 1 months'));
                    } else if ((date('Y-m-d', strtotime($result[$i][$y]['debut'] . ' + 1 months')) > $datesortie) && ($result[$i][$y]['debut'] < $datesortie)) {
                        $result[$i][$y]['fin'] = $datesortie;
                    } else {
                        $result[$i][$y]['fin'] = 'a quitté la société';

                    }

                } else {
                    $result[$i][$y]['fin'] = date('Y-m-d', strtotime($result[$i][$y]['debut'] . ' + 1 months'));
                }
                if ($salarie->getCommentaireRh() != '') {
                    $result[$i][$y]['com_rh'] = $salarie->getCommentaireRh();
                } else {
                    $result[$i][$y]['com_rh'] = 'Pas de remarque';
                }

                if ($salarie->getCommentaireManager() != '') {
                    $result[$i][$y]['com_manag'] = $salarie->getCommentaireManager();
                } else {
                    $result[$i][$y]['com_manag'] = 'Pas de remarque';
                }

                $y++;
            }
            $y = 0;
            $i++;
        }

        if (count($result) > 0) {

            $return = $result;
            $response = new Response();
            $data = json_encode($return);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;

        }
        return new Response('Erreur');
    }

    /**
     * @Route("/nextDate")
     * @Template()
     */
    function nextDateAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $result = array();
        $currentDate = $request->get('currentDate');
        $date = $em->createQuery('SELECT min(t.mois)
                FROM SuiviHierarchieBundle:DetSalarie t
                WHERE t.referent is NULL and t.mois >:id
                ')
            ->setParameter('id', $currentDate)
            ->getResult();

        $managers = $em->createQuery('SELECT t
                FROM SuiviHierarchieBundle:DetSalarie t
                WHERE t.referent is NULL and t.mois =:id
                ')
            ->setParameter('id', $date)
            ->getResult();

        //var_dump($managers);die;
        $i = 0;
        $y = 0;
        foreach ($managers as $manager) {
            $salaries = $em->getRepository('SuiviHierarchieBundle:DetSalarie')->findBy(array('referent' => $manager->getSalarie(), 'mois' => $manager->getmois(), 'societe' => $manager->getSociete()));

            $j = 0;
            foreach ($salaries as $salarie) {
                $result[$i][$y]['idManager'] = $manager->getId();
                $result[$i][$y]['manager'] = $manager->getSalarie()->getId() . ' ' . $manager->getSalarie()->getNom() . ' ' . $manager->getSalarie()->getPrenom();
                $result[$i][$y]['nom'] = $salarie->getSalarie()->getId() . ' ' . $salarie->getSalarie()->getNom() . ' ' . $salarie->getSalarie()->getPrenom();
                $result[$i][$y]['societe'] = $salarie->getSociete()->getRaison();
                $sql = "SELECT *
				FROM det_detachement t where  t.salarie_id = \"" . $salarie->getId() . "\"";
                $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
                $query->execute();
                $detachements = $query->fetchAll();
                if (count($detachements) > 0) {
                    foreach ($detachements as $detachement) {
                        $det = $em->getRepository('AdministrationSocieteBundle:Societe')->findBy(array('id' => $detachement['societe_id']));
                        $result[$i][$y]['detachement'][$j]['societe_id'] = $det[0]->getLibelle();
                        $result[$i][$y]['detachement'][$j]['nb_hours'] = $detachement['nb_hours'];
                        $j++;
                    }
                } else {
                    $result[$i][$y]['detachement'] = '';
                }
                $j = 0;
                $result[$i][$y]['debut'] = $salarie->getMois()->format('Y-m-d');
                $result[$i][$y]['fin'] = date('Y-m-d', strtotime($result[$i][$y]['debut'] . ' + 1 months'));
                $sql2 = 'SELECT *
				FROM pandore.contrat t where  t.matricule = "' . $salarie->getSalarie()->getId() . '"';
                $query2 = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql2);
                $query2->execute();
                $contrats = $query2->fetchAll();
                $test = true;
                $x = 0;
                $datesortie = '';
                while ($test && ($x < count($contrats))) {
                foreach ($contrats as $contrat) {

                        $x++;
                        if ($contrat['date_sortie'] == '' && $contrat['date_fincont'] == '') {
                            $datesortie = '';
                            $test = false;

                        } elseif ($contrat['date_sortie'] == '' && $contrat['date_fincont'] != '') {
                            if ($datesortie <= $contrat['date_fincont']) {
                                $datesortie = $contrat['date_fincont'];
                            }
                        } elseif ($contrat['date_sortie'] != '') {
                            if ($datesortie <= $contrat['date_sortie']) {
                                $datesortie = $contrat['date_sortie'];
                            }
                        }
                    }
                }
                if ($datesortie != '') {
                    if (strpos($datesortie, '-') == false) {
                        $a = substr($datesortie, 0, 4);
                        $b = substr($datesortie, 4, 2);
                        $c = substr($datesortie, 6, 2);
                        $res = $a . '-' . $b . '-' . $c;
                        $datesortie = $res;
                    }
                    if (date('Y-m-d', strtotime($result[$i][$y]['debut'] . ' + 1 months')) < $datesortie) {
                        $result[$i][$y]['fin'] = date('Y-m-d', strtotime($result[$i][$y]['debut'] . ' + 1 months'));
                    } else if ((date('Y-m-d', strtotime($result[$i][$y]['debut'] . ' + 1 months')) > $datesortie) && ($result[$i][$y]['debut'] < $datesortie)) {
                        $result[$i][$y]['fin'] = $datesortie;
                    } else {
                        $result[$i][$y]['fin'] = 'a quitté la société';

                    }

                } else {
                    $result[$i][$y]['fin'] = date('Y-m-d', strtotime($result[$i][$y]['debut'] . ' + 1 months'));
                }

                if ($salarie->getCommentaireRh() != '') {
                    $result[$i][$y]['com_rh'] = $salarie->getCommentaireRh();
                } else {
                    $result[$i][$y]['com_rh'] = 'Pas de remarque';
                }

                if ($salarie->getCommentaireManager() != '') {
                    $result[$i][$y]['com_manag'] = $salarie->getCommentaireManager();
                } else {
                    $result[$i][$y]['com_manag'] = 'Pas de remarque';
                }
                $y++;
            }
            $y = 0;
            $i++;
        }

        if (count($result) > 0) {

            $return = $result;
            $response = new Response();
            $data = json_encode($return);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;

        }
        return new Response('Erreur');
    }

    /**
     * @Route("/societe")
     * @Template()
     */
    function societeAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $result = array();
        $data = $request->get('data');
        if ($data[0] == '') {
            $date = $em->createQuery('SELECT max(t.mois)
                FROM SuiviHierarchieBundle:DetSalarie t
                WHERE t.referent is NULL
                ')
                ->getResult();
            $data[0] = $date[0][1];

        }

        $societe = $em->createQuery('SELECT t.libelle FROM AdministrationSocieteBundle:Societe t WHERE t.id =:id')
            ->setParameter('id', $data[1])
            ->getResult();

        $societePandore = $em->createQuery('SELECT t
                FROM AdministrationSocieteBundle:PandoreSociete t
                WHERE t.id =:id
                ')
            ->setParameter('id', $data[1])
            ->getResult();

        if (count($societePandore) > 0) {
            $sql = "SELECT *
				FROM det_salarie t where t.referent_id is null and  t.mois = \"" . $data[0] . "\" and t.societe_id= " . $societePandore[0]->getId() . "";
            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $managers = $query->fetchAll();
            $i = 0;
            $y = 0;
            foreach ($managers as $manager) {
                //  var_dump($manager);die;
                $sql = "SELECT *
				FROM det_salarie t where t.referent_id = " . $manager['salarie_id'] . " and  t.mois = \"" . $data[0] . "\" and t.societe_id= " . $societePandore[0]->getId() . "";
                $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
                $query->execute();
                $salaries = $query->fetchAll();
                // var_dump($manager);die;
                // $salaries = $em->getRepository('SuiviHierarchieBundle:DetSalarie')->findBy(array('referent' => $manager, 'mois' => $manager['mois'], 'societe'=>$societePandore[0]->getId()));
                //$result=$manager->getSalarie()->getId();
                //  var_dump($salaries);die;
                $manager = $em->getRepository('AdministrationSocieteBundle:PandoreIndividu')->findBy(array('id' => $manager['salarie_id']));
                foreach ($salaries as $salarie) {
                    $salariee = $em->getRepository('AdministrationSocieteBundle:PandoreIndividu')->findBy(array('id' => $salarie['salarie_id']));
                    $result[$i][$y]['manager'] = $manager[0]->getId() . ' ' . $manager[0]->getNom() . ' ' . $manager[0]->getPrenom();
                    $result[$i][$y]['nom'] = $salariee[0]->getId() . ' ' . $salariee[0]->getNom() . ' ' . $salariee[0]->getPrenom();
                    $result[$i][$y]['societe'] = $societePandore[0]->getRaison();
                    $result[$i][$y]['debut'] = $salarie['mois'];
                    $result[$i][$y]['fin'] = date('Y-m-d', strtotime($result[$i][$y]['debut'] . ' + 1 months'));
                    $sql2 = 'SELECT *
				FROM pandore.contrat t where  t.matricule = "' . $salarie['salarie_id'] .'"';
                    $query2 = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql2);
                    $query2->execute();
                    $contrats = $query2->fetchAll();
                    $test = true;
                    $x = 0;
                    $datesortie = '';
                    while ($test && ($x < count($contrats))) {
                    foreach ($contrats as $contrat) {

                            $x++;
                            if ($contrat['date_sortie'] == '' && $contrat['date_fincont'] == '') {
                                $datesortie = '';
                                $test = false;

                            } elseif ($contrat['date_sortie'] == '' && $contrat['date_fincont'] != '') {
                                if ($datesortie <= $contrat['date_fincont']) {
                                    $datesortie = $contrat['date_fincont'];
                                }
                            } elseif ($contrat['date_sortie'] != '') {
                                if ($datesortie <= $contrat['date_sortie']) {
                                    $datesortie = $contrat['date_sortie'];
                                }
                            }
                        }
                    }


                    if ($datesortie != '') {
                        if (strpos($datesortie, '-') == false) {
                            $a = substr($datesortie, 0, 4);
                            $b = substr($datesortie, 4, 2);
                            $c = substr($datesortie, 6, 2);
                            $res = $a . '-' . $b . '-' . $c;
                            $datesortie = $res;
                        }
                        if (date('Y-m-d', strtotime($result[$i][$y]['debut'] . ' + 1 months')) < $datesortie) {
                            $result[$i][$y]['fin'] = date('Y-m-d', strtotime($result[$i][$y]['debut'] . ' + 1 months'));
                        } else if ((date('Y-m-d', strtotime($result[$i][$y]['debut'] . ' + 1 months')) > $datesortie) && ($result[$i][$y]['debut'] < $datesortie)) {
                            $result[$i][$y]['fin'] = $datesortie;
                        } else {
                            $result[$i][$y]['fin'] = 'a quitté la société';

                        }

                    } else {
                        $result[$i][$y]['fin'] = date('Y-m-d', strtotime($result[$i][$y]['debut'] . ' + 1 months'));
                    }

                    if ($salarie['commentaire_rh'] != '') {
                        $result[$i][$y]['com_rh'] = $salarie['commentaire_rh'];
                    } else {
                        $result[$i][$y]['com_rh'] = 'Pas de remarque';
                    }

                    if ($salarie['commentaire_manager'] != '') {
                        $result[$i][$y]['com_manag'] = $salarie['commentaire_manager'];
                    } else {
                        $result[$i][$y]['com_manag'] = 'Pas de remarque';
                    }
                    $y++;
                }
                $y = 0;
                $i++;
            }

            if (count($result) > 0) {

                $return = $result;
                $response = new Response();
                $data = json_encode($return);
                $response->headers->set('Content-Type', 'application/json');
                $response->setContent($data);
                return $response;

            }
            return new Response('Erreur');
        }
        return new Response('Erreur');
    }

//send monthly mails


    public function sendMail($data)
    {
        $em = $this->getDoctrine()->getManager();

        $urlFormulaire = $data[0];

        $message = \Swift_Message::newInstance()
            ->setSubject("Suivi hierarchie")
            ->setFrom('detachement@noz.fr')
            ->setTo($data[2])
            ->setBody($this->renderView(
            // app/Resources/views/Emails/registration.html.twig
                $data[1], array('url' => $urlFormulaire, 'nom' => $data[3])
            ),
                'text/html'
            );
        $this->get('mailer')->send($message);

        return 'send monthly mails';

    }
    //send mail to remind manager to complete the assignment


    /**
     * @Route("/sendMailRelancer")
     * @Template()
     */
    public function sendMailRelancerAction()
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();


        if ($request->get('data') != '') {
            $data = $request->get('data');
            if (in_array($this->get('kernel')->getEnvironment(), array('test', 'prod'))) {
                $urlFormulaire = $this->container->getParameter('url_serveur') . $this->container->getParameter('url_det_hier_prod') . $data;
                $viewmail = 'SuiviHierarchieBundle:MailProd:mail.html.twig';
                $mailProd = $this->container->getParameter('mailThierry_prod');
            } else {
                $urlFormulaire = $this->container->getParameter('url_serveur_recette') . $this->container->getParameter('url_det_hier_dev') . $data;
                $viewmail = 'SuiviHierarchieBundle:Mail:mail.html.twig';
                $mailProd = $this->container->getParameter('mailThierry_dev');
            }

            //$urlFormulaire='srv-web/applications_li/web/app_dev.php/det_hie/collaborateur/'.$data;
            $salarie = $em->getRepository('SuiviHierarchieBundle:DetSalarie')->findBy(array('id' => $data));

            $salarie[0]->setRelancer(0);
            $em->persist($salarie[0]);
            $em->flush();

            //delete detachements
            $mois = $salarie[0]->getMois();

            $employes = $em->getRepository('SuiviHierarchieBundle:DetSalarie')->findBy(array('referent' => $salarie[0]->getSalarie(), 'mois' => $mois));

            foreach ($employes as $employe) {
                if ($employe->getDetacher() == 1) {
                    $employe->setDetacher(0);
                    $em->flush();
                    $detachements = $em->getRepository('SuiviHierarchieBundle:DetDetachement')->findBy(array('salarie' => $employe));
                    foreach ($detachements as $detachement) {
                        $em->remove($detachement);
                    }
                    $em->flush();
                }
            }


            $sql = "SELECT *
				FROM pandore.Individu t where t.matricule= " . $salarie[0]->getSalarie()->getId() . "";
            $query = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
            $query->execute();
            $managers = $query->fetchAll();
            $user = $em->getRepository('UserUserBundle:User')->findBy(array('givenname' => $managers[0]['prenom'], 'surname' => $managers[0]['nom']));

            if (count($user) == 1) {
                $userdefin = '';
                $donnee = $user[0]->getEmail();
            } else {
                $userdefin = $managers[0]['prenom'] . $managers[0]['nom'];
                $donnee = $mailProd;
            }

            $message = \Swift_Message::newInstance()
                ->setSubject("Suivi hierarchie")
                ->setFrom('detachement@noz.fr')
                ->setTo($donnee)
                ->setBody($this->renderView(
                // app/Resources/views/Emails/registration.html.twig
                    $viewmail, array('url' => $urlFormulaire, 'nom' => $userdefin)
                ),
                    'text/html'
                );

            // var_dump($message);die;
            $this->get('mailer')->send($message);

            $return = "success";
            $response = new Response();
            $data = json_encode($return);
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent($data);
            return $response;
            //return new Response('send mail to remind manager to complete the assignment');
        }
        return new Response('Erreur');
    }

}
