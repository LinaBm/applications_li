<?php

namespace SuiviHierarchieBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints\False;
use SuiviHierarchieBundle\Entity\DetSalarie;
use Symfony\Component\Validator\Constraints\Null;

class AssignationController extends Controller
{
    /**
     * @Route("/collaborateur/{id}", name="collab")
     * @Template()
     */
    public function indexAction($id)
    {



        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $form = $this->get('form.factory')->createNamedBuilder('', 'form', null, array())
            ->add('Valider', 'submit', array('attr'=>array('class'=>'btn btn-info pull-right')))
            ->add('textareaComment', 'textarea', array('required'=>false,'mapped' => false, 'label' => 'Commentaires', 'attr'=>array('class'=>'form-control','style'=>'
    height: 80px;
    width: auto;')))
            ->add('libelle', 'entity', array(
                'class' => 'AdministrationSocieteBundle:Societe',
                'query_builder' => function (EntityRepository $em) {
                    return $em->createQueryBuilder('p')
                        ->orderBy('p.libelle', 'ASC');
                },
                'property' => 'libelle',
                'required' => false,
                'empty_value' => 'Choisissez une société',

            ))
            ->add('attester', 'checkbox', array('mapped' => true, 'label' => 'J\'atteste que les renseignements fournis sont exacts','required'=>false, 'attr'=>array('unchecked'=>'unchecked') ))

            ->getForm();

        $result= array();
        $date= $em->createQuery('SELECT max(t.mois)
                FROM SuiviHierarchieBundle:DetSalarie t
                WHERE t.id =:id
                ')
            ->setParameter('id', $id)
            ->getResult();
      // var_dump($date);die;
        $manager= $em->getRepository('SuiviHierarchieBundle:DetSalarie')->findBy(array('id' => $id, 'mois' => $date[0]));
if($manager[0]->getRelancer() == '1'){
    $this->get('session')->getFlashBag()->add('myError', 'Vous avez déjà rempli les detachements!!');
    return $this->render('SuiviHierarchieBundle:Default:referent.html.twig');
}

        $manag['nom']=$manager[0]->getSalarie()->getNom().' '.$manager[0]->getSalarie()->getPrenom();
        $manag['societe']=$manager[0]->getSociete()->getId();
        $societe= $em->createQuery('SELECT t FROM AdministrationSocieteBundle:Societe t WHERE t.reference =:id')
            ->setParameter('id' ,$manag['societe'])
            ->getResult();
       // var_dump($societe);die;
      if(count($societe)==0){
          $this->get('session')->getFlashBag()->add('myError', 'Ce n\'est pas une société !!');
            return $this->render('SuiviHierarchieBundle:Default:referent.html.twig');
        }
        $manag['societe_libelle']=$manager[0]->getSociete()->getRaison();
        $manag['idsociete']=$societe[0]->getId();
        $manag['managId']=$id;
      //var_dump($societe);die;

        $salaries = $em->getRepository('SuiviHierarchieBundle:DetSalarie')->findBy(array('referent' => $manager[0]->getSalarie()->getId(), 'mois' => $manager[0]->getmois(),'societe'=>$manager[0]->getSociete()));
        //$result=$manager->getSalarie()->getId();

        $y = 0;
        foreach ($salaries as $salarie) {
            $contrats=$em->getRepository('AdministrationSocieteBundle:PandoreContrat')->findBy(array('user' => $salarie->getSalarie()));
          foreach($contrats as $contrat){
            $result[$y]['id']=$salarie->getSalarie()->getId();
            $result[$y]['idcont']=$contrat->getId();
            $result[$y]['nom'] = $salarie->getSalarie()->getId() . ' ' . $salarie->getSalarie()->getNom() . ' ' . $salarie->getSalarie()->getPrenom();
            $result[$y]['societe'] = $salarie->getSociete()->getRaison();
            $result[$y]['debut'] = $salarie->getMois()->format('Y-m-d');
            $result[$y]['com_rh'] = $salarie->getCommentaireRh();

    $result[$y]['contrat']=$contrat->getTypeContrat();
              $datesortie=$contrat->getDateSortie() ;
              $datefincontrat=$contrat->getDateFin();
    if (strpos($datesortie,'-') == false && $datesortie != ''){
                  $a =substr($contrat->getDateSortie(),0,4);
                  $b =substr($contrat->getDateSortie(),4,2);
                  $c =substr($contrat->getDateSortie(),6,2);
                  $res = $a.'-'.$b.'-'.$c;
        $datesortie = $res;
          }
              if (strpos($datefincontrat,'-') == false && $datefincontrat != ''){
                  $a =substr($contrat->getDateFincont(),0,4);
                  $b =substr($contrat->getDateFincont(),4,2);
                  $c =substr($contrat->getDateFincont(),6,2);
                  $res = $a.'-'.$b.'-'.$c;
                  $datefincontrat = $res;
              }

              if($datesortie != '') {
                  if (date('Y-m-d', strtotime($result[$y]['debut'] . ' + 1 months')) < $datesortie) {
                      $result[$y]['fin'] = date('Y-m-d', strtotime($result[$y]['debut'] . ' + 1 months'));
                      $y++;
                  } else if ((date('Y-m-d', strtotime($result[$y]['debut'] . ' + 1 months')) > $datesortie) && ($result[$y]['debut'] < $datesortie)) {

                      $result[$y]['fin'] = $datesortie;
                      $y++;
                  }  else {
                      $result[$y]['debut'] = 'fin contrat';
                      $result[$y]['fin'] = 'fin contrat';

                  }

              }elseif ($datefincontrat !=''){
                  if (date('Y-m-d', strtotime($result[$y]['debut'] . ' + 1 months')) < $datefincontrat) {
                      $result[$y]['fin'] = date('Y-m-d', strtotime($result[$y]['debut'] . ' + 1 months'));
                      $y++;
                  } else if ((date('Y-m-d', strtotime($result[$y]['debut'] . ' + 1 months')) > $datefincontrat) && ($result[$y]['debut'] < $datefincontrat)) {

                      $result[$y]['fin'] = $datesortie;
                      $y++;
                  }
                  else {
                      $result[$y]['debut'] = 'fin contrat';
                      $result[$y]['fin'] = 'fin contrat';

                  }
              } else {
               $result[$y]['fin'] = date('Y-m-d', strtotime($result[$y]['debut'] . ' + 1 months'));
               $y++;
                }


}
        }
        //var_dump($result);die;
        return $this->render('SuiviHierarchieBundle:Default:admin.html.twig', array(
            'form' => $form->createView(), 'result'=>$result, 'manag'=>$manag));
    }

}
