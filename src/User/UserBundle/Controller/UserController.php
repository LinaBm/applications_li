<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace User\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserController extends Controller {

    public function ajaxsuiviAction(Request $request) {
        $listeUser = $this->getDoctrine()->getManager()
                ->getRepository('UserUserBundle:User')
                ->findAll();
        $data = array();
        $i = 0;
        foreach ($listeUser as $user) {
            $data[$i] = array("user" => $user->getDisplayname());
            $i++;
        }
        return new JsonResponse($data);
    }

    public function ajaxtechnicienAction(Request $request) {
        $listeUser = $this->getDoctrine()->getManager()
                ->getRepository('UserUserBundle:User')
                ->findByRoles('ROLE_TECH');
        $data = array();
        $i = 0;
        foreach ($listeUser as $user) {
            $data[$i] = array("user" => $user->getDisplayname());
            $i++;
        }
        return new JsonResponse($data);
    }

    protected function allowedTechnicians() {
        $techArray = $this->getDoctrine()->getManager()
                ->getRepository('UserUserBundle:User')
                ->listMailsByRole('ROLE_CHANGE_ROLES');

        $userEmail = $this->getUser()->getEmail();
        $isAllowed = false;
        foreach ($techArray as $tech) {
            if ($tech['email'] == $userEmail) {
                $isAllowed = true;
            }
        }
        return $isAllowed;
    }

    public function listAction(Request $request) {
        $cookies = $request->cookies;
        if ($this->allowedTechnicians() == false) {
            return $this->redirect($this->generateUrl('suivi_etudes_accueil'));
        }
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('UserUserBundle:User')->findAllByName();
        $securityRoles = $this->container->getParameter('security.role_hierarchy.roles');
        if ($request->isMethod('POST')) {
            $userId = $request->request->get('user');
            $roles = $request->request->get('role');
            $user = $em->getRepository('UserUserBundle:User')->find($userId);
            if ($user && $roles) {
                $userRoles = $user->getRoles();
                // remove user roles
                foreach ($userRoles as $role) {
                    $user->removeRole($role);
                }
                // add new roles 
                foreach ($roles as $role) {
                    $user->addRole($role);
                }
                $user->setLocked(1);
                $em->persist($user);
                $em->flush();
                return $this->redirect($this->generateUrl('user_list'));
            }
        }
        return $this->render('UserUserBundle:User:list.html.twig', array(
                    'users' => $users,
                    'roles' => $securityRoles,
        ));
    }

    public function loadUserRolesAction($userId) {
        if ($this->allowedTechnicians() == false) {
            return $this->redirect($this->generateUrl('suivi_etudes_accueil'));
        }
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserUserBundle:User')->find($userId);
        return new JsonResponse($user->getRoles());
    }

}
