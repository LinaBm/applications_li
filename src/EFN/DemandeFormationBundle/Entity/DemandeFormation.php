<?php

namespace EFN\DemandeFormationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use EFN\DemandeFormationBundle\Entity\TypeDemande;
use Administration\DirectionBundle\Entity\Direction;
use Administration\DirectionBundle\Entity\Service;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * DemandeFormation
 *
 * @ORM\Table(name="efn_demande")
 * @ORM\Entity(repositoryClass="EFN\DemandeFormationBundle\Entity\DemandeFormationRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Assert\Callback(methods={"validateData"})
 */
class DemandeFormation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="\Administration\DirectionBundle\Entity\Direction")
	 * @ORM\JoinColumn(nullable=false)
	 * @Assert\NotBlank(message="La direction doit être renseignée.")
	 */
    private $direction;

	/**
	 * @ORM\ManyToOne(targetEntity="\Administration\DirectionBundle\Entity\Service")
	 * @ORM\JoinColumn(nullable=false)
	 * @Assert\NotBlank(message="Le service doit être renseigné.")
	 */
    private $service;

    /**
     * @var string
     *
     * @ORM\Column(name="nomResponsable", type="string", length=255)
	 * @Assert\NotBlank(message="Le nom du responsable doit être renseigné.")
     */
    private $nomResponsable;

    /**
     * @var string
     *
     * @ORM\Column(name="prenomResponsable", type="string", length=255)
	 * @Assert\NotBlank(message="Le prénom du responsable doit être renseigné.")
     */
    private $prenomResponsable;

    /**
     * @var string
     *
     * @ORM\Column(name="fonctionResponsable", type="string", length=255)
	 * @Assert\NotBlank(message="La fonction du responsable doit être renseignée.")
     */
    private $fonctionResponsable;

    /**
     * @var string
     *
     * @ORM\Column(name="emailResponsable", type="string", length=255)
	 * @Assert\NotBlank(message="L'adresse email du responsable doit être renseignée.")
	 * @Assert\Email(message="L'adresse mail du responsable n'est pas une adresse mail valide.")
     */
    private $emailResponsable;

    /**
     * @var string
     *
     * @ORM\Column(name="telephoneResponsable", type="string", length=10, nullable=true)
     */
    private $telephoneResponsable;
    
    /**
     * @ORM\ManyToOne(targetEntity="EFN\DemandeFormationBundle\Entity\TypeDemande")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="Vous devez choisir un type de demande.")
     */
	private $type;
	
	/**
	 * @ORM\Column(name="description", type="text", nullable=true)
	 */
	private $description;

	/**
	 * @ORM\ManyToOne(targetEntity="EFN\DemandeFormationBundle\Entity\Formation")
	 * @ORM\JoinColumn(nullable=true)
	 */
	private $formation;

	/**
	 * @ORM\Column(name="date", type="datetime")
	 */
	private $date;
	
	/**
	 * @ORM\ManyToOne(targetEntity="EFN\DemandeFormationBundle\Entity\Priorite")
	 * @ORM\JoinColumn(nullable=true)
	 */
	private $delais;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="lieu", type="string", nullable=true)
	 */
	private $lieu;
	
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set direction
     *
     * @param Administration\DirectionBundle\Entity\Direction $direction
     * @return DemandeFormation
     */
    public function setDirection(\Administration\DirectionBundle\Entity\Direction $direction)
    {
        $this->direction = $direction;
    
        return $this;
    }

    /**
     * Get direction
     *
     * @return Administration\DirectionBundle\Entity\Direction 
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * Set service
     *
     * @param Administration\DirectionBundle\Entity\Service $service
     * @return DemandeFormation
     */
    public function setService(\Administration\DirectionBundle\Entity\Service $service)
    {
        $this->service = $service;
    
        return $this;
    }

    /**
     * Get service
     *
     * @return Administration\DirectionBundle\Entity\Service 
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set nomResponsable
     *
     * @param string $nomResponsable
     * @return DemandeFormation
     */
    public function setNomResponsable($nomResponsable)
    {
        $this->nomResponsable = $nomResponsable;
    
        return $this;
    }

    /**
     * Get nomResponsable
     *
     * @return string 
     */
    public function getNomResponsable()
    {
        return $this->nomResponsable;
    }

    /**
     * Set prenomResponsable
     *
     * @param string $prenomResponsable
     * @return DemandeFormation
     */
    public function setPrenomResponsable($prenomResponsable)
    {
        $this->prenomResponsable = $prenomResponsable;
    
        return $this;
    }

    /**
     * Get prenomResponsable
     *
     * @return string 
     */
    public function getPrenomResponsable()
    {
        return $this->prenomResponsable;
    }

    /**
     * Set fonctionResponsable
     *
     * @param string $fonctionResponsable
     * @return DemandeFormation
     */
    public function setFonctionResponsable($fonctionResponsable)
    {
        $this->fonctionResponsable = $fonctionResponsable;
    
        return $this;
    }

    /**
     * Get fonctionResponsable
     *
     * @return string 
     */
    public function getFonctionResponsable()
    {
        return $this->fonctionResponsable;
    }

    /**
     * Set emailResponsable
     *
     * @param string $emailResponsable
     * @return DemandeFormation
     */
    public function setEmailResponsable($emailResponsable)
    {
        $this->emailResponsable = $emailResponsable;
    
        return $this;
    }

    /**
     * Get emailResponsable
     *
     * @return string 
     */
    public function getEmailResponsable()
    {
        return $this->emailResponsable;
    }

    /**
     * Set telephoneResponsable
     *
     * @param string $telephoneResponsable
     * @return DemandeFormation
     */
    public function setTelephoneResponsable($telephoneResponsable)
    {
        $this->telephoneResponsable = $telephoneResponsable;
    
        return $this;
    }

    /**
     * Get telephoneResponsable
     *
     * @return string 
     */
    public function getTelephoneResponsable()
    {
        return $this->telephoneResponsable;
    }

    /**
     * Set type
     *
     * @param \EFN\DemandeFormationBundle\Entity\TypeDemande $type
     * @return DemandeFormation
     */
    public function setType(\EFN\DemandeFormationBundle\Entity\TypeDemande $type = null)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return \EFN\DemandeFormationBundle\Entity\TypeDemande 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return DemandeFormation
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set formation
     *
     * @param \EFN\DemandeFormationBundle\Entity\Formation $formation
     * @return DemandeFormation
     */
    public function setFormation(\EFN\DemandeFormationBundle\Entity\Formation $formation = null)
    {
        $this->formation = $formation;
    
        return $this;
    }

    /**
     * Get formation
     *
     * @return \EFN\DemandeFormationBundle\Entity\Formation 
     */
    public function getFormation()
    {
        return $this->formation;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return DemandeFormation
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }



    /**
     * Set delais
     *
     * @param \EFN\DemandeFormationBundle\Entity\Priorite $delais
     * @return DemandeFormation
     */
    public function setDelais(\EFN\DemandeFormationBundle\Entity\Priorite $delais = null)
    {
        $this->delais = $delais;
    
        return $this;
    }

    /**
     * Get delais
     *
     * @return \EFN\DemandeFormationBundle\Entity\Priorite 
     */
    public function getDelais()
    {
        return $this->delais;
    }

    /**
     * Set lieu
     *
     * @param string $lieu
     * @return DemandeFormation
     */
    public function setLieu($lieu)
    {
        $this->lieu = $lieu;
    
        return $this;
    }

    /**
     * Get lieu
     *
     * @return string 
     */
    public function getLieu()
    {
        return $this->lieu;
    }
    
    /**
     * @Assert\Callback
     */
    public function validateData(ExecutionContextInterface $context)
    {
    	if ($this->getType()){
    	if (($this->getType()->getId()== TypeDemande::FORMATION_EXISTANTE) &&!($this->getFormation()))
    	{
    		$context->addViolationAt(
    				'formation',
    				'Vous devez renseigner une formation.',
    				array(),
    				null
    		);
    	}
    	if (($this->getType()->getId()!= TypeDemande::FORMATION_EXISTANTE) &&!($this->getDescription()))
    	{
    		$context->addViolationAt(
    				'description',
    				'Vous devez décrire votre demande.',
    				array(),
    				null
    		);    		
    	}
    }}
}
