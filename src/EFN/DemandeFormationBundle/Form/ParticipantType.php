<?php

namespace EFN\DemandeFormationBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;



class ParticipantType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			
			->add('nom', 'text')
				
			->add('prenom', 'text')

			->add('fonction', 'text', array(
					'required'=> false,					
					))

			->add('email', 'text')

			->add('telephone', 'text', array(
					'required'=> false,					
			))
			
			->add('societe', 'entity', array(
					'class'    => 'AdministrationSocieteBundle:Societe',
					'property' => 'libelle',
					'empty_value' => '',
			))
		;
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
				'data_class' => 'EFN\DemandeFormationBundle\Entity\Participant'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'efn_participant_demande';
	}
}