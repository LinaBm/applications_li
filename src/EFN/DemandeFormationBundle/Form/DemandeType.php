<?php

namespace EFN\DemandeFormationBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;



class DemandeType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
	
			->add('direction', 'entity', array(
					'class'    => 'AdministrationDirectionBundle:Direction',
					'property' => 'libelle',
					'empty_value' => '',
				))
	
			->add('service', 'entity', array(
					'class'    => 'AdministrationDirectionBundle:Service',
					'property' => 'libelle',
					'empty_value' => '',
			))
			
			->add('nomResponsable', 'text')
				
			->add('prenomResponsable', 'text')

			->add('fonctionResponsable', 'text')

			->add('emailResponsable', 'text')

			->add('telephoneResponsable', 'text', array(
					'required'=> false,					
					))
			
			->add('type', 'entity', array(
					'class'=> 'EFNDemandeFormationBundle:TypeDemande',
					'property' => 'libelle',
					'empty_value' => '',					
					))
					
			->add('delais', 'entity', array(
					'class'=> 'EFNDemandeFormationBundle:Priorite',
					'property' => 'libelle',
					'empty_value' => '',
					))
					
			->add('description', 'textarea', array(
					'required'=> false,					
					))			

			->add('formation', 'entity', array(
					'class'=> 'EFNDemandeFormationBundle:Formation',
					'empty_value' => '',
					'required'=> false,
			))			

			->add('participants', 'collection', array(
					'type'         => new ParticipantType(),
					'allow_add'    => true,
					'allow_delete' => true,
					'mapped'	   => false,
					'by_reference' => false,
					'required'		=> false,
					'label_attr'	=>array(
							'style'		=>'display: none;' 
			)))
			
			->add('Ajouter', 'submit', array(
					'label'	=>	'Envoyer votre demande',
			))
		;
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
				'data_class' => 'EFN\DemandeFormationBundle\Entity\DemandeFormation'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'efn_demandeformation_demande';
	}
}