<?php

namespace EFN\DemandeFormationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use EFN\DemandeFormationBundle\Entity\DemandeFormation;
use EFN\DemandeFormationBundle\Form\DemandeType;
use EFN\DemandeFormationBundle\Entity\TypeDemande;
use Symfony\Component\Form\FormError;

class DemandeController extends Controller
{
	
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $demande = new DemandeFormation();
        $demande->setPrenomResponsable($this->getUser()->getGivenName());
        $demande->setNomResponsable($this->getUser()->getSurname());
        $demande->setEmailResponsable($this->getUser()->getEmail());
        $form = $this->createForm(new DemandeType, $demande);
        $form=$form->handleRequest($request);
        $validator = $this->get('validator');
        if($form->get('participants')->getData()){
	        foreach($form->get('participants')->getData() as $participant){
	        	if ($participant){
	        		$listErrors = $validator->validate($participant);
	        	}
	        }
        }
        if ($form->isValid() and count($listErrors)==0) {
        	$demande->setDate(new \DateTime());
        	$em->persist($demande);
        	$em->flush();
	        foreach($form->get('participants')->getData() as $participant){
	        	if ($participant){
		        	$participant->setFormation($demande);
		        	$em->persist($participant);
		        	$em->flush();
	        	}
	        }     		
        	$request->getSession()->getFlashBag()->add('success', 'Demande bien enregistrée.');
        	/* on envoie un mail récapitulatif à l'EFN */
        	$sujet="Création d'une demande d'intervention";
        	if ($this->container->get('kernel')->getEnvironment()!='prod'){
        		$sujet= '[TEST]'.$sujet;
        	}
        	$messageEFN = \Swift_Message::newInstance()
        	->setSubject($sujet)
        	->setFrom('efn-web@noz.fr')
        	->setTo('formation@noz.fr')
        	->setBcc('poleweb@noz.fr')
        	->setContentType('text/html')
        	->setBody($this->renderView('EFNDemandeFormationBundle:Demande:mailEFN.html.twig', array(
           			'demande' => $demande,
        			'participants' =>  $form->get('participants')->getData(),
        			'environnement'=>$this->container->get('kernel')->getEnvironment(),
        	)));
        	$this->get('mailer')->send($messageEFN);
			/* on envoie un mail récapitulatif au demandeur */
        	$sujet="Création de votre demande d'intervention";
        	if ($this->container->get('kernel')->getEnvironment()!='prod'){
        		$sujet= '[TEST]'.$sujet;
        	}
        	$messageDemandeur = \Swift_Message::newInstance()
        	->setSubject($sujet)
        	->setFrom('efn-web@noz.fr')
        	->setTo($demande->getEmailResponsable())
        	->setBcc('poleweb@noz.fr')
        	->setContentType('text/html')
        	->setBody($this->renderView('EFNDemandeFormationBundle:Demande:mailDemandeur.html.twig', array(
        			'demande' => $demande,
        			'participants' =>  $form->get('participants')->getData(),
        			'environnement'=>$this->container->get('kernel')->getEnvironment(),
        	)));
        	$this->get('mailer')->send($messageDemandeur); 
        	return $this->render('EFNDemandeFormationBundle:Demande:voir.html.twig', array(
           			'demande' => $demande,
        			'participants' =>  $form->get('participants')->getData(),
        	));
        } else {
        	if (isset($listErrors)){
        		foreach ($listErrors as $error){
        			$form->addError(new FormError($error->getMessage()));
        		}        		
			}
        }
        
        return $this->render('EFNDemandeFormationBundle:Demande:ajouter.html.twig', array(
        		'form'           => $form->createView(),
        ));
    }
}
