function getArret(idArret, getArret, getFilteredArrets){
	$('input:radio').each(function(){
		if($(this).attr('name') != idArret){
			$(this).prop('checked', false);
		}else{
			$(this).prop('checked', true);
		}
	});
	$.ajax({
		dataType: "html",
		type: "POST",
		url: url_get_arret,
		data: { idArretDeTravail : parseInt(idArret)},  
		async:false,
		cache: false,
		error: function(jqXHR,textStatus, errorThrown ) {
		},
		success: function(arret){
			$('#detailSociete').html(arret);
		},
		complete: function(){
			$("#loader").hide();
		}                                                                               
	});
}


/* this founction can be called after the user made a search, or at the opening
 * opening = 1 at the opening, fromsearch=0 if the user enter criterions. */ 
function getFilteredArrets(page, opening){
	var datain = $('form[name="search_arret_travail"]').serialize();
	datain = datain + '&page=' + page + '&opening=' + opening;
	$("#loader").show();
	$.ajax({
		type: "POST",
		data: datain,
		url: url_arrets_filtered
		})
		.done(function( msg ) {
			$('#filteredArrets').html(msg);
			$("#loader").hide();
		});
}

$(function(){
	$("#ajouterArret").on("hide.bs.modal", function(){
		$("#ajouterArretModalBody").html("");
	});
	if(selected > 0){
		getArret(selected);
		getFilteredArrets('0', 1);
	}else{
		$("#loader").show();
		getFilteredArrets('0', 1);
	}
	$('#nomSalarie, #prenomSalarie, #codeSociete, #libelleSociete, #gestPaie, #gestRH, #tranche, #isTraite').change(function(){
		$('#detailArret').html('');
		getFilteredArrets('0', 0);
	});
	
	 $('.widgetSearch').change(function(){
		 getFilteredArrets('0', 0);
		 });
	
});