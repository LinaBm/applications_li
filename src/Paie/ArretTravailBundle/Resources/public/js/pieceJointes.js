$(document).ready(function() {
      
	$(function () {
		$('[data-toggle="tooltip"]').tooltip();
    });


	// On récupère la balise <div> en question qui contient l'attribut « data-prototype » qui nous intéresse.
	var $container = $('.div_piecejointes');
    
	// On ajoute des liens pour ajouter une nouvelle pièce jointe
	var $addLink = $('<a href="#" id="add_piecejointe" class="btn btn-default">Ajouter un document</a>');
	$container.append($addLink);
    
	// On ajoute un nouveau champ à chaque clic sur un lien d'ajout.
	$addLink.click(function(e) {
		addPieceJointe($container);
		e.preventDefault(); // évite qu'un # apparaisse dans l'URL
		return false;
	});

	// On définit un compteur unique pour nommer les champs qu'on va ajouter dynamiquement
	var index = $container.find(':input').length;
	
	// Pour chaque pièce jointe déjà existante, on ajoute un lien de suppression
	$container.children('div').each(function() {
		addDeleteLink($(this));
	});
    
	// La fonction qui ajoute un formulaire Categorie
	function addPieceJointe($container) {
	    // Dans le contenu de l'attribut « data-prototype », on remplace :
		// - le texte "__name__label__" qu'il contient par le label du champ
		// - le texte "__name__" qu'il contient par le numéro du champ
	    var $prototype = $($container.attr('data-prototype').replace(/__name__label__/g, '')
	    		.replace(/__name__/g, index++));
	    // On ajoute au prototype un lien pour pouvoir supprimer la pièce jointe
	    addDeleteLink($prototype);
	    // On ajoute les prototype modifié à la fin de leur balise <div>
	    $container.append($prototype);
	    // Enfin, on incrémente les compteurs pour que les prochains ajouts se fassent avec un autre numéro
	    index++;
	}

	// La fonction qui ajoute un lien de suppression d'une catégorie
	function addDeleteLink($prototype) {
		// Création du lien
	    $deleteLink = $('<a href="#" class="">Supprimer</a>');
	    // Ajout du lien
		$prototype.append($deleteLink);
		
		// Ajout du listener sur le clic du lien
		$deleteLink.click(function(e) {
			$prototype.remove();
		    e.preventDefault(); // évite qu'un # apparaisse dans l'URL
		    return false;
		});
    }
});
