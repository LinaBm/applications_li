$(function(){
	$('tr').each(function(){
		if($(this).attr('id') == selected){
			$(this).find('td').find('input:radio').prop('checked', true);
		}
	});
	
	$('.arretDeTravail').click(function() {
		var selectedId = $(this).parent('tr').attr('id');
		$("#loader").show();
		$('input:radio').each(function(){
			if($(this).attr('name') != selectedId){
				$(this).prop('checked', false);
			}else{
				$(this).prop('checked', true);
			}
		});
		$.ajax({
			type: "POST",
			url: url_get_arret,
			data: { idArretDeTravail: selectedId },
			success: function(arret){
				$('#detailArret').html(arret);
			},
			complete: function(){
				$("#loader").hide();
			}      
		});
	});	
	
	$('#ajouterArretBttn').click(function(){
		$('#detailSociete').html('');
		$("#loader").show();
		$.ajax({
			type: "POST",
			url: url_add_arret,
			data: { idArretDeTravail: '0' },
			success: function(arret){
				$('#ajouterArret').modal('show');
				$('#ajouterArretModalBody').html(arret);
			},
			complete: function(){
				$("#loader").hide();
			}      
		});
	});

	$('#exportArrets').click(function(){
		$("#loader").show();
		$.ajax({
			dataType: "text",
			type: "POST",
			url: url_arrets_export,
			async: false,
			cache: false,
			error: function(jqXHR,textStatus, errorThrown ) {
				alert(jqXHR.responseText); return false;
			},
			success: function(data){
			},
			complete: function(){
				$("#loader").hide();
			}                                                                            
		});
	});	
	
	$("#listApplications").tablesorter(); 
	
	$('#actualiserArrets').click(function(){
		getFilteredArrets();
	});
	
	$('.pagination').on('click', ".page a, .previous a, .next a, .first a, .last a" , function(e){
		e.preventDefault();	
		var thisHref = $(this).attr('href');
		var page = thisHref.substring(thisHref.indexOf('page=')+5);
		getFilteredArrets(page);
	});	
});