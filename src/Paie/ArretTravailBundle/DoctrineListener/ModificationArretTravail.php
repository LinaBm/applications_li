<?php

namespace Paie\ArretTravailBundle\DoctrineListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Paie\ArretTravailBundle\Entity\ModificationArret;
use Paie\ArretTravailBundle\Entity\ArretTravail;


class ModificationArretTravail
{
	protected $container;
	private $modifs;
	
    public function __construct($container)
    {
        $this->container = $container;
    }
    
    /**
     * 
     * @param LifecycleEventArgs $args
     */
	public function preUpdate(LifecycleEventArgs $args)
	{
		$entity = $args->getEntity();
		$em = $args->getEntityManager();

		if ($entity instanceof ArretTravail){
			$i=0;
			$listeModif=array();		
			foreach ($args->getEntityChangeSet() as $colonne)
			{
                                if ($colonne[0] instanceof \DateTime){$colonne[0] = $colonne[0]->format('Y-m-d H:i:s');}
                                if ($colonne[1] instanceof \DateTime){$colonne[1] = $colonne[1]->format('Y-m-d H:i:s');}
				$modification = new ModificationArret();
				$modification->setDate(new \DateTime());
				$modification->setType(array_keys($args->getEntityChangeSet())[$i]);
				$modification->setAncienneValeur($colonne[0]);
				$modification->setNouvelleValeur($colonne[1]);
				$modification->setUser($this->container->get('security.context')->getToken()->getUser());
				$modification->setArretTravail($entity);
				$listeModif[$i] = $modification;
				$i++;
				$this->modif=$modification;
			}
			$this->modifs=$listeModif;
		}
	}
	public function postUpdate(LifecycleEventArgs $args){
		$entity = $args->getEntity();
		$em = $args->getEntityManager();
			if ($entity instanceof ArretTravail){
				foreach ($this->modifs as $colonne){
					$em->persist($colonne);
					$em->flush();
			}
		}		
	}
}