<?php

namespace Paie\ArretTravailBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Paie\ArretTravailBundle\Form\PiecejointeType;

class PiecesJointesType extends AbstractType
{	
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{			
		$builder
			->add('id', 'hidden')  
            ->add('piecejointes', 'collection', array(
                'type'         => new PiecejointeType(),
                'allow_add'    => true,
                'allow_delete' => true,
            	'mapped'=>false,
            ))
			->add('save', 'submit', array(
				'label' => 'Enregistrer',
				'attr' => array('class' => 'btn btn-info', 'aria-label' => 'Left Align', 'type' => 'button'),
			));
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Paie\ArretTravailBundle\Entity\ArretTravail'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'paie_pieces_jointes';
	}
}