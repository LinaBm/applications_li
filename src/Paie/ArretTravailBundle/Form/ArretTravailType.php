<?php

namespace Paie\ArretTravailBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use \Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\NotEqualTo;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\NotIdenticalTo;
use Symfony\Component\Validator\Constraints\Required;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Regex;
use \DateTime;
use Paie\ArretTravailBundle\Form\PiecejointeType;

class ArretTravailType extends AbstractType
{
	private $em;

	public function __construct($em){
		$this->em = $em;
		
	}

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('id', 'hidden')
			->add('arretInitial', 'hidden', array('mapped' => false))
			->add('gestPaie', 'entity',	array(
				'class' => 'User\UserBundle\Entity\User',
				'em' => 'default',
				'property' => 'displayname',
				'query_builder' => function (EntityRepository $er) {
					return $er->createQueryBuilder('u')
						->where('u.roles LIKE \'%ROLE_PAIE_GESTIONNAIRE%\'')
						->orderBy('u.displayname');	},
				'multiple' => false,
				'expanded' => false,
				'empty_value' => '',
				'required'  => false,
				'label' => 'Gest. Paie',
				'attr' => array('class' => 'chosen', 'data-placeholder' => 'choisir user', 'style' => 'width:350px;')
			))
			->add('gestRH',	'entity', array(
				'class' => 'User\UserBundle\Entity\User',
				'em' => 'default',
				'property' => 'displayname',
				'query_builder' => function (EntityRepository $er) {
					return $er->createQueryBuilder('u')
						->where('u.roles LIKE \'%ROLE_PAIE_RH%\'')
						->orderBy('u.displayname');	},
				'multiple' => false,
				'expanded' => false,
				'empty_value' => '',
				'required'  => false,
				'label' => 'Gest. RH',
				'attr' => array('class' => 'chosen', 'data-placeholder' => 'choisir user', 'style' => 'width:350px;')
			))
			->add('gestContrat', 'entity', array(
					'class' => 'User\UserBundle\Entity\User',
					'em' => 'default',
					'property' => 'displayname',
					'query_builder' => function (EntityRepository $er) {
						return $er->createQueryBuilder('u')
						->where('u.roles LIKE \'%ROLE_PAIE_CONTRAT%\'')
						->orderBy('u.displayname');	},
					'multiple' => false,
					'expanded' => false,
					'empty_value' => '',
					'required'  => false,
					'label' => 'Gest. RH',
					'attr' => array('class' => 'chosen', 'data-placeholder' => 'choisir user', 'style' => 'width:350px;')
			))
			->add('nature', 'entity', array(
					'class' => 'PaieArretTravailBundle:ArretNature',
					'property' => 'libelle',
					'empty_value' => '',
					'label'     => 'Nature',
					'constraints' => array(new NotBlank(array('message' => 'La nature ne peut pas être vide!'))),
						
			))
			->add('categorie', 'entity', array(
				'class' => 'PaieArretTravailBundle:ArretCategorie',
				'property' => 'libelle',
				'label'     => 'Categorie',
				'empty_value' => '',
				'required'  => false,
				'constraints' => array(new NotBlank(array('groups' => 'initial', 'message' => 'La categorie ne peut pas être vide!'))),
				'multiple' => false,
				'expanded' => false,
			))
			->add('dateDebut', 'date', array(
				'widget' => 'single_text',
				'format' => 'dd/MM/yyyy',
				'label' => 'Debut',
				'invalid_message' => 'la date debut n\'est pas valide',
				'constraints' => array(new NotBlank(array('message' => 'Date obligatoire!')))
			))
			->add('dateFin', 'date', array(
				'widget' => 'single_text',
				'format' => 'dd/MM/yyyy',
				'label' => 'Fin',
				'invalid_message' => 'la date fin n\'est pas valide'
			))
			->add('dateReception', 'date', array(
				'widget' => 'single_text',
				'format' => 'dd/MM/yyyy',
				'label' => 'Reception',
				'invalid_message' => 'la date reception n\'est pas valide',
				'constraints' => array(new NotBlank(array('message' => 'Date obligatoire!')))
			))
			->add('typeReception', 'choice', array(
				'choices'   => array(
					'1' => 'mail',
					'2' => 'papier',
				),
				'label'     => 'Type Reception',
				'required'  => true,
				'constraints' => array(new NotBlank(array('message' => 'Le type reception ne peut pas être vide!'))),
				'multiple' => false,
				'expanded' => false,
			))
			->add('commentaire', 'textarea', array(
				'required'  => false,
			))
            ->add('piecejointes', 'collection', array(
                'type'         => new PiecejointeType(),
                'allow_add'    => true,
                'allow_delete' => true,
				'prototype' => true,
				'by_reference' => false,
				'required' => false
            ))
            ->add('grossesse', 'checkbox', array(
            		'label'     => 'En rapport avec un état pathologique résultant de la grossesse',
            ))
			->add('save', 'submit', array(
				'label' => 'Enregistrer',
				'attr' => array('class' => 'btn btn-info', 'aria-label' => 'Left Align', 'type' => 'button'),
			));
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Paie\ArretTravailBundle\Entity\ArretTravail',
			'em' => 'default',
			'csrf_protection' => false,
            'validation_groups' => function(FormInterface $form) {
				$data = $form->getData();
				$arretInitial = $form->get('arretInitial')->getData();
				$result = array('Default');
				$categorie = $data->getCategorie();
				if ($data->getId() !== null || $arretInitial !== null) {
					$result[] = 'initial';
				}
				return $result;
			}
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'paie_arret_travail';
	}
}