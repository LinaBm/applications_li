<?php

namespace Paie\ArretTravailBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormInterface;
use Doctrine\ORM\EntityRepository;


class SearchArretType extends AbstractType
{	
	private $em;
	private $listeprenoms=array();
	private $listenoms=array();
	
	
	public function __construct($em){
		$this->em = $em;
		$prenoms = $em->createQueryBuilder()
			->select('i.prenom')
			->from('PaieArretTravailBundle:ArretTravail', 'a')
           	->innerJoin	('Administration\SocieteBundle\Entity\PandoreContrat', 'c', 'WITH', 'a.contrat = c.id')
           	->innerJoin	('Administration\SocieteBundle\Entity\PandoreIndividu', 'i', 'WITH', 'c.user = i.id')
			->distinct()
			->orderBy('i.prenom')
			->getQuery()
			->getResult();
		foreach ($prenoms as $prenom){
			$this->listeprenoms[$prenom['prenom']]=$prenom['prenom'];
		}
		$noms = $em->createQueryBuilder()
			->select('i.nom')
			->from('PaieArretTravailBundle:ArretTravail', 'a')
            ->innerJoin	('Administration\SocieteBundle\Entity\PandoreContrat', 'c', 'WITH', 'a.contrat = c.id')
            ->innerJoin	('Administration\SocieteBundle\Entity\PandoreIndividu', 'i', 'WITH', 'c.user = i.id')
			->distinct()
			->orderBy('i.nom')
			->getQuery()
			->getResult();
		foreach ($noms as $nom){
			$this->listenoms[$nom['nom']]=$nom['nom'];
		}		
	}
	

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('nomSalarie', 'choice', array(
				'choices'=>$this->listenoms,
				'required'  => false,
		))
		->add('prenomSalarie', 'choice', array(
				'choices'=>$this->listeprenoms,
				'required'  => false,
		))
		->add('libelle', 'entity',	array(
				'class' => 'Administration\SocieteBundle\Entity\PandoreSociete',
				'em' => 'default',
				'property' => 'raison',
				'query_builder' => function (EntityRepository $er) {
					return $er->createQueryBuilder('s')
					->orderBy('s.raison');	},
				'multiple' => false,
				'expanded' => false,
				'required'  => false,
		))
		->add('reference', 'entity',	array(
				'class' => 'Administration\SocieteBundle\Entity\PandoreSociete',
				'em' => 'default',
				'property' => 'id',
				'multiple' => false,
				'expanded' => false,
				'required'  => false,
		))		
		->add('gestPaie', 'entity',	array(
			'class' => 'User\UserBundle\Entity\User',
			'em' => 'default',
			'property' => 'displayname',
			'query_builder' => function (EntityRepository $er) {
				return $er->createQueryBuilder('u')
					->Join('PaieArretTravailBundle:ArretTravail', 'a')
					->where('a.gestPaie = u.id')
					->orderBy('u.displayname');	},
			'multiple' => false,
			'expanded' => false,
			'required'  => false,
		))
		->add('gestRH',	'entity', array(
			'class' => 'User\UserBundle\Entity\User',
			'em' => 'default',
			'property' => 'displayname',
			'query_builder' => function (EntityRepository $er) {
				return $er->createQueryBuilder('u')
					->Join('PaieArretTravailBundle:ArretTravail', 'a')
					->where('a.gestRH = u.id')
					->orderBy('u.displayname');	},
			'multiple' => false,
			'expanded' => false,
			'required'  => false,
		))
		->add('tranche', 'choice', array(
				'choices'=>array(null=>'', 1 => '< 15', 2 => 'entre 15 et 30', 3 => 'entre 30 et 45', 4 => '>= 45'),
				'required'  => false,
		))
			->add('isTraite', 'choice', array(
				'choices'=>array(null=>'', 0 => 'Non', 1 => 'Oui'),
				'required'  => false,
			));
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Paie\ArretTravailBundle\Entity\ArretTravail',
			'em' => 'default',
			'csrf_protection' => false,
            'validation_groups' => function(FormInterface $form) {
				$data = $form->getData();
				$result = array('Default');
				return $result;
			}
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'search_arret_travail';
	}
}