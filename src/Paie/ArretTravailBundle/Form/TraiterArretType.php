<?php

namespace Paie\ArretTravailBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Symfony\Component\Validator\Constraints\NotBlank;

class TraiterArretType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('dateTraitement', 'date', array(
				'widget' => 'single_text',
				'format' => 'dd/MM/yyyy',
				'label' => 'Date du traitement de l\'attestation de salaire',
				'required' => false,
				'data' => new \DateTime(),
				'invalid_message' => 'la date n\'est pas valide',
				'constraints' => array(new NotBlank(array('message' => 'Date obligatoire!')))
			))
			
			->add('save', 'submit', array(
				'label' => 'Enregistrer',
				'attr' => array('class' => 'btn btn-info', 'aria-label' => 'Left Align', 'type' => 'button'),
			))			
			->getForm();
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Paie\ArretTravailBundle\Entity\ArretTravail'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'traiter_arret';
	}
}
