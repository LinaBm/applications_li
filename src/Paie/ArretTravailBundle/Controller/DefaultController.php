<?php

namespace Paie\ArretTravailBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Ob\HighchartsBundle\Highcharts\Highchart;
use \DateTime;
use \DateInterval;
use Paie\ArretTravailBundle\Entity\ArretTravail;
use Paie\ArretTravailBundle\Form\ArretTravailType;
use Paie\ArretTravailBundle\Form\AddArretTravailType;
use Paie\ArretTravailBundle\Form\TraiterArretType;
use Paie\ArretTravailBundle\Form\PiecesJointesType;
use Proxies\__CG__\Administration\SocieteBundle\Entity\PandoreIndividu;
use Paie\ArretTravailBundle\PaieArretTravailBundle;
use Paie\ArretTravailBundle\Entity\ArretNature;
use Paie\ArretTravailBundle\Entity\ArretCategorie;
use Paie\ArretTravailBundle\Form\SearchArretType;

class DefaultController extends Controller {

	/**
	 * @Route("/reporting/{annee}", name="paie_arret_de_travail_reporting")
	 * @Template()
	 */
	public function reportingAction($annee = null) {
		if (!$this->getUser()->isGranted("ROLE_PAIE_MANAGER")) {
			return $this->redirect($this->generateUrl('paie_arrets_de_travail'));
		}
		$listeMois = array('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'aout', 'septembre', 'octobre', 'novembre', 'décembre');
		if ($annee === null) {
			$annee = date('Y');
		}
		$em = $this->getDoctrine()->getManager();
		$data = array();
		$data[0]['name'] = '> 45 jours';
		$data[0]['color'] = '#ff0000';
		$data[1]['name'] = 'entre 30 et 45 jours';
		$data[1]['color'] = '#BC1717';
		$data[2]['name'] = 'entre 15 et 30 jours';
		$data[2]['color'] = '#ff7518';
		$data[3]['name'] = '< 15 jours';
		$data[3]['color'] = '#18B738';
		for ($i = 1; $i < 12; $i++) {
			$data[0]['data'][$i - 1] = 0;
			$data[1]['data'][$i - 1] = 0;
			$data[2]['data'][$i - 1] = 0;
			$data[3]['data'][$i - 1] = 0;
			$format = "%d-%'02d";
			$listeArret = $em->getRepository('PaieArretTravailBundle:ArretTravail')->getNbArretsThisMonth(sprintf($format, $annee, $i));
			foreach ($listeArret as $arret) {
				if ($arret->getTempsTraitement() < 15) {
					$data[3]['data'][$i - 1] ++;
				} else if ($arret->getTempsTraitement() < 30) {
					$data[2]['data'][$i - 1] ++;
				} else if ($arret->getTempsTraitement() < 45) {
					$data[1]['data'][$i - 1] ++;
				} else {
					$data[0]['data'][$i - 1] ++;
				}
			}
		}
		//Création du graphique
		$column = new Highchart();
		//Identification du conteneur HTML
		$column->chart->renderTo('barchart');
		//Titre du graphique
		$column->title->text("nombre d'arrêt de travail traité par mois");
		//Type de graphique
		$column->chart->type('column');
		//Options 3D
		$column->chart->options3d(
				array(
					'enabled' => true,
					'alpha' => 10,
					'beta' => 10,
					'viewDistance' => 100,
					'depth' => 50
				)
		);
		//Marges
		$column->chart->marginTop(60);
		$column->chart->marginRight(30);
		//Définition des abscisses et ordonnées
		$column->yAxis->title(array('text' => "nombre d'arrêt de travail traités"));
		$column->yAxis->allowDecimals(false);
		$column->xAxis->title(array('text' => "mois"));
		$column->xAxis->categories($listeMois);
		//Options globales au graphique
		$column->plotOptions->column(
				array(
					'stacking' => 'normal',
					'depth' => 50,
				)
		);
		//Ajout des données
		$column->series($data);
		return $this->render('PaieArretTravailBundle:default:reporting.html.twig', array(
					'data' => json_encode($data),
					'categories' => json_encode($listeMois)
		));
	}

	/**
	 * @Route("/arret-de-travail/prise-en-compte/{id}", name="paie_arret_de_travail_prise_en_compte")
	 * @Template()
	 */
	public function priseEnCompteArretDeTravailAction(Request $request, $id) {
		$arretDeTravail = $this->getDoctrine()
				->getRepository('PaieArretTravailBundle:ArretTravail')
				->find($id);
		$session = $this->getRequest()->getSession();
		$session->set('selectedArret', $id);
		$message = $arretDeTravail->getDatePriseEnCompte() !== '' ? 'Arret de travail déjà prisé en compte!' : '';
		if ($message !== '') {
			$this->get('session')->getFlashBag()->add('arretDeTravailSucces', $message);
			return $this->redirect($this->generateUrl('paie_arrets_de_travail'));
		}
		$arretDeTravail->setDatePriseEnCompte(new DateTime('now'));
		$em = $this->getDoctrine()->getManager();
		$em->persist($arretDeTravail);
		$em->flush();
		$message = 'L\'enregistrement de la prise en compte de l\'arret de travail dans Pandore s’est bien déroulée!';
		$this->get('session')->getFlashBag()->add('arretDeTravailSucces', $message);
		return $this->redirect($this->generateUrl('paie_arrets_de_travail'));
	}

	/**
	 * @Route("/arret-de-travail/traitement/{id}", name="paie_arret_de_travail_traitement")
	 * @Template()
	 */
	public function traitementArretDeTravailAction(Request $request, $id) {
		if (!$this->getUser()->isGranted("ROLE_PAIE_GESTIONNAIRE")) {
			return $this->redirect($this->generateUrl('paie_arrets_de_travail'));
		}
		$arretDeTravail = $this->getDoctrine()
				->getRepository('PaieArretTravailBundle:ArretTravail')
				->find($id);
		$session = $this->getRequest()->getSession();
		$session->set('selectedArret', $id);
		if ($arretDeTravail->getDateTraitement() !== '') {
			$message = 'Arret de travail déjà traité!';
			$this->get('session')->getFlashBag()->add('arretDeTravailSucces', $message);
			return $this->redirect($this->generateUrl('paie_arrets_de_travail'));
		}
		$dateTraitement = $request->get('traiter_arret');
		$arretDeTravail->setDateTraitement(DateTime::createFromFormat('d/m/Y', $dateTraitement['dateTraitement']));
		$arretDeTravail->setStatut(ArretTravail::SOLDE);
		$em = $this->getDoctrine()->getManager();
		$em->persist($arretDeTravail);
		$em->flush();
		$message = 'Le traitement d\'arret de travail s’est bien déroulée!';
		$this->get('session')->getFlashBag()->add('arretDeTravailSucces', $message);
		return $this->redirect($this->generateUrl('paie_arrets_de_travail'));
	}

	/**
	 * @Route("/arret-de-travail/pieces-jointes/{id}", name="paie_arret_de_travail_pieces_jointes")
	 * @Template()
	 */
	public function piecesJointesArretDeTravailAction(Request $request, $id) {
		$arretDeTravail = $this->getDoctrine()
				->getRepository('PaieArretTravailBundle:ArretTravail')
				->find($id);
		$session = $this->getRequest()->getSession();
		$session->set('selectedArret', $id);
		$em = $this->getDoctrine()->getManager();
		//Récupération des PJ associés à la demande ($demande->getPieceJointes ?)
		$listePJ = $em->getRepository('PaieArretTravailBundle:PieceJointe')->getPJparArretTravail((int) $id);
		//Récupération de l'extension pour afficher le logo
		$extensions = null;
		foreach ($listePJ as $pj) {
			$extension = substr(strrchr($pj->getName(), '.'), 1);
			switch ($extension) {
				case 'png' :
				case 'jpg' : $img = 'jpg_64.png';
					break;
				case 'pdf' : $img = 'pdf_64.png';
					break;
				case 'doc' :
				case 'docx' : $img = 'doc_64.png';
					break;
				case 'xls' :
				case 'xlsx' :
				case 'csv' : $img = 'xls_64.png';
					break;
				case 'txt' : $img = 'txt_64.png';
					break;
				case 'ppt' :
				case 'pptx' : $img = 'ppt_64.png';
					break;
				default : $img = 'txt_64.png';
					break;
			}
			$extensions[$pj->getId()] = $img;
		}
		$formPiecesJointes = $this->createForm(new PiecesJointesType(), $arretDeTravail);
		$formPiecesJointes->handleRequest($request);
		//Validation du formulaire, et traitement
		if ($formPiecesJointes->isValid()) {

			if($request->get('deleted') && count($request->get('deleted'))){
				$enlevePJs = array();
				foreach($request->get('deleted') as $key => $val){
					$enlevePJ = $this->getDoctrine()
							->getRepository('PaieArretTravailBundle:PieceJointe')
							->find($key);
					$em->remove($enlevePJ);
					$enlevePJs[] = $val;
					// En PostRemove, on n'a pas accès à l'id, on utilise notre nom sauvegardé
					if (is_dir('uploads') &&
						is_dir('uploads/attachments') &&
						is_dir('uploads/attachments/paie') &&
						is_dir('uploads/attachments/paie/'.$id) &&
						!is_dir('uploads/attachments/paie/'.$id.'/'.$val) &&
						file_exists('uploads/attachments/paie/'.$id.'/'.$val)) {
					  // On supprime le fichier
					  unlink('uploads/attachments/paie/'.$id.'/'.$val);
					}
				}
			}

			$listePJ = $em->getRepository('PaieArretTravailBundle:PieceJointe')->getPJparArretTravail($arretDeTravail->getId());
			//on enregistre les nouvelles pièces jointes
			foreach ($formPiecesJointes->get('piecejointes')->getData() as $piecejointe) {
				$i = 0;
				//Si la pièce jointe existe déjà
				foreach ($listePJ as $pj) {
					if ($pj->getName() == $piecejointe->getName()) {
						//On remplace le fichier existant, et on garde la ligne PJ existante
						$pj->setFile($piecejointe->getFile());
						$pj->upload();
						$i++;
					}
				}
				if ($i == 0) {
					$piecejointe->setArrets($arretDeTravail);
					$em->persist($piecejointe);
				}
			}

			$em->persist($arretDeTravail);
			$em->flush();
			$message = "Les pieces jointes sont enregistré!\n";
			if(isset($enlevePJs) and count($enlevePJs)){
				$message .= "\nPieces jointes enleve: " . implode(", ", $enlevePJs);
			}
			$this->get('session')->getFlashBag()->add('arretDeTravailSucces', $message);
		}
		return $this->redirect($this->generateUrl('paie_arrets_de_travail'));
	}

	/**
	 * @Route("/arret-de-travail/prolongation/{id}", name="paie_arret_de_travail_prolongation")
	 * @Template()
	 */
	public function prolongationArretDeTravailAction(Request $request, $id) {
		if (!$this->get('security.context')->isGranted('ROLE_PAIE_ARRET')) {
			$message = 'Interdit!';
			$this->get('session')->getFlashBag()->add('arretsDeTravailSucces', $message);
			return $this->redirect($this->generateUrl('paie_arrets_de_travail'));
		}
		$arretInitial = $this->getDoctrine()
				->getRepository('PaieArretTravailBundle:ArretTravail')
				->find($id);
		$session = $this->getRequest()->getSession();
		$session->set('selectedArret', $id);
		$arretNouveau = new ArretTravail();
		$arretNouveau->setIdArretInitial($arretInitial);
		$arretNouveau->setContrat($arretInitial->getContrat());
		$arretNouveau->setNature($arretInitial->getNature());
		$em = $this->getDoctrine()->getManager();
		$form = $this->createForm(new ArretTravailType($em), $arretNouveau);
		$form->handleRequest($request);
		if ($form->isValid()) {
			$arretNouveau->setGestPaie($arretInitial->getGestPaie() ? $arretInitial->getGestPaie() : null);
			$arretNouveau->setGestRH($arretInitial->getGestRH() ? $arretInitial->getGestRH() : null);
			$arretNouveau->setGestContrat($arretInitial->getGestRH() ? $arretInitial->getGestRH() : null);
			$arretNouveau->setDateTraitement(new DateTime('1970-01-01 00:00:00'));
			$arretNouveau->setDatePriseEnCompte(new DateTime('1970-01-01 00:00:00'));
			// the new arret don't need to be treated... if the duration of the arret + duration of his "parents" are <3 days.
			$arretNouveau->setStatut(ArretTravail::NON_CONCERNE);
			if ($arretNouveau->getDureeTotale() >3) {
				$arretNouveau->setStatut(ArretTravail::EN_ATTENTE);
				// we check if one of the "parent" arret has been treated... If it's the case, the new extended arret didn't need to be treated.
				$arretTemp= $arretNouveau;
				while ($arretTemp->getIdArretInitial()){
					if ($arretTemp->getIdArretInitial()->getStatut() != ArretTravail::NON_CONCERNE)
						$arretNouveau->setStatut(ArretTravail::NON_CONCERNE);
						break;					
				}
			}
			//if a leaf is a relapse of an AT, it need a new teatment => EN_ATTENTE
			if ($arretNouveau->getNature()->getId()== ArretNature::AT && $arretNouveau->getCategorie()->getId()== ArretCategorie::Rechute){
				$arretNouveau->setStatut(ArretTravail::EN_ATTENTE);
			}
			$em = $this->getDoctrine()->getManager();
			$em->persist($arretNouveau);
			$em->flush();
			$message = 'L’opération s’est bien déroulée!';
			$this->get('session')->getFlashBag()->add('arretsDeTravailSucces', $message);
			$selected = $arretNouveau->getId();
			$session = $this->getRequest()->getSession();
			$session->set('selectedArret', $selected);
			return $this->redirect($this->generateUrl('paie_arrets_de_travail'));
		}
		return $this->render('PaieArretTravailBundle:default:prolongerArretDeTravail.html.twig', array(
					'form' => $form->createView(),
					'title' => 'Ajouter Arret de travail',
					'idArretInitial' => $arretNouveau->getIdArretInitial()->getId(),
		));
	}

	/**
	 * @Route("/getArretDetails", name="paie_get_arret_details")
	 */
	public function getArretDetailsAction() {
		$request = $this->container->get('request');
		$idArret = $request->request->get('idArret');
		$em = $this->getDoctrine()->getManager();
		if ($idArret === '0' || $idArret === '') {
			$arret = new ArretTravail();
		} else {
			$arret = $this->getDoctrine()
					->getRepository('PaieArretTravailBundle:ArretTravail')
					->find($idArret);
		}
		$test = new DateTime();
		$arretStatut = '';
		if ($arret->getDateDebut()->format('Y-m-d') === $test->format('Y-m-d')) {
			$arretStatut = 'en attente';
		} else if (((int) $arret->getDateDebut()->diff($test)->format('%a') + 1) <= 3) {
			$arretStatut = 'non concerné';
		} else if ($arret->getDateTraitement()->format('Y-m-d H:i:s') !== '1970-01-01 00:00:00') {
			$arretStatut = 'soldé';
		}
		$arretDureeInteger = (int) $arret->getDateDebut()->diff($arret->getDateFin())->format('%a') + 1;
		$arretDuree = (string) $arretDureeInteger . ' jour' . ($arretDureeInteger > 1 ? 's' : '');
		$tempArret = $arret;
		while ($tempArret->getIdArretInitial() !== null) {
			$tempArret = $tempArret->getIdArretInitial();
			$origDureeInteger = (int) $tempArret->getDateDebut()->diff($tempArret->getDateFin())->format('%a') + 1;
			$origDuree = (string) $origDureeInteger . ' jour' . ($origDureeInteger > 1 ? 's' : '' );
			$arretDuree .= ' + inital: ' . $origDuree;
		}
		$arretCategorie = $arret->getCategorie();
		$arretCommentaire = $arret->getCommentaire();
		$arretDelaiReceptionInteger = (int) $arret->getDateDebut()->diff($arret->getDateReception())->format('%a') + 1;
		$arretDelaiReception = (string) $arretDelaiReceptionInteger . ' jour' . ($arretDelaiReceptionInteger > 1 ? 's' : '');
		$dateTraitement = $arret->getDateTraitement()->format('Y-m-d H:i:s');
		if ($dateTraitement !== '1970-01-01 00:00:00') {
			$arretTraitement = 'Traité';
			$arretDateTraitement = $arret->getDateTraitement()->format('d/m/Y');
			$arretDelaiTraitementInteger = (int) $arret->getDateReception()->diff($arret->getDateTraitement())->format('%a') + 1;
			$arretDelaiTraitement = (string) $arretDelaiTraitement . ' jour' . ($arretDelaiTraitementInteger > 1 ? 's' : '');
		} else {
			$arretTraitement = 'Non traité';
			$arretDateTraitement = '';
			$arretDelaiTraiteentInteger = (int) $arret->getDateReception()->diff($test)->format('%a') + 1;
			$arretDelaiTraitement = (string) $arretDelaiTraiteentInteger . ' jour' . ($arretDelaiTraiteentInteger > 1 ? 's' : '');
		}
		$arretDatePriseEnCompte = $arret->getDatePriseEnCompte()->format('Y-m-d H:i:s') !== '1970-01-01 00:00:00' ?
				$arret->getDatePriseEnCompte()->format('d/m/Y') : '';
		$arretHistorique = '';
		$nature = array(
			'1' => 'Initial',
			'2' => 'Prolongation',
			'3' => 'Rechute',
			'4' => 'Final',
		);
		$arretCategorie = $arretCategorie ? $nature[$arretCategorie] : $arretCategorie;
		return new JsonResponse(
				array(
			'arretStatut' => $arretStatut,
			'arretDuree' => $arretDuree,
			'arretCategorie' => $arretCategorie,
			'arretCommentaire' => $arretCommentaire,
			'arretDelaiReception' => $arretDelaiReception,
			'arretTraitement' => $arretTraitement,
			'arretDateTraitement' => $arretDateTraitement,
			'arretDelaiTraitement' => $arretDelaiTraitement,
			'arretDatePriseEnCompte' => $arretDatePriseEnCompte,
			'arretHistorique' => $arretHistorique,
				)
		);
	}

	/**
	 * @Route("/getSocieteDetails", name="paie_get_societe_details")
	 */
	public function getSocieteDetailsAction() {
		$request = $this->container->get('request');
		$idSociete = $request->request->get('idSociete');
		$societe = $this->getDoctrine()
				->getRepository('AdministrationSocieteBundle:Societe')
				->find($idSociete);
		$direction = $societe->getDirection() ? $societe->getDirection()->getLibelle() : '';
		$gestPaie = $societe->getGestionnairePaie() ? $societe->getGestionnairePaie()->getUsername() : '';
		$gestRH = $societe->getGestionnaireRH() ? $societe->getGestionnaireRH()->getUsername() : '';
		$gestContrat = $societe->getGestionnaireContrat() ? $societe->getGestionnaireContrat()->getUsername() : '';
		return new JsonResponse(
				array(
			'societeCode' => $societe->getReference(),
			'societeCategorie' => $societe->getCategorie(),
			'societeRegion' => $societe->getRegion(),
			'societeDirection' => $direction,
			'societeGestPaie' => $gestPaie,
			'societeGestRH' => $gestRH,
			'societeGestContrat' => $gestContrat,
				)
		);
	}

	/**
	 * @Route("/getArretModifications", name="paie_get_arret_modifications")
	 */
	public function getArretModificationsAction() {
		$request = $this->container->get('request');
		$idArret = $request->request->get('idArret');
		$arret = $this->getDoctrine()
				->getRepository('PaieArretTravailBundle:ArretTravail')
				->find($idArret);
		$modifications = $this->getDoctrine()
				->getRepository('PaieArretTravailBundle:ModificationArret')
				->getModificationsParArret($arret);
		$results = array();
		$convertibles = array(
			'nature' => array(
				'1' => 'Maladie',
				'2' => 'AT',
				'3' => 'maternité',
				'4' => 'paternité',
				'5' => 'maladie professionnelle',
				'6' => 'mi-temps thérapeutique',
				'7' => 'hospitalisation',
			),
			'typeReception' => array(
				'1' => 'mail',
				'2' => 'papier',
			),
			'categorie' => array(
				'1' => 'Initial',
				'2' => 'Prolongation',
				'3' => 'Rechute',
				'4' => 'Final',
			),
			'statut' => array(
				'1' => 'NON_CONCERNE',
				'2' => 'EN_ATTENTE',
				'3' => 'SOLDE',
			)
		);
		foreach ($modifications as $key => $modification) {
			$results[$key]['modificationDate'] = $modification->getDate()->format('d/m/Y');
			$results[$key]['modificationType'] = $modification->getType();
			if ($modification->getType() === 'dateTraitement' ||
					$modification->getType() === 'datePriseEnCompte' ||
					$modification->getType() === 'dateReception') {
				$results[$key]['modificationAncienneValeur'] = substr($modification->getAncienneValeur(), 0, 10);
				$results[$key]['modificationNouvelleValeur'] = substr($modification->getNouvelleValeur(), 0, 10);
			} else {
				$results[$key]['modificationAncienneValeur'] = $modification->getAncienneValeur();
				$results[$key]['modificationNouvelleValeur'] = $modification->getNouvelleValeur();
			}
			if (array_key_exists($modification->getType(), $convertibles)) {
				$results[$key]['modificationAncienneValeur'] = $convertibles[$modification->getType()][$results[$key]['modificationAncienneValeur']];
				$results[$key]['modificationNouvelleValeur'] = $convertibles[$modification->getType()][$results[$key]['modificationNouvelleValeur']];
			}
		}
		return new JsonResponse($results);
	}

	/**
	 * @Route("/getArretAllData", name="paie_get_arret_all_data")
	 */
	public function getArretAllData() {
		$request = $this->container->get('request');
		$idArret = $request->request->get('idArret');
		if ($idArret === '0' || $idArret === '') {
			$arret = new ArretTravail();
		} else {
			$arret = $this->getDoctrine()
					->getRepository('PaieArretTravailBundle:ArretTravail')
					->find($idArret);
		}
		$modifications = $this->getDoctrine()
				->getRepository('PaieArretTravailBundle:ModificationArret')
				->getModificationsParArret($arret);
		$results = array();
		$convertibles = array(
			'nature' => array(
				'1' => 'Maladie',
				'2' => 'AT',
				'3' => 'maternité',
				'4' => 'paternité',
				'5' => 'maladie professionnelle',
				'6' => 'mi-temps thérapeutique',
				'7' => 'hospitalisation',
			),
			'typeReception' => array(
				'1' => 'mail',
				'2' => 'papier',
			),
			'categorie' => array(
				'1' => 'Initial',
				'2' => 'Prolongation',
				'3' => 'Rechute',
				'4' => 'Final',
			),
			'statut' => array(
				'1' => 'NON_CONCERNE',
				'2' => 'EN_ATTENTE',
				'3' => 'SOLDE',
			)
		);
		foreach ($modifications as $key => $modification) {
			$results[$key]['modificationDate'] = $modification->getDate()->format('d/m/Y');
			$results[$key]['modificationType'] = $modification->getType();
			if ($modification->getType() === 'dateTraitement' ||
					$modification->getType() === 'datePriseEnCompte' ||
					$modification->getType() === 'dateDebut' ||
					$modification->getType() === 'dateFin' ||
					$modification->getType() === 'dateReception') {
				$results[$key]['modificationAncienneValeur'] = substr($modification->getAncienneValeur(), 0, 10) === '1970-01-01' ?
						'' : substr($modification->getAncienneValeur(), 0, 10);
				$results[$key]['modificationNouvelleValeur'] = substr($modification->getNouvelleValeur(), 0, 10);
			} else {
				$results[$key]['modificationAncienneValeur'] = $modification->getAncienneValeur();
				$results[$key]['modificationNouvelleValeur'] = $modification->getNouvelleValeur();
			}
			if (array_key_exists($modification->getType(), $convertibles)) {
				$results[$key]['modificationAncienneValeur'] = $convertibles[$modification->getType()][$results[$key]['modificationAncienneValeur']];
				$results[$key]['modificationNouvelleValeur'] = $convertibles[$modification->getType()][$results[$key]['modificationNouvelleValeur']];
			}
		}
		$test = new DateTime();
		$arretStatut = '';
		if ($idArret) {
			$arretDureeInteger = (int) $arret->getDateDebut()->diff($arret->getDateFin())->format('%a') + 1;
			if ($arret->getDateDebut()->format('Y-m-d') === $test->format('Y-m-d') ||
					$arretDureeInteger > 3) {
				$arretStatut = 'en attente';
			} else if ($arretDureeInteger <= 3) {
				$arretStatut = 'non concerné';
			} else if ($arret->getDateTraitement() && $arret->getDateTraitement()->format('Y-m-d H:i:s') !== '1970-01-01 00:00:00') {
				$arretStatut = 'soldé';
			}
			$arretDuree = (string) ($arretDureeInteger) . ' jour' . ($arretDureeInteger > 1 ? 's' : '');
			$tempArret = $arret;
			while ($tempArret->getIdArretInitial() !== null) {
				$tempArret = $tempArret->getIdArretInitial();
				$tempArretDureeInteger = (int) $tempArret->getDateDebut()->diff($tempArret->getDateFin())->format('%a') + 1;
				$origDuree = (string) $tempArretDureeInteger . ' jour' . ($tempArretDureeInteger > 1 ? 's' : '');
				$arretDuree .= ' + inital: ' . $origDuree;
			}
		}
		$arretCategorie = $arret->getCategorie();
		$nature = array(
			'1' => 'Initial',
			'2' => 'Prolongation',
			'3' => 'Rechute',
			'4' => 'Final',
		);
		$arretCategorie = $arretCategorie ? $nature[$arretCategorie] : $arretCategorie;
		$arretCommentaire = $arret->getCommentaire();
		if ($idArret) {
			$arretDelaiReceptionInteger = (int) $arret->getDateDebut()->diff($arret->getDateReception())->format('%a') + 1;
			$arretDelaiReception = (string) $arretDelaiReceptionInteger . ' jour' . ($arretDelaiReceptionInteger > 1 ? 's' : '');
			$dateTraitement = $arret->getDateTraitement()->format('Y-m-d H:i:s');
			if ($dateTraitement !== '1970-01-01 00:00:00') {
				$arretTraitement = 'Traité';
				$arretDateTraitement = $arret->getDateTraitement()->format('d/m/Y');
			} else {
				$arretTraitement = 'Non traité';
				$arretDateTraitement = '';
//				$arretDelaiTraitement = (string)((int)$arret->getDateReception()->diff($test)->format('%a') + 1) . ' jours';
			}
			if ($arret->getDateReception()->format('Y-m-d H:i:s') === '1970-01-01 00:00:00' || $arretStatut === 'non concerné') {
				$arretDelaiTraitement = '0 jours';
			} else if ($arretStatut === 'en attente') {
				$arretDelaiTraitementInteger = (int) $arret->getDateReception()->diff($test)->format('%a') + 1;
				$arretDelaiTraitement = (string) $arretDelaiTraitementInteger . ' jour' . ($arretDelaiTraitementInteger > 1 ? 's' : '');
			} else if ($arretStatut === 'soldé') {
				$arretDelaiTraitementInteger = (int) $arret->getDateReception()->diff($arret->getDatePriseEnCompte())->format('%a') + 1;
				$arretDelaiTraitement = (string) $arretDelaiTraitementInteger . ' jour' . ($arretDelaiTraitementInteger > 1 ? 's' : '');
			}
			$arretDatePriseEnCompte = $arret->getDatePriseEnCompte()->format('Y-m-d H:i:s') !== '1970-01-01 00:00:00' ?
					$arret->getDatePriseEnCompte()->format('d/m/Y') : '';
		} else {
			$arretStatut = '';
			$arretDuree = '';
			$arretDelaiReception = '';
			$arretTraitement = '';
			$arretDateTraitement = '';
			$arretDelaiTraitement = '';
			$arretDatePriseEnCompte = '';
		}
		$arretHistorique = '';
		$societe = $arret->getSociete();
		$direction = $societe->getDirection() ? $societe->getDirection()->getLibelle() : '';
		$gestPaie = $societe->getGestionnairePaie() ? $societe->getGestionnairePaie()->getSurname() . ' ' . $societe->getGestionnairePaie()->getGivenname() : '';
		$gestRH = $societe->getGestionnaireRH() ? $societe->getGestionnaireRH()->getSurname() . ' ' . $societe->getGestionnaireRH()->getGivenname() : '';
		$gestContrat = $societe->getGestionnaireContrat() ? $societe->getGestionnaireContrat()->getSurname() . ' ' . $societe->getGestionnaireContrat()->getGivenname() : '';
		$arretDetails = array(
			'arretDetails' => array(
				'arretStatut' => $arretStatut,
				'arretDuree' => $arretDuree,
				'arretCategorie' => $arretCategorie,
				'arretCommentaire' => $arretCommentaire,
				'arretDelaiReception' => $arretDelaiReception,
				'arretTraitement' => $arretTraitement,
				'arretDateTraitement' => $arretDateTraitement,
				'arretDelaiTraitement' => $arretDelaiTraitement,
				'arretDatePriseEnCompte' => $arretDatePriseEnCompte,
				'arretHistorique' => $arretHistorique,
		));
		$societeDetails = array(
			'societeDetails' => array(
				'societeLibelle' => $societe->getLibelle(),
				'societeCode' => $societe->getReference(),
				'societeCategorie' => $societe->getCategorie(),
				'societeRegion' => $societe->getRegion(),
				'societeDirection' => $direction,
				'societeGestPaie' => $gestPaie,
				'societeGestRH' => $gestRH,
				'societeGestContrat' => $gestContrat,
		));
		return new JsonResponse(
				array_merge($arretDetails, $societeDetails, $results)
		);
	}

	private function supprimerArretEtTous($id){
		$em = $this->getDoctrine()->getManager('default');
		while(count($em->getRepository('PaieArretTravailBundle:ArretTravail')->getProlongations($id))){
			$plusieurs = $em->getRepository('PaieArretTravailBundle:ArretTravail')->getProlongations($id);
			foreach($plusieurs as $prolongation){
			$this->supprimerArretEtTous($prolongation->getId());
			}
		}
		$arretId = (int)$id;
		$arret = $this->getDoctrine()
						->getRepository('PaieArretTravailBundle:ArretTravail', 'default')
						->find($arretId);
		$arretModifications = $em->getRepository('PaieArretTravailBundle:ModificationArret')
				->getModificationsParArret($arret);
		foreach($arretModifications as $modificationEnleve){
			$em->remove($modificationEnleve);
		}
		$listePJ = $em->getRepository('PaieArretTravailBundle:PieceJointe')
				->getPJparArretTravail($arretId);
		foreach($listePJ as $pjEnleve){
			$pjName = $pjEnleve->getName();
			if (is_dir('uploads') &&
				is_dir('uploads/attachments') &&
				is_dir('uploads/attachments/paie') &&
				is_dir('uploads/attachments/paie/'.$id) &&
				!is_dir('uploads/attachments/paie/'.$id.'/'.$pjName) &&
				file_exists('uploads/attachments/paie/'.$id.'/'.$pjName)) {
				unlink('uploads/attachments/paie/'.$id.'/'.$pjName);
			}
			$em->remove($pjEnleve);
		}
		$em->remove($arret);
		$em->flush();
	}

	/**
	 * @Route("/suprimmerArret/{id}", name="paie_suprimmer_arret")
	 */
	public function suprimmerArretAction($id) {
		$session = $this->getRequest()->getSession();
		$this->supprimerArretEtTous($id);
		$session->set('selectedArret', 0);
		$message = 'L\'arret de travail vient de supprimer!';
		$this->get('session')->getFlashBag()->add('arretsDeTravailSucces', $message);
		return $this->redirect($this->generateUrl('paie_arrets_de_travail'));
	}

	/**
	 * @Route("/getArret", name="paie_get_arret")
	 */
	public function getArretAction() {
		$em = $this->getDoctrine()->getManager('default');
		$request = $this->container->get('request');
		$session = $this->getRequest()->getSession();
		if ($request->request->get('paie_arret_travail')) {
			$idArret = $request->request->get('paie_arret_travail')['id'];
		} else if ($request->request->get('idArretDeTravail') || $request->request->get('idArretDeTravail') === '0') {
			$idArret = $request->request->get('idArretDeTravail');
		}		
		// for test by using the route directly
		$today=new DateTime();
		$arret = $this->getDoctrine()
				->getRepository('PaieArretTravailBundle:ArretTravail', 'default')
				->find($idArret);
		$form = $this->createForm(new ArretTravailType($em), $arret);				
		$formModal = $this->createForm(new TraiterArretType(), $arret)->createView();
		//Récupération des PJ associés à la demande ($demande->getPieceJointes ?)
		$listeVieillesPJ = $em->getRepository('PaieArretTravailBundle:PieceJointe')->getPJparArretTravail((int) $idArret);
		//Récupération de l'extension pour afficher le logo
		$extensions = null;
		foreach ($listeVieillesPJ as $pj) {
			$extension = substr(strrchr($pj->getName(), '.'), 1);
			switch ($extension) {
				case 'png' :
				case 'jpg' : $img = 'jpg_64.png';
					break;
				case 'pdf' : $img = 'pdf_64.png';
					break;
				case 'doc' :
				case 'docx' : $img = 'doc_64.png';
					break;
				case 'xls' :
				case 'xlsx' :
				case 'csv' : $img = 'xls_64.png';
					break;
				case 'txt' : $img = 'txt_64.png';
					break;
				case 'ppt' :
				case 'pptx' : $img = 'ppt_64.png';
					break;
				default : $img = 'txt_64.png';
					break;
			}
			$extensions[$pj->getId()] = $img;
		}
		$formPiecesJointes = $this->createForm(new PiecesJointesType(), $arret)->createView();
		$errors = 0;
		$form->handleRequest($request);
		$validationModal = 0;
		$modifications = $this->getDoctrine()
			->getRepository('PaieArretTravailBundle:ModificationArret')
			->getModificationsParArret($arret);
		$selected = $idArret;
		if ($form->isValid()) {
			$dateDebut = $form['dateDebut']->getData();
			$dateFin = $form['dateFin']->getData();
			$arret->setStatut($dateFin ? (((int) $dateDebut->diff($dateFin)->format('%a') + 1) > 1 ? ArretTravail::EN_ATTENTE : ArretTravail::NON_CONCERNE) : ArretTravail::NON_CONCERNE);
			$em->persist($arret);
			$em->flush();
			$message = 'L’ arret de travail à été enregistré!';
			$this->get('session')->getFlashBag()->add('arretDeTravailSucces', $message);
			$errors = 0;
			$validationModal = 1;
			$session->set('validationModal', $validationModal);
			$session->set('errors', 0);
			$session->save();
			if ($idArret === '0' || $idArret === '') {
				$form = $this->createForm(new ArretTravailType($pourModifier, $categorieModifier, $em), $arret);
			}
		} else {
			$errors = 1;
			$session->set('errors', 1);
			$session->save();
		}		
		return $this->render('PaieArretTravailBundle:default:arretDeTravail.html.twig', array(
								'form' => $form->createView(),
								'formModal' => $formModal,
								'formPiecesJointes' => $formPiecesJointes,
								'vieillesPiecesJointes' => $listeVieillesPJ,
								'extensions' => $extensions,
								'arret' => $arret,
								'selected' => $selected,
								'errors' => $errors,
								'modifications' => $modifications,
							));
	}

	
	/**
	 * @Route("/addArret", name="paie_add_arret")
	 */
	public function addArretAction() {
		$em = $this->getDoctrine()->getManager();
		$request = $this->container->get('request');
		$session = $this->getRequest()->getSession();
		// for test by using the route directly
		$today=new DateTime();
		$today->sub(new DateInterval('P40D'));
		$contrats = $em->createQuery(
				'SELECT c,s,u
							FROM AdministrationSocieteBundle:PandoreContrat c
							JOIN c.societe s
							JOIN c.user u
							WHERE ((c.dateSortie = :p1 and c.typeContrat=:p2)
							OR (c.dateSortie > :p3 and c.typeContrat=:p2)
							OR (c.dateFin > :p3 and c.typeContrat!=:p4))'
		)
			->setParameter('p1', '')
			->setParameter('p2', 'CDI') 
			->setParameter('p3', $today->format('Ymd'))
			->setParameter('p4', 'CDI')
			->getResult();
		
		$utilisateurs = array();
		foreach ($contrats as $contrat) {
			$utilisateurs[$contrat->getId()] = $contrat->getUser()->getNom() . ' ' . $contrat->getUser()->getPrenom() . ' - ' . $contrat->getUser()->getNumeroSecu() . ' - ' . $contrat->getSociete()->getId() . ' ' . $contrat->getSociete()->getRaison();
		}		
		$arret = new ArretTravail();
		$form = $this->createForm(new AddArretTravailType($utilisateurs, $em), $arret);
		$formModal = null;
		$formPiecesJointes = null;
		$listeVieillesPJ = array();
		$extensions = array();
		$errors = 0;			
		$form->handleRequest($request);		
		$validationModal = 0;
		if ($form->isValid()) { 
			$arret->setDateTraitement(new DateTime('1970-01-01 00:00:00'));
			$arret->setDatePriseEnCompte(new DateTime('1970-01-01 00:00:00'));
			$arret->setCategorie($this->getDoctrine()
				->getRepository('PaieArretTravailBundle:ArretCategorie')
				->find(1));
			$contratId = $form['contrat']->getData();
			$contrat= $this->getDoctrine()
				->getRepository('AdministrationSocieteBundle:PandoreContrat')
				->find($contratId);
			$arret->setContrat($contrat);
			$etablissement = $this->getDoctrine()
				->getRepository('AdministrationSocieteBundle:PandoreEtablissement')
				->findOneBy(array('societe' => $contrat->getSociete(), 'numero' => $contrat->getEtab()));
			$societePaie =  $this->getDoctrine()
				->getRepository('PaieAdministrationBundle:PaieSociete')
				->findOneBy(array('etablissement' => $etablissement));
			$gestPaie = $societePaie->getGestionnairePaie() ? $societePaie->getGestionnairePaie() : null;
			$gestRH = $societePaie->getGestionnaireRH() ? $societePaie->getGestionnaireRH() : null;
			$gestContrat = $societePaie->getGestionnaireContrat() ? $societePaie->getGestionnaireContrat() : null;
			$arret->setGestPaie($gestPaie);
			$arret->setGestRH($gestRH);
			$arret->setGestContrat($gestContrat);
			$dateDebut = $form['dateDebut']->getData();
			$dateFin = $form['dateFin']->getData();
			$arret->setStatut($arret->getDureeTotale() > 3 ? ArretTravail::EN_ATTENTE : ArretTravail::NON_CONCERNE);
			$em->persist($arret);
			$em->flush();
			$selected = $arret->getId();
			$message = 'L’arret de travail a bien été enregistré !';
			$this->get('session')->getFlashBag()->add('arretDeTravailSucces', $message);
			$errors = 0;
			$validationModal = 1;
			$session->set('validationModal', $validationModal);
			$session->set('errors', 0);
			$session->save(); 
		} else {
			$errors = 1;
			$session->set('errors', 1);
			$session->save();
			$selected = 0;
		}
		return $this->render('PaieArretTravailBundle:default:ajouterArret.html.twig', array(
				'form' => $form->createView(),
				'formModal' => $formModal,
				'formPiecesJointes' => $formPiecesJointes,
				'vieillesPiecesJointes' => $listeVieillesPJ,
				'extensions' => $extensions,
				'arret' => $arret,
				'selected' => $selected,
				'errors' => $errors,
				'arretId' => $arret->getId(),
		));
	}	
	
	/**
	 * @Route("/arrets-filtered", name="paie_arrets_filtered")
	 * @Method({"GET", "POST"})
	 */
	public function getFilteredArrets() {
		$request = $this->container->get('request');		
		$params = $request->request->get('search_arret_travail');		
		$params['page'] = $request->request->getInt('page', 1);
		$session = $this->getRequest()->getSession();
		$session->set('params', $params);
		// If it's the opening of the page, we try to display only a few arret.	
		if ($request->request->get('opening')==1){
			if ($this->getUser()->isGranted("ROLE_PAIE_GESTIONNAIRE")) {
				$params['gestPaie']=$this->getUser()->getID();
			} elseif ($this->getUser()->isGranted("ROLE_PAIE_RH")) {
				$params['gestRH']=$this->getUser()->getID();
			} elseif ($this->getUser()->isGranted("ROLE_PAIE_CONTRAT")) {
				$params['gestContrat']=$this->getUser()->getID();
			}		
		}
			
		if ($this->getUser()->isGranted("ROLE_PAIE_VENTE")) {
			$params['direction']='2';
		} else {
			$params['direction']='';
		}
		$em = $this->getDoctrine()->getManager();
		if (($request->request->get('opening')==1) && ($this->getUser()->isGranted("ROLE_PAIE_ARRET")) ){
			$aretsDeTravail=array();
		} else {
			$aretsDeTravail = $em->getRepository('PaieArretTravailBundle:ArretTravail')->getFilteredArret($params);		
			$start = 0;
		}
		$selected = $request->request->get('idArret');
		
		$filteredIds = array();
		foreach ($aretsDeTravail as $individual) {
			$filteredIds[] = $individual[0]->getId();
		}
		$request->getSession()->set('filteredIds', $filteredIds);
		$paginator = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
				$aretsDeTravail,
				$params['page'] ? $params['page'] : 1, 70
		);
		$pagination->setTemplate('KnpPaginatorBundle:Pagination:sliding.html.twig');
		return $this->render('PaieArretTravailBundle:Default:filteredArrets.html.twig', array(
					'aretsDeTravail' => $pagination,
					'selected' => $selected				)
		);
	}

	/**
	 *
	 * @Route("/arrets-de-travail", name="paie_arrets_de_travail")
	 * @Method({"GET", "POST"})
	 */
	public function arretsDeTravailAction() {
		$em = $this->getDoctrine()->getManager();
		$session = $this->getRequest()->getSession();
		$selected = $session->get('selectedArret') !== NULL ? $session->get('selectedArret') : 0;
		$session->set('selectedArret', 0);
		$formSearch= $this->createForm(new SearchArretType($em));
		//get last search from the session
		$param=$session->get('params');
		if($param){
			//fill the form
			foreach ($param as $key => $value) {
				if ($key != 'page'){
					$formSearch->get($key)->setData($value);
				}
			}			
		}
		return $this->render('PaieArretTravailBundle:Default:arretsDeTravail.html.twig', array(
					'selected' => $selected,
					'formsearch' => $formSearch->createView(),
						)
		);
	}

	public static function stringFromColumnIndex($pColumnIndex = 0) {
		// Determine column string
		if ($pColumnIndex < 26) {
			return chr(65 + $pColumnIndex);
		} elseif ($pColumnIndex < 702) {
			return chr(64 + ($pColumnIndex / 26)) . chr(65 + $pColumnIndex % 26);
		}
		return chr(64 + (($pColumnIndex - 26) / 676)) . chr(65 + ((($pColumnIndex - 26) % 676) / 26)) . chr(65 + $pColumnIndex % 26);
	}

	/**
	 * @Route("/arrets-export", name="paie_arrets_export")
	 */
	public function arretsExportAction() {
		$session = $this->getRequest()->getSession();
		$filteredIds = $session->get('filteredIds');
		$em = $this->getDoctrine()->getManager();
		$query = $em->createQuery("SELECT i.nom AS Nom, i.prenom AS Prenom, "
						. "a.dateDebut AS Debut, a.dateFin AS Fin,"
						. "s.id AS Code_Societe, s.raison AS Societe, "
						. "a.dateReception AS Reception, n.libelle as Nature, cat.libelle as Categorie, "
						. "a.commentaire, (CASE When a.dateTraitement = '1970-01-01 00:00:00' then 'NON' else 'OUI' END) as Traite "
						. "FROM PaieArretTravailBundle:ArretTravail a "
					    . "LEFT JOIN AdministrationSocieteBundle:PandoreContrat c WITH a.contrat = c.id "
						. "LEFT JOIN AdministrationSocieteBundle:PandoreIndividu i WITH c.user = i.id "
						. "LEFT JOIN PaieArretTravailBundle:ArretCategorie cat WITH a.categorie = cat.id "
						. "LEFT JOIN PaieArretTravailBundle:ArretNature n WITH a.nature = n.id "
						. "LEFT JOIN Administration\SocieteBundle\Entity\PandoreSociete s WITH c.societe = s.id "
						. "WHERE a.id IN (:filteredIds)")->setParameters(array('filteredIds' => $filteredIds));
		$rezultatele = $query->getArrayResult();
		$rezultate = array();
		$i = 0;
		$z = 0;
		foreach ($rezultatele as $firstKey => $rezult) {
			$z = 0;
			foreach ($rezult as $secondKey => $val) {
				if ($firstKey === 0) {
					$rezultate[$firstKey][$z] = $secondKey;
				}
				if (is_object($val)) {
					if ($val->format('Y-m-d') !== '1970-01-01') {
						$rezultate[$firstKey + 1][$z] = $val->format('Y-m-d');
					}
				} else {
					$rezultate[$firstKey + 1][$z] = $val;
				}
				$z++;
			}
		}
		// ask the service for a Excel5
		$phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
		$phpExcelObject->getProperties()->setCreator('export program')
				->setLastModifiedBy('export program')
				->setTitle('Arrets de travail list');
		$phpExcelObject->getActiveSheet()->setTitle('Export_Arrets');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$this->get('phpexcel');
		$headers = array();
		foreach ($rezultatele as $nr => $rezultat) {
			$i = 0;
			foreach ($rezultat as $key => $value) {
				if (count($headers) < 11) {
					$headers[] = $key;
				}
				if (is_object($value)) {
					$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex($i) . ($nr + 2), $value->format('Y-m-d'));
				} else {
					$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex($i) . ($nr + 2), $value);
				}
				$i++;
			}
		}
		foreach ($headers as $key => $header) {
			$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex($key) . '1', ucfirst(strtolower($header)));
		}
		// create the writer
		$writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
		// create the response
		$response = $this->get('phpexcel')->createStreamedResponse($writer);
		// adding headers
		$response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
		$response->headers->set('Content-Disposition', 'attachment;filename=' . 'Export_Arrets' . '.xls');
		$response->headers->set('Pragma', 'public');
		$response->headers->set('Cache-Control', 'maxage=1');
		return $response;
	}


	/**
	 * @Route("/arrets-export-annee", name="paie_arrets_export_annee")
	 */
	public function arretsExportAnneeAction() {
		$session = $this->getRequest()->getSession();
		$filteredIds = $session->get('filteredIds');
		$em = $this->getDoctrine()->getManager();
		$dateLastYear = new DateTime(date('Y').'-12-31');
		$dateLastYear->sub(DateInterval::createFromDateString('1 year'));
		$query = $em->createQuery("SELECT i.nom AS Nom, i.prenom AS Prenom, "
				. "a.dateDebut AS Debut, a.dateFin AS Fin,"
				. "s.id AS Code_Societe, s.raison AS Societe, "
				. "a.dateReception AS Reception, n.libelle as Nature, cat.libelle as Categorie, "
				. "a.commentaire, (CASE When a.dateTraitement = '1970-01-01 00:00:00' then 'NON' else 'OUI' END) as Traite "
				. "FROM PaieArretTravailBundle:ArretTravail a "
				. "LEFT JOIN AdministrationSocieteBundle:PandoreContrat c WITH a.contrat = c.id "
				. "LEFT JOIN AdministrationSocieteBundle:PandoreIndividu i WITH c.user = i.id "
				. "LEFT JOIN PaieArretTravailBundle:ArretCategorie cat WITH a.categorie = cat.id "
				. "LEFT JOIN PaieArretTravailBundle:ArretNature n WITH a.nature = n.id "
				. "LEFT JOIN Administration\SocieteBundle\Entity\PandoreSociete s WITH c.societe = s.id "
				. "WHERE a.dateReception > :dateLastYear ")->setParameters(array('dateLastYear' => $dateLastYear));
		$rezultatele = $query->getArrayResult();
		$rezultate = array();		
		$i = 0;
		$z = 0;
		foreach ($rezultatele as $firstKey => $rezult) {
			$z = 0;
			foreach ($rezult as $secondKey => $val) {
				if ($firstKey === 0) {
					$rezultate[$firstKey][$z] = $secondKey;
				}
				if (is_object($val)) {
					if ($val->format('Y-m-d') !== '1970-01-01') {
						$rezultate[$firstKey + 1][$z] = $val->format('Y-m-d');
					}
				} else {
					$rezultate[$firstKey + 1][$z] = $val;
				}
				$z++;
			}
		}		
		// ask the service for a Excel5
		$phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
		$phpExcelObject->getProperties()->setCreator('export program')
		->setLastModifiedBy('export program')
		->setTitle('Arrets de travail list');
		$phpExcelObject->getActiveSheet()->setTitle('Export_Arrets');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$this->get('phpexcel');
		$headers = array();
		foreach ($rezultatele as $nr => $rezultat) {
			$i = 0;
			foreach ($rezultat as $key => $value) {				
				if (count($headers) < 11) {
					$headers[] = $key;
				}
				if (is_object($value)) {
					$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex($i) . ($nr + 2), $value->format('Y-m-d'));
				} else {
					$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex($i) . ($nr + 2), $value);
				}
				$i++;
			}
		}
		
		foreach ($headers as $key => $header) {
			$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex($key) . '1', ucfirst(strtolower($header)));
		}
		// create the writer
		$writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
		// create the response
		$response = $this->get('phpexcel')->createStreamedResponse($writer);
		// adding headers
		$response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
		$response->headers->set('Content-Disposition', 'attachment;filename=' . 'Export_Arrets' . '.xls');
		$response->headers->set('Pragma', 'public');
		$response->headers->set('Cache-Control', 'maxage=1');
		return $response;
	}	
	
	
	
	
}
