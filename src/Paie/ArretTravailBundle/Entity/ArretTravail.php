<?php

namespace Paie\ArretTravailBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Administration\SocieteBundle\Entity\PandoreIndividu;
use \DateTime;

/**
 * ArretTravail
 *
 * @ORM\Entity(repositoryClass="Paie\ArretTravailBundle\Entity\ArretTravailRepository")
 * @ORM\Table(name="pai_arrets"))
 * @ORM\HasLifecycleCallbacks()
 */
class ArretTravail
{
	
	const NON_CONCERNE  	= 1;
	const EN_ATTENTE        = 2;
	const SOLDE				= 3;
	
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

	/**
     * @ORM\ManyToOne(targetEntity="ArretTravail")
     * @ORM\JoinColumn(name="id_arret_initial", referencedColumnName="id")
     **/
    private $IdArretInitial;


    /**
     * @var smallint
     *
     * @ORM\Column(name="statut", type="smallint")
     */
    private $statut;

	 /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="gestPaie", referencedColumnName="id")
     **/
    private $gestPaie;

	 /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="gestRH", referencedColumnName="id")
     **/
    private $gestRH;

    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="gestContrat", referencedColumnName="id")
     **/
    private $gestContrat;

    /**
     * @ORM\ManyToOne(targetEntity="\Administration\SocieteBundle\Entity\PandoreContrat")
     * @ORM\JoinColumn(name="contrat", referencedColumnName="id")
     **/
    private $contrat;

    /**
     * @ORM\Column(name="dateDebut", type="datetime")
     */
    private $dateDebut;

    /**
     * @ORM\Column(name="dateFin", type="datetime", nullable=true)
     */
    private $dateFin;

    /**
     * @ORM\ManyToOne(targetEntity="\Paie\ArretTravailBundle\Entity\ArretNature")
     * @ORM\JoinColumn(name="nature", referencedColumnName="id")
     **/
    private $nature;

    /**
     * @ORM\ManyToOne(targetEntity="\Paie\ArretTravailBundle\Entity\ArretCategorie")
     * @ORM\JoinColumn(name="categorie", referencedColumnName="id")
     **/
    private $categorie;

    /**
     * @ORM\Column(name="commentaire", type="text", nullable=true)
     */
    private $commentaire;

    /**
     * @ORM\Column(name="dateReception", type="datetime")
     */
    private $dateReception;

    /**
     * @var smallint
     *
     * @ORM\Column(name="typeReception", type="smallint")
     */
    private $typeReception;

    /**
     * @ORM\Column(name="dateTraitement", type="datetime", nullable=true)
     */
    private $dateTraitement;

    /**
     * @ORM\Column(name="datePriseEnCompte", type="datetime", nullable=true)
     */
    private $datePriseEnCompte;

    /**
     * @ORM\OneToMany(targetEntity="Paie\ArretTravailBundle\Entity\Piecejointe", mappedBy="arrets", cascade={"persist", "remove"})
     */
    private $piecejointes; //Notez le « s », un Arret de Travail est liée à plusieurs pièces jointes
    
    /**
     * @ORM\Column(name="grossesse", type="boolean", nullable=false)
     */
    private $grossesse;
    
    /**
     * Constructor
     */
    public function __construct()
    {
    	$this->piecejointes = new \Doctrine\Common\Collections\ArrayCollection();
    	$this->dateTraitement = new \DateTime();
    	$this->grossesse = false;
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /* fonction ne servant à rien, mais j'ai besoin d'une fonction setId */
    /**
     * Set id
     *
     * @param string $id
     * @return ArretTravail
     */
    public function setId($id)
    {
    }

    /**
     * Set idArretInitial
     *
     * @param \Paie\ArretTravailBundle\Entity\ArretTravail
     * @return ArretTravail
     */
    public function setIdArretInitial(\Paie\ArretTravailBundle\Entity\ArretTravail $IdArretInitial = null)
    {
        $this->IdArretInitial = $IdArretInitial;
        return $this;
    }

    /**
     * Get idArretInitial
     *
     * @return \Paie\ArretTravailBundle\Entity\ArretTravail
     */
    public function getIdArretInitial()
    {
        return $this->IdArretInitial;
    }

    /**
     * Set statut
     *
     * @param string $statut
     *
     * @return ArretTravail
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;
        return $this;
    }

    /**
     * Get statut
     *
     * @return string
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Get display statut
     *
     * @return string
     */
    public function getDisplayStatut(){
    	switch ($this->statut){
    		case 1:
    			return "non concerné";
    			break;
   			case 2:
   				return "en attente";
   				break;
			case 3:
    			return "soldé";
    			break;    		
    	}
    }
    
    /**
     * Get societe
     *
     * @return Paie\AdministrationBundle\Entity\PandoreSociete
     */
    public function getSociete()
    {
    	if ($this->contrat) {
    		return $this->contrat->getSociete();
    	} else {
    		return '';
    	}
    }

    /**
     * Get contrat
     *
     * @return Paie\AdministrationBundle\Entity\PandoreContrat
     */
    public function getContrat()
    {
    	return $this->contrat;
    }

    /**
     * Set contrat
     *
     * @param \Administration\SocieteBundle\Entity\PandoreContrat
     * @return ArretTravail
     */
    public function setContrat(\Administration\SocieteBundle\Entity\PandoreContrat $contrat = null)
    {
    	$this->contrat = $contrat;
    	return $this;
    }   
    
    /**
     * Set gestPaie
     *
     * @param User\UserBundle\Entity\User
     * @return ArretTravail
     */
    public function setGestPaie(\User\UserBundle\Entity\User $gestPaie = null)
    {
        $this->gestPaie = $gestPaie;
        return $this;
    }

    /**
     * Get gestPaie
     *
     * @return \User\UserBundle\Entity\User
     */
    public function getGestPaie()
    {
        return $this->gestPaie;
    }
    
    /**
     * Get gestPaie
     *
     * @return \Administration\SocieteBundle\Entity\PandoreIndividu
     */
    public function getSalarie()
    {
        if ($this->contrat) {
    		return $this->contrat->getUser();
    	} else {
    		return '';
    	}
    }       
    
    /**
     * Set gestPaie
     *
     * @param User\UserBundle\Entity\User
     * @return ArretTravail
     */
    public function setGestContrat(\User\UserBundle\Entity\User $gestContrat = null)
    {
    	$this->gestContrat = $gestContrat;
    	return $this;
    }
    
    /**
     * Get gestPaie
     *
     * @return \User\UserBundle\Entity\User
     */
    public function getGestContrat()
    {
    	return $this->gestContrat;
    }
    
    /**
     * Set gestRH
     *
     * @param User\UserBundle\Entity\User
     * @return ArretTravail
     */
    public function setGestRH(\User\UserBundle\Entity\User $gestRH = null)
    {
        $this->gestRH = $gestRH;
        return $this;
    }

    /**
     * Get gestRH
     *
     * @return \User\UserBundle\Entity\User
     */
    public function getGestRH()
    {
        return $this->gestRH;
    }

    /**
     * Get nomSalarie
     *
     * @return string
     */
    public function getNomSalarie()
    {
        if ($this->contrat) {
    		return $this->contrat->getUser();
    	} else {
    		return '';
    	}
    }
    

    /**
     * Get PrenomSalarie
     *
     * @return string
     */
    public function getPrenomSalarie()
    {
    	if ($this->salarie == null){
    		return '';
    	} else {
    		return $this->salarie->getPrenom();
    	}
    }

    /**
     * Get numeroSecuriteSociale
     *
     * @return string
     */
    public function getNumeroSecuriteSociale()
    {
    	if ($this->contrat == null){
    		return '';
    	} else {
	        return $this->contrat->getUser()->getNumeroSecu();
    	}
    }

    /**
     * Set dateDebut
     *
     * @param \DateTime $dateDebut
     *
     * @return ArretTravail
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;
        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return \DateTime
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateFin
     *
     * @param \DateTime $dateFin
     *
     * @return ArretTravail
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;
        return $this;
    }

    /**
     * Get dateFin
     *
     * @return \DateTime
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * Set nature
     *
     * @param string $nature
     *
     * @return ArretTravail
     */
    public function setNature($nature)
    {
        $this->nature = $nature;
        return $this;
    }

    /**
     * Get nature
     *
     * @return string
     */
    public function getNature()
    {
        return $this->nature;
    }

    /**
     * Set categorie
     *
     * @param string $categorie
     *
     * @return ArretTravail
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;
        return $this;
    }

    /**
     * Get categorie
     *
     * @return string
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set commentaire
     *
     * @param string $commentaire
     *
     * @return ArretTravail
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;
        return $this;
    }

    /**
     * Get commentaire
     *
     * @return string
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * Set dateReception
     *
     * @param \DateTime $dateReception
     *
     * @return ArretTravail
     */
    public function setDateReception($dateReception)
    {
        $this->dateReception = $dateReception;
        return $this;
    }

    /**
     * Get dateReception
     *
     * @return \DateTime
     */
    public function getDateReception()
    {
        return $this->dateReception;
    }

    /**
     * Set typeReception
     *
     * @param string $typeReception
     *
     * @return ArretTravail
     */
    public function setTypeReception($typeReception)
    {
        $this->typeReception = $typeReception;
        return $this;
    }

    /**
     * Get typeReception
     *
     * @return string
     */
    public function getTypeReception()
    {
        return $this->typeReception;
    }

    /**
     * Set dateTraitement
     *
     * @param \DateTime $dateTraitement
     *
     * @return ArretTravail
     */
    public function setDateTraitement($dateTraitement)
    {
        $this->dateTraitement = $dateTraitement;
        return $this;
    }

    /**
     * Get dateTraitement
     *
     * @return string
     */
    public function getDateTraitement()
    {	if ($this->dateTraitement->format('Y-m-d H:i:s') !== '1970-01-01 00:00:00') {
			return $this->dateTraitement->format('d/m/Y');
		} else {
			return '';
		}
    }

    /**
     * Set datePriseEnCompte
     *
     * @param \DateTime $datePriseEnCompte
     *
     * @return ArretTravail
     */
    public function setDatePriseEnCompte($datePriseEnCompte)
    {
        $this->datePriseEnCompte = $datePriseEnCompte;
        return $this;
    }

    /**
     * Get datePriseEnCompte
     *
     * @return \DateTime
     */
    public function getDatePriseEnCompte()
    {
    	if ($this->datePriseEnCompte->format('Y-m-d H:i:s') !== '1970-01-01 00:00:00') {
			return $this->datePriseEnCompte->format('Y-m-d H:i:s');
		} else {
			return '';
		}
    }

    /**
     * Get datePriseEnCompte
     *
     * @return \DateTime
     */
    public function DisplayDatePriseEnCompte()
    {
    	if ($this->datePriseEnCompte->format('Y-m-d H:i:s') !== '1970-01-01 00:00:00') {
    		return $this->datePriseEnCompte->format('d/m/Y');
    	} else {
    		return '';
    	}
    }
    
    
    /**
     * Get tempsTraitement
     *
     */
    public function getTempsTraitement()
    {
    	return date_diff($this->dateReception, $this->datePriseEnCompte)->days;
    }


    /**
     * Add piecejointes
     *
     * @param \Paie\ArretTravailBundle\Entity\Piecejointe $piecejointe
     * @return ArretTravail
     */
    public function addPiecejointe(\Paie\ArretTravailBundle\Entity\Piecejointe $piecejointe)
    {
        $this->piecejointes[] = $piecejointe;

        $piecejointe->setArrets($this);

        return $this;
    }

    /**
    * Get piecejointes
    * @return \Doctrine\Common\Collections\Collection
	*/
    public function getPiecejointes()
    {
    	return $this->piecejointes->toArray();
    }

    /**
     * Remove piecejointe
     *
     * @param \Paie\ArretTravailBundle\Entity\PieceJointe $piecejointe
     */
    public function removePiecejointe(\Paie\ArretTravailBundle\Entity\Piecejointe $piecejointe)
    {
        $this->piecejointes->removeElement($piecejointe);
    }
    
    /**
     * Get Reference
     *
     * @return string
     */
    public function getReferenceSociete()
    {
    	return $this->societe->getId();
    }
    
    /**
     * Get Duree
     *
     * @return int
     */
    public function getDuree()
    {
    	return  (int) $this->getDateDebut()->diff($this->getDateFin())->format('%a') + 1;
    }    

    /**
     * Get OrigDuree
     *
     * @return int
     */
    public function getDureeTotale()
    {
		return  (int) $this->getDateDebutInitiale()->diff($this->getDateFin())->format('%a') + 1;
    }

    /**
     * Get OrigDuree
     *
     * @return int
     */
    public function getDateDebutInitiale()
    {
    	if ($this->IdArretInitial){
    		return  $this->IdArretInitial->getDateDebutInitiale();
    	} else {
    		return $this->dateDebut;
    	}
    }   
    
    
    /**
     * Get Delai Reception
     *
     * @return int
     */
    public function getDelaiReception()
    {
    	return (int) $this->getDateDebut()->diff($this->getDateReception())->format('%a') + 1;
    }    

    /**
     * Get Delai Traitement
     *
     * @return int
     */
    public function getDelaiTraitement()
    
    {    
	    if ($this->dateReception->format('Y-m-d H:i:s') === '1970-01-01 00:00:00' || $this->statut === 1) {
	    	return  0;
	    } else if ($this->statut === 2) {
	    	$today = new DateTime();
	    	return (int) $this->getDateReception()->diff($today)->format('%a') + 1;
	    } else if ($this->statut === 3) {
			return (int) $this->dateReception->diff($this->dateTraitement)->format('%a') + 1;
	    }
    }
    
    /**
     * Get grossesse
     *
     * @return booelan
     */
    public function getGrossesse()
    {
    	return $this->grossesse;    	
    }


    /**
     * Set grossess
     *
     * @param boolean
     * @return ArretTravail
     */
    public function setGrossesse($grossesse)
    {
    	$this->grossesse = $grossesse;
    	return $this;
    }    
    
    /**
     * Get Modifications
     *
     * @return array
     */
    public function getModifications()
    
    {
    	return $this->modifications;
    }
    
    /**
     * Is Traité
     *
     * @return boolean
     */
    public function isTraite()
    
    {
        if ($this->dateTraitement->format('Y-m-d H:i:s') !== '1970-01-01 00:00:00') {
			return true;
		} else {
			return false;
		}
    }
    
    /**
     * Is Pris en compte
     *
     * @return boolean
     */
    public function isPrisEnCompte()
    
    {
    	if ($this->datePriseEnCompte->format('Y-m-d H:i:s') !== '1970-01-01 00:00:00') {
    		return true;
    	} else {
    		return false;
    	}
    }
    
    
    
}
