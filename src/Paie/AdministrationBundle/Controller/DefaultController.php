<?php

namespace Paie\AdministrationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Paie\AdministrationBundle\Entity\PaieSociete;
use Paie\AdministrationBundle\Form\SocieteSearchByGestionnaireType;
use Paie\AdministrationBundle\Form\SocieteType;


class DefaultController extends Controller
{
    /**
     *  
     * @Route("/", name="paie_homepage")
     */
    public function indexAction()
    {
    	return $this->render('PaieAdministrationBundle:Default:index.html.twig');
    }
    
    /**
     *
     * @Route("/administration/societes", name="paie_societe")
     */
    public function societesAction()
    {
    	$em = $this->getDoctrine()->getManager();
    	$request = $this->container->get('request');   	 
    	$request->request->get('societe');
    	// display the detail of an establishment is one have been selected
    	if($request->request->get('societe')){
       		$societe=new PaieSociete();
    		$societe = $em->getRepository('PaieAdministrationBundle:PaieSociete')->find($request->request->get('societe')['id']);
			$form = $this->createForm(new SocieteType($em),$societe);			
    		$form->handleRequest($request);
    		// if the establishment has been modify, we change the changes. useless?????
    		if ($form->isValid() and ($form->get('save')->isClicked())) {
    			$em->persist($societe);
    			$em->flush();
    		}
    	}
    	//creation du formulaire de recherche.
    	$societeSearch=new PaieSociete();
    	$formSearch=$this->createForm(new SocieteSearchByGestionnaireType($em),$societeSearch);    	 
    	$formSearch->handleRequest($request);   	 
    	$societes = $this->getDoctrine()
    		->getRepository('PaieAdministrationBundle:PaieSociete')
    		->research($societeSearch);
    	return $this->render('PaieAdministrationBundle:Default:societe.html.twig',
				array(
					'formsearch'=>$formSearch->createView(),
				)
		);
    }

    /**
     * @Route("/administration/listesocietes", name="paie_societe_liste")
     */
    public function societesListeAction()
    {
    	$request = $this->container->get('request');	
    	$em = $this->getDoctrine()->getManager();   	 
    	$societeSearch=new PaieSociete();
    	if($request->request->get('direction')!==''){    		
			$societeSearch->setDirection($this->getDoctrine()
	        	->getRepository('AdministrationDirectionBundle:Direction')
	        	->find($request->request->get('direction')));
    	}
		$societeSearch->setGestionnairePaie($this->getDoctrine()
			->getRepository('UserUserBundle:User')
			->find($request->request->get('gestionnairePaie')));
		$societeSearch->setGestionnaireContrat($this->getDoctrine()
				->getRepository('UserUserBundle:User')
				->find($request->request->get('gestionnaireContrat')));
		$societeSearch->setGestionnaireRH($this->getDoctrine()
				->getRepository('UserUserBundle:User')
				->find($request->request->get('gestionnaireRH')));
		
    	$societes = $this->getDoctrine()
	    	->getRepository('PaieAdministrationBundle:PaieSociete')
	    	->research($societeSearch);
    	
    	return $this->render('PaieAdministrationBundle:Default:listesociete.html.twig',
    			array(
    					'listeSocietes' => $societes,
    			)
    	);
    }


    
    /**
     *
     * @Route("/administration/updatesocietes", name="paie_societe_update")
     */
    public function societesUpdateAction()
    {
    	$em = $this->getDoctrine()->getManager('default');
    	$request = $this->container->get('request');
    	$societe=new PaieSociete();
    	$societe = $em->getRepository('PaieAdministrationBundle:PaieSociete')->find($request->request->get('societe')['id']);
    	$form = $this->createForm(new SocieteType($em),$societe);
		$form->handleRequest($request);
		if ($form->isValid()) {
			$em->persist($societe);
			$em->flush();
			$message = 'L’enregistrement s\'est bien déroulé !';
			$session = $this->getRequest()->getSession();
			$this->get('session')->getFlashBag()->add('modificationSociete', $message);
			$session->set('errors',0);
			$session->save();
		}
    	return $this->render('PaieAdministrationBundle:Default:formsociete.html.twig',
    			array(
    				'formSociete' => $form->createView(),
    				'societe' => $societe,
    			)
    	);
    }    

    /**
     * @Route("/administration/exportsocietes", name="paie_societe_export")
     */
    public function getExportSocieteAction(){
    	$request = $this->container->get('request');
    	$em = $this->getDoctrine()->getManager();
    	$societeSearch=new PaieSociete();
    	$societes = $this->getDoctrine()
    	->getRepository('PaieAdministrationBundle:PaieSociete')
    	->research($societeSearch);
    	 
    	// ask the service for a Excel5
    	$phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
    	$phpExcelObject->getProperties()->setCreator('export program')
    	->setLastModifiedBy('export program')
    	->setTitle('Liste de Société');
    	$phpExcelObject->getActiveSheet()->setTitle('Export_Societe');
    	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
    	$this->get('phpexcel');
    	$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(0).(1), "Réference");
    	$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(1).(1), "Raison");
    	$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(2).(1), "Direction");
    	$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(3).(1), "Région");
    	$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(4).(1), "Gerant");
    	$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(5).(1), "Categorie");
    	$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(6).(1), "Gestionnaire Paie");
    	$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(7).(1), "Gestionnaire Contrat");
    	$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(8).(1), "Gestionnaire RH");
    	$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(9).(1), "Animateur de Zone");
    	$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(10).(1), "ADV");
    	$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(11).(1), "Capital");
    	$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(12).(1), "Adresse");
    	$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(13).(1), "Code Postal");
    	$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(14).(1), "Ville");
    	$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(15).(1), "Siret");
    	$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(16).(1), "Mutuelle");
    	$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(17).(1), "RCS");
    	$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(18).(1), "Code APE");
    	$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(19).(1), "CCN");
    	$i = 2;
    	foreach($societes as $societe){
    		$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(0).($i), $societe->getReference());
    		$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(1).($i), $societe->getRaison());
    		if ($societe->getDirection()){
    			$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(2).($i), $societe->getDirection()->getLibelle());
    		}
    		$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(3).($i), $societe->getRegion());
    		$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(4).($i), $societe->getGerant());
    		$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(5).($i), $societe->getCategorie());
    		if ($societe->getGestionnairePaie()){
    			$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(6).($i), $societe->getGestionnairePaie()->getDisplayName());
    		}
    		if ($societe->getGestionnaireContrat()){
    			$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(7).($i), $societe->getGestionnaireContrat()->getDisplayName());
    		}
    		if ($societe->getGestionnaireRH()){
    			$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(8).($i), $societe->getGestionnaireRH()->getDisplayName());
    		}
    		$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(9).($i), $societe->getAnimateurZone());
    		$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(10).($i), $societe->getADV());
    		$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(11).($i), str_replace(' ','',$societe->getCapital()));
    		$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(12).($i), $societe->getAdresse());
    		$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(13).($i), $societe->getCodePostal());
    		$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(14).($i), $societe->getVille());
    		$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(15).($i), $societe->getSiret());
    		$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(16).($i), $societe->getMutuelle());
    		$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(17).($i), $societe->getRCS());
    		$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(18).($i), $societe->getCodeAPE());
    		$phpExcelObject->getActiveSheet()->SetCellValue($this->stringFromColumnIndex(19).($i), $societe->getCCN());
    		$i++;
    	}
    	// create the writer
    	$writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
    	// create the response
    	$response = $this->get('phpexcel')->createStreamedResponse($writer);
    	// adding headers
    	$response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
    	$response->headers->set('Content-Disposition', 'attachment;filename=' . 'Export_Arrets' . '.xls');
    	$response->headers->set('Pragma', 'public');
    	$response->headers->set('Cache-Control', 'maxage=1');
    	return $response;
    }
    
    public static function stringFromColumnIndex($pColumnIndex = 0) {
    	// Determine column string
    	if ($pColumnIndex < 26) {
    		return chr(65 + $pColumnIndex);
    	} elseif ($pColumnIndex < 702) {
    		return chr(64 + ($pColumnIndex / 26)) . chr(65 + $pColumnIndex % 26);
    	}
    	return chr(64 + (($pColumnIndex - 26) / 676)) . chr(65 + ((($pColumnIndex - 26) % 676) / 26)) . chr(65 + $pColumnIndex % 26);
    }
    
    
    /**
     * @Route("/administration/getsocietes", name="paie_get_societe")
     */
    public function getSocieteAction()
    {
    	$request = $this->container->get('request');
    	$idsociete = $request->request->get('idsociete');
    	$societe=$this->getDoctrine()
    		->getRepository('PaieAdministrationBundle:PaieSociete')
    		->find($idsociete);    	 
    	$formSociete = $this->createForm(new SocieteType($this->getDoctrine()->getManager()), $societe);
    	return  $this->render('PaieAdministrationBundle:Default:form.html.twig', array(
    			'formSociete' => $formSociete->createView(),
    			'societe' => $societe
    	));
    }
    
}
