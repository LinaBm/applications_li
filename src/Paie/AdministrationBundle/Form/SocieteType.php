<?php

namespace Paie\AdministrationBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\File;

class SocieteType extends AbstractType
{
	private $em;
	
	public function __construct($em){
		$this->em = $em;
	}
	
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
		->add('id', 'hidden')
		->add('direction', 'entity', array(
				'class' => 'AdministrationDirectionBundle:Direction',
				'property' => 'libelle',
				'multiple' => false,
				'expanded' => false,				
				'required'  => false,
				'label' => "Direction"
		))
		->add('categorie', 'text', array(
				'required'  => false,
				'label' => "Categorie"
		))
		->add('region', 'text', array(
				'required'  => false,
				'label' => "Région"
		))
		->add('gestionnairePaie', 'entity', array(
			'class' => 'UserUserBundle:User',
			'property' => 'displayname',
			'multiple' => false,
			'expanded' => false,
			'required' => false,
			'label' => 'Gestionnaire Paie',
			'empty_data'  => null,
			'query_builder'=> $this->em->getRepository('UserUserBundle:User')
				->getByRole('ROLE_PAIE_GESTIONNAIRE'),
		))
		->add('gestionnaireContrat', 'entity', array(
			'class' => 'UserUserBundle:User',
			'property' => 'displayname',
			'multiple' => false,
			'expanded' => false,
			'required' => false,
			'label' => 'Gestionnaire Contrat',
			'empty_data'  => null,
			'query_builder'=> $this->em->getRepository('UserUserBundle:User')
				->getByRole('ROLE_PAIE_CONTRAT'),
		))
		->add('gestionnaireRH', 'entity', array(
			'class' => 'UserUserBundle:User',
			'property' => 'displayname',
			'multiple' => false,
			'expanded' => false,
			'required' => false,
			'label' => 'Gestionnaire RH',
			'empty_data'  => null,
			'query_builder'=> $this->em->getRepository('UserUserBundle:User')
				->getByRole('ROLE_PAIE_RH'),
		))
		->add('AnimateurZone', 'text', array(
				'required'  => false,
				'label' => "Animateur de Zone"
		))
		->add('adv', 'text', array(
				'required'  => false,
				'label' => "A.D.V."
		))		
		->add('RCS', 'text', array(
				'required'  => false,
				'label' => "RCS"
		))		
		->add('codeAPE', 'text', array(
				'required'  => false,
				'label' => "code APE"
		))		
		->add('CCN', 'text', array(
				'required'  => false,
				'label' => "CCN"
		))		
		->add('nomNetEntreprise', 'text', array(
				'required'  => false,
				'label' => "Nom"
		))		
		->add('prenomNetEntreprise', 'text', array(
				'required'  => false,
				'label' => "Prénom"
		))		
		->add('passwordNetEntreprise', 'text', array(
				'required'  => false,
				'label' => "Mot de Passe"
		))
		->add('gerant', 'text', array(
				'required'  => false,
				'label' => "Gérant"
		))
		->add('mutuelle', 'text', array(
				'required'  => false,
				'label' => "Mutuelle"
		))
		;
	}
	
	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Paie\AdministrationBundle\Entity\PaieSociete'
		));
	}
	
	/**
	 * @return string
	 */
	public function getName()
	{
		return 'societe';
	}
}
