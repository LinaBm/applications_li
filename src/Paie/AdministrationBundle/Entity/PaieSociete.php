<?php

namespace Paie\AdministrationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use User\UserBundle\Entity\User;
use Administration\SocieteBundle\Entity\PandoreSociete;


/**
 * PaieSociete
 *
 * @ORM\Table("pai_societes")
 * @ORM\Entity(repositoryClass="Paie\AdministrationBundle\Entity\PaieSocieteRepository")
 */
class PaieSociete
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
	* @ORM\OneToOne(targetEntity="Administration\SocieteBundle\Entity\PandoreEtablissement", fetch="EAGER")
    * @ORM\JoinColumn(referencedColumnName="siret")
    */
    private $etablissement;
    
    /**
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;

    /**
     * @ORM\Column(name="categorie", type="string", length=255)
     */
    private $categorie;
    
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User" , inversedBy="Societe")
     */
    private $gestionnairePaie;
    
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User" , inversedBy="Societe")
     */
    private $gestionnaireContrat;
    
    /**
     * @ORM\ManyToOne(targetEntity="User\UserBundle\Entity\User" , inversedBy="Societe")
     */
    private $gestionnaireRH;
    
    /**
     * @ORM\Column(name="region", type="string", length=255)
     */
    private $region;

    /**
     * @ORM\Column(name="animateurZone", type="string", length=255, nullable=true)
     */
    private $AnimateurZone;

    /**
     * @ORM\Column(name="adv", type="string", length=255, nullable=true)
     */
    private $adv;
    
    /**
     * @ORM\Column(name="RCS", type="string", length=255, nullable=true)
     */
    private $RCS;

    /**
     * @ORM\Column(name="APE", type="string", length=255, nullable=true)
     */
    private $codeAPE;

    /**
     * @ORM\Column(name="CCN", type="string", length=255, nullable=true)
     */
    private $CCN;
    
    /**
     * @ORM\Column(name="nomNetEntreprise", type="string", length=255, nullable=true)
     */
    private $nomNetEntreprise;
    
    /**
     * @ORM\Column(name="prenomNetEntreprise", type="string", length=255, nullable=true)
     */
    private $prenomNetEntreprise;
    
    /**
     * @ORM\Column(name="passwordNetEntreprise", type="string", length=255, nullable=true)
     */
    private $passwordNetEntreprise;
    
    /**
     * @ORM\Column(name="gerant", type="string", length=255, nullable=true)
     */
    private $gerant;
    
    /**
     * @ORM\Column(name="mutuelle", type="string", length=255, nullable=true)
     */
    private $mutuelle;
    
	public function getUniqueName()
	{
		return "a";
	}

	/**
	 * @ORM\ManyToOne(targetEntity="Administration\DirectionBundle\Entity\Direction" , inversedBy="societes")
	 */
	private $direction;

	/**
	 * Set region
	 *
	 * @param string $region
	 *
	 * @return Societe
	 */
	public function setRegion($region)
	{
		$this->region = $region;
	
		return $this;
	}
	
	/**
	 * Get region
	 *
	 * @return string
	 */
	public function getRegion()
	{
		return $this->region;	
	}
	
	public function __toString()
	{
		return $this->getLibelle();
	}
	
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /* fonction ne servant à rien, mais j'ai besoin d'une fonction setId */
    /**
     * Set id
     *
     * @param string $libelle
     * @return PaieSociete
     */
    public function setId($id)
    {
    }   


    /**
     * Get Etablissement
     *
     * @return Administration\SocieteBundle\Entity\PandoreEtablissement 
     */
    public function getEtablissement()
    {
        return $this->etablissement;
    }
    
    /**
     * Set Etablissement
     *
     * @param string Administration\SocieteBundle\Entity\PandoreEtablissement
     * @return PaieSociete
     */
    public function setEtablissement($etablissement)
    {
    	$this->etablissement = $etablissement;
    	
    	return $this;
    }  
    
    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Societe
     */
    public function setActive($active)
    {
    	$this->active = $active;
    
    	return $this;
    }

    /**
     * Set rCS
     *
     * @param string $rCS
     *
     * @return Societe
     */
    public function setRCS($rCS)
    {
    	$this->RCS = $rCS;
    
    	return $this;
    }
    
    /**
     * Get code APE
     *
     * @return string
     */
    public function getCodeAPE()
    {
    	return $this->codeAPE;
    }

    /**
     * Set code APE
     *
     * @param string $rCS
     *
     * @return Societe
     */
    public function setCodeAPE($codeAPE)
    {
    	$this->codeAPE = $codeAPE;
    
    	return $this;
    }
    
    /**
     * Get rCS
     *
     * @return string
     */
    public function getRCS()
    {
    	return $this->RCS;
    }
    
    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
    	return $this->active;
    }
    
    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
    	return $this->etablissement->getSociete()->getId();
    }    
    
    /**
     * Get raison
     *
     * @return string
     */
    public function getRaison()
    {
    	return $this->etablissement->getRaison();
    }
    
    /**
     * Get Siret
     *
     * @return string
     */
    public function getSiret()
    {
    	return $this->etablissement->getSiret();
    }
    
    /**
     * Get Capital
     *
     * @return string
     */
    public function getCapital()
    {
    	return $this->etablissement->getCapital();
    }

    /**
     * Get Ville
     *
     * @return string
     */
    public function getVille()
    {
    	return $this->etablissement->getVille();
    }


    /**
     * Get NumEtablissement
     *
     * @return string
     */
    public function getNumEtablissement()
    {
    	return $this->etablissement->getNumero();
    }    
    
    
    /**
     * Set categorie
     *
     * @param string $categorie
     *
     * @return Societe
     */
    public function setCategorie($categorie)
    {
    	$this->categorie = $categorie;
    
    	return $this;
    }
    
    /**
     * Get categorie
     *
     * @return string
     */
    public function getCategorie()
    {
    	return $this->categorie;
    }
    /**
     * Set direction
     *
     * @param \Administration\DirectionBundle\Entity\Direction $direction
     * @return Service
     */
    public function setDirection(\Administration\DirectionBundle\Entity\Direction $direction)
    {
    	$this->direction = $direction;
    
    	return $this;
    }
    
    /**
     * Get direction
     *
     * @return \Administration\DirectionBundle\Entity\Direction
     */
    public function getDirection()
    {
    	return $this->direction;
    }
    
    /**
     * Set gestionnairePaie
     *
     * @param \User\UserBundle\Entity\User $gestionnairePaie
     *
     * @return Societe
     */
    public function setGestionnairePaie(\User\UserBundle\Entity\User $gestionnairePaie = null)
    {
    	$this->gestionnairePaie = $gestionnairePaie;
    
    	return $this;
    }
    
    /**
     * Get gestionnairePaie
     *
     * @return \User\UserBundle\Entity\User
     */
    public function getGestionnairePaie()
    {
    	return $this->gestionnairePaie;
    }
    
    /**
     * Set gestionnaireContrat
     *
     * @param \User\UserBundle\Entity\User $gestionnaireContrat
     *
     * @return Societe
     */
    public function setGestionnaireContrat(\User\UserBundle\Entity\User $gestionnaireContrat = null)
    {
    	$this->gestionnaireContrat = $gestionnaireContrat;
    
    	return $this;
    }
    
    /**
     * Get gestionnaireContrat
     *
     * @return \User\UserBundle\Entity\User
     */
    public function getGestionnaireContrat()
    {
    	return $this->gestionnaireContrat;
    }
    
    /**
     * Set gestionnaireRH
     *
     * @param \User\UserBundle\Entity\User $gestionnaireRH
     *
     * @return Societe
     */
    public function setGestionnaireRH(\User\UserBundle\Entity\User $gestionnaireRH = null)
    {
    	$this->gestionnaireRH = $gestionnaireRH;
    
    	return $this;
    }
    
    /**
     * Get gestionnaireRH
     *
     * @return \User\UserBundle\Entity\User
     */
    public function getGestionnaireRH()
    {
    	return $this->gestionnaireRH;
    }
    
    
    /**
     * Set animateurZone
     *
     * @param string $animateurZone
     *
     * @return Societe
     */
    public function setAnimateurZone($animateurZone)
    {
    	$this->AnimateurZone = $animateurZone;
    
    	return $this;
    }
    
    /**
     * Get animateurZone
     *
     * @return string
     */
    public function getAnimateurZone()
    {
    	return $this->AnimateurZone;
    }
    
    /**
     * Set adv
     *
     * @param string $adv
     *
     * @return Societe
     */
    public function setAdv($adv)
    {
    	$this->adv = $adv;
    
    	return $this;
    } 
    
    /**
     * Get adv
     *
     * @return string
     */
    public function getAdv()
    {
    	return $this->adv;
    }
    
    
    /**
     * Set CCN
     *
     * @param string $CCN
     *
     * @return Societe
     */
    public function setCCN($CCN)
    {
    	$this->CCN = $CCN;
    
    	return $this;
    }
    
    /**
     * Get CCN
     *
     * @return string
     */
    public function getCCN()
    {
    	return $this->CCN;
    }
    
    /**
     * Set netEntreprise
     *
     * @param string $netEntreprise
     *
     * @return Societe
     */
    public function setNetEntreprise($netEntreprise)
    {
    	$this->netEntreprise = $netEntreprise;
    
    	return $this;
    }
    
    /**
     * Get netEntreprise
     *
     * @return string
     */
    public function getNetEntreprise()
    {
    	return $this->netEntreprise;
    }
    
    /**
     * Set nomNetEntreprise
     *
     * @param string $nomNetEntreprise
     *
     * @return Societe
     */
    public function setNomNetEntreprise($nomNetEntreprise)
    {
    	$this->nomNetEntreprise = $nomNetEntreprise;
    
    	return $this;
    }
    
    /**
     * Get nomNetEntreprise
     *
     * @return string
     */
    public function getNomNetEntreprise()
    {
    	return $this->nomNetEntreprise;
    }
    
    /**
     * Set prenomNetEntreprise
     *
     * @param string $prenomNetEntreprise
     *
     * @return Societe
     */
    public function setPrenomNetEntreprise($prenomNetEntreprise)
    {
    	$this->prenomNetEntreprise = $prenomNetEntreprise;
    
    	return $this;
    }
    
    /**
     * Get prenomNetEntreprise
     *
     * @return string
     */
    public function getPrenomNetEntreprise()
    {
    	return $this->prenomNetEntreprise;
    }
    
    /**
     * Set passwordNetEntreprise
     *
     * @param string $passwordNetEntreprise
     *
     * @return Societe
     */
    public function setPasswordNetEntreprise($passwordNetEntreprise)
    {
    	$this->passwordNetEntreprise = $passwordNetEntreprise;
    
    	return $this;
    }
    
    /**
     * Get passwordNetEntreprise
     *
     * @return string
     */
    public function getPasswordNetEntreprise()
    {
    	return $this->passwordNetEntreprise;
    }
    
    /**
     * Get formeJuridique
     *
     * @return string
     */
    public function getFormeJuridique()
    {
    	return $this->etablissement->getFormeJuridique();
    }
   
    /**
     * Get adv
     *
     * @return string
     */
    public function getAdresse()
    {
    	return $this->etablissement->getAdresse();
    }    


    /**
     * Get Code Postal
     *
     * @return string
     */
    public function getCodePostal()
    {
    	return $this->etablissement->getCodePostal();
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
    	if ($this->etablissement == null){
    		return '';
    	} else {
    	return $this->etablissement->getlibelle();
    	}
    }
    
    /**
     * Get gerant
     *
     * @return string
     */
    public function getGerant()
    {
    	return $this->gerant;
    }
    
    /**
     * Set gerant
     *
     * @param string $gerant
     *
     * @return Societe
     */
    public function setGerant($gerant)
    {
    	$this->gerant = $gerant;
    
    	return $this;
    }

    /**
     * Get mutuelle
     *
     * @return string
     */
    public function getMutuelle()
    {
    	return $this->mutuelle;
    }
    
    /**
     * Set mutuelle
     *
     * @param string $mutuelle
     *
     * @return Societe
     */
    public function setMutuelle($mutuelle)
    {
    	$this->mutuelle = $mutuelle;
    
    	return $this;
    }

}