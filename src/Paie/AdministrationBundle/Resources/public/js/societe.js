function getSociete(){
	var idsociete=$('input[type=radio][name=id_societe]:checked').val();
	if (typeof idsociete != 'undefined') {
		$.ajax({
			dataType: "html",
			type: "POST",
			url: url_get_societe,
			data: { idsociete : idsociete},
			async:false,
			cache: false,
			error: function(jqXHR,textStatus, errorThrown ) {
				alert(jqXHR.responseText); return false;
			},
			success: function(societe){
				$('#detailSociete').html(societe);
				$('#enregistrerSociete').click(function(){
					$("#loader").show();
					var datain = $('form[name="societe"]').serialize();
					$('#formulaireSociete').html("");
					$.ajax({
						dataType: "html",
						type: "POST",
						url: url_update_societe,
						data: datain, 
						success: function(dataout){
							$('#formulaireSociete').html(dataout);
						},
						complete: function(){
							$("#loader").hide();
						}
					});
				});
			}                                                                            
		});
	}
}

function getListeSociete(){
	$('#listeSocietes').html('');
	var direction = $('#societe_search_direction').val();
	var gestionnairePaie = $('#societe_search_gestionnairePaie').val();
	var gestionnaireContrat = $('#societe_search_gestionnaireContrat').val();
	var gestionnaireRH = $('#societe_search_gestionnaireRH').val();
	$("#loader").show();
	$.ajax({
		dataType: "html",
		type: "POST",
		url: url_get_liste_societe,
		data: { direction : direction, gestionnairePaie : gestionnairePaie, gestionnaireContrat : gestionnaireContrat, gestionnaireRH : gestionnaireRH },  
		async: false,
		cache: false,
		error: function(jqXHR,textStatus, errorThrown ) {
		},
		success: function(data){
			$('#listeSocietes').html(data);
			$("#listSociete").tablesorter({
				widthFixed : true,
				widgets:['filter']
			});
			$('.societe').click(function() {
				var selectedId = $(this).parent('tr').attr('id');
				$("#loader").show();
				$('input:radio').each(function(){
					if($(this).attr('value') != selectedId){
						$(this).prop('checked', false);
					}else{
						$(this).prop('checked', true);
					}
				});
				getSociete();
				$("#loader").hide();
			});
		},
		complete: function(){
			$("#loader").hide();
		}
	});
	return false;
}

$(document).ready(function() {
	getSociete();
	getListeSociete();
	
	 $('.widgetSearch').change(function(){
		 getListeSociete();
		 });
	
	 $('input[name=id_societe]').change(function(){
		getSociete();
	 });
});