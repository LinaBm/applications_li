<?php

namespace DPDCBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CommentaireN2
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="DPDCBundle\Entity\CommentaireN2Repository")
 * @ORM\Table(name="dpdc_commentairen2s"))
 */
class CommentaireN2
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="mois", type="date", nullable=false)
     */
    private $mois;

    /**
     * @var integer
     *
     * @ORM\Column(name="niveau1", type="integer", nullable=false)
     */
    private $niveau1;

    /**
     * @var integer
     *
     * @ORM\Column(name="niveau2", type="integer", nullable=false)
     */
    private $niveau2;

    /**
     * @var string
     *
     * @ORM\Column(name="commentaire", type="text", nullable=false)
     */
    private $commentaire;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAdded", type="date", nullable=true)
     */
    private $dateAdded;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateModified", type="date", nullable=true)
     */
    private $dateModified;

    /**
     * @var string
     *
     * @ORM\Column(name="addedBy", type="string", length=255, nullable=true)
     */
    private $addedBy;

    /**
     * @var string
     *
     * @ORM\Column(name="modifiedBy", type="string", length=255, nullable=true)
     */
    private $modifiedBy;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mois
     *
     * @param \DateTime $mois
     *
     * @return CommentaireN2
     */
    public function setMois($mois)
    {
        $this->mois = $mois;

        return $this;
    }

    /**
     * Get mois
     *
     * @return \DateTime
     */
    public function getMois()
    {
        return $this->mois;
    }

    /**
     * Set niveau1
     *
     * @param string $niveau1
     *
     * @return CommentaireN2
     */
    public function setNiveau1($niveau1)
    {
        $this->niveau1 = $niveau1;

        return $this;
    }

    /**
     * Get niveau1
     *
     * @return string
     */
    public function getNiveau1()
    {
        return $this->niveau1;
    }

    /**
     * Set niveau2
     *
     * @param integer $niveau2
     *
     * @return CommentaireN2
     */
    public function setNiveau2($niveau2)
    {
        $this->niveau2 = $niveau2;

        return $this;
    }

    /**
     * Get niveau2
     *
     * @return integer
     */
    public function getNiveau2()
    {
        return $this->niveau2;
    }

    /**
     * Set commentaire
     *
     * @param string $commentaire
     *
     * @return CommentaireN2
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * Get commentaire
     *
     * @return string
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * Set dateAdded
     *
     * @param \DateTime $dateAdded
     *
     * @return CommentaireN2
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;
        return $this;
    }

    /**
     * Get dateAdded
     *
     * @return \DateTime
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * Set dateModified
     *
     * @param \DateTime $dateModified
     *
     * @return CommentaireN2
     */
    public function setDateModified($dateModified)
    {
        $this->dateModified = $dateModified;
        return $this;
    }

    /**
     * Get dateModified
     *
     * @return \DateTime
     */
    public function getDateModified()
    {
        return $this->dateModified;
    }

    /**
     * Set addedBy
     *
     * @param string $addedBy
     *
     * @return CommentaireN2
     */
    public function setAddedBy($addedBy)
    {
        $this->addedBy = $addedBy;
        return $this;
    }

    /**
     * Get addedBy
     *
     * @return string
     */
    public function getAddedBy()
    {
        return $this->addedBy;
    }

    /**
     * Set modifiedBy
     *
     * @param string $modifiedBy
     *
     * @return CommentaireN2
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;
        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return string
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }
}

