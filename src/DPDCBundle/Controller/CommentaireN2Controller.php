<?php

namespace DPDCBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use DPDCBundle\Entity\CommentaireN2;
use DPDCBundle\Form\CommentaireN2Type;
use \DateTime;
use \DateInterval;
use \DatePeriod;

class CommentaireN2Controller extends Controller
{
    /**
     * @Route("/commentaireN2/modifier/{id}", name="dpdc_commentaireN2_modifier")
     */
	public function modifierCommentaireN2Action($id, Request $request)
	{
		$user = $this->getUser();
    	$em = $this->getDoctrine()->getManager('default');
		$commentaireN2 = $this->getDoctrine()
			->getRepository('DPDCBundle:CommentaireN2')
			->find((int)$id);		
		if(!$commentaireN2){
			return new Response('pas trouvé');
		}
		if($commentaireN2->getNiveau2() !== $user->getId()){
			return new Response('pas votre id');
		}
		$mois = $commentaireN2->getMois()->format('m-y');
		$niveau1 = $commentaireN2->getNiveau1();
		$niveau2 = $commentaireN2->getNiveau2();
		$form = $this->createForm(new CommentaireN2Type($mois, $niveau1, $niveau2), $commentaireN2);
		$form->handleRequest($request);
		if ($form->isValid()) {
			$commentaireN2->setDateModified(new Datetime());
			$commentaireN2->setModifiedBy($user->getDisplayName());
			$em = $this->getDoctrine()->getManager();
			$em->persist($commentaireN2);
			$em->flush();
			$this->get('session')->getFlashBag()->add('dpdcCommentaireN2Success', 'Le commentaire est enregistré!');
			return $this->redirect($this->generateUrl('dpdc_commentaireN2_modifier', array('id' => $commentaireN2->getId())));
		}
		$niveau1 = $this->getDoctrine()
			->getRepository('UserUserBundle:User')
			->find((int)$niveau1);
        return $this->render('DPDCBundle:commentaireN2:ajouterCommentaireN2.html.twig', array(
			'formCommentaireN2' => $form->createView(),
			'mois' => $mois,
			'N2' => $user->getDisplayName(),
			'N1' => $niveau1->getDisplayName(),
			'idCommentaireN2' => $commentaireN2->getId()
		));
	}

    /**
     * @Route("/commentaireN2/ajouter", name="dpdc_commentaireN2_ajouter")
     */
	public function ajouterCommentaireN2Action(Request $request)
	{
		$user = $this->getUser();
    	$em = $this->getDoctrine()->getManager('default');
		$userId = $user->getId();
		$commentaireN2 = new CommentaireN2();
		if($request->request->get('dpdc_commentaireN2')){			
			$data = $request->request->get('dpdc_commentaireN2');
			$mois = $data['mois'];
			$niveau1 = $data['niveau1'];
			$niveau2 = $data['niveau2'];
		}else{
			$mois = $request->request->get('mois');
			$niveau1 = $request->request->get('niveau1');
			$niveau2 = $request->request->get('niveau2');
			$myData = explode('-', $mois);
			$theMonth = $myData[0];
			$theYear = '20' . $myData[1];
			$oldCommentaireN2 = $this->getDoctrine()
				->getRepository('DPDCBundle:CommentaireN2')
				->findOneByYearMonth($theYear, $theMonth, $userId, $niveau1);
			if(count($oldCommentaireN2) > 0){
				return $this->redirect($this->generateUrl('dpdc_commentaireN2_modifier', array('id' => $oldCommentaireN2[0]->getId())));
			}
		}						
		$planifies = $this->getDoctrine()
			->getRepository('DPDCBundle:PointDeControle')
			->getPlanifiesMoisN1N2($mois, $niveau1, $niveau2);
		$resultats = $this->getDoctrine()
			->getRepository('DPDCBundle:Resultat')
			->getResultatsMoisN1N2($mois, $niveau1, $niveau2, $planifies);
		$pasEnregistres = array_diff_key($planifies, $resultats);
		$reponsesOui = count(array_filter($resultats));
		$reponsesNOn = count($resultats) - $reponsesOui;
		if(!count($pasEnregistres)){
			return new Response('toutes enregistrées');
		}	
		$form = $this->createForm(new CommentaireN2Type($mois, $niveau1, $niveau2), $commentaireN2);
		$form->handleRequest($request);
		if ($form->isValid()) {
			$data = $request->request->get('dpdc_commentaireN2');
			$commentaireN2->setDateAdded(new Datetime());
			$commentaireN2->setAddedBy($user->getDisplayname());
			$commentaireN2->setMois(DateTime::createFromFormat('m-y', $mois));
			$commentaireN2->setNiveau1($data['niveau1']);
			$commentaireN2->setNiveau2($data['niveau2']);
			$em = $this->getDoctrine()->getManager();
			$em->persist($commentaireN2);
			$em->flush();
			$this->get('session')->getFlashBag()->add('dpdcCommentaireN2Success', 'Le resultat est enregistré!');
			return $this->redirect($this->generateUrl('dpdc_commentaireN2_modifier', array('id' => $commentaireN2->getId())));
		}
		$niveau1 = $this->getDoctrine()
			->getRepository('UserUserBundle:User')
			->find((int)$niveau1);
        return $this->render('DPDCBundle:commentaireN2:ajouterCommentaireN2.html.twig', array(
			'formCommentaireN2' => $form->createView(),
			'mois' => $mois,
			'N2' => $user->getDisplayName(),
			'N1' => $niveau1->getDisplayName()
		));
	}

}
