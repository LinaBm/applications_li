$(function(){
	$('.chosen').chosen();
});

$('.enregistrerResultat').click(function(){
	var selectedId = $(this).parent('td').parent('tr').attr('id');
	var selectedMonth = $(this).parent('td').parent('tr').attr('class');
	$("#loader").show();
	$.ajax({
		type: "POST",
		url: url_ajouter_resultat,
		data: { idPoint: selectedId, month: selectedMonth, niveau: 0 },
		success: function(resultat){
			$('#pointResultats').modal('show');
			$('#pointResultatsModalBody').html(resultat);
		},
		complete: function(){
			$("#loader").hide();
		}
	});
});