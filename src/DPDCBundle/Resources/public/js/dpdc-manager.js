$(function(){
	$('.chosen').chosen();
	$('.enregistrerCommentaireN2').click(function(){
		var selectedId = $(this).parent('td').parent('tr').attr('id');
		var selectedMois = $(this).parent('td').parent('tr').attr('class');
		if($(this).hasClass('dejaEnregistre')){
			
		}else{
			$("#loader").show();
			$.ajax({
				type: "POST",
				url: url_commentaireN2_ajouter,
				data: { niveau2: userId, niveau1: selectedId, mois: selectedMois },
				success: function(commentaireN2){
					$('#niveau1CommentaireN2').modal('show');
					$('#niveau1CommentaireN2ModalBody').html(commentaireN2);
				},
				complete: function(){
					$("#loader").hide();
				}
			});
		}	
	});
});