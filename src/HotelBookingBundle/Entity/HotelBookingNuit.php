<?php

namespace HotelBookingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HotelBookingNuit
 *
 * @ORM\Table(name="hotel_booking_nuits")
 * @ORM\Entity(repositoryClass="HotelBookingBundle\Entity\HotelBookingNuitRepository")
 */
class HotelBookingNuit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="idHotelBooking", type="integer")
     */
    private $idHotelBooking;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="arrivee", type="date")
     */
    private $arrivee;

    /**
     * @var boolean
     *
     * @ORM\Column(name="dinner", type="boolean", nullable=true)
     */
    private $dinner;

    /**
     * @var string
     *
     * @ORM\Column(name="commentaire", type="text", nullable=true)
     */
    private $commentaire;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idHotelBooking
     *
     * @param integer $idHotelBooking
     *
     * @return HotelBookingNuit
     */
    public function setIdHotelBooking($idHotelBooking)
    {
        $this->idHotelBooking = $idHotelBooking;

        return $this;
    }

    /**
     * Get idHotelBooking
     *
     * @return integer
     */
    public function getIdHotelBooking()
    {
        return $this->idHotelBooking;
    }

    /**
     * Set arrivee
     *
     * @param \DateTime $arrivee
     *
     * @return HotelBookingNuit
     */
    public function setArrivee($arrivee)
    {
        $this->arrivee = $arrivee;

        return $this;
    }

    /**
     * Get arrivee
     *
     * @return \DateTime
     */
    public function getArrivee()
    {
        return $this->arrivee;
    }

    /**
     * Set dinner
     *
     * @param boolean $dinner
     *
     * @return HotelBookingNuit
     */
    public function setDinner($dinner)
    {
        $this->dinner = $dinner;

        return $this;
    }

    /**
     * Get dinner
     *
     * @return boolean
     */
    public function getDinner()
    {
        return $this->dinner;
    }

    /**
     * Set commentaire
     *
     * @param string $commentaire
     *
     * @return HotelBookingNuit
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * Get commentaire
     *
     * @return string
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }
}

