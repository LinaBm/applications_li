<?php

namespace HotelBookingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HotelBooking
 *
 * @ORM\Table(name="hotel_bookings")
 * @ORM\Entity(repositoryClass="HotelBookingBundle\Entity\HotelBookingRepository")
 */
class HotelBooking
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="civilite", type="boolean")
     */
    private $civilite;

    /**
     * @var string
	  * 
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
	  * 
     * @ORM\Column(name="prenom", type="string", length=255)
     */
    private $prenom;

    /**
     * @var string
	  * 
     * @ORM\Column(name="addresse_email", type="string", length=255)
     */
    private $adresseMail;

    /**
     * @var string
	  * 
     * @ORM\Column(name="telephone", type="string", length=255)
     */
    private $telephone;

    /**
     * @var string
	 * 
     * @ORM\Column(name="direction", type="string", nullable=true)
     */
    private $direction;

    /**
     * @var string
	 * 
     * @ORM\Column(name="service", type="string", nullable=true)
     */
    private $service;

    /**
     * @var string
	  * 
     * @ORM\Column(name="nom_responsible", type="string", length=255)
     */
    private $nomDuResponsible;

    /**
     * @var integer
	 * 
     * @ORM\Column(name="societe", type="integer", nullable=true)
     */
    private $societe;

    /**
     * @var string
	  * 
     * @ORM\Column(name="autreSociete", type="string", length=255, nullable=true)
     */
    private $autreSociete;

    /**
     * @var string
	  * 
     * @ORM\Column(name="addresse", type="string", length=255, nullable=true)
     */
    private $adresse;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_arrivee", type="date")
     */
    private $dateArrivee;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_depart", type="date")
     */
    private $dateDepart;

    /**
     * @var integer
     *
     * @ORM\Column(name="nombreDeNuits", type="integer")
     */
    private $nombreDeNuits;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="heureArrivee", type="time")
     */
    private $heureArrivee;

    /**
     * @var boolean
     *
     * @ORM\Column(name="transport_gare_hotel", type="boolean", nullable=true)
     */
    private $transportGareHotel;

    /**
     * @var string
     *
     * @ORM\Column(name="commentaire", type="text", nullable=true)
     */
    private $commentaire;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set civilite
     *
     * @param boolean $civilite
     *
     * @return HotelBooking
     */
    public function setCivilite($civilite)
    {
        $this->civilite = $civilite;

        return $this;
    }

    /**
     * Get civilite
     *
     * @return boolean
     */
    public function getCivilite()
    {
        return $this->civilite;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return HotelBooking
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return HotelBooking
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set autreSociete
     *
     * @param string $autreSociete
     *
     * @return HotelBooking
     */
    public function setAutreSociete($autreSociete)
    {
        $this->autreSociete = $autreSociete;

        return $this;
    }

    /**
     * Get autreSociete
     *
     * @return string
     */
    public function getAutreSociete()
    {
        return $this->autreSociete;
    }

    /**
     * Set adresseMail
     *
     * @param string $adresseMail
     *
     * @return HotelBooking
     */
    public function setAdresseMail($adresseMail)
    {
        $this->adresseMail = $adresseMail;

        return $this;
    }

    /**
     * Get adresseMail
     *
     * @return string
     */
    public function getAdresseMail()
    {
        return $this->adresseMail;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return HotelBooking
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set direction
     *
     * @param integer $direction
     *
     * @return HotelBooking
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;

        return $this;
    }

    /**
     * Get direction
     *
     * @return integer
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * Set service
     *
     * @param integer $service
     *
     * @return HotelBooking
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return integer
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set nomDuResponsible
     *
     * @param string $nomDuResponsible
     *
     * @return HotelBooking
     */
    public function setNomDuResponsible($nomDuResponsible)
    {
        $this->nomDuResponsible = $nomDuResponsible;

        return $this;
    }

    /**
     * Get nomDuResponsible
     *
     * @return string
     */
    public function getNomDuResponsible()
    {
        return $this->nomDuResponsible;
    }

    /**
     * Set societe
     *
     * @param integer $societe
     *
     * @return HotelBooking
     */
    public function setSociete($societe)
    {
        $this->societe = $societe;

        return $this;
    }

    /**
     * Get societe
     *
     * @return integer
     */
    public function getSociete()
    {
        return $this->societe;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return HotelBooking
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set dateArrivee
     *
     * @param \DateTime $dateArrivee
     *
     * @return HotelBooking
     */
    public function setDateArrivee($dateArrivee)
    {
        $this->dateArrivee = $dateArrivee;

        return $this;
    }

    /**
     * Get dateArrivee
     *
     * @return \DateTime
     */
    public function getDateArrivee()
    {
        return $this->dateArrivee;
    }

    /**
     * Set dateDepart
     *
     * @param \DateTime $dateDepart
     *
     * @return HotelBooking
     */
    public function setDateDepart($dateDepart)
    {
        $this->dateDepart = $dateDepart;

        return $this;
    }

    /**
     * Get dateDepart
     *
     * @return \DateTime
     */
    public function getDateDepart()
    {
        return $this->dateDepart;
    }

    /**
     * Set nombreDeNuits
     *
     * @param integer $nombreDeNuits
     *
     * @return HotelBooking
     */
    public function setNombreDeNuits($nombreDeNuits)
    {
        $this->nombreDeNuits = $nombreDeNuits;

        return $this;
    }

    /**
     * Get nombreDeNuits
     *
     * @return integer
     */
    public function getNombreDeNuits()
    {
        return $this->nombreDeNuits;
    }

    /**
     * Set heureArrivee
     *
     * @param \DateTime $heureArrivee
     *
     * @return HotelBooking
     */
    public function setHeureArrivee($heureArrivee)
    {
        $this->heureArrivee = $heureArrivee;

        return $this;
    }

    /**
     * Get heureArrivee
     *
     * @return \DateTime
     */
    public function getHeureArrivee()
    {
        return $this->heureArrivee;
    }

    /**
     * Set transportGareHotel
     *
     * @param boolean $transportGareHotel
     *
     * @return HotelBooking
     */
    public function setTransportGareHotel($transportGareHotel)
    {
        $this->transportGareHotel = $transportGareHotel;

        return $this;
    }

    /**
     * Get transportGareHotel
     *
     * @return boolean
     */
    public function getTransportGareHotel()
    {
        return $this->transportGareHotel;
    }

    /**
     * Set commentaire
     *
     * @param string $commentaire
     *
     * @return HotelBooking
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * Get commentaire
     *
     * @return string
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }
}

