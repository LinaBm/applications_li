<?php

namespace HotelBookingBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use HotelBookingBundle\Form\HotelBookingType;
use HotelBookingBundle\Entity\HotelBooking;
use HotelBookingBundle\Entity\HotelBookingNuit;

class DefaultController extends Controller
{
	/**
	 * @Route("/update/service", name="hotel_default_update_service")
	 */
	public function getUpdateService()
	{
		$request = $this->container->get('request');
		$direction = $request->request->get('direction');			
		$reponse = array();
		if($direction){
			$services=$this->getDoctrine()
				->getRepository('AdministrationDirectionBundle:Service')
				->findBy(array('direction' => (int)$direction));
			foreach($services as $service){
				$reponse[$service->getId()] = $service->getLibelle();
			}
		}				
		return new JsonResponse($reponse);
	}
	
	/**
	 *
	 * @Route("/booking", name="hotel_default_booking")
	 * @Method({"GET", "POST"})
	 */
	public function bookingAction(Request $request) {
		$hotelBooking = new HotelBooking();
		$form = $this->createForm(new HotelBookingType, $hotelBooking, array('csrf_protection' => false));
		$form->handleRequest($request);
		$arrivee = $form['dateArrivee']->getData();			
		$depart = $form['dateDepart']->getData();
		$tempNights = array();
		
		$reservationNights = false;
		if($arrivee && $depart){
			$nombreDeNuits = (int) $arrivee->diff($depart)->format('%a');			
			for ($i = 0; $i < $nombreDeNuits; $i++) {
				$tempNights[$i]['dinner'] = $request->get('dinner_' . $i);
				if($request->get('dinner_' . $i) === null){
					$reservationNights = true;
				}
				if((int) $request->get('dinner_' . $i)){
					$tempNights[$i]['commentaire'] = $request->get('commentaire_' . $i);
				}else{
					$tempNights[$i]['commentaire'] = '';
				}
			}
		}
		if($reservationNights){		
			$messageNights = 'Toutes les nuits sont obligatoires!';
			$this->get('session')->getFlashBag()->add('reservationNights', $messageNights);
		}
			
		if ($form->isValid() && !$reservationNights) {
			$em = $this->getDoctrine()->getManager();
//			$nombreDeNuits = (int) $arrivee->diff($depart)->format('%a');
			$hotelBooking->setNombreDeNuits($nombreDeNuits);
			$hotelBooking->setDirection($form['direction']->getData() ? $form['direction']->getData() : null);
			$hotelBooking->setService($form['service']->getData() ? $form['service']->getData() : null);
			$em->persist($hotelBooking);
			$em->flush();		
			$direction = $em->getRepository('AdministrationDirectionBundle:Direction')->find((int)$hotelBooking->getDirection());		
			$service = $em->getRepository('AdministrationDirectionBundle:Service')->find((int)$hotelBooking->getService());		
			$hotelBookingData = array(
				'Civilité' => $hotelBooking->getCivilite() ? 'Monsieur' : 'Madame',
				'Nom' => $hotelBooking->getNom(),
				'Prénom' => $hotelBooking->getPrenom(),
				'Adresse mail' => $hotelBooking->getAdresseMail(),
				'Téléphone' => $hotelBooking->getTelephone(),				
				'Direction' => $hotelBooking->getDirection(),
				'Service' => $hotelBooking->getService(),
				'Nom du responsable' => $hotelBooking->getNomDuResponsible(),
			//	'Société' => $societe ? $societe->getUniqueName() : ($form['autreSociete']->getData() ? $form['autreSociete']->getData() : ''),	
				'Société' => $form['autreSociete']->getData() ? $form['autreSociete']->getData() : '',				
				'Date d\' arrivée' => $hotelBooking->getDateArrivee()->format('d/m/Y'),
				'Date de depart' => $hotelBooking->getDateDepart()->format('d/m/Y'),
				'Nombre de nuits' => $hotelBooking->getNombreDeNuits(),
				'Heure d\' arrivée' => $hotelBooking->getHeureArrivee()->format('H:i'),
				'Transport \'gare / hôtel\'' => $hotelBooking->getTransportGareHotel() ? 'Oui' : 'Non',
				'Commentaire' => $hotelBooking->getCommentaire()
			);
			for ($i = 0; $i < $nombreDeNuits; $i++) {
				$hotelBookingNuit = new HotelBookingNuit();
				$hotelBookingNuit->setIdHotelBooking($hotelBooking->getId());
				$hotelBookingNuit->setArrivee($arrivee);
				$hotelBookingNuit->setDinner((int) $request->get('dinner_' . $i));
				if((int) $request->get('dinner_' . $i)){
					$hotelBookingNuit->setCommentaire($request->get('commentaire_' . $i));
				}
				$em->persist($hotelBookingNuit);
				$em->flush();
				$arrivee->modify('+1 day');
				$hotelBookingData['Nuit no. '.($i+1).' Dîner - '] = $hotelBookingNuit->getDinner() ? 'Oui - '.$hotelBookingNuit->getCommentaire() : 'Non';
			}			
			$mailMessage = \Swift_Message::newInstance()
			->setContentType('text/html')
			->setSubject('Reservation pour la date ' . $hotelBooking->getDateArrivee()->format('d/m/Y'))
			->setFrom('reservation.hotel@noz.fr', 'Réservation Hotel')
			->setTo(explode(" ", $this->container->getParameter('mailHotel'))) // hotel and Valerie
			->setCC($hotelBooking->getAdresseMail())
			->setBCC('poleweb@noz.fr')
			->setBody($this->renderView('HotelBookingBundle:email:hotelBooking.html.twig', array(
				'hotelBookingData' => $hotelBookingData
			)));
			$reponse=$this->get('mailer')->send($mailMessage);			
			$message = 'L’opération s’est bien déroulée!';
			$this->get('session')->getFlashBag()->add('hotelBookingSuccess', $message);
			return $this->redirect($this->generateUrl('hotel_default_booking'));
		}
		return $this->render('HotelBookingBundle:default:booking.html.twig', array(
			'form' => $form->createView(),
			'title' => 'Ajouter reservation',
			'tempNights' => $tempNights
		));
	}
}
